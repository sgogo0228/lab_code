%% nakagami image的預設變數
% file_set = char('D:\子悠_Journal\子悠資料_contusion\子悠實驗資料\實驗data\contusion10(G6_2_head)\20180609\left\left_day18_C5.dat',...
%     'D:\子悠_Journal\子悠資料_contusion\子悠實驗資料\實驗data\contusion10(G6_2_head)\20180611\left\left_day21_C5.dat');
noiseFile = 'F:\子悠_Journal\子悠資料_contusion\子悠實驗資料\實驗data\20171102_water\30MHz_water_noise_B1.dat';
noiseRange = [4801 1 200 100]; % [pos_y pos_x H W]
transducer_freq = 30;
dynamic_range = 42;
resolutionAxial = 0.0459; %mm
resolutionLateral = 0.0743;
WMC_window_fold=[9; 11; 13; 15];
ROI_window_fold=[30 60];
img_id_for_nakagami = (8:12);  %計算nakagami image的影像序列編號(因為時間考量，可能file_set裡的每個.dat檔裡面只有特定幾張要計算nakagami img)
estimateNoiseLevel;
filter_range = [25 38.5];
stainlessBlockData = 'F:\子悠_Journal\子悠資料_contusion\子悠實驗資料\實驗data\20190915_skin_TranX\pulseecho\30MHz#2_filter25to35_amp0p5_freq20_attn6_B0.dat';    %skin跟contusion是用同一顆
glcm_dist = [4 26; 6 39];
nfft=4096;

file_set = readcell('K:\黃大銘實驗資料\contusion\Bmode_path.txt', 'Delimiter', ',');


%% 算Nakagami image
AxialROIFold = WMC_window_fold;
LateralROIFold = WMC_window_fold;

for f_iter = 1:length(file_set)
    full_file_name = [file_set{f_iter, :} '.dat'];
    param = headFile_NI(full_file_name);
    us_rf = get_us_data(full_file_name, 'is_save', 0, 'dynamic_range', dynamic_range);
    for i = 1:size(us_rf, 3)
        temp_rf = us_rf(:, :, i);
        Nakagami_image_v8;
    end
end

%% 框選ROI
for f_iter = 1:size(file_set, 1)
    file = [file_set{f_iter} '.dat'];
    roi_selection_v2;
    roi_position(f_iter) = roi_info(1, end);
    % save(['D:\子悠_Journal\子悠資料_contusion\子悠實驗資料\實驗data\roi_position_' num2str(r_iter) '.mat'], 'roi_position', '-v7.3');
end

%% 讀取所有data的roi位置
clear roi_position

path_set = char('D:\子悠_Journal\子悠資料_contusion\子悠實驗資料\實驗data\contusion05(G5_back)',...
    'D:\子悠_Journal\子悠資料_contusion\子悠實驗資料\實驗data\contusion06(G5_head)',...
    'D:\子悠_Journal\子悠資料_contusion\子悠實驗資料\實驗data\contusion07(G6_1_back)',...
    'D:\子悠_Journal\子悠資料_contusion\子悠實驗資料\實驗data\contusion08(G6_1_head)',...
    'D:\子悠_Journal\子悠資料_contusion\子悠實驗資料\實驗data\contusion09(G6_2_back)',...
    'D:\子悠_Journal\子悠資料_contusion\子悠實驗資料\實驗data\contusion10(G6_2_head)');
roi_position = [];
for i=1:size(path_set)
    temp = load([strtrim(path_set(i, :)) '\roi_position.mat']);
    roi_position = cat(3, roi_position, temp.roi_position);
end
q=q
% % roi_position(frame, day, rat)

%%  算參數，需搭配"框選ROI"或"讀取所有data的roi位置"
% clear us_qus
h = waitbar(1/size(file_set, 1), 'f = 1');
for f_iter = 1:92
        waitbar(f_iter/size(file_set, 1), h, ['f = ' num2str(f_iter)]);
        tic
        file = [file_set{f_iter} '.dat'];
        temp_pos = roi_position(:, f_iter);
        parameter_calculation_v3;
        toc
        us_qus(1:length(QUS), f_iter) = QUS;
end
save('K:\黃大銘實驗資料\contusion\30cm.mat', 'us_qus', '-v7.3');

clear us_qus
file_set = readcell('K:\黃大銘實驗資料\contusion\45cm\Bmode_path.txt', 'Delimiter', ',');
load('K:\黃大銘實驗資料\contusion\45cm\QUS分析\roi.mat');
h = waitbar(1/size(file_set, 1), 'f = 1');
for f_iter = [1:114 116:length(file_set)]
        waitbar(f_iter/size(file_set, 1), h, ['f = ' num2str(f_iter)]);
        tic
        file = [file_set{f_iter} '.dat'];
        temp_pos = roi_position(:, f_iter);
        parameter_calculation_v3;
        toc
        us_qus(1:length(QUS), f_iter) = QUS;
end

% NoiseROI = 2;
% NakagamiimageV3
% nakagamiimg2 = nakagamiImg
% snr2 = snr_img;
q=q

%%  因為glcm太大，計算參數時每隻老鼠都會存一個.mat，此段是用來讀取除了glcm的.mat資料,新增算glcm的ux, uy, sx, sy
clear QUS qus;
r_num = 12;
d_num = 12;
qus.ib = zeros(20, d_num, r_num);
qus.nakagami = zeros(20, d_num, r_num);
qus.snr = zeros(20, d_num, r_num);
qus.hist_mean = zeros(20, d_num, r_num);
qus.hist_var = zeros(20, d_num, r_num);
qus.hist_skew = zeros(20, d_num, r_num);
qus.hist_kurt = zeros(20, d_num, r_num);
qus.u_x = zeros(4, length(glcm_dist), 20, d_num, r_num);
qus.u_y = zeros(4, length(glcm_dist), 20, d_num, r_num);
qus.s_x = zeros(4, length(glcm_dist), 20, d_num, r_num);
qus.s_y = zeros(4, length(glcm_dist), 20, d_num, r_num);
qus.energ = zeros(4, length(glcm_dist), 20, d_num, r_num);
qus.entro = zeros(4, length(glcm_dist), 20, d_num, r_num);
qus.homo = zeros(4, length(glcm_dist), 20, d_num, r_num);
qus.cont = zeros(4, length(glcm_dist), 20, d_num, r_num);
qus.corr = zeros(4, length(glcm_dist), 20, d_num, r_num);
qus.sumavg = zeros(4, length(glcm_dist), 20, d_num, r_num);
qus.shade = zeros(4, length(glcm_dist), 20, d_num, r_num);

tic
for i=1:r_num
    temp = load(['D:\子悠_Journal\子悠資料_contusion\子悠實驗資料\實驗data\qus_' num2str(i) '.mat']);
    for j=1:d_num
        for k=1:20
            qus.ib(k, j, i) = temp.us_qus(k, j).IB;
            qus.nakagami(k, j, i) = temp.us_qus(k, j).nakagami;
            qus.snr(k, j, i) = temp.us_qus(k, j).SNR;
            qus.hist_mean(k, j, i) = temp.us_qus(k, j).hist_mean;
            qus.hist_var(k, j, i) = temp.us_qus(k, j).hist_var;
            qus.hist_skew(k, j, i) = temp.us_qus(k, j).hist_skew;
            qus.hist_kurt(k, j, i) = temp.us_qus(k, j).hist_kurt;
%             其實在"算參數"中，就可以直接抓到glcm feature，但因為想排除第一排跟第一列所以重算
            for m=1:length(glcm_dist)
                temp_feature = GLCM_Features(temp.us_qus(k, j).glcm(m).array(2:end, 2:end, :), 0);
                for n=1:4
                    qus.u_x(n, m, k, j, i) = temp_feature.u_x(n);
                    qus.diss(n, m, k, j, i) = temp_feature.dissi(n);
                    qus.u_y(n, m, k, j, i) = temp_feature.u_y(n);
                    qus.s_x(n, m, k, j, i) = temp_feature.s_x(n);
                    qus.s_y(n, m, k, j, i) = temp_feature.s_y(n);
                    qus.energ(n, m, k, j, i) = temp_feature.energ(n);
                    qus.entro(n, m, k, j, i) = temp_feature.entro(n);
                    qus.homo(n, m, k, j, i) = temp_feature.homom(n);
                    qus.cont(n, m, k, j, i) = temp_feature.contr(n);
                    qus.corr(n, m, k, j, i) = temp_feature.corrm(n);
                    qus.sumavg(n, m, k, j, i) = temp_feature.savgh(n);
                    qus.shade(n, m, k, j, i) = temp_feature.cshad(n);
                end
            end
        end
    end
end
toc
q=q

%% 輸出glcm
3
temp = load(['D:\子悠_Journal\子悠資料_contusion\子悠實驗資料\實驗data\qus_8.mat']);
distance_index = [1 5 7 8 9 1 5 7 8 9 15 16 17 18 19 15 16 17 18 19];
degree_index = [1 3 1 3];

figure('Position', [50 50 1000 1500])
[h, pos] =tight_subplot(4, 5, [0.07 0.03], [0.07 0.03], [0.05 0.03]);
    for i=1:numel(h)
        axes(h(i))
        temp_img = temp.us_qus(5, 1).glcm(distance_index(i)).array(:, :, degree_index(ceil(i/5)));
        image(temp_img/max(temp_img, [], 'all')*256)
        colormap(gray(256))
        axis image
        xlabel('Gray level', 'fontSize', 16, 'fontName', 'Times', 'fontWeight', 'bold')
        ylabel('Gray level', 'fontSize', 16, 'fontName', 'Times', 'fontWeight', 'bold')
        set(gca, 'fontSize', 12, 'fontName', 'Times', 'fontWeight', 'bold')
    end
    
 for r_iter = 1:2:r_num
     roi_position = roi_position2(:, :, r_iter:r_iter+1);
     save(['D:\子悠_Journal\子悠資料_contusion\子悠實驗資料\實驗data\roi_position' num2str(r_iter) '.mat'], 'roi_position', '-v7.3');
 end
 
%% multi-class SVM
k = k+1;
k
left_index = [1 3 5 7 9 11];
right_index = [2 4 6 8 10 12];
feature_num = 6;
angle = [1 3];
remodeling_number = 3;
qus_data = cat(4, cat(2, qus.ib(6:15, 1, left_index), qus.ib(6:15, :, right_index)),...
    cat(2, qus.nakagami(6:15, 1, left_index), qus.nakagami(6:15, :, right_index)),...
    cat(2, reshape(permute(qus.sumavg(angle, 1, 6:15, 1, left_index), [3 4 5 1 2]), 10, 1, 6, 2), reshape(permute(qus.sumavg(angle, 1, 6:15, :, right_index), [3 4 5 1 2]), 10, [], 6, 2)),...
    cat(2, reshape(permute(qus.diss(angle, 1, 6:15, 1, left_index), [3 4 5 1 2]), 10, 1, 6, 2), reshape(permute(qus.diss(angle, 1, 6:15, :, right_index), [3 4 5 1 2]), 10, [], 6, 2)));

qus_data = qus_data./mean(qus_data(: , 1:2, :, :), [1 2]);

parfor ks = 1:50
    for bc = 1:50
         KernelScale = 0.01*ks+4.45;
         BoxConstraint = 0.01*bc+0.25;
acc = zeros(4,2,6);
labels = zeros(80+remodeling_number*10, 2);
% KernelScale = 2.8;
% BoxConstraint = 0.5;
polytp = templateSVM('KernelFunction', 'polynomial', 'PolynomialOrder', 3, 'KernelScale', KernelScale, 'Standardize', true, 'Boxconstraint', BoxConstraint);
gaustp = templateSVM('KernelFunction', 'gaussian', 'KernelScale', KernelScale, 'Standardize', true, 'Boxconstraint', BoxConstraint);
% classes = [zeros(100, 1); zeros(150, 1)+1; zeros(300, 1)+2];
% test_classes = [zeros(20, 1); zeros(30, 1)+1; zeros(60, 1)+2];
classes = [zeros(100, 1); zeros(150, 1)+1; zeros(150, 1)+2; zeros(remodeling_number*50, 1)+3];
test_classes = [zeros(20, 1); zeros(30, 1)+1; zeros(30, 1)+2; zeros(remodeling_number*10, 1)+3];
% clear labels acc

    for i=1:6
        trained_data = qus_data;
        trained_data(:, :, i, :) = [];
        trained_data(:, 9:10, :, :) = [];
        trained_data = reshape(permute(trained_data, [1 3 2 4]), [], feature_num);
    %     'OptimizeHyperparameters', 'auto'
        polySVMmd = fitcecoc(trained_data, classes, 'Learners', polytp, 'Coding', 'onevsone', 'ClassNames',[0 1 2 3], 'Weight', [ones(100, 1)*1; ones(150, 1)*1; ones(150, 1)*1; ones(remodeling_number*50, 1)*1]);
        gausSVMmd = fitcecoc(trained_data, classes, 'Learners', gaustp, 'Coding', 'onevsone', 'ClassNames',[0 1 2 3], 'Weight', [ones(100, 1)*1; ones(150, 1)*1; ones(150, 1)*1; ones(remodeling_number*50, 1)*1]);
        test_data = reshape(permute(qus_data(:, [1 2 3 4 5 6 7 8 11 12 13], i, :), [1 3 2 4]), [], feature_num);
%         test_data = reshape(permute(qus_data(:, :, i, :), [1 3 2 4]), [], feature_num);
        labels(:, 1) = predict(polySVMmd, test_data);
        labels(:, 2) = predict(gausSVMmd, test_data);
%         cm(:, :, i) = confusionmat(labels(:, 2),test_classes);
        for j=1:4
            acc(j, 1, i) = sum(labels(test_classes==(j-1), 1)==(j-1));
            acc(j, 2, i) = sum(labels(test_classes==(j-1), 2)==(j-1));
        end
        acc(5, 1, i) = sum((test_classes==labels(:, 1)))/length(test_classes);
        acc(5, 2, i) = sum((test_classes==labels(:, 2)))/length(test_classes);
    end

temp = mean(acc, [3 4])
temp3(:, ks, bc) = temp(5, :)';
    end
end

%% multi-class SVM
k = k+1;
k
left_index = [1 3 5 7 9 11];
right_index = [2 4 6 8 10 12];
feature_num = 6;
angle = [1 3];
remodeling_number = 5;
qus_data = cat(4, cat(2, qus.ib(6:15, 1, left_index), qus.ib(6:15, :, right_index)),...
    cat(2, qus.nakagami(6:15, 1, left_index), qus.nakagami(6:15, :, right_index)),...
    cat(2, reshape(permute(qus.sumavg(angle, 1, 6:15, 1, left_index), [3 4 5 1 2]), 10, 1, 6, 2), reshape(permute(qus.sumavg(angle, 1, 6:15, :, right_index), [3 4 5 1 2]), 10, [], 6, 2)),...
    cat(2, reshape(permute(qus.diss(angle, 1, 6:15, 1, left_index), [3 4 5 1 2]), 10, 1, 6, 2), reshape(permute(qus.diss(angle, 1, 6:15, :, right_index), [3 4 5 1 2]), 10, [], 6, 2)));

qus_data = qus_data./mean(qus_data(: , 1:2, :, :), [1 2]);

% parfor ks = 1:50
%     for bc = 1:25
%          KernelScale = 0.1*ks;
%          BoxConstraint = 0.1*bc;
acc = zeros(4,2,6);
labels = zeros(80+remodeling_number*10, 2);
KernelScale = 0.6;
BoxConstraint = 0.4;
polytp = templateSVM('KernelFunction', 'polynomial', 'PolynomialOrder', 3, 'KernelScale', KernelScale, 'Standardize', true, 'Boxconstraint', BoxConstraint);
gaustp = templateSVM('KernelFunction', 'gaussian', 'KernelScale', KernelScale, 'Standardize', true, 'Boxconstraint', BoxConstraint);
classes = [zeros(100, 1); zeros(150, 1)+1; zeros(400, 1)+2];
test_classes = [zeros(20, 1); zeros(30, 1)+1; zeros(80, 1)+2];
% clear labels acc

% rng;
% temp = randi([1 6])
% val_data_index(temp)= [];
    for i=1:6
        trained_data = qus_data;
        trained_data(:, :, i, :) = [];
%         trained_data(:, 9:10, :, :) = [];
        trained_data = reshape(permute(trained_data, [1 3 2 4]), [], feature_num);
        polySVMmd = fitcecoc(trained_data, classes, 'Learners', polytp, 'Coding', 'onevsone', 'ClassNames',[0 1 2], 'Weight', [ones(100, 1)*1; ones(150, 1)*1; ones(400, 1)*1]);
        gausSVMmd = fitcecoc(trained_data, classes, 'Learners', gaustp, 'Coding', 'onevsone', 'ClassNames',[0 1 2], 'Weight', [ones(100, 1)*1; ones(150, 1)*1; ones(400, 1)*1]);
%         test_data = reshape(permute(qus_data(:, [1 2 3 4 5 6 7 8 11 12 13], i, :), [1 3 2 4]), [], feature_num);
        test_data = reshape(permute(qus_data(:, :, i, :), [1 3 2 4]), [], feature_num);
        labels(:, 1) = predict(polySVMmd, test_data);
        labels(:, 2) = predict(gausSVMmd, test_data);
        cm(:, :, i) = confusionmat(labels(:, 2),test_classes);
        for j=1:3
            acc(j, 1, i) = sum(labels(test_classes==(j-1), 1)==(j-1));
            acc(j, 2, i) = sum(labels(test_classes==(j-1), 2)==(j-1));
        end
        acc(5, 1, i) = sum((test_classes==labels(:, 1)))/length(test_classes);
        acc(5, 2, i) = sum((test_classes==labels(:, 2)))/length(test_classes);
    end

temp = mean(acc, [3 4])
% temp1(:, ks, bc) = temp(5, :)';
%     end
% end



%%
left_index = [1 3 5 7 9 11];
right_index = [2 4 6 8 10 12];
rownum = 10;
[squeeze(qus.sumavg(1, 2, 6:15, :, 2)) zeros(rownum, 1)*nan...
    squeeze(qus.sumavg(1, 2, 6:15, :, 4)) zeros(rownum, 1)*nan...
    squeeze(qus.sumavg(1, 2, 6:15, :, 6)) zeros(rownum, 1)*nan...
    squeeze(qus.sumavg(1, 2, 6:15, :, 8)) zeros(rownum, 1)*nan...
    squeeze(qus.sumavg(1, 2, 6:15, :, 10)) zeros(rownum, 1)*nan...
    squeeze(qus.sumavg(1, 2, 6:15, :, 12)) zeros(rownum, 1)*nan];

[reshape(permute(qus.sumavg(1, 2, 6:15, :, right_index), [1 2 4 3 5]), rownum, []) zeros(rownum, 1)*nan...
    reshape(permute(qus.sumavg(1, 2, 6:15, :, right_index), [1 2 4 3 5]), rownum, [])];

rownum = 60;
[reshape(permute(qus.diss(1, 8, 6:15, :, 1:6), [1 2 3 5 4]), rownum, []) zeros(rownum, 1)*nan...
    reshape(permute(qus.diss(3, 8, 6:15, :, 1:6), [1 2 3 5 4]), rownum, [])];

temp = load(['D:\子悠_Journal\子悠資料_contusion\子悠實驗資料\實驗data\qus_' num2str(4) '.mat']);
figure('position', [100 100 500 500])
co = log10(1+(0:255)/255*9)*255;
temp_glcm = temp.us_qus(10, 3).glcm(8).array(2:end, 2:end, 3);
imagesc(temp_glcm)
line((1:63), 32*ones(63), 'color', 'y', 'linewidth', 3, 'linestyle', '--')
line(32*ones(63), (1:63), 'color', 'y', 'linewidth', 3, 'linestyle', '--')
figure('position', [100 100 500 500])
%     surf(2:64, 2:64, co(1+round(temp_glcm./max(temp_glcm, [], 'all')*255)), 'FaceColor', 'r' , 'edgecolor', 'interp');
% temp_glcm = co(1+round(temp_glcm./max(temp_glcm, [], 'all')*255));
% surf(1:64, 1:64, temp_glcm, 'edgecolor', 'interp');
% axis([0 63 0 63 0 300])
axis image
set(gca, 'FontSize', 24, 'FontName', 'Times', 'FontWeight', 'bold', 'XTick', (10:10:60), 'YTick', (10:10:60))
xlabel('Gray level', 'FontSize', 28, 'FontName', 'Times', 'FontWeight', 'bold')
ylabel('Gray level', 'FontSize', 28, 'FontName', 'Times', 'FontWeight', 'bold');
colormap(gray(255))

temp = load(['D:\子悠_Journal\子悠資料_contusion\子悠實驗資料\實驗data\qus_' num2str(4) '.mat']);
fdir = 'D:\子悠_Journal\子悠資料_contusion\子悠實驗資料\實驗data';
for i=1:12
    temp_glcm = temp.us_qus(10, i).glcm(8).array(2:end, 2:end, 3);
    total_num = sum(temp_glcm, 'all');
    
%     figure('position', [100 100 500 500])
%     imagesc(temp_glcm/total_num, [0 0.004])
%     line((1:63), 32*ones(63), 'color', 'y', 'linewidth', 3, 'linestyle', '--')
%     line(32*ones(63), (1:63), 'color', 'y', 'linewidth', 3, 'linestyle', '--')
%     axis image
%     set(gca, 'FontSize', 24, 'FontName', 'Times', 'FontWeight', 'bold', 'XTick', (10:10:60), 'YTick', (10:10:60))
%     xlabel('Gray level', 'FontSize', 28, 'FontName', 'Times', 'FontWeight', 'bold')
%     ylabel('Gray level', 'FontSize', 28, 'FontName', 'Times', 'FontWeight', 'bold');
%     cb = colorbar('fontsize', 10);
    figure('position', [100 100 800 600])
    surf(2:64, 2:64, temp_glcm/total_num, temp_glcm/total_num , 'edgecolor', 'interp');
    set(gca, 'FontSize', 16, 'FontName', 'Times', 'FontWeight', 'bold', 'XTick', (0:10:60), 'YTick', (0:10:60), 'XGrid', false, 'YGrid', false, 'ZGrid', false)
    set(gca, 'zlim', [0 0.004])
    view(-20, 25)
    colormap(gray(256))
    [X,Map] = rgb2ind(frame2im(getframe(gcf)), 256);
    imwrite(X, Map, [fdir '\glcm\2d_degree90_' num2str(i) '.jpg'], 'JPEG');
end
close all

for i=1:12
    glcm_set_0(:, :, i) = temp.us_qus(10, i).glcm(8).array(2:end, 2:end, 1);
    glcm_set_90(:, :, i) = temp.us_qus(10, i).glcm(8).array(2:end, 2:end, 3);
end



temp = load(['D:\子悠_Journal\子悠資料_contusion\子悠實驗資料\實驗data\qus_' num2str(4) '.mat']);
image([roi_position(10, 1, 4).roi_pos(1) roi_position(10, 1, 4).roi_pos(1)+roi_position(10, 1, 4).roi_pos(3)], [roi_position(10, 1, 4).roi_pos(2) roi_position(10, 1, 4).roi_pos(2)+roi_position(10, 1, 4).roi_pos(4)], temp.us_qus(10, 1).roi)
set(gca, 'fontsize', 20, 'fontname', 'Times', 'FontWeight', 'bold', 'XTick', [floor(roi_position(10, 1, 4).roi_pos(1)):0.5:ceil(roi_position(10, 1, 4).roi_pos(1)+roi_position(10, 1, 4).roi_pos(3))])
xlabel('Distance (mm)', 'FontSize', 20, 'FontName', 'Times', 'FontWeight', 'bold')
ylabel('Depth (mm)', 'FontSize', 20, 'FontName', 'Times', 'FontWeight', 'bold');
colormap(gray(255))
axis equal tight

pos(2) = round(roi_position(10, 1).roi_pos(2)/1540/1000*500e6*2-param.Delay);
pos(1) = round(roi_position(10, 1).roi_pos(1)/0.01);
pos(4) = round(roi_position(10, 1).roi_pos(2)/1540/1000*500e6*2+roi_position(10, 1).roi_pos(4)/1540/1000*500e6*2-param.Delay);
pos(3) = round(roi_position(10, 1).roi_pos(1)/0.01+roi_position(10, 1).roi_pos(3)/0.01);
figure
imagesc(mean(nakagami_img(pos(2):pos(4), pos(1):pos(3), :), 3))
caxis([0 1])
set(gca, 'xtick', [], 'ytick', [])

roi_position(10, 1).roi_pos(1)/1540/1000*500e6*2-param.Delay

trainedData = [reshape(permute(qus.ib(6:15, :, :), [1 3 2]), [], 1),...
    reshape(permute(qus.nakagami(6:15, :, :), [1 3 2]), [], 1),...
    reshape(permute(qus.sumavg(:, 8, 6:15, :, :), [3 5 4 1 2]), [], 4),...
    reshape(permute(qus.diss(:, 8, 6:15, :, :), [3 5 4 1 2]), [], 4),...
    reshape(permute(qus.energ(:, 8, 6:15, :, :), [3 5 4 1 2]), [], 4),...
    reshape(permute(qus.entro(:, 8, 6:15, :, :), [3 5 4 1 2]), [], 4),...
    reshape(permute(qus.homo(:, 8, 6:15, :, :), [3 5 4 1 2]), [], 4),...
    reshape(permute(qus.cont(:, 8, 6:15, :, :), [3 5 4 1 2]), [], 4),...
    reshape(permute(qus.corr(:, 8, 6:15, :, :), [3 5 4 1 2]), [], 4)];
classes = [zeros(60, 1); zeros(180, 1)+1; zeros(180, 1)+2; zeros(300, 1)+3];

%%  estimate shearwave by demod/NCC method, and can output rf, IQ, shearwave as .npy file
shear_wave_algorithm_20221207

%% show spatiotemporal image 這邊使用sliding window的方式可以輸出elastography
spatiotemporal_output

%% 將b-mode影像跟elastography做蝶圖

imagesc(sw_feature{1, 6})
caxis([0 10])
% cmap = [gray; jet];
% post = velocity;
% post(post<0)=0;
% post = post/10*256;
% post = post+256;
% post = my_interp_23D(post, [5, 3]);
% get_us_data('K:\temp\20230630_inclusionPhantom\30MHzNo1_5mm_B0.dat');
% hold on
% im = image(x, y, post);
% caxis([0 10])
% im.AlphaData = 0.3;
% colormap(cmap)

%% output spatiotemporal image of label and predicted from .npy files
data_dir = 'D:\DaMing\sw_data\phantom';
position_num = 30;
start = 1;
output = zeros(1000, 184, position_num);
label = zeros(1000, 184, position_num);
for i=start:start + position_num - 1
    output(:, :, i - start + 1) = readNPY([data_dir '\predicted\' num2str(i) '.npy']);
    temp = readNPY([data_dir '\label\' num2str(i) '.npy']);
    label(:, :, i - start + 1) = mean(reshape(temp(3001:4000, :), [], 184, 5), 3);
end

figure
temp = reshape(label, [50, 20, 184, 30]);
temp = squeeze(mean(temp, 2));
imagesc(squeeze(temp(25, :, :))')
figure
temp = reshape(output, [50, 20, 184, 30]);
temp = squeeze(mean(temp, 2));
imagesc(squeeze(temp(25, :, :))')

%%
clear predicted_sgn
clear label_sgn
position_num = 10;
modifiedUnitAline = 184;
analyzeInterval = 200;
file_dir = 'D:\DaMing\sw_data\all_data';
predicted = dir([file_dir '\predicted\*.npy']);
temp = arrayfun(@(x) x.name(1:end-4), predicted, 'UniformOutput', false);
[~, temp] = sort(str2num(char(temp)));
predicted = predicted(temp);
for i=1:length(predicted)
    temp = str2num(predicted(i).name(1:end-4));
    if mod(temp, position_num) == 0
        for j=0:position_num-1
            % predicted_sgn{1+floor(temp/10)}(:, :, j+1) = readNPY([file_dir '\predicted\' num2str(temp+j) '.npy'])';
            predicted_sgn{1+floor(temp/10)}(:, :, j+1) = readNPY([file_dir '\predicted\' num2str(temp+j) '.npy']);
            label_sgn{1+floor(temp/10)}(:, :, j+1) = readNPY([file_dir '\label\' num2str(temp+j) '.npy'])';
        end
        predicted_sgn{1+floor(temp/10)} = squeeze(mean(reshape(predicted_sgn{1+floor(temp/10)}, modifiedUnitAline, analyzeInterval, [], position_num), 2));
        label_sgn{1+floor(temp/10)} = squeeze(mean(reshape(label_sgn{1+floor(temp/10)}, modifiedUnitAline, 5, analyzeInterval, [], position_num), [2 3]));
        % label_sgn{floor(1+temp/10)} = squeeze(mean(reshape(label_sgn{1+floor(temp/10)}, modifiedUnitAline, 5, [], position_num), 2));
    end
end

temp = zeros(15, 10);
for k=1:length(label_sgn)
    for i=1:10
        for j=1:15
            temp(j, i) = 0.5*sum(corrcoef(label_sgn{k}(:, j, i), predicted_sgn{k}(:, j, i)), 'all')-1;
        end
    end
    cc{k} = temp;
end