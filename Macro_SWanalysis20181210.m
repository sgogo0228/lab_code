% clear
% clc
% close all
%%
sgn = temp;
timeAxis = (0:(modelParams.pushNum*modelParams.unitAline-1))/detectSamplingRate;
% 去除push造成的雜訊
for i=pushNum-1:-1:0
    shift = modelParams.unitAline*i;
    sgn(:, :, :, (1+modelParams.unitAline-modelParams.damping)+shift:modelParams.unitAline+shift) = [];
    sgn(:, :, :, 1+shift:(modelParams.frontNoise+modelParams.backNoise)+shift) = [];
    timeAxis(1+(modelParams.unitAline-modelParams.backNoise-modelParams.damping)+shift:modelParams.unitAline+shift) = [];
    timeAxis(1+shift:(modelParams.frontNoise)+shift) = [];
end
timeAxis = timeAxis(1:modifiedUnitAline*3);

% 去除低頻雜訊(動物呼吸、環境干擾等)
[paramB, paramA] = butter(3, bandFilter/(detectSamplingRate/2));    %第一個參數須視情況調整
tempSgn = permute(sgn, [4 3 2 1]);
tempSgn = filtfilt(paramB, paramA, tempSgn);
sgn = permute(tempSgn, [4 3 2 1]);
clear tempSgn phase_50hz

% 去除前後較不穩定的訊號(刪除第一個和最後一個shearwave)
sgn(:, :, :, 1:modifiedUnitAline) = [];
sgn(:, :, :, end-(unusedCycle-1)*modifiedUnitAline+1:end) = [];

% % 將剩餘的較穩定的訊號做平均作為kalman filter輸出
for f=1:fnum
    for i=1:distNum
        for d = 1:analysisDistRange(2)-analysisDistRange(1)+1
            tempSgn = mean(reshape(sgn(f, i, d, :), modifiedUnitAline, modelParams.usedCycle), 2)';
            for j=0:modelParams.usedCycle-1
                sgn(f, i, d, (1+j*modifiedUnitAline):((j+1)*modifiedUnitAline)) = tempSgn;
            end
        end
    end
end
sgn(:, :, :, modifiedUnitAline*modelParams.usedCycle+1:end) = [];

%   CC
for r=1:analysisDistRange(2)-analysisDistRange(1)+1
    maxCCindex = zeros(fnum, distNum);
    phaseAngle = zeros(fnum, 20, hormonicNum);
    maxCCindex(:, 1) = size(sgn, 4);
    for f=1:fnum
        xcr = zeros(distNum, 2*size(sgn, 4)-1);
        for d=2:distNum
            xcr(d, :) = xcorr(squeeze(sgn(f, 1, r, :)), squeeze(sgn(f, d, r, :)));
            [~, I] = findpeaks(squeeze(xcr(d, :)));
            [~, tempIdx] = min(abs(I-maxCCindex(f, d-1)));
            maxCCindex(f, d) = I(tempIdx);
        end
    end
    maxCCindex = maxCCindex-size(sgn, 4);
    phaseAngle(:, 1:distNum, 1) = maxCCindex*2*pi/(detectSamplingRate/baseFrequency); %2*pi/(detectSamplingRate/baseFrequency)用於計算一個detect
    phaseAngle = permute(phaseAngle, [2 3 1]);

    phase_50hz(r, :, :) = linearFitForStatistic20181210(analyzeFilePath, ['0_analyze_20170403_sample (' num2str(r) ')'], [1 fnum], phaseAngle, slopeFitDotNum);
end

tempPhase = [];
for i=1:length(slopeFitDotNum)
    tempPhase = [tempPhase; squeeze(phase_50hz(:, i, :))];
end
phase_50hz = tempPhase;
myXlswrite([analyzeFilePath '/variousRangedDepth.xlsx' ], baseFrequency*360*0.001./phase_50hz, 'velocity', 'B1');
