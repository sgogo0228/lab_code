%{
paramin
    'is_save' 是否要執行figure並儲存在硬碟中，default=1
    'dynamic_range' DR，default=42
    'output_range' 指定顯示的x軸與z軸範圍，default=[min_y, max_y]
%}
function [us_image, us_rf, env_rf, xAxis, yAxis] = get_one_us_data(fullPath, paramin)
%     close all
    if nargin < 2
        paramin = struct();
    end
    if isfield(paramin,'is_save'), is_save = paramin.is_save; else, is_save = 1; end
    if isfield(paramin,'dynamic_range'), dynamic_range = paramin.dynamic_range; else, dynamic_range = 42; end
    param = headFile_NI(fullPath);
    us_rf = param.rf;
    
    xAxis = (1:param.Aline)*param.XInterval/1000;
    yAxis = ((1:param.DataLength)+param.Delay)/2/(param.SamplingRate*1e6)*1540*1000;
    if isfield(paramin,'output_range') 
        output_range = paramin.output_range; 
    else
        output_range = [xAxis(1) xAxis(end) yAxis(1) yAxis(end)]; 
    end
    env_rf=abs(hilbert(us_rf));

    % 用demodulator實現demodulation
%     [I, Q] = demod(rf, 30*1e6, 1e9,'qam');
%     env_rf = abs(hilbert(I, Q));
    log_rf=20*log10(env_rf./max(env_rf, [], [1 2]));
    us_image=(log_rf+dynamic_range)/dynamic_range*255;

    if is_save
        figure('color', 'white'); 
        for i=1:size(us_image, 3)
            image(xAxis, yAxis, us_image(:, :, i))
            set(gca, 'FontSize', 16, 'FontName', 'Times', 'FontWeight', 'bold', 'XTick', round(xAxis(1)):1:xAxis(end), 'YTick', round(yAxis(1)):1:yAxis(end))
            xlabel('Distance (mm)', 'FontSize', 20, 'FontName', 'Times', 'FontWeight', 'bold')
            ylabel('Depth (mm)', 'FontSize', 20, 'FontName', 'Times', 'FontWeight', 'bold');
            colormap(gray(255))
            axis equal tight
            axis(output_range)
            [X,Map] = rgb2ind(frame2im(getframe(1)), 256);
            savePath = [dirPath '\us_image\' fileName '_' num2str(i) '.png'];
            if exist([dirPath '\us_image'], 'dir')==0
                mkdir([dirPath '\us_image']);
            end
            if exist(savePath, 'file')==2
                delete(savePath);
            end

            imwrite(X, Map, savePath, 'PNG');
        end
    end
end

