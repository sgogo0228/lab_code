function [output, origSignal] = shearWaveAnalyze2(filename, depth, time)
    close all
    nfft = 4096;
    % 讀取檔案
    params = headFile_NI(filename);
    data = params.rf;
    data = data*params.Vpp/255;

    % demodulation
    [I, Q] = demod(data,30*1e6,1e9,'qam');
    bigX = zeros(2000, time);
    bigY = zeros(2000, time);
    Q = -Q;
    for i=1:time-1
        bigX(:, i) = I(:, i).*I(:, i+1)+Q(:, i).*Q(:, i+1);
        bigY(:, i) = Q(:, i).*I(:, i+1)-I(:, i).*Q(:, i+1);
    end
%     VQ = I(:, 1:4000).*I(:, 2:4001)+Q(:, 1:4000).*Q(:, 2:4001);
%     VI = Q(:, 1:4000).*I(:, 2:4001)-I(:, 1:4000).*Q(:, 2:4001);
    s = atan(bigY./bigX);
    figure
    plot(s(1000, :))
    % 取相位角
%     s=-atan(Q./I);
    % tan相位角限定在-pi到pi之間，因此可能造成不連續(如：連續兩個點相位角為90和91會變成90和-89)
    % 若判定跟前一個點相差太多，就把它調回來
    for i=1:2000
        for j=5:time-1
            if abs(s(i, j)-s(i, j+1))>1
                if(s(i, j+1)<0)
                    s(i, j+1) = s(i, j+1)+pi;
                else
                    s(i, j+1) = s(i, j+1)-pi;
                end
            end
        end
    end
    kalmanInput = s(depth, :);
    % 求s平均並和s相減可消掉載波相位的影響
%     sMean = zeros(2000, 1);
%     for i=1:2000
%         meanValue = mean(s(i, :));
%         for j=5:4000
%             sMean(i, j) = meanValue;
%         end
%     end
%     y = s-sMean;
    % 理論上y已和fast time(縱向深度)無關，因此取fast time之平均獲得待測物振動之相位角
%     kalmanInput = y(1000, :);
    % % kalmanInput = 3*sin(2*pi*50*t+pi/8)+randn(1, 4000);

    % 各種參數初始化
    % 此部分參照Kalman Filter Motion Detection for Vibro-acoustography using Pulse Echo Ultrasound 
    t = [0:time-1]/20/1e3;
    signalNum = 5;
    X = zeros(2*signalNum, time);
    X(1, 1) = 1;
    X(3, 1) = 1;
    X(5, 1) = 1;
    X(7, 1) = 1;
    P = 20*eye(2*signalNum);
    F = eye(2*signalNum);
    H = zeros(time, 2*signalNum);
    for i=1:signalNum
        H(:, 2*i-1) = -2*sin(i*50*2*pi/2/20000)*cos(2*pi*50*i*t)';
        H(:, 2*i) = -2*sin(i*50*2*pi/2/20000)*-sin(2*pi*50*i*t)';
%         H(:, 2*i-1) = sin(2*pi*50*i*t)';
%         H(:, 2*i) = cos(2*pi*50*i*t)';
    end
    % % H = [sin(2*pi*50*t)' cos(2*pi*50*t)' sin(2*pi*100*t)' cos(2*pi*100*t)' sin(2*pi*150*t)' cos(2*pi*150*t)' sin(2*pi*200*t)' cos(2*pi*200*t)' sin(2*pi*250*t)' cos(2*pi*250*t)'];
    Q = 0.0001*eye(2*signalNum);
    R = 10;

    % kalmanfilter
    for i=2:time
        xMinus = F*X(:, i-1);
        pMinus = F*P*F'+Q;
        G = pMinus*H(i, :)'/(H(i, :)*pMinus*H(i, :)'+R);
        X(:, i) = xMinus+G*(kalmanInput(i)-H(i, :)*xMinus);
        P = (eye(2*signalNum)-G*H(i, :))*pMinus;
    end
    filteredSignal = zeros(1, time);
    for i=1:2:(2*signalNum-1)
        filteredSignal = filteredSignal+H(:, i)'.*X(i, :)+H(:, i+1)'.*X(i+1, :);
    end
    output = X;
    origSignal = kalmanInput;
    
    figure
    subplot(3, 1, 1)
    plot(t, kalmanInput);
    title('original signal without noise')
    ylabel('phase (rad)')

    subplot(3, 1, 2)
    plot(t, kalmanInput)
    title('original signal with noise')
    ylabel('phase (rad)')

    subplot(3, 1, 3)
    plot(t, filteredSignal)
    title('filtered signal')
    xlabel('time (s)')
    ylabel('phase (rad)')

    figure
    plot(t, atan(X(2, :)./X(1, :)));
    xlabel('time (s)')
    ylabel('cos value')
end