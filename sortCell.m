%% 針對用dir()函式找出的檔案進行檔名排序(這個函示找出來的檔案只會把檔名當成字串排序，不會按照裡面的特定數字排序)
fileMatched = fileMatched;  % 輸入的cell(含有檔名路徑等欄位)
clear strPart
for i=1:length(fileMatched)
    strPart(i, :) = cellfun(@str2num, regexp(fileMatched(i).name, '\d+', 'match')); %將檔名中所有數字找出並轉成double陣列
end
[~, index] = sortrows(strPart, [1:1:size(strPart, 2)]); %針對這個陣列每個column進行排序
fileMatched = fileMatched(index);   %將排序的索引輸入原cell以進行排序