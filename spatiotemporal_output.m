%% show spatiotemporal image 這邊使用sliding window的方式可以輸出elastography
interp_dist_fold = 5;                  %內差的倍數
interp_time_fold = 1;
interp_depth_fold = 1;
moving_window = false;
rsd_threshold = 10;
fnum = length(sgn);
max_point_num = 3;
file_range = 1:fnum;
position_num = 10;
if not(moving_window)
    analysis_dist = 1;
end

% x = linspace(0, (params.MoveTimes-1)*0.5, size(sgn, 3));
% y = linspace(params.Delay/2/(params.SamplingRate*1e6)*1540*1000, (params.Delay+params.DataLength)/2/(params.SamplingRate*1e6)*1540*1000, size(sgn, 2));
h = waitbar(1/analysis_dist(end), ['dp = 1 (1/' num2str(analysis_dist(end)) ')']);
for f = file_range
    temp_interp_sgn = my_interp_23D(sgn{f}, [interp_time_fold, interp_depth_fold, interp_dist_fold]);

    %   設定分析的深度數量與moving window的滑動範圍預設temp_interp_sgn的全範圍
    dist_window_width = size(temp_interp_sgn, 3);     %最後用Y軸上的sliding window算出elastography，這個window的寬度，最大值是size(sgn, 3)代表沒有sliding
    analysis_depth = 1:size(temp_interp_sgn, 2);
    analysis_dist = 1:size(temp_interp_sgn, 3);

    %   如果不做moving_window，只需做一次寬度為size(sgn, 3)的window下的計算參數就好
    if not(moving_window)
        dist_window_width=size(temp_interp_sgn, 3);
        analysis_dist = 1;
    end
    depth_num = analysis_depth(2)-analysis_depth(1)+1;
    dist_num = analysis_dist(end)-analysis_dist(1)+1;

    velocity = zeros(depth_num, dist_num);
    attenuation = zeros(depth_num, dist_num);

    % for dp = 35
    for dp = analysis_depth
        temp_st_image = squeeze(temp_interp_sgn(:, dp, :));

%         % -這邊先輸出spatiotemporal image
%         figure('color', 'white', 'position', [500 200 1000 500])
%         x = (0:size(temp_st_image, 2)-1) / size(temp_st_image, 2) * (params.MoveTimes-1)*0.5;
%         y = (0:size(temp_st_image, 1)-1) * 0.1 / interp_time_fold;
%         image(y, x, (64-(temp_st_image-min(min(temp_st_image)))/(max(max(temp_st_image))-min(min(temp_st_image)))*64)')
%         set(gca,'YDir','normal')
%         colormap jet(64)
%         colorbar('Ticks', [1 12.8 25.6 38.4 51.2 64], 'TickLabels', {'-0.3','-0.2','-0.1','0','0.1','0.2'})
%         set(gca, 'FontSize', 16, 'FontName', 'Times', 'FontWeight', 'bold', 'XTick', round(y(1)):2:y(end), 'YTick', round(x(1)):1:x(end))
%         xlabel('Time (ms), T', 'FontSize', 20, 'FontName', 'Times', 'FontWeight', 'bold', 'fontName', 'Times')
%         ylabel('Distance (mm), D', 'FontSize', 20, 'FontName', 'Times', 'FontWeight', 'bold', 'fontName', 'Times');
        
%         [X,Map] = rgb2ind(frame2im(getframe(gcf)), 256);
%         savePath = [dirPath '\spatiotemporal\' fileName '_' num2str(dp) '.jpg'];
%         if exist([dirPath '\spatiotemporal\'], 'dir')==0
%             mkdir([dirPath '\spatiotemporal\']);
%         end
%         imwrite(X, Map, savePath, 'JPEG');
        
        % -找出st image中的峰值追蹤該條shear wave的位移
        max_point = zeros(3, size(temp_st_image, 2) * max_point_num);
        for i = 1:size(temp_st_image, 2)
            [value, index] = maxk(-temp_st_image(:, i), max_point_num);
            max_point(:, (i-1)*max_point_num+1:(i-1)*max_point_num+max_point_num) = [ones(1, max_point_num)*i; index'; value'-min(-temp_st_image(:, i))];
        end

        % hold on
        % scatter((max_point(2, :)-1) * 0.1 / interp_time_fold, (max_point(1, :)-1) / interp_dist_pts * 4.5, 'k')

        cluster_num = 1;
        while (var(max_point(2, :)) / mean(max_point(2, :)) > rsd_threshold) && (size(max_point, 2)>cluster_num)
            cluster_num = cluster_num + 1;
            [temp_idx, temp_C] = kmeans(max_point(2:3, :)', cluster_num);
            [~, temp_min_cluster_id] = min(temp_C(:, 1));
            max_point = max_point(:, (temp_idx==temp_min_cluster_id));
        end

        % hold on
        % scatter((max_point(2, :)-1) * 0.1 / interp_time_fold, (max_point(1, :)-1) / interp_dist_pts * 4.5, 'r')
        
        max_point_avg = zeros(3, size(temp_interp_sgn, 3));
        for i=1:size(temp_interp_sgn, 3)
            if(sum(max_point(1, :)==i) > 0)
            % -這邊是要確定對應的測量點有峰值沒有被刪光，才進行取平均
                max_point_avg(1, i) = i-1;
                max_point_avg(2, i) = mean(max_point(2, (max_point(1, :)==i)));
                max_point_avg(3, i) = mean(max_point(3, (max_point(1, :)==i)));
            end
        end
        
        for dist = analysis_dist
            waitbar(dist/analysis_dist(end), h, ['f=' num2str(f) ',dp = ' num2str(dp) '(' num2str(dist) '/' num2str(analysis_dist(end)) ')']);
%         % -對dist&amplitude與time做fit
            if dist+dist_window_width-1 <= size(temp_interp_sgn, 3)
                valid_dist = [dist:dist+dist_window_width-1];
            else
                valid_dist = [dist:size(temp_interp_sgn, 3)];
            end

            fit_x = max_point_avg(1, valid_dist)' * 0.5 / interp_dist_fold;
            fit_y1 = max_point_avg(2, valid_dist)' * 0.1 / interp_time_fold;
            fit_y2 = max_point_avg(3, valid_dist)';
            fit_y1(fit_x == 0) = [];
            fit_y2(fit_x == 0) = [];
            if sum(fit_x ~= 0) >= size(temp_interp_sgn, 3)*0.5
                % fit_y2 = fit_y2/max(fit_y2);  %因為會有負值log會有問題
                fit_y2 = log(fit_y2);
                fit_x(fit_x == 0) = [];
                slope = [polyfit(fit_x, fit_y1, 1); polyfit(fit_x, fit_y2, 1)];
                velocity(dp, dist) = 1/slope(1, 1); %m/s
                attenuation(dp, dist) = -slope(2, 1);
            else
                velocity(dp, dist) = nan;
                attenuation(dp, dist) = nan;
            end
        end
%         close all
    end

    sw_feature{1, f} = velocity;
    sw_feature{2, f} = attenuation;
    % result(:, f) = [mean(velocity(analysis_depth)); mean(attenuation(analysis_depth))];

    % % 輸出exel檔
    % C = ['depth', num2cell(round(((((analysis_depth)-1) * analyzeInterval / interp_depth_fold) + depth(1)...
    %     + params.Delay - 1) / 2 / params.SamplingRate / 1e6 * 1000 * 1540, 2))];
    % C = [C; 'velocity', num2cell(velocity(analysis_depth))];
    % C = [C; 'attenuation', num2cell(attenuation(analysis_depth))];
    % writecell(C, [dirPath '\vel_and_atten_' fileName '.xlsx']);
    % clear C
end
f = findall(0,'type','figure');
close(f)
% delete(gcp('nocreate'));