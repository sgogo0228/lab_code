% figure
% close all
clear;
% clc

% % 如果是Cscan只需要輸入第一筆(_C1.dat)的檔名就好
% % 如果檔名顯示有衰減過，需要在讀檔時補償回去
stainlessBlockData = 'K:\黃大銘實驗資料\skinflap\20190624_XDCR_30MHz\30MHz#2_atten10dB_A1.dat';   
fileSet = char('J:\實驗資料\子悠實驗資料\實驗data\contusion06(G5_head)\20180409\left\left_day0_C1.dat',...
    'J:\實驗資料\子悠實驗資料\實驗data\contusion06(G5_head)\20180411\left\left_day1_C1.dat',...
    'J:\實驗資料\子悠實驗資料\實驗data\contusion06(G5_head)\20180412\left\left_day2_C1.dat',...
    'J:\實驗資料\子悠實驗資料\實驗data\contusion06(G5_head)\20180413\left\left_day3_C1.dat',...
    'J:\實驗資料\子悠實驗資料\實驗data\contusion06(G5_head)\20180415\left\left_day5_C1.dat',...
    'J:\實驗資料\子悠實驗資料\實驗data\contusion06(G5_head)\20180417\left\left_day7_C1.dat',...
    'J:\實驗資料\子悠實驗資料\實驗data\contusion06(G5_head)\20180419\left\left_day9_C1.dat',...
    'J:\實驗資料\子悠實驗資料\實驗data\contusion06(G5_head)\20180421\left\left_day11_C1.dat',...
    'J:\實驗資料\子悠實驗資料\實驗data\contusion06(G5_head)\20180423\left\left_day13_C1.dat',...
    'J:\實驗資料\子悠實驗資料\實驗data\contusion06(G5_head)\20180425\left\left_day15_C1.dat',...
    'J:\實驗資料\子悠實驗資料\實驗data\contusion06(G5_head)\20180427\left\left_day18_C1.dat',...
    'J:\實驗資料\子悠實驗資料\實驗data\contusion06(G5_head)\20180501\left\left_day21_C1.dat',...
    'J:\實驗資料\子悠實驗資料\實驗data\contusion06(G5_head)\20180409\right\right_day0_C1.dat',...
    'J:\實驗資料\子悠實驗資料\實驗data\contusion06(G5_head)\20180411\right\right_day1_C1.dat',...
    'J:\實驗資料\子悠實驗資料\實驗data\contusion06(G5_head)\20180412\right\right_day2_C1.dat',...
    'J:\實驗資料\子悠實驗資料\實驗data\contusion06(G5_head)\20180413\right\right_day3_C1.dat',...
    'J:\實驗資料\子悠實驗資料\實驗data\contusion06(G5_head)\20180415\right\right_day5_C1.dat',...
    'J:\實驗資料\子悠實驗資料\實驗data\contusion06(G5_head)\20180417\right\right_day7_C1.dat',...
    'J:\實驗資料\子悠實驗資料\實驗data\contusion06(G5_head)\20180419\right\right_day9_C1.dat',...
    'J:\實驗資料\子悠實驗資料\實驗data\contusion06(G5_head)\20180421\right\right_day11_C1.dat',...
    'J:\實驗資料\子悠實驗資料\實驗data\contusion06(G5_head)\20180423\right\right_day13_C1.dat',...
    'J:\實驗資料\子悠實驗資料\實驗data\contusion06(G5_head)\20180425\right\right_day15_C1.dat',...
    'J:\實驗資料\子悠實驗資料\實驗data\contusion06(G5_head)\20180427\right\right_day18_C1.dat',...
    'J:\實驗資料\子悠實驗資料\實驗data\contusion06(G5_head)\20180501\right\right_day21_C1.dat');
for f = 1:size(fileSet)
    if ~exist(fileSet(f, :), 'file')
        f
        disp(fileSet(f, :));
        return;
    end
end
dynamicRange = 42;
resolutionAxial = 0.0459; %mm
resolutionLateral = 0.0743;
AxialROIFold = 10;
LateralROIFold = 20;
nfft = 4096;

% 計算雜訊
estimateNoiseLevel;

% % 計算鋼塊頻譜
param = headFile_NI_forCscan(stainlessBlockData);
% % 根據是否有加上衰減器對rf做補償
% param = headFile_NI_forCscan_Wulab(stainlessBlockData);
stainlessBlockSgn = mean(param.rf*2, 2);
stainlessBlockFFT = abs(fft(stainlessBlockSgn, nfft));

for f=1:size(fileSet)
    fullFilename = strtrim(fileSet(f, :));
    BmodeDirection = regexp(fullFilename, '_(B|C)\d+.dat', 'match');
    if (BmodeDirection{1}(2) == 'B')
        param = headFile_NI(fullFilename);
        BmodeDirection = regexp(regexp(fullFilename, '_B\d+.dat', 'match'), '\d+', 'match'); %找出該影像是否需要左右相反(B後面是奇數)
        if mod(str2num(BmodeDirection{1}{1}), 2)==1
            rf = fliplr(param.rf);
        else
            rf = param.rf;
        end
    else
      param = headFile_NI_forCscan(fullFilename);
%         param = headFile_NI_forCscan_Wulab(fullFilename);
        rf = param.rf;
        batchSize = length(dir([fullFilename(1:end-5) '*']));
        for b = 2:batchSize
            param = headFile_NI_forCscan([fullFilename(1:end-5) num2str(b) '.dat']);
%             param = headFile_NI_forCscan_Wulab([fullFilename(1:end-5) num2str(b) '.dat']);
            rf(:, :, size(rf, 3)+1:size(rf, 3)+size(param.rf, 3)) = param.rf;
        end
        for i=2:2:size(rf, 3)
            rf(:, :, i) = fliplr(rf(:, :, i));
        end
    end
    
    for i = 1:size(rf, 3)
        env_rf=abs(hilbert(rf(:, :, i)));
        log_rf=20*log10(env_rf/max(max(env_rf)));
        img=255*(log_rf+dynamicRange)/dynamicRange;
        xAxis = (1:param.Aline)*param.XInterval/1000;
        yAxis = ((1:param.DataLength)+param.Delay)/2/(param.SamplingRate*1e6)*1540*1000;
        figure('color', 'white', 'position', [500 200 1000 500])
        image(xAxis, yAxis, img)
        colormap(gray(255))
        axis equal tight
    %     axis([0 8 7.5 10.5])
        title(i)
        xlabel('Distance (mm)', 'fontSize', 28, 'fontWeight', 'bold', 'fontName', 'Times')
        ylabel('Depth (mm)', 'fontSize', 28, 'fontWeight', 'bold', 'fontName', 'Times')
        set(gca, 'FontSize', 16, 'FontName', 'Times', 'FontWeight', 'bold', 'XTick', round(xAxis(1)):2:xAxis(end), 'YTick', round(yAxis(1)):1:yAxis(end))

        % for f=1:frameNum %c-scan
        % for f=1:1 %c-scan
        %     cal_times = cal_times+1;
        % 	if mod(f,2)==0       
        %         rf1 = fliplr(reshape(rf(datalength*aline*(f-1)+1:datalength*aline*f), datalength, aline)); % fliplr：左右翻轉
        %     else
        %         rf1 = reshape(rf(datalength*aline*(f-1)+1:datalength*aline*f), datalength, aline);
        %     end
        % 	%% Bandpass Filter
        % 	fs = sampling_rate*10^(3); % sample rate (kHz)
        % 	order = 2;   % order of filter
        % 	lowcut = 25*10^(3);   % low cut frequency (kHz)
        %  	highcut = 40*10^(3);   % high cut frequency (kHz)
        %  	[b,a] = butter(order, [lowcut, highcut]/(fs/2), 'bandpass'); %-6db bandwidth
        %  	rf_filter = filter(b, a, rf1);       
            %% Select ROI
        origY = (1+param.Delay)/(param.SamplingRate*10^6)*1540*1000/2;
        if (exist('pos', 'var'))
            h = imrect(gca, pos);
        else
            h = imrect(gca,[0 origY resolutionLateral*LateralROIFold resolutionAxial*AxialROIFold]);
        end
    %     [g,api]=imRectRot('pos', [1 9 1 1 10], 'showLims', 0, 'cross', 2);
        wait(h);


        %     k = waitforbuttonpress;
        % 	api = iptgetapi(h); 
        % 	api.addNewPositionCallback(@(p) title(mat2str(p,3)));  %得到最新座標值
        % 	fcn = makeConstrainToRectFcn('imrect',get(gca,'XLim'),get(gca,'YLim'));  %不能將框挪至影像以外區域
        %  	api.setPositionConstraintFcn(fcn);  %顯示
        % 
        % 	k = waitforbuttonpress;
        pos = getPosition(h) %record position
        close(gcf);

        pos_x1=pos(1);
        pos_x2=pos(1)+pos(3);
        pos_y1=pos(2);
        pos_y2=pos(2)+pos(4);

        roi_x1 = 1+round(pos_x1/(param.XInterval*10^(-3)));
        roi_x2 = 1+round(pos_x2/(param.XInterval*10^(-3)));
        roi_y1 = 1+round(2*pos_y1*param.SamplingRate/(1540*1000*10^(-6)))-param.Delay;
        roi_y2 = 1+round(2*pos_y2*param.SamplingRate/(1540*1000*10^(-6)))-param.Delay;

        xlen = roi_x2-roi_x1+1;
        ylen = roi_y2-roi_y1+1;
    %     env_rf(log_rf < noiseLevel) = nan;
        roi = rf(roi_y1:roi_y2, roi_x1:roi_x2, i);
% %         原公式應是20*log，但這邊分子分母都已經各自平方
        NakagamiSNR(i, f) = 10*log10(sum(sum(roi.^2))/noiseSquareSum);
        env_roi = abs(hilbert(roi));
%         figure; hist = histogram(env_roi,'BinWidth' , 9.9);
        env_roi = env_roi/max(max(env_roi))*255;
        roi_reshape=reshape(env_roi, xlen*ylen,1);
        
%         figure; hist = histogram(roi_reshape,'BinWidth' , 9.9);
        
        nakagamiParam = mle(roi_reshape(:,1), 'distribution', 'nakagami');
        NakagamiMLE(i, f) = nakagamiParam(1);

        fft_roi = abs(fft(roi, nfft));
        fft_roi_avg = mean(fft_roi, 2);
        log_roi = 20*log10(fft_roi_avg/max(fft_roi_avg));

        [value, index] = max(log_roi(1:end/2)); % Find center freq.
        x1 = index; x2 = x1;
        while log_roi(x1) > -6 && x1>1
           x1 = x1-1;
        end
        while log_roi(x2) > -6 && x2<length(log_roi)
           x2 = x2+1;
        end
    %        x2 = [index-6:index+5]*(sampling_rate/ylen);   

        s1 = sum(fft_roi_avg(x1+1:x2));
        s2 = sum(stainlessBlockFFT(x1+1:x2));
        IB(i, f) = 20*log10((s1/s2)/(x2-x1));  %鋼塊 bandwidth：12  取得點數要相同  
    end
end
a = c