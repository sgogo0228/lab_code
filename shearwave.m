figure
nfft = 16384;
temp = squeeze(demod_signal(1, 9, 4500, :));
subplot(2, 1, 1)
plot(temp)
temp = abs(fft(temp, nfft));
subplot(2, 1, 2)
plot((0:nfft-1)/nfft*10000, temp)