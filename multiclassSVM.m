%% train SVM model
rng();
[train_ind,test_ind] = dividerand(1552,0.8,0.2);

train_data = data(train_ind, :);
test_data = data(test_ind, :);
train_label = label(train_ind);
test_label = label(test_ind);

polytp = templateSVM('KernelFunction', 'polynomial', 'PolynomialOrder', 3, 'KernelScale', 'auto', 'Standardize', false, 'BoxConstraint', 1, 'ClipAlphas', false);
% gaustp = templateSVM('KernelFunction', 'gaussian', 'KernelScale', 'auto', 'Standardize', true, 'BoxConstraint', 1);
% rbftp = templateSVM('KernelFunction', 'rbf', 'KernelScale', 'auto', 'Standardize', true, 'BoxConstraint', 1);
polySVMmd = fitcecoc(train_data, train_label, 'Learners', polytp, 'Coding', 'onevsall', 'ClassNames',[0 1 2 3]);
% gausSVMmd = fitcecoc(train_data, train_label, 'Learners', gaustp, 'Coding', 'onevsall', 'ClassNames',[0 1 2 3]);
% rbfSVMmd = fitcecoc(train_data, train_label, 'Learners', rbftp, 'Coding', 'onevsall', 'ClassNames',[0 1 2 3]);
% acc = zeros(4, 10);
% for i=1:5
%     % acc(1, i) = 1-kfoldLoss(crossval(linearSVMmd));
%     acc(2, i) = 1-kfoldLoss(crossval(polySVMmd));
%     acc(3, i) = 1-kfoldLoss(crossval(gausSVMmd));
%     acc(4, i) = 1-kfoldLoss(crossval(rbfSVMmd));
% end
% disp(acc(:, 1));
% disp(mean(acc, 2));

% figure
% hold on
[poly_label,polyScore] = predict(polySVMmd, test_data);
% [gaus_label,gausScore] = predict(gausSVMmd, test_data);
% [rbf_label,rbfScore] = predict(rbfSVMmd, test_data);
my_confusion_matrix(test_label, poly_label)

% [Xsvm,Ysvm,Tsvm,polyAUC] = perfcurve(classes,polyScore(:,3),'week1');
% plot(Xsvm, Ysvm, 'k')
% [Xsvm,Ysvm,Tsvm,gausAUC] = perfcurve(classes,gausScore(:,2),'inflammation');
% plot(Xsvm, Ysvm, 'b')
% [Xsvm,Ysvm,Tsvm,rbfAUC] = perfcurve(classes,rbfScore(:,2),'inflammation');
% plot(Xsvm, Ysvm, 'r')
