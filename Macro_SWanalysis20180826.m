% clear
% clc
% close all
%%
% sgn = tempSgn;
% temp = sgn;
timeAxis = (0:(modelParams.pushNum*modelParams.unitAline-1))/detectSamplingRate;
% 去除push造成的雜訊
for i=pushNum-1:-1:0
    shift = modelParams.unitAline*i;
    sgn(:, :, :, (1+modelParams.unitAline-modelParams.damping)+shift:modelParams.unitAline+shift) = [];
    sgn(:, :, :, 1+shift:(modelParams.frontNoise+modelParams.backNoise)+shift) = [];
    timeAxis(1+(modelParams.unitAline-modelParams.backNoise-modelParams.damping)+shift:modelParams.unitAline+shift) = [];
    timeAxis(1+shift:(modelParams.frontNoise)+shift) = [];
end
timeAxis = timeAxis(1:modifiedUnitAline*3);

% 去除低頻雜訊(動物呼吸、環境干擾等)
[paramB, paramA] = butter(3, bandFilter/(detectSamplingRate/2));    %第一個參數須視情況調整
temp = permute(sgn, [4 3 2 1]);
temp = filtfilt(paramB, paramA, temp);
sgn = permute(temp, [4 3 2 1]);

% 去除前後較不穩定的訊號(刪除第一個和最後一個shearwave)
sgn(:, :, :, 1:(unusedCycle-1)*modifiedUnitAline) = [];
sgn(:, :, :, end-modifiedUnitAline+1:end) = [];

% % 將剩餘的較穩定的訊號做平均作為kalman filter輸出
for f=1:fnum
    for i=1:distNum
        for d = 1:analysisDistRange(2)-analysisDistRange(1)+1
            tempSgn = mean(reshape(sgn(f, i, d, :), modifiedUnitAline, modelParams.usedCycle), 2)';
            for j=0:modelParams.usedCycle-1
                sgn(f, i, d, (1+j*modifiedUnitAline):((j+1)*modifiedUnitAline)) = tempSgn;
            end
        end
    end
end
sgn(:, :, :, modifiedUnitAline*modelParams.usedCycle+1:end) = [];

%   CC
for r=analysisDistRange(1):analysisDistRange(2)
    maxCCindex = zeros(fnum, distNum);
    phaseAngle = zeros(fnum, 20, hormonicNum);
    maxCCindex(:, 1) = size(sgn, 4);
    for f=1:fnum
        xcr = zeros(distNum, 2*size(sgn, 4)-1);
        for d=2:distNum
            xcr(d, :) = xcorr(squeeze(sgn(f, 1, r, :)), squeeze(sgn(f, d, r, :)));
            [~, I] = findpeaks(squeeze(xcr(d, :)));
            [~, tempIdx] = min(abs(I-maxCCindex(f, d-1)));
            maxCCindex(f, d) = I(tempIdx);
        end
    end
    maxCCindex = maxCCindex-size(sgn, 4);
    phaseAngle(:, 1:distNum, 1) = maxCCindex*2*pi/(detectSamplingRate/baseFrequency); %2*pi/(detectSamplingRate/baseFrequency)用於計算一個detect
    phaseAngle = permute(phaseAngle, [2 3 1]);

%     phase_50hz = zeros(analysisDistRange(2), fnum);
% pool = parpool(4);
    phase_50hz(r, :) = linearFitForStatistic20181029(path, ['0_analyze_20170403_sample (' num2str(r) ')'], [1 fnum], phaseAngle, [5 20]);
end
% delete(pool);
% myXlswrite([path '/variousRangedDepth.xlsx' ], baseFrequency*360*0.001./phase_50hz, 'velocity', 'B1');
