function [] = data2excel(filePath, fileName, dataRange, phaseAngle)
    xlsFile = [filePath '/' fileName '.xlsx'];
    data = zeros(20, 6*dataRange(2)-1);
    for i=dataRange(1):dataRange(2)
        data(:, (1+(i-dataRange(1))*6)) = [0:0.5:9.5];
        data(:, (2+(i-dataRange(1))*6):(5+(i-dataRange(1))*6)) = squeeze(phaseAngle(i, :, 1:4));
    end
    xlswrite(xlsFile, data, 'phaseAngle', 'A2');
end

