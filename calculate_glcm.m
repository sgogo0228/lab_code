% figure
% close all
clear;
% clc

% 如果是Cscan只需要輸入第一筆(_C1.dat)的檔名就好
stainlessBlockData = 'D:\子悠_Journal\子悠資料_contusion\子悠實驗資料\實驗data\20190915_skin_TranX\pulseecho\30MHz#2_filter25to35_amp0p5_freq20_attn6_A0.dat';
% file = char('J:\實驗資料\子悠實驗資料\實驗data\contusion07(G6_1_back)\20180523_day0\left\left_day0_C1.dat');
for f = 1:size(file, 1)
    if ~exist(file(f, :), 'file')
        f
        disp(file(f, :));
        return;
    end
end
dynamicRange = 42;
resolutionAxial = 0.0459; %mm
resolutionLateral = 0.0743;
AxialROIFold = 30;
LateralROIFold = 60;
nfft = 4096;

% 計算雜訊
% estimateNoiseLevel;

% % 計算鋼筷頻譜
param = headFile_NI(stainlessBlockData);
stainlessBlockSgn = mean(param.rf*2, 2);
stainlessBlockFFT = abs(fft(stainlessBlockSgn, nfft)).^2;

gcp;
us_rf = [];
for f=1:size(file, 1)
    full_file_name = strtrim(file(f, :));
    batch_size = length(dir([full_file_name(1:end-5) '*dat']));
%     可讀取Bmode data或Cscan data，同時偶數張data會左右翻轉確保X軸是正確的
    for b = 1:batch_size
        param = headFile_NI([full_file_name(1:end-5) num2str(b) '.dat']);
        us_rf = cat(3, us_rf, param.rf);
        temp = abs(hilbert(param.rf));
        log_rf = cat(3, log_rf, 20*log10(temp/max(temp, [], 'all')));
    end
    
    for i = 1:size(us_rf, 3)
    env_rf=abs(hilbert(rf(:, :, f)));
    log_rf=20*log10(env_rf/max(max(env_rf)));
    img=255*(log_rf+dynamicRange)/dynamicRange;
    xAxis = (1:param.Aline)*param.XInterval/1000;
    yAxis = ((1:param.DataLength)+param.Delay)/2/(param.SamplingRate*1e6)*1540*1000;
    
    figure('color', 'white', 'position', [500 200 1000 500])
    image(xAxis, yAxis, img)
    colormap(gray(255))
    axis equal tight
%     axis([0 8 7.5 10.5])
    title(i)
    xlabel('Distance (mm)', 'fontSize', 28, 'fontWeight', 'bold', 'fontName', 'Times')
    ylabel('Depth (mm)', 'fontSize', 28, 'fontWeight', 'bold', 'fontName', 'Times')
    set(gca, 'FontSize', 16, 'FontName', 'Times', 'FontWeight', 'bold', 'XTick', round(xAxis(1)):2:xAxis(end), 'YTick', round(yAxis(1)):1:yAxis(end))

    % for f=1:frameNum %c-scan
    % for f=1:1 %c-scan
    %     cal_times = cal_times+1;
    % 	if mod(f,2)==0       
    %         rf1 = fliplr(reshape(rf(datalength*aline*(f-1)+1:datalength*aline*f), datalength, aline)); % fliplr：左右翻轉
    %     else
    %         rf1 = reshape(rf(datalength*aline*(f-1)+1:datalength*aline*f), datalength, aline);
    %     end
    % 	%% Bandpass Filter
    % 	fs = sampling_rate*10^(3); % sample rate (kHz)
    % 	order = 2;   % order of filter
    % 	lowcut = 25*10^(3);   % low cut frequency (kHz)
    %  	highcut = 40*10^(3);   % high cut frequency (kHz)
    %  	[b,a] = butter(order, [lowcut, highcut]/(fs/2), 'bandpass'); %-6db bandwidth
    %  	rf_filter = filter(b, a, rf1);       

% %         選擇ROI
    origY = (1+param.Delay)/(param.SamplingRate*10^6)*1540*1000/2;
    if (exist('pos', 'var'))
        h = imrect(gca, pos);
    else
        h = imrect(gca,[0 origY resolutionLateral*LateralROIFold resolutionAxial*AxialROIFold]);
    end
    wait(h);
    pos = getPosition(h); %record position
    close(gcf);

% %         計算ROI四個點的座標
    pos_x1=pos(1);
    pos_x2=pos(1)+pos(3);
    pos_y1=pos(2);
    pos_y2=pos(2)+pos(4);

% %         換算成pixel數
    roi_x1 = 1+round(pos_x1/(param.XInterval*10^(-3)));
    roi_x2 = 1+round(pos_x2/(param.XInterval*10^(-3)));
    roi_y1 = 1+round(2*pos_y1*param.SamplingRate/(1540*1000*10^(-6)))-param.Delay;
    roi_y2 = 1+round(2*pos_y2*param.SamplingRate/(1540*1000*10^(-6)))-param.Delay;

% %         計算長寬的pixel數
    xlen = roi_x2-roi_x1+1;
    ylen = roi_y2-roi_y1+1;
    cimage_set = zeros(size(img, 1), size(img, 2), size(rf, 3));
    clear env_rf log_rf
    
    for i = 9:9
%     parfor i = 1:size(rf, 3)
        env_rf=abs(hilbert(rf(:, :, i)));
        log_rf=20*log10(env_rf/max(max(env_rf)));
        img = floor(255*(log_rf+dynamicRange)/dynamicRange);
        img(img<0) = 0;
        img_set(:, :, i) = img;
        
        roi = rf(roi_y1:roi_y2, roi_x1:roi_x2, i);
        NakagamiSNR(i, f) = 10*log10(sum(sum(roi.^2))/noiseSquareSum);
        
        env_roi = abs(hilbert(roi));
        env_roi = env_roi/max(max(env_roi))*255;
        roi_reshape=reshape(env_roi, xlen*ylen,1);
        nakagamiParam = mle(roi_reshape(:,1), 'distribution', 'nakagami');
        nakagami_m(i, f) = nakagamiParam(1);

        fft_roi = abs(fft(roi, nfft));
        fft_roi_avg = mean(fft_roi, 2);
        log_roi = 20*log10(fft_roi_avg/max(fft_roi_avg));

        [value, index] = max(log_roi(1:end/2)); % Find center freq.
        x1 = index; x2 = x1;
        while log_roi(x1) > -6 && x1>1
           x1 = x1-1;
        end
        while log_roi(x2) > -6 && x2<length(log_roi)
           x2 = x2+1;
        end
        
        s1 = sum(fft_roi_avg(x1+1:x2));
        s2 = sum(stainlessBlockFFT(x1+1:x2));
        ib(i, f) = 20*log10((s1/s2)/(x2-x1));  %鋼塊 bandwidth：12  取得點數要相同
        
        roi_for_glcm = img(roi_y1:roi_y2, roi_x1:roi_x2);
        glcm = get_glcm(roi_for_glcm, 4*2, 13*2);
        glcm_feature(i, f) = GLCM_Features(glcm, 0);
        glcm_set(:, :, :, i) = glcm;
    end
% %     輸出Bmode影像
%     figure('color', 'white', 'position', [500 200 1000 500])
%     colormap(gray(255))
%     for i = 1:size(rf, 3)
%         image(xAxis, yAxis, img_set(:, :, i))
%         axis equal tight
%         set(gca, 'FontSize', 20, 'FontName', 'Times', 'FontWeight', 'bold', 'XTick', round(xAxis(1)):2:xAxis(end), 'YTick', round(yAxis(1)):1:yAxis(end))
%         xlabel('Distance (mm)', 'fontSize', 28, 'fontWeight', 'bold', 'fontName', 'Times')
%         ylabel('Depth (mm)', 'fontSize', 28, 'fontWeight', 'bold', 'fontName', 'Times')
%         [dirPath, fileName, ~] = fileparts(strtrim(file(f, :)));
%         [X,Map] = rgb2ind(frame2im(getframe(gcf)), 256);
%         savePath = [dirPath '\bmode\' fileName '_' num2str(i) '.jpg'];
%         if exist([dirPath '\bmode\'], 'dir')==0
%             mkdir([dirPath '\bmode\']);
%         end
%         if exist(savePath, 'file')==2
%             delete(savePath);
%         end
%         imwrite(X, Map, savePath, 'JPEG');
%     end
% %     輸出glcm
%     for i = 1:size(rf, 3)
%         for j=1:5
%             image([0 255], [0 255], glcm_set(:, :, j, i)*2)
%             axis equal tight
%             set(gca, 'FontSize', 12, 'FontName', 'Times', 'FontWeight', 'bold', 'XTick', 0:50:255)
%             [dirPath, fileName, ~] = fileparts(strtrim(file(f, :)));
%             [X,Map] = rgb2ind(frame2im(getframe(gcf)), 256);
%             savePath = [dirPath '\glcm\' fileName '_d' num2str(j) '_' num2str(i) '.jpg'];
%             if exist([dirPath '\glcm\'], 'dir')==0
%                 mkdir([dirPath '\glcm\']);
%             end
%             if exist(savePath, 'file')==2
%                 delete(savePath);
%             end
%             imwrite(X, Map, savePath, 'JPEG');
%         end
%     end
    close all
end
delete(gcp('nocreate'));
a = c