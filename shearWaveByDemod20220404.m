%     --Zheng, Y.; Chen, S.; Tan, W.; Kinnick, R.; Greenleaf, J.F. Detection of tissue harmonic motion induced by ultrasonic radiation 
%       force using pulse-echo ultrasound and Kalman filter. IEEE Trans. Ultrason. Ferroelectr. Freq. Control 2007.
%     --Zheng, Y.; Greenleaf, J.F. Stable and unbiased flow turbulence estimation from pulse echo ultrasound. IEEE Trans. Ultrason.
%       Ferroelectr. Freq. Control 1999, 46, 1074-1087.
%     根據這兩篇用X跟Y抓出SW但效果好像不比用IQ好
function [params, demodSignal] = shearWaveByDemod20220404(filename, depth, unitAline, pushNum, push_noise, W0)
    % nfft = 4096;
    % 讀取檔案
    params = headFile_NI(filename);
    data = params.rf;
    data = data*params.Vpp/255;

    % demodulation
    [I, Q] = demod(data,W0,1e9,'qam');

    X = [zeros(size(I, 1), 1), I(:, 1:end-1).*I(:, 2:end)+Q(:, 1:end-1).*Q(:, 2:end)];
    Y = [zeros(size(I, 1), 1), Q(:, 1:end-1).*I(:, 2:end)-I(:, 1:end-1).*Q(:, 2:end)];
        
    temp = atan(Y./X);
    temp = permute(temp, [2 1]);
    temp = reshape(temp, unitAline, [], depth(2)-depth(1)+1);
    temp(1:push_noise, :, :) = [];
    demodSignal = reshape(temp, size(temp, 1)*size(temp, 2), depth(2)-depth(1)+1);
    
    gradient_y = demodSignal(2:end, :) - demodSignal(1:end-1, :);
    gradient_y(abs(gradient_y) <= pi/2) = 0;
    gradient_y(gradient_y > 0.3) = pi;
    gradient_y(gradient_y < -0.3) = -pi;
    grad_cumsum = cumsum(gradient_y);
    demodSignal(2:end, :) = demodSignal(2:end, :) - grad_cumsum;
    demodSignal = demodSignal - mean(demodSignal, 1);

%     Ws = 2*pi*50;
%     T = 1/10000;
%     demodSignal = -1/2/sin(Ws*T/2)*demodSignal;
end
