clear
clc
close all
%%
% 能夠一次分析好幾組shear wave
unitAline = 199;
pushNum = 5;
distNum = 10;
depth = [1 8000];
% CImage = '30MHz_dist0to5_depth5to14';
analyzeFilePath = ['H:\school\shearWaveData\20180305_DM_w7\rat#2_withDM_head\'];
fileSet = char('H:\school\shearWaveData\20180106_DM_w0\head_right\30MHz_dist0to10_depth10to15k_amp600_cycle5100_1',...
    'H:\school\shearWaveData\20180106_DM_w0\head_right\30MHz_dist0to10_depth10to15k_amp600_cycle5100_2',...
    'H:\school\shearWaveData\20180106_DM_w0\head_right\30MHz_dist0to10_depth10to15k_amp600_cycle5100_3');
pushNoise=10;
detectSamplingRate=10000;
hormonicNum = 5;
baseFrequency=50;
analyzeInterval = 250;
modelParams = struct('unitAline', unitAline, 'pushNum', pushNum, 'usedCycle', 3, 'frontNoise', pushNoise-3, 'backNoise', 3, 'damping', 0);

fnum = size(fileSet, 1);
bandFilter = [baseFrequency*0.7 baseFrequency*6];
OverallDepth = depth(2)-depth(1)+1;
modifiedUnitAline = (modelParams.unitAline-modelParams.frontNoise-modelParams.backNoise-modelParams.damping);
unusedCycle=modelParams.pushNum-modelParams.usedCycle;
distPostfix = {'0', '0p5', '1', '1p5', '2', '2p5', '3', '3p5', '4', '4p5'};

for i=1:depth(2)/analyzeInterval
    range(i, 1) = (i-1)*analyzeInterval+1;
    range(i, 2) = i*analyzeInterval;
end
analysisDistRange = [1 depth(2)/analyzeInterval];
xcorrDepth = 10;
sgn = zeros(fnum, distNum, analysisDistRange(2)-analysisDistRange(1)+1, unitAline*pushNum);

for f = 1:fnum
    filename = [strtrim(fileSet(f, :)) '_SW0.dat'];
    if ~exist(filename, 'file')
        f
        disp(filename);
        return;
    end
end


try
    for f=1:fnum
        waitbar(f/fnum)
        demodSignal = zeros(distNum, OverallDepth, unitAline*pushNum);
        tempidx = regexp(fileSet(f, :), 'dist0');
        parfor j=1:distNum
            fullFilename = [strtrim(fileSet(f, 1:tempidx+3)) strtrim(distPostfix{j}) strtrim(fileSet(f, tempidx+5:end)) '_SW0.dat'];
            [sgnParam demodSignal(j, :, :)]=shearWaveByDemod20170327(fullFilename, depth, unitAline, pushNum, pushNoise);
        end
        for r = 1:analysisDistRange(2)-analysisDistRange(1)+1
            sgn(f, :, r, :) = squeeze(mean(demodSignal(:, range(r, 1):range(r, 2), :), 2));
        end
    end
catch
    delete(gcp('nocreate'));
end
delete(gcp('nocreate'));

% shearWaveImage = zeros(fnum,  distNum, overallDepth/depthInterval, unitAline*pushNum);
% for f=1:fnum
%     for j=0:distNum-1
%         for d = 1:depthInterval:overallDepth-depthInterval+1
%             shearWaveImage(f, j+1, (d+depthInterval-1)/depthInterval, :) = mean(squeeze(demodSignal(f, j+1, d:d+depthInterval-1, :)), 1);
%         end
%     end
% end
% sgn = shearWaveImage;
% timeAxis = (0:(modelParams.pushNum*modelParams.unitAline-1))/samplingRate*1000;

% 去除push造成的雜訊
for i=pushNum-1:-1:0
    shift = modelParams.unitAline*i;
    sgn(:, :, :, 1+shift:(modelParams.frontNoise+modelParams.backNoise)+shift) = [];
%     timeAxis(1+shift:(modelParams.frontNoise+modelParams.backNoise)+shift) = [];
end

% 去除低頻雜訊(動物呼吸、環境干擾等)
[paramB, paramA] = butter(5, bandFilter/(detectSamplingRate/2));
tempSgn = permute(sgn, [4 3 2 1]);
tempSgn = filtfilt(paramB, paramA, tempSgn);
sgn = permute(tempSgn, [4 3 2 1]);

% 去除前後較不穩定的訊號
sgn(:, :, :, 1:(unusedCycle-1)*modifiedUnitAline) = [];
sgn(:, :, :, end-modifiedUnitAline+1:end) = [];

% 將中間較穩定之訊號座平均作為最終的分析訊號
for f=1:fnum
    for i=1:distNum
        for d=1:OverallDepth/analyzeInterval
            tempSgn = mean(reshape(sgn(f, i, d, :), (modelParams.unitAline-modelParams.frontNoise-modelParams.backNoise), modelParams.usedCycle), 2)';
            tempSgn((end-modelParams.damping+1):end) = [];
            for j=0:modelParams.usedCycle-1
                sgn(f, i, d, (1+j*modifiedUnitAline):((j+1)*modifiedUnitAline)) = tempSgn;
            end
            sgn(f, i, d, :) = squeeze(sgn(f, i, d, :))-min(sgn(f, i, d, :));
            sgn(f, i, d, :) = squeeze(sgn(f, i, d, :))/max(sgn(f, i, d, :));
        end
    end
end
% sgn(:, :, :, modifiedUnitAline*modelParams.usedCycle+1:end) = [];

load('H:\school\shearWaveData\colorMapGroup.mat')
figure(1)
graphInterval = 2;
colorScale = [0 1];

% 輸出2D的SW圖
for f=1:fnum
    imgSgn(f, :, :, :) = sgn(f, :, :, :)-min(min(min(min(sgn(f, :, :, :)))));
    [filepath, filename, ~] = fileparts(fileSet(f, :));
%     if isempty(innerFolderIndex)==0
        savePath = [filepath '\gif_' strtrim(filename) '.gif'];
%     else
%         savePath = [path '0_' file(f, :) '_test.gif'];
%     end
    i=1;
%     imagesc(squeeze((sgn(f, :, :, i)))');
    param = headFile_NI([strtrim(fileSet(f, :)) '_SW0.dat']);
    xAxis = (0:distNum-1)*0.5;
    yAxis = (((1:param.DataLength)+param.Delay)/1000000000/2*1540*1000);
    imagesc(xAxis, yAxis, squeeze((imgSgn(f, :, :, i)))');
    caxis(colorScale)
    title(num2str(i))
    set(gca, 'FontSize', 24, 'FontName', 'Times', 'FontWeight', 'bold')
    xlabel('Distance (mm)', 'FontSize', 28, 'FontWeight', 'bold')
    ylabel('Depth (mm)', 'FontSize', 28, 'FontWeight', 'bold')
%     axis image
    [X,Map] = rgb2ind(frame2im(getframe(1)), 256);
    if exist(savePath, 'file')==2
        delete(savePath);
    end
    imwrite(X, Map, savePath, 'GIF', 'WriteMode', 'overwrite', 'DelayTime', 0, 'LoopCount', Inf);
    i = graphInterval+1;
    while i<=modifiedUnitAline                % 任何一個 loop 
%         imagesc(squeeze((sgn(f, :, :, i)))');
        imagesc(xAxis, yAxis, squeeze((imgSgn(f, :, :, i)))');
        caxis(colorScale)
        colorbar
        title([num2str(i*0.1) 'ms'])
        set(gca, 'FontSize', 24, 'FontName', 'Times', 'FontWeight', 'bold')
        xlabel('Distance (mm)', 'FontSize', 28, 'FontWeight', 'bold')
        ylabel('Depth (mm)', 'FontSize', 28, 'FontWeight', 'bold')
%         axis image
        [X,Map] = rgb2ind(frame2im(getframe(1)), 256);
        %在 loop內 的writemode 須改為 append ,這時候frame才會不斷的增加進去
        imwrite(X, Map, savePath,'GIF','WriteMode','append','DelayTime', 0);
        i = i+graphInterval;
    end
end

% % % 輸出3D的SW圖
% % for f=1:fnum
% %     innerFolderIndex = strfind(file(f, :), '\');
% %     if isempty(innerFolderIndex)==0
% %         savePath3D = [path file(f, 1:innerFolderIndex)  '0_' file(f, innerFolderIndex+1:end) '_3D.gif'];
% %     else
% %         savePath3D = [path '0_' file(f, :) '_3D.gif'];
% %     end
% %     close all
% %     figure('position', [900 200 670 500])
% %     surf(xAxis, yAxis, squeeze((sgn(f, :, :, i)))');
% %     zlim(colorScale)
% %     caxis(colorScale)
% %     view(-45, -60);
% %     title(num2str(i))
% %     xlabel('Distance (mm)', 'FontSize', 14)
% %     ylabel('Depth (mm)', 'FontSize', 14)
% %     pause(0.5)
% % %         print('-djpeg', [photoPath fname num2str(i) '.jpg']);
% %     [X,Map] = rgb2ind(frame2im(getframe(gcf)), 256);
% %     if exist(savePath3D, 'file')==2
% %         delete(savePath3D);
% %     end
% %     imwrite(X, Map, savePath3D, 'GIF', 'WriteMode', 'overwrite', 'DelayTime', 0, 'LoopCount', Inf);
% %     
% %     i = graphInterval+1;
% %     while i<=modifiedUnitAline                % 任何一個 loop
% %         close
% %         figure('position', [900 200 670 500])
% %         surf(xAxis, yAxis, squeeze((sgn(f, :, :, i)))');
% %         zlim(colorScale)
% %         caxis(colorScale)
% %         view(-45, -60);
% %         title(num2str(i))
% %         xlabel('Distance (mm)', 'FontSize', 14)
% %         ylabel('Depth (mm)', 'FontSize', 14)
% %         pause(0.5)
% %     %         print('-djpeg', [photoPath fname num2str(i) '.jpg']);
% %         [X,Map] = rgb2ind(frame2im(getframe(gcf)), 256);
% %         imwrite(X, Map, savePath3D, 'GIF', 'WriteMode', 'append','DelayTime', 0);
% % 
% %         i = i+graphInterval;
% %     end
% % end