#%%
from functools import partial
import numpy as np
import os, glob
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torch.utils.data as dataclass
import torchvision
import torchvision.transforms as transforms
from torchvision import models
from ray import tune
from ray.tune import CLIReporter
from ray.tune.schedulers import ASHAScheduler
from yaml import MarkedYAMLError
from tqdm import tqdm
import warnings
import myLib
from pytictoc import TicToc

warnings.filterwarnings('ignore')

num_samples=1
max_num_epochs=15
bs = 4
val_bs = 4
lr = 1e-4
config = {
    "lr": tune.grid_search([5e-4]),
    # "batch_size": tune.choice([4, 8])
}

class myNet(nn.Module):
    def __init__(self):
        super(myNet, self).__init__()
        # net = models.vgg16_bn(pretrained=True)
        # for vgg net
        
        conv_1 = nn.Sequential(nn.Conv2d(1, 32, kernel_size=(7, 7), stride=(1, 1), padding=3), 
                                nn.ReLU(), 
                                nn.MaxPool2d(kernel_size=2, stride=2))
        conv_2 = nn.Sequential(nn.Conv2d(32, 64, kernel_size=(7, 7), stride=(1, 1), padding=3), 
                                nn.ReLU(), 
                                nn.MaxPool2d(kernel_size=2, stride=2))
        conv_3 = nn.Sequential(nn.Conv2d(64, 128, kernel_size=(7, 7), stride=(1, 1), padding=3), 
                                nn.ReLU(), 
                                nn.MaxPool2d(kernel_size=2, stride=2))
        conv_4 = nn.Sequential(nn.Conv2d(128, 256, kernel_size=(7, 7), stride=(1, 1), padding=3), 
                                nn.ReLU())
        tran_conv_1 = nn.Sequential(nn.ConvTranspose2d(256, 128, kernel_size=(2, 2), stride=(2, 2)), 
                                nn.ReLU())
        tran_conv_2 = nn.Sequential(nn.ConvTranspose2d(128, 64, kernel_size=(2, 3), stride=(2, 2)), 
                                nn.ReLU())
        tran_conv_3 = nn.Sequential(nn.ConvTranspose2d(64, 32, kernel_size=(2, 3), stride=(2, 2)), 
                                nn.ReLU())
        out = nn.Conv2d(32, 1, kernel_size=(3, 3), stride=(1, 1), padding=1)
        self.out = nn.Sequential(conv_1, conv_2, conv_3, conv_4, tran_conv_1, tran_conv_2, tran_conv_3, out)

    def forward(self, x):
        x = self.out(x)
        return x

class Displacement_dataset(dataclass.Dataset):
    def __init__(self, root, transform):
        self.transform = transform
        self.data = os.listdir(f'{root}/data')
        self.data = [f'{root}/data/{i}' for i in self.data]
        self.label = os.listdir(f'{root}/label')
        self.label = [f'{root}/label/{i}' for i in self.label]
        assert len(self.data) == len(self.label), 'data and label numbers are different.'

    def __getitem__(self, index):
        data  = np.load(self.data[index]).astype(np.float32)
        label = np.load(self.label[index]).astype(np.float32)
        data = self.transform(data)
        label = self.transform(label)
        return data, label
    
    def __len__(self):
        return len(self.data)

def load_data(data_dir="./"):
    train_transform = transforms.Compose([
        transforms.ToTensor(),
    ])
    val_transform = transforms.Compose([
        transforms.ToTensor(),
    ])

    train_dir = data_dir + '/training'
    train_datasets = Displacement_dataset(train_dir, transform=train_transform)

    val_dir = data_dir + '/validation'
    val_datasets = Displacement_dataset(val_dir, transform=val_transform)

    return train_datasets, val_datasets

def train_cifar(config, checkpoint_dir=None, data_dir=None, max_epoch=20):
    # confusion = np.zeros((4, 4))
    fold_loss = 0.0
    net = myNet()
    t = TicToc()

    if torch.cuda.is_available():
        net.cuda()
    device = "cpu"
    if torch.cuda.is_available():
        device = "cuda:0"
        if torch.cuda.device_count() > 1:
            net = nn.DataParallel(net)
    net.to(device)
    criterion = nn.MSELoss()
    optimizer = optim.Adam(net.parameters(), lr=config["lr"])
    # optimizer = optim.Adam(net.parameters(), lr=lr)

    trainset, valset = load_data(data_dir=data_dir)
    train_dataloader = torch.utils.data.DataLoader(trainset, 
        batch_size=bs, 
        shuffle=True,
        num_workers=0)
    
    val_dataloader = torch.utils.data.DataLoader(valset, 
        batch_size=val_bs, 
        shuffle=True,
        num_workers=0)

    mse = np.zeros((max_epoch, len(train_dataloader)))
    val_mse = np.zeros(len(val_dataloader))
    val_mse_without_push = np.zeros(len(val_dataloader))
    indexes = np.arange(995).reshape((5, 199))
    indexes = indexes[:, 15:].reshape((-1))

    t.tic()
    for epoch in range(max_epoch):
        running_loss = 0.0
        running_acc = 0.0
        epoch_steps = 0
        for idx, (img, label) in enumerate(train_dataloader):
            inputs = img.cuda()
            labels = label.cuda()
            # zero the parameter gradients
            optimizer.zero_grad()

            # forward + backward + optimize
            outputs = net(inputs)
            loss = criterion(outputs, labels)
            train_correct = loss
            mse[epoch, idx] = loss.item()

            loss = loss*2
            loss.backward()
            optimizer.step()

            # print statistics
            running_acc += train_correct.item()
            running_loss += loss.item()
        
        Loss = float(running_acc / len(train_dataloader))
        # Acc = float(running_acc / (len(trainset)))
        print(f"epoch: {epoch+1}")
        print("Train Loss:%.9f" % Loss)
        # print("Train Accuracy:%.9f" % Acc)
    t.toc()
    training_time = t.tocvalue()

    torch.cuda.empty_cache()
    # Validation loss
    net.eval()
    with torch.no_grad():
        val_loss = 0.0
        val_acc = 0.0
        val_acc_without_push = 0.0
        
        t.tic()
        for idx, (img, label) in enumerate(val_dataloader):
            inputs = img.cuda()
            labels = label.cuda()
            outputs = net(inputs)
            loss = criterion(outputs, labels)
            val_loss += loss.item()
            val_correct = loss
            val_acc += val_correct.item()

            outputs2 = outputs[:, :, :, indexes]
            labels2 = labels[:, :, :, indexes]
            loss2 = criterion(outputs2, labels2)
            val_acc_without_push += loss2.item()
            val_mse_without_push[idx] = loss2.item()

            val_mse[idx] = loss.item()
        t.toc()
    valid_time = t.tocvalue()

    # with tune.checkpoint_dir(0) as checkpoint_dir:
    #     path = os.path.join(checkpoint_dir, "checkpoint")
    #     torch.save((net.state_dict(), optimizer.state_dict()), path)

    Loss = float(val_loss / len(val_dataloader))
    # Acc = float(val_acc / (len(valset)))
    print("Validation Loss:%.9f" % Loss)
    print(f"Validation Loss without Push: {val_acc_without_push / len(val_dataloader)}")
    # print("Validation Accuracy:%.9f; correct number:%d; total: %d" % (Acc, val_acc, len(valset)))
    print("Finished")
    
    file_copy = 1
    while os.path.exists(rf'C:\Users\Buslab\Desktop\Experiment data\DaMing\contusion\displacement_estimation\model_lr{config["lr"]}_{file_copy}.pt')==True :
        file_copy += 1
    myLib.output_data({'mse': mse, 'val_mse': val_mse, "val_mse_without_push": val_mse_without_push, 'training_time': training_time, 'valid_time': valid_time}, r'C:\Users\Buslab\Desktop\Experiment data\DaMing\contusion\displacement_estimation', name=f'temp_lr{config["lr"]}_{file_copy}')
    torch.save(net.state_dict(), rf'C:\Users\Buslab\Desktop\Experiment data\DaMing\contusion\displacement_estimation\model_lr{config["lr"]}_{file_copy}.pt')
    tune.report(loss=Loss)

gpus_per_trial=1
data_dir = os.path.abspath(r"C:\Users\Buslab\Desktop\Experiment data\DaMing\contusion\displacement_estimation")
# train_cifar(config=config, data_dir=data_dir, max_epoch=max_num_epochs)


scheduler = ASHAScheduler(
    metric="loss",
    mode="min",
    max_t=max_num_epochs,
    grace_period=1,
    reduction_factor=2)
reporter = CLIReporter(
    # parameter_columns=["l1", "l2", "lr", "batch_size"],
    metric_columns=["loss", "accuracy", "training_iteration"])
result = tune.run(
    partial(train_cifar, data_dir=data_dir, max_epoch=max_num_epochs),
    resources_per_trial={"cpu": 8, "gpu": gpus_per_trial},
    config=config,
    num_samples=num_samples,
    scheduler=scheduler,
    progress_reporter=reporter)

best_trial = result.get_best_trial("loss", "min", "last")
print("Best trial config: {}".format(best_trial.config))
print("Best trial final validation loss: {}".format(
    best_trial.last_result["loss"]))
# print("Best trial final validation accuracy: {}".format(
#     best_trial.last_result["accuracy"]))

# %%
# import torch
# import numpy as np
# import scipy.io as matlabio
# import matplotlib.pyplot as plt
# import importlib
# import glob
# import myLib
# importlib.reload(myLib)

# torch.cuda.empty_cache()
# print(torch.cuda.memory_allocated())
# print(torch.cuda.memory_reserved())
# print(torch.cuda.max_memory_reserved())

# data_path = glob.glob(r'C:\Users\Buslab\Desktop\Experiment data\DaMing\contusion\displacement_estimation\validation\data\*.npy')
# label_path = glob.glob(r'C:\Users\Buslab\Desktop\Experiment data\DaMing\contusion\displacement_estimation\validation\label\*.npy')
# mse_val = np.zeros(len(data_path))
# mse_without_push = np.zeros(len(data_path))
# for i in range(len(data_path)):
#     data = np.load(data_path[i]).reshape((1, 1, 1000, 995)).astype(np.float32)
#     data = torch.tensor(data)
#     output = net(data).detach().numpy()
#     label = np.load(label_path[i])
#     mse_val[i] = ((output-label)**2).mean(axis=1).mean()
#     output = np.reshape(output, (1000, 5, 199))
#     label = np.reshape(label, (1000, 5, 199))
#     output = output[:, 1:4, 15:].reshape((1000, 184*3))
#     label = label[:, 1:4, 15:].reshape((1000, 184*3))
#     mse_without_push[i] = ((output-label)**2).mean(axis=1).mean()
    
# #%%
# print(mse_val[401:601].mean())
# print(mse_without_push[401:601].mean())
