import numpy as np
import pandas as pd
# import pymrmr
import mrmr
import sklearn_relief as relief
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
from sklearn.metrics import ConfusionMatrixDisplay
import sklearn.preprocessing as prep
from sklearn.pipeline import make_pipeline
from sklearn import decomposition
import shap
import sklearn
from sklearn.feature_selection import RFE
from sklearn import svm
from xgboost.sklearn import XGBClassifier
import lightgbm as lgbm
import matplotlib


matplotlib.interactive('True')
feature_reduction = 'pca'
classifier = "LGBM"
reduced_dimension = 40

data_path = rf'D:\DaMing\contusion_classification\data.xlsx'
label_path = rf'D:\DaMing\contusion_classification\label.xlsx'
data = pd.read_excel(data_path)
label = pd.read_excel(label_path).squeeze().to_numpy()
max_feature_num = len(data.columns)
x_feature_names = data.columns

# -regularization, performance significantly decreases without regularization for svm (StandardScaler is most suitable for svm)
# -while only minor improvement occurs for XGB and LGBM (MaxAbsScaler for the two) 
def data_normalization(classifier, data):
    if classifier == 'SVM':
        return prep.StandardScaler().fit_transform(data)
    else:
        return prep.MaxAbsScaler().fit_transform(data)
    
# 用pymrmr這個package跑的,可能會近c++會遇到overflow的問題，而且跑出來的結果比較不理想
# data = pd.read_excel('D:\DaMing\contusion_classification\data2.xlsx')
# print(data)
# selected_features = pymrmr.mRMR(data, 'MIQ', 10)

# normalization is necessary before pca and relief
if feature_reduction == 'pca':
    scale_data = data_normalization(classifier, data)
    pca = decomposition.PCA(n_components=reduced_dimension)
    reduced_data = pca.fit_transform(scale_data)
elif feature_reduction == 'mrmr':
    sf  = mrmr.mrmr_classif(X=data, y=label, K=reduced_dimension)
    reduced_data = data_normalization(classifier, data[sf])
elif feature_reduction == 'rfe':
    reduced_data = data_normalization(classifier, data)
    if classifier=="SVM":
        clf = svm.SVC(kernel='rbf', C=1, gamma='auto', probability=True)
    elif classifier=="XGB":
        # importance_type='gain' should be given to generate feature_importances_ for further performance evaluation by RFE
        clf = XGBClassifier(importance_type='gain')
    else:
        clf = lgbm.LGBMClassifier(importance_type='gain')
    sf = RFE(estimator=clf, n_features_to_select=reduced_dimension)
    reduced_data = sf.fit_transform(reduced_data, label)
elif feature_reduction == 'relief':
    scale_data = data_normalization(classifier, data)
    r = relief.Relief(n_features=reduced_dimension, n_jobs=1) # Choose the best 3 features
    # r = relief.ReliefF(n_features=reduced_dimension, n_jobs=1)
    # r = relief.RReliefF(n_features=reduced_dimension, n_jobs=1)
    reduced_data = r.fit_transform(scale_data, label)
else:
    reduced_data = data_normalization(classifier, data)

X_train, X_test, y_train, y_test = train_test_split(reduced_data, label, test_size=0.2, random_state=19)


if classifier=="SVM":
    clf = svm.SVC(kernel='rbf', C=1, gamma='auto', probability=True)
elif classifier=="XGB":
    clf = XGBClassifier()
else:
    # clf = lgbm.LGBMClassifier()
    clf = lgbm.LGBMClassifier(min_child_samples=40, num_leaves=127, max_depth=9, learning_rate=0.5, n_estimators=45)
clf.fit(X_train,y_train)
print(clf.score(X_train,y_train))
print(clf.score(X_test, y_test))

# y_predict = clf.predict(X_test)

# output the confusion matrix
# class_names = ['normal', 'destruction', 'repair', 'remodeling']
# titles_options = [
#     ("Confusion matrix, without normalization", None),
#     ("Normalized confusion matrix", "true"),
# ]
# for title, normalize in titles_options:
#     disp = ConfusionMatrixDisplay.from_estimator(
#         clf,
#         X_test,
#         y_test,
#         display_labels=class_names,
#         cmap=plt.cm.Blues,
#         normalize=normalize,
#     )
#     # disp.ax_.set_title(title)

#     print(title)
#     print(disp.confusion_matrix)

# # execute SHAP
class_names = ['normal', 'destruction', 'repair', 'remodeling']
explainer = shap.KernelExplainer(model=clf.predict_proba, data=X_test, link='logit')
shap_values = explainer.shap_values(X=X_test)
# This line can only run on jupyter and show visual picture of shapley value
shap.summary_plot(shap_values[0], X_test, max_display=10, class_names= class_names, feature_names = x_feature_names)
