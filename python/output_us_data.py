# output rf, I, and Q from raw data file
import matplotlib
import myLib
import importlib
import os, glob
import numpy as np
importlib.reload(myLib)

importlib.reload(myLib)
pos_num = 30
path = rf'D:\DaMing\sw_data\phantom\path.txt'
dir, _ = os.path.split(path)
file_num = 1
push_noise_idx = push_noise_idx = np.array([np.arange(15, 199)+199*i for i in range(0, 5)]).reshape(-1)
with open(path, 'r') as f:
    filename_set = f.readlines()
    for i in range(0, len(filename_set)):
        for j in range(0, pos_num):
            filename = rf'{filename_set[i][:-2]}{j}'
            rf = myLib.headFile_NI(filename)
            fc = 30*1e6
            fs = rf['SamplingRate']*1e6
            data = rf['rf'].squeeze()
            data = data[:, push_noise_idx]
            I, Q = myLib.IQ_demod(data, fc, fs)
            filename, _ = os.path.splitext(filename)
            np.save(rf'{dir}\data\{file_num}', data)
            np.save(rf'{dir}\I\{file_num}', I)
            np.save(rf'{dir}\Q\{file_num}', Q)
            file_num = file_num + 1