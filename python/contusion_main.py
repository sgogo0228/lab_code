#%% 準備training data
import torch.nn as nn
import torchvision.models as model
import torchsummary
from torchsummary import summary
from torch.utils import data
from torchvision import transforms
from torchvision import datasets
import matplotlib.pyplot as plt
import numpy as np
import cv2
import scipy.io as matlabio
import codecs
import struct
import re
import pandas as pd
import myLib
data_name = pd.read_csv('E:/temp/path.txt', header=None)
data_name = data_name.iloc[:, 0]
data_counter = 0
for i in range(1):
    for j in range(1):
        name = re.sub('SW0', 'SW'+str(j), data_name[i])
        param = myLib.headFile_NI(name)
        Delay = param['Delay']
        rf = param['rf']
        w = 500
        data_range = (round(9/1540/1000*1e9*2-Delay-w), round(9/1540/1000*1e9*2-Delay+w))
        data_out = rf[data_range[0]:data_range[1], :, 0]

        np.save('C:/Users/Buslab/Desktop/Experiment data/DaMing/contusion/displacement_estimation/train_data/' + str(data_counter), data_out)
        data_counter += 1

#%%
import matplotlib.pyplot as plt
import numpy as np
import cv2
import scipy.io as matlabio
import codecs
import struct
import re
import pandas as pd
import myLib

data_dir = r'C:\Users\Buslab\Desktop\Experiment data\DaMing\contusion\displacement_estimation\path.txt'
file_name = pd.read_csv(data_dir, header=None)
file_name = file_name.iloc[:, 0]
file_counter = 0
for i in range(562):
    name = file_name[i];
    # print(name.strip())
    rf = matlabio.loadmat(name.strip()[1:-1])['temp'][0][0][0]
    for j in range(10):
        out = rf[:, :, j] - rf[:, 0, j].reshape((1000, 1))
        np.save(r'C:\Users\Buslab\Desktop\Experiment data\DaMing\contusion\displacement_estimation\ground_truth\\' + str(file_counter), rf[:, :, j])
        file_counter += 1