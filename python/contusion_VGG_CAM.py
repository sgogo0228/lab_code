#%%
from __future__ import print_function, division

import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torchvision import datasets, transforms
from torch.autograd import Variable
import numpy as np
from torchvision import models

import time
import scipy.io as io
import cv2
import matplotlib.pyplot as plt

img_size = (131, 512);

train_transforms = transforms.Compose([
    transforms.RandomHorizontalFlip(),
    transforms.ToTensor()
])
val_transforms = transforms.Compose([
    transforms.ToTensor()
])

net_name = 'vgg11_bn'
class VGGNet(nn.Module):
    def __init__(self, num_classes=4):
        super(VGGNet, self).__init__()
        net = models.vgg11(pretrained=True)
        # for vgg net
        self.features = nn.Sequential(*list(net.features.children())[:-1])
        self.classifier = nn.Linear(512, num_classes)

        # for resnet
        # self.features = nn.Sequential(net.conv1, net.bn1, net.relu, net.maxpool, net.layer1, net.layer2, net.layer3, net.layer4)
        # self.classifier = nn.Linear(512, num_classes)

    def forward(self, x):
        x = self.features(x)
        x = x.mean([2, 3])
        # x = x.view(x.size()[0], 512, 4*16).mean(2)
        x = self.classifier(x)
        return x


#--------------------訓練過程---------------------------------

batch_size = 8
epoch = 30
learning_rate = [0.00001]
accuracy = np.zeros(len(learning_rate));
for lr_idx in range(len(learning_rate)):
    confusion = np.zeros((5, 4, 4))
    frame_count = 1
    for fold_iter in range(5):
        train_dir = 'C:\\Users\\Buslab\\Desktop\\Experiment data\\DaMing\\contusion\\5-fold validation_v2\\' + str(fold_iter+1) + '\\training'
        train_datasets = datasets.ImageFolder(train_dir, transform=train_transforms)
        train_dataloader = torch.utils.data.DataLoader(train_datasets, batch_size=batch_size, shuffle=True)
        
        val_dir = 'C:\\Users\\Buslab\\Desktop\\Experiment data\\DaMing\\contusion\\5-fold validation_v2\\' + str(fold_iter+1) + '\\testing'
        val_datasets = datasets.ImageFolder(val_dir, transform=val_transforms)
        val_dataloader = torch.utils.data.DataLoader(val_datasets, batch_size=batch_size, shuffle=True)
        
        label_name = train_datasets.classes
        model = VGGNet()
        if torch.cuda.is_available():
            model.cuda()
        params = [{'params': md.parameters()} for md in model.children()
                if md in [model.classifier]]
        optimizer = optim.Adam(model.parameters(), lr=learning_rate[lr_idx])
        loss_func = nn.CrossEntropyLoss()

        start = time.time()
        model.train()
        # for i in range(2):
        for i in range(epoch):
            print('epoch {}'.format(i + 1))
            train_loss = 0.
            train_acc = 0.
            for img, label in train_dataloader:
                img = img.cuda()
                label = label.cuda()
                optimizer.zero_grad()
                output = model(img)
                # print(output.size())
                loss = loss_func(output, label)
                train_loss += loss.item()
                pred = torch.max(output, 1)[1]
                train_correct = (pred == label).sum()
                train_acc += train_correct.item()
                loss.backward()
                optimizer.step()
            Loss = float(train_loss / (len(train_datasets)))
            Acc = float(train_acc / (len(train_datasets)))
            print("Train Loss:%.9f" % Loss)
            print("Train Accuracy:%.9f" % Acc)

        end = time.time()
        print("Training Time: %.2f mins" % ((end-start)/60))

        gap_weight = model.classifier.weight
        #%%
        label_name = ['destruction', 'normal', 'remodeling', 'repair']
        def get_CAM(feature_map, weight, labels):
            output_cam = [];
            bs, ch, w, h = feature_map.shape
            for f in range(bs):
                temp = feature_map[f].reshape(ch, -1)
                temp = np.matmul(weight[labels[f]], temp).reshape(w, h)
                temp = (temp - np.min(temp)) / np.max(temp) *255
                output_cam.append(cv2.resize(temp, (img_size[1], img_size[0])))
            return output_cam

        model.eval()
        val_loss = 0.
        val_acc = 0.
        for img, label in val_dataloader:
            img = img.cuda()
            label = label.cuda()
            output = model(img)
            loss = loss_func(output, label)
            val_loss += loss.item()
            pred = torch.max(output, 1)[1]
            val_correct = (pred == label).sum()
            val_acc += val_correct.item()

            for idx in range(label.size(dim=0)):
                confusion[fold_iter, label[idx], pred[idx]] = confusion[fold_iter, label[idx], pred[idx]] + 1

            cam_img = get_CAM(model.features(img).cpu().detach().numpy(), gap_weight.cpu().detach().numpy(), pred)
            for f in range(len(cam_img)):
                heatmap = cv2.applyColorMap(cam_img[f].astype('uint8'), cv2.COLORMAP_JET)
                overlaid_img = img[f].cpu().detach().numpy() * 255
                overlaid_img = overlaid_img.astype('uint8').transpose((1, 2, 0))
                overlaid_img = heatmap * 0.5 + overlaid_img * 0.5

                font = cv2.FONT_HERSHEY_SIMPLEX
                font_size = 0.5
                font_color = (128, 128, 128)
                font_thickness = 2
                text = 'predict: ' + label_name[pred[f]] + ', GT: ' + label_name[label[f]]
                x,y = 200,20
                overlaid_img = cv2.putText(overlaid_img, text, (x,y), font, font_size, font_color, font_thickness, cv2.LINE_AA)
                # overlaid_img = cv2.putText(overlaid_img, 'test', (10, 10))
                if pred[f] == label[f]:
                    cv2.imwrite('5-fold validation_2\\cam\\correct\\CAM image' + str(frame_count) + '.jpg', overlaid_img)
                else:
                    cv2.imwrite('5-fold validation_2\\cam\\wrong\\CAM image' + str(frame_count) + '.jpg', overlaid_img)
                frame_count += 1

        Loss = float(val_loss / (len(val_datasets)))
        Acc = float(val_acc / (len(val_datasets)))
        print("Validation Loss:%.9f" % Loss)
        print("Validation Accuracy:%.9f" % Acc)
        torch.cuda.empty_cache()
        # print(confusion)

    correct_case = 0
    for vg in range(confusion.shape[0]):
        for i in range(confusion.shape[1]):
            correct_case += confusion[vg][i][i]
    accuracy[lr_idx] = correct_case/confusion.sum()

io.savemat('confusion_'+ net_name +'.mat', {'weight': gap_weight.cpu().detach().numpy()})
io.savemat('confusion_'+ net_name +'.mat', {'conf': confusion})

#%%
io.savemat('confusion.mat', {'weight': gap_weight.cpu().detach().numpy()})
io.savemat('confusion.mat', {'conf': confusion})
#%%
print(confusion)
print(accuracy)
torch.cuda.empty_cache()
# %%
