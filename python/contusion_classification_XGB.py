import numpy as np
import pandas as pd
import pymrmr
import mrmr
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
from sklearn.metrics import ConfusionMatrixDisplay
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import make_pipeline
import shap
from xgboost.sklearn import XGBClassifier
from sklearn import decomposition

def my_mrmr(data_path, label_path, reduced_dimension):
    #  run mrmr by package mrmr
    data = pd.read_excel(data_path)
    label = pd.read_excel(label_path)
    selected_features = mrmr.mrmr_classif(X=data, y=label, K=reduced_dimension)
    return selected_features

    # Run mrmr by package pymrmr
    # Overflow problem happens may be caused by the algorithm based on C++, and the performance is less accurate
    # data = pd.read_excel('D:\DaMing\contusion_classification\data2.xlsx')
    # print(data)
    # selected_features = pymrmr.mRMR(data, 'MIQ', 10)
    return

is_run_mrmr = True
is_run_pca = False
reduced_dimension = 50

data_path = rf'D:\DaMing\contusion_classification\data.xlsx'
label_path = rf'D:\DaMing\contusion_classification\label.xlsx'
data = pd.read_excel(data_path)
label = pd.read_excel(label_path).squeeze().to_numpy()
max_feature_num = len(data.columns)

dim_range = np.arange(5, max_feature_num, 5)
dim_range = np.append(dim_range, [max_feature_num])
train_acc = np.zeros(dim_range.shape[0])
val_acc = np.zeros(dim_range.shape[0])

for i in range(0, dim_range.shape[0]):
    reduced_dimension = dim_range[i]
    if is_run_mrmr == True:
        sf  = mrmr.mrmr_classif(X=data, y=label, K=reduced_dimension)
        reduced_data = data[sf]
    else:
        reduced_data = data

    x_feature_names = reduced_data.columns
    reduced_data = reduced_data.to_numpy()

    if is_run_pca == True:
        pca = decomposition.PCA(n_components=reduced_dimension)
        pca.fit(reduced_data)
        reduced_data = pca.transform(reduced_data)

    X_train, X_test, y_train, y_test = train_test_split(reduced_data, label, test_size=0.2, random_state=0)

    # 這邊有做正規化，沒有做結果會差很多
    clf = make_pipeline(StandardScaler(), XGBClassifier())
    clf.fit(X_train,y_train)
    train_acc[i] = clf.score(X_train,y_train)
    val_acc[i] = clf.score(X_test, y_test)

    # y_predict = clf.predict(X_test)

    # class_names = ['normal', 'destruction', 'repair', 'remodeling']
    # titles_options = [
    #     ("Confusion matrix, without normalization", None),
    #     ("Normalized confusion matrix", "true"),
    # ]
    # for title, normalize in titles_options:
    #     disp = ConfusionMatrixDisplay.from_estimator(
    #         clf,
    #         X_test,
    #         y_test,
    #         display_labels=class_names,
    #         cmap=plt.cm.Blues,
    #         normalize=normalize,
    #     )
    #     disp.ax_.set_title(title)

    #     print(title)
    #     print(disp.confusion_matrix)
    
    
# explainer = shap.KernelExplainer(model=clf.predict_proba, data=X_test, link='logit')
# shap_values = explainer.shap_values(X=X_test)
# # shap_values = explainer(X=X_test, nsamples=300)

# # This line can only run on jupyter and show visual picture of shapley value
# shap.summary_plot(shap_values[0], X_test, max_display=10, class_names= class_names, feature_names = x_feature_names)
