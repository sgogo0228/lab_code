# import package
import numpy as np
import myLib
import os, glob
from torch.utils.data import random_split
import importlib
import myLib
import shutil
import os
importlib.reload(myLib)

# 篩選5次的push sw相似度較高的data，並隨機取出training&validation set
# 將所有的data計算5次push sw的correlation coefficient，只取cc>0.6的data分成training與validation
training_ratio=0.8
path = r'D:\DaMing\sw_data\all_data'
target_path = r'D:\DaMing\sw_data\train_data_with_IQ'
label = os.listdir(rf'{path}\label')
data_num = len(label)
cc = np.zeros((data_num, 1000))
for i in range(0, data_num):
    sample = np.load(rf'{path}\label\{label[i]}')
    sample = np.reshape(sample[1000:2000], (1000, 5, 184))
    for j in range(0, 1000):
        cc[i, j] = (np.sum((np.corrcoef(sample[j, :, :])))-5)/20
cc_over_thres = np.where(cc.mean(axis=1)>0.6)[0]

# 將所有data分為training&validation
data = os.listdir(rf'{path}\data')
I = os.listdir(rf'{path}\I')
Q = os.listdir(rf'{path}\Q')
training_num = int(cc_over_thres.shape[0]*training_ratio)
[train_idx, val_idx] = random_split(range(cc_over_thres.shape[0]), [training_num, cc_over_thres.shape[0]-training_num])
for i in train_idx:
    shutil.copy(rf'{path}\data\{data[cc_over_thres[i]]}', 
    rf'{target_path}\training\data\{data[cc_over_thres[i]]}')
    shutil.copy(rf'{path}\I\{I[cc_over_thres[i]]}', 
    rf'{target_path}\training\I\{I[cc_over_thres[i]]}')
    shutil.copy(rf'{path}\Q\{Q[cc_over_thres[i]]}', 
    rf'{target_path}\training\Q\{Q[cc_over_thres[i]]}')
    shutil.copy(rf'{path}\label\{label[cc_over_thres[i]]}', 
    rf'{target_path}\training\label\{label[cc_over_thres[i]]}')
for i in val_idx:
    shutil.copy(rf'{path}\data\{data[cc_over_thres[i]]}', 
    rf'{target_path}\validation\data\{data[cc_over_thres[i]]}')
    shutil.copy(rf'{path}\I\{I[cc_over_thres[i]]}', 
    rf'{target_path}\validation\I\{I[cc_over_thres[i]]}')
    shutil.copy(rf'{path}\Q\{Q[cc_over_thres[i]]}', 
    rf'{target_path}\validation\Q\{Q[cc_over_thres[i]]}')
    shutil.copy(rf'{path}\label\{label[cc_over_thres[i]]}', 
    rf'{target_path}\validation\label\{label[cc_over_thres[i]]}')
