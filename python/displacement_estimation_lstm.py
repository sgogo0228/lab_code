#%%
from functools import partial
import numpy as np
import os, glob
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torch.utils.data as dataclass
import torchvision
import torchvision.transforms as transforms
from torchvision import models
from ray import tune
from ray.tune import CLIReporter
from ray.tune.schedulers import ASHAScheduler
# from yaml import MarkedYAMLError
from tqdm import tqdm
import warnings
import myLib
import custom_module
import time
import gc
from torchsummary import summary
import matplotlib.pyplot as plt
import importlib
importlib.reload(myLib)
importlib.reload(custom_module)

warnings.filterwarnings('ignore')
gc.collect()
torch.cuda.empty_cache()

num_samples=1
max_num_epochs=100
config = {
    "lr": tune.grid_search([1e-4]),
    "batch_size": tune.grid_search([2])
}

class Displacement_dataset(dataclass.Dataset):
    def __init__(self, root, transform):
        self.transform = transform
        self.data = os.listdir(f'{root}/data')
        self.data = [f'{root}/data/{i}' for i in self.data]
        self.label = os.listdir(f'{root}/label')
        self.label = [f'{root}/label/{i}' for i in self.label]
        assert len(self.data) == len(self.label), 'data and label numbers are different.'

    def __getitem__(self, index):
        data  = np.load(self.data[index]).astype(np.float32)
        label = np.load(self.label[index]).astype(np.float32)
        data = self.transform(data)
        label = self.transform(label)
        return data, label
    
    def __len__(self):
        return len(self.data)

def load_data(data_dir="./"):
    train_transform = transforms.Compose([
        transforms.ToTensor(),
        # transforms.Resize([1024, 920]),
        # transforms.Normalize(-5, 0.5),
    ])
    val_transform = transforms.Compose([
        transforms.ToTensor(),
        # transforms.Resize([1024, 920]),
        # transforms.Normalize(-5, 0.5),
    ])

    train_dir = data_dir + r'\training'
    train_datasets = Displacement_dataset(train_dir, transform=train_transform)

    val_dir = data_dir + r'\validation'
    val_datasets = Displacement_dataset(val_dir, transform=val_transform)

    return train_datasets, val_datasets

def train_cifar(config, checkpoint_dir=None, data_dir=None, max_epoch=200):
    # confusion = np.zeros((4, 4))
    # fold_loss = 0.0
    # learning_rate = config["lr"]
    # batch_size = config["batch_size"]
    # input_feature_dim = 1000
    # hidden_feature_dim = 1000
    # hidden_layer_num = 1
    encode_input_channel = 1000
    encode_output_channel = 1000
    encode_layer_num = 2
    decode_input_channel = 1000*2
    decode_output_channel = 1000
    decode_layer_num = 2
    learning_rate = 1e-4
    batch_size = 30
    time_steps= 920
    is_run_test = False
    val_epoch_interval = 5
    is_corr_fun_calc = True

    file_copy = 1
    file_name_prefix = (rf'D:\DaMing\sw_data\model_output' + 
    rf'\gru_{encode_input_channel}_{encode_layer_num}layer_bs{batch_size}_epoch{max_epoch}')
    while os.path.exists(rf'{file_name_prefix}_{file_copy}.pt')==True :
        file_copy += 1
    file_name = rf'{file_name_prefix}_{file_copy}'
    # net = custom_module.myNet(kernel=kernel)
    # net.apply(myLib.init_weights)

    # if torch.cuda.is_available():
    #     net.cuda()
    # device = "cpu"
    # if torch.cuda.is_available():
    #     device = "cuda:0"
    #     if torch.cuda.device_count() > 1:
    #         net = nn.DataParallel(net)
    # net.to(device)

    # criterion = custom_module.custom_loss(weight_g=0.5, weight_b=0.5, pow = losspow)
    # val_criterion = nn.MSELoss()
    # optimizer = optim.Adam(net.parameters(), lr=learning_rate*batch_size/2)
    # optimizer = optim.SGD(net.parameters(), lr=learning_rate*batch_size/2, momentum=0.9)

    trainset, valset = load_data(data_dir=data_dir)
    train_dataloader = torch.utils.data.DataLoader(trainset, 
        batch_size=batch_size, 
        shuffle=True,
        num_workers=0)
    
    val_dataloader = torch.utils.data.DataLoader(valset, 
        batch_size=batch_size, 
        shuffle=True,
        num_workers=0)
    
    # lstm=custom_module.myLSTM(input_feature_dim*2,hidden_feature_dim,hidden_layer_num)
    model=custom_module.myGRU_encode_decode_bi(encode_input_channel, encode_output_channel, encode_layer_num, decode_input_channel, decode_output_channel, decode_layer_num)
    if torch.cuda.is_available():
        model.cuda()
    device = "cpu"
    if torch.cuda.is_available():
        device = "cuda:0"
        if torch.cuda.device_count() > 1:
            model = nn.DataParallel(model)
    model.to(device)
    
    #建構LSTM物件
    optimizer=optim.Adam(model.parameters(),lr=learning_rate)
    #宣告反向傳播機制
    loss_g = custom_module.gradient_loss(dim=1)
    loss_ml = custom_module.mean_loss(dim=(0, 1))
    loss_xcorr = custom_module.xcorr_loss(dim=1)
    loss_val = nn.MSELoss()
    train_loss = np.zeros((max_epoch))
    train_mse = np.zeros((max_epoch))
    train_xcorr = np.zeros((max_epoch))
    train_ml = np.zeros((max_epoch))
    val_loss = np.zeros((max_epoch))
    val_mse = np.zeros((max_epoch))
    val_xcorr = np.zeros((max_epoch))
    val_mse = np.zeros((max_epoch))
    val_ml = np.zeros((max_epoch))
    training_time = np.zeros((max_epoch))
    val_time = np.zeros((max_epoch))
    best_loss = 50000
    best_net = model
    input_feature_dim = encode_input_channel
    
    #宣告損失函數
    start_time = time.time()
    print(time.ctime())
    for epoch in tqdm(range(max_epoch)):#每一輪
        running_loss = 0.
        running_mse = 0.
        running_xcorr = 0.
        running_ml = 0.
        t = time.time()
        for inputs, labels in train_dataloader: #數據集內的每個數據和標籤
            inputs = inputs.cuda()
            labels = labels.cuda()
            input = inputs.reshape(-1, input_feature_dim, time_steps)
            label = labels.reshape(-1, input_feature_dim, time_steps)
            local_bz = input.size()[0]
            output = torch.zeros(local_bz, time_steps, input_feature_dim)
            
            if is_corr_fun_calc:
                xcorr_func = torch.zeros(local_bz, time_steps, input_feature_dim)
                # 需更改model的in_feature_dim
                for bz in range(0, local_bz):
                    temp1 = input[bz:bz+1, :, :].permute((0, 2, 1))
                    temp2 = input[bz:bz+1, :, 0:1].repeat((1, 1, time_steps)).permute((2, 0, 1))
                    xcorr_func[bz, :, :] = nn.functional.conv1d(temp1, temp2, groups=time_steps, padding='same')[0, :, :]
                    xcorr_func[bz, :, :] = (xcorr_func[bz, :, :]-xcorr_func[bz, :, :].min())/(xcorr_func[bz, :, :].max()-xcorr_func[bz, :, :].min())
            else:
                xcorr_func = torch.zeros(local_bz, time_steps, 0)
            
            label_sample = label.permute((0, 2, 1))
            input_sample = input.permute((0, 2, 1))
            # input_sample = torch.cat((input_sample, xcorr_func), dim=2)

            xcorr_func = xcorr_func.cuda()
            temp,hn=model(input_sample, xcorr_func) #正向傳播
            output = temp
            # for seq in range(0, time_steps-3):
            # #(sequence num, batch, feature)
            #     temp,(hn,cn)=lstm(input_sample[:, seq:seq+3, :]) #正向傳播
            #     output[:, seq+3, :] = temp

            model.zero_grad() #清除lstm的上個數據的偏微分暫存值，否則會一直累加
            xcorr_loss = loss_xcorr(output,label_sample) #計算loss
            mse_loss = loss_val(output,label_sample)
            ml_loss = loss_ml(output,label_sample)
            loss = 0.4*mse_loss + 0.6*xcorr_loss
            loss.backward() #從loss計算反向傳播
            optimizer.step() #更新所有權種和偏差
            running_loss += loss.item()
            running_mse += mse_loss.item()
            running_xcorr += xcorr_loss.item()
            running_ml += ml_loss.item()
            # _, preds = torch.max(output, 1) #從output中取最大的出來作為預測值
            # if preds==label: #如果預測值和標籤一致
            #     correct+=1

        train_loss[epoch] = float(running_loss / len(train_dataloader))
        train_mse[epoch] = float(running_mse / len(train_dataloader))
        train_xcorr[epoch] = float(running_xcorr / len(train_dataloader))
        train_ml[epoch] = float(running_ml / len(train_dataloader))
        
        print(f"epoch: {epoch+1}, Train loss: {train_loss[epoch]}, train mse: {train_mse[epoch]}, train xcorr: {train_xcorr[epoch]}, train ml: {train_ml[epoch]}")
        training_time[epoch] = time.time()-t
        print(time.ctime())

        if is_run_test==False and ((epoch == 0) or (epoch % val_epoch_interval == val_epoch_interval-1)):
            t = time.time()
            model.eval()
            with torch.no_grad():
                running_loss = 0.
                running_mse = 0.
                running_xcorr = 0.
                running_ml = 0.
                
                for inputs, labels in val_dataloader:
                    inputs = inputs.cuda()
                    labels = labels.cuda()
                    input = inputs.reshape(-1, input_feature_dim, time_steps)
                    label = labels.reshape(-1, input_feature_dim, time_steps)
                    local_bz = input.size()[0]
                    output = torch.zeros(local_bz, time_steps, input_feature_dim)
                    xcorr_func = torch.zeros(local_bz, time_steps, input_feature_dim)

                    for bz in range(0, local_bz):
                        temp1 = input[bz:bz+1, :, :].permute((0, 2, 1))
                        temp2 = input[bz:bz+1, :, 0:1].repeat((1, 1, time_steps)).permute((2, 0, 1))
                        xcorr_func[bz, :, :] = nn.functional.conv1d(temp1, temp2, groups=time_steps, padding='same')[0, :, :]
                        xcorr_func[bz, :, :] = (xcorr_func[bz, :, :]-xcorr_func[bz, :, :].min())/(xcorr_func[bz, :, :].max()-xcorr_func[bz, :, :].min())
                    
                    label_sample = label.permute((0, 2, 1))
                    input_sample = input.permute((0, 2, 1))
                    # input_sample = torch.cat((input_sample, xcorr_func), dim=2)

                    xcorr_func = xcorr_func.cuda()
                    temp,hn=model(input_sample, xcorr_func) #正向傳播
                    output = temp
                    # for seq in range(0, time_steps-3):
                    # #(sequence num, batch, feature)
                    #     temp,(hn,cn)=lstm(input_sample[:, seq:seq+3, :]) #正向傳播
                    #     output[:, seq+3, :] = temp
                    xcorr_loss = loss_xcorr(output,label_sample) #計算loss
                    mse_loss = loss_val(output,label_sample)
                    ml_loss = loss_ml(output,label_sample)
                    loss = 0.4*mse_loss + 0.6*xcorr_loss
                    running_loss += loss.item()
                    running_mse += mse_loss.item()
                    running_xcorr += xcorr_loss.item()
                    running_ml += ml_loss.item()

                val_loss[epoch] = float(running_loss / len(val_dataloader))
                val_mse[epoch] = float(running_mse / len(val_dataloader))
                val_xcorr[epoch] = float(running_xcorr / len(val_dataloader))
                val_ml[epoch] = float(running_ml / len(val_dataloader))
                # val_mse_without_push[epoch] = float(val_acc_without_push / len(val_dataloader))
                # torch.save(net.state_dict(), rf'C:\Users\Buslab\Desktop\Experiment data\DaMing\contusion\displacement_estimation\test_epoch{epoch}.pt')

                if val_mse[epoch] < best_loss:
                    best_loss = val_mse[epoch]
                    best_net = model

            print(f'Validation Loss:{val_loss[epoch]}, Validation mse:{val_mse[epoch]}, Validation xcorr:{val_xcorr[epoch]}, validation ,ml: {val_ml[epoch]}')
            # print(f"Validation Loss without Push: {val_mse_without_push[epoch]}")
            # Acc = float(val_acc / (len(valset)))
            # print("Validation Accuracy:%.9f; correct number:%d; total: %d" % (Acc, val_acc, len(valset)))
            model.train()
            val_time[epoch] = time.time()-t
    
    myLib.output_data({'train_loss': train_loss, 'train_mse': train_mse, 'train_xcorr': train_xcorr,
    'val_loss': val_loss, 'val_mse': val_mse, 'val_xcorr': val_xcorr,
    'training_time': training_time, 'val_time': val_time}, file_name=file_name)
    torch.save(best_net.state_dict(), f'{file_name}.pt')

data_dir = os.path.abspath(r"D:\DaMing\sw_data\demod_adjust")
train_cifar(config=config, data_dir=data_dir)

