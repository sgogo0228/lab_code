#%%
from functools import partial
import numpy as np
import os
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.utils.data import random_split
import torchvision
import torchvision.transforms as transforms
from torchvision import models
from ray import tune
from ray.tune import CLIReporter
from ray.tune.schedulers import ASHAScheduler
from yaml import MarkedYAMLError

num_samples=40
max_num_epochs=30
config = {
    # "lr": tune.loguniform(1e-4, 1e-2),
    "lr": tune.choice([0.001, 0.0001, 0.00001, 0.000001, 0.0000001]),
    "batch_size": tune.choice([4, 8, 16])
}

class myNet(nn.Module):
    def __init__(self, num_classes=4):
        super(myNet, self).__init__()
        net = models.vgg11(pretrained=True)
        # for vgg net
        self.features = nn.Sequential(*list(net.features.children())[:-1])
        self.classifier = nn.Linear(512, num_classes)

        # # for resnet
        # self.features = nn.Sequential(net.conv1, net.bn1, net.relu, net.maxpool, net.layer1, net.layer2, net.layer3, net.layer4)
        # self.classifier = nn.Linear(2048, num_classes)

        # for inceptionV3
        # self.features = nn.Sequential(net.Conv2d_1a_3x3, 
        #                                 net.Conv2d_2a_3x3,
        #                                 net.Conv2d_2b_3x3,
        #                                 net.maxpool1, 
        #                                 net.Conv2d_3b_1x1,
        #                                 net.Conv2d_4a_3x3,
        #                                 net.maxpool2,
        #                                 net.Mixed_5b,
        #                                 net.Mixed_5c,
        #                                 net.Mixed_5d,
        #                                 net.Mixed_6a,
        #                                 net.Mixed_6b,
        #                                 net.Mixed_6c,
        #                                 net.Mixed_6d,
        #                                 net.Mixed_6e)
        # self.classifier = nn.Linear(768, num_classes)

        # # for googlenet
        # self.features = nn.Sequential(net.conv1, 
        #                                 net.maxpool1,
        #                                 net.conv2,
        #                                 net.conv3, 
        #                                 net.maxpool2,
        #                                 net.inception3a,
        #                                 net.inception3b,
        #                                 net.maxpool3,
        #                                 net.inception4a,
        #                                 net.inception4b,
        #                                 net.inception4c,
        #                                 net.inception4d,
        #                                 net.inception4e,
        #                                 net.maxpool4,
        #                                 net.inception5a,
        #                                 net.inception5b)

        # self.classifier = nn.Linear(1024, num_classes)

    def forward(self, x):
        x = self.features(x)
        x = x.mean([2, 3])
        # x = x.view(x.size()[0], 512, 4*16).mean(2)
        x = self.classifier(x)
        return x

def load_data(data_dir="./5-fold validation", fold_iter=0):
    train_transform = transforms.Compose([
        transforms.RandomHorizontalFlip(),
        transforms.ToTensor(),
    ])

    val_transform = transforms.Compose([
        transforms.ToTensor(),
    ])

    train_dir = data_dir + '/' + str(fold_iter+1) + '/training'
    train_datasets = torchvision.datasets.ImageFolder(train_dir, transform=train_transform)

    val_dir = data_dir + '/' + str(fold_iter+1) + '/testing'
    val_datasets = torchvision.datasets.ImageFolder(val_dir, transform=val_transform)
    return train_datasets, val_datasets


def train_cifar(config, checkpoint_dir=None, data_dir=None, max_epoch=30):
    confusion = np.zeros((4, 4))
    fold_loss = 0.0
    for fold_iter in range(5):  # loop over the dataset multiple times
        net = myNet(num_classes = 4)
        # if torch.cuda.is_available():
        #     net.cuda()
        device = "cpu"
        if torch.cuda.is_available():
            device = "cuda:0"
            if torch.cuda.device_count() > 1:
                net = nn.DataParallel(net)
        net.to(device)
        criterion = nn.CrossEntropyLoss()
        optimizer = optim.Adam(net.parameters(), lr=config["lr"])

        trainset, valset = load_data(data_dir=data_dir, fold_iter=fold_iter)
        train_dataloader = torch.utils.data.DataLoader(trainset, 
            batch_size=config["batch_size"], 
            shuffle=True,
            num_workers=2)
        
        val_dataloader = torch.utils.data.DataLoader(valset, 
            batch_size=config["batch_size"], 
            shuffle=True,
            num_workers=0)

        for epoch in range(max_epoch):
            running_loss = 0.0
            running_acc = 0.0
            epoch_steps = 0
            for img, label in train_dataloader:
                inputs = img.cuda()
                labels = label.cuda()

                # zero the parameter gradients
                optimizer.zero_grad()

                # forward + backward + optimize
                outputs = net(inputs)
                loss = criterion(outputs, labels)

                pred = torch.max(outputs, 1)[1]
                train_correct = (pred == labels).sum()

                loss.backward()
                optimizer.step()

                # print statistics
                running_acc += train_correct.item()
                running_loss += loss.item()
            
            Loss = float(running_acc / (len(trainset)))
            Acc = float(running_acc / (len(trainset)))
            print("fold_number: %d; epoch: %d" % (fold_iter+1, epoch+1))
            print("Train Loss:%.9f" % Loss)
            print("Train Accuracy:%.9f" % Acc)

        net.eval()
        # Validation loss
        val_loss = 0.0
        val_acc = 0.0
        for img, label in val_dataloader:
            inputs = img.cuda()
            labels = label.cuda()
            outputs = net(inputs)
            loss = criterion(outputs, labels)
            val_loss += loss.item()
            pred = torch.max(outputs, 1)[1]
            val_correct = (pred == labels).sum()
            val_acc += val_correct.item()

            for idx in range(pred.shape[0]):
                confusion[label[idx], pred[idx]] = confusion[label[idx], pred[idx]] + 1

        with tune.checkpoint_dir(fold_iter) as checkpoint_dir:
            path = os.path.join(checkpoint_dir, "checkpoint")
            torch.save((net.state_dict(), optimizer.state_dict()), path)

        Loss = float(val_loss / (len(valset)))
        Acc = float(val_acc / (len(valset)))
        print("Validation Loss:%.9f" % Loss)
        print("Validation Accuracy:%.9f; correct number:%d; total: %d" % (Acc, val_acc, len(valset)))

        fold_loss = fold_loss + Loss/5
        # fold_acc = (confusion[0][0] + confusion[1][1] + confusion[2][2] + confusion[3][3])/confusion.sum()
        # tune.report(loss=Loss, accuracy=fold_acc, confusion=confusion)

    fold_acc = (confusion[0][0] + confusion[1][1] + confusion[2][2] + confusion[3][3])/confusion.sum()
    tune.report(loss=fold_loss, accuracy=round(fold_acc * 100, 2), confusion=confusion)
    print("Finished")
    
    torch.cuda.empty_cache()

# def test_accuracy(net, device="cpu"):
#     trainset, testset = load_data()

#     testloader = torch.utils.data.DataLoader(
#         testset, batch_size=4, shuffle=False, num_workers=2)

#     correct = 0
#     total = 0
#     with torch.no_grad():
#         for data in testloader:
#             images, labels = data
#             images, labels = images.to(device), labels.to(device)
#             outputs = net(images)
#             _, predicted = torch.max(outputs.data, 1)
#             total += labels.size(0)
#             correct += (predicted == labels).sum().item()

#     return correct / total

# def main(num_samples=10, max_num_epochs=30, gpus_per_trial=1):

gpus_per_trial=1
data_dir = os.path.abspath("./sw_data/5-fold validation")

scheduler = ASHAScheduler(
    metric="loss",
    mode="min",
    max_t=max_num_epochs,
    grace_period=1,
    reduction_factor=2)
reporter = CLIReporter(
    # parameter_columns=["l1", "l2", "lr", "batch_size"],
    metric_columns=["loss", "accuracy", "training_iteration"])
result = tune.run(
    partial(train_cifar, data_dir=data_dir, max_epoch=max_num_epochs),
    resources_per_trial={"cpu": 8, "gpu": gpus_per_trial},
    config=config,
    num_samples=num_samples,
    scheduler=scheduler,
    progress_reporter=reporter)

best_trial = result.get_best_trial("loss", "min", "last")
print("Best trial config: {}".format(best_trial.config))
print("Best trial final validation loss: {}".format(
    best_trial.last_result["loss"]))
print("Best trial final validation accuracy: {}".format(
    best_trial.last_result["accuracy"]))

    # best_trained_model = Net(best_trial.config["l1"], best_trial.config["l2"])
    # device = "cpu"
    # if torch.cuda.is_available():
    #     device = "cuda:0"
    #     if gpus_per_trial > 1:
    #         best_trained_model = nn.DataParallel(best_trained_model)
    # best_trained_model.to(device)

    # best_checkpoint_dir = best_trial.checkpoint.value
    # model_state, optimizer_state = torch.load(os.path.join(
    #     best_checkpoint_dir, "checkpoint"))
    # best_trained_model.load_state_dict(model_state)

    # test_acc = test_accuracy(best_trained_model, device)
    # print("Best trial test set accuracy: {}".format(test_acc))


# if __name__ == "__main__":
#     # You can change the number of GPUs per trial here:
#     main(num_samples=1, max_num_epochs=30, gpus_per_trial=1)


# %%
# best_checkpoint_dir = best_trial.checkpoint.value
# print(best_checkpoint_dir)
# model_state, optimizer_state = torch.load(os.path.join(
#     best_checkpoint_dir, "checkpoint"))
# best_trained_model = myNet()
# best_trained_model.load_state_dict(model_state)

# trainset, testset = load_data(data_dir=data_dir, fold_iter=4)

# testloader = torch.utils.data.DataLoader(
# testset, batch_size=8, shuffle=True)
# correct = 0
# total = 0
# #%%
# # with torch.no_grad():
# best_trained_model.eval()
# for img, label in testloader:
#     images = img
#     labels = label
#     outputs = best_trained_model(images)
#     _, predicted = torch.max(outputs.data, 1)
#     total += labels.size(0)
#     correct += (predicted == labels).sum().item()
#     print(predicted)
#     print(labels)
    
# print("%d, %d" % (correct, total))
# # print("Best trial test set accuracy: {}".format(test_acc))
# # %%
# import torch
# torch.cuda.empty_cache()
# print(torch.cuda.memory_allocated())
# print(torch.cuda.memory_reserved())
# print(torch.cuda.max_memory_reserved())
# %%
