#%%
from functools import partial
import numpy as np
import os, glob
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torch.utils.data as dataclass
import torchvision
import torchvision.transforms as transforms
from torchvision import models
from ray import tune
from ray.tune import CLIReporter
from ray.tune.schedulers import ASHAScheduler
from yaml import MarkedYAMLError
from tqdm import tqdm
import warnings
import myLib
import custom_module
import gc
from torchsummary import summary
import matplotlib.pyplot as plt
import time
import importlib
importlib.reload(myLib)
importlib.reload(custom_module)

warnings.filterwarnings('ignore')
gc.collect()
torch.cuda.empty_cache()

num_samples=1
max_num_epochs=50
config = {
    "lr": tune.grid_search([1e-4]),
    "batch_size": tune.grid_search([2])
}

class Displacement_dataset(dataclass.Dataset):
    def __init__(self, root, transform):
        self.transform = transform
        self.data = os.listdir(f'{root}/data')
        self.data = [f'{root}/data/{i}' for i in self.data]
        self.label = os.listdir(f'{root}/label')
        self.label = [f'{root}/label/{i}' for i in self.label]
        assert len(self.data) == len(self.label), 'data and label numbers are different.'

    def __getitem__(self, index):
        data  = np.load(self.data[index]).astype(np.float32)
        label = np.load(self.label[index]).astype(np.float32)
        data = self.transform(data)
        label = self.transform(label)
        return data, label
    
    def __len__(self):
        return len(self.data)

def load_data(data_dir="./"):
    train_transform = transforms.Compose([
        transforms.ToTensor(),
        # transforms.Normalize(-5, 0.5),
    ])
    val_transform = transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize(-1, 0.5),
    ])

    train_dir = data_dir + '/training'
    train_datasets = Displacement_dataset(train_dir, transform=train_transform)

    val_dir = data_dir + '/validation'
    val_datasets = Displacement_dataset(val_dir, transform=val_transform)

    return train_datasets, val_datasets

def train_cifar(config, checkpoint_dir=None, data_dir=None, max_epoch=50):
    # confusion = np.zeros((4, 4))
    # fold_loss = 0.0
    # learning_rate = config["lr"]
    # batch_size = config["batch_size"]
    learning_rate = 1e-4
    batch_size = 4
    kernel_height = 7
    losspow = 0.7
    val_epoch_interval = 10
    is_run_test = False
    
    model = custom_module.test_net(kernel=kernel_height)
    # net.apply(myLib.init_weights)

    if torch.cuda.is_available():
        model.cuda()
    device = "cpu"
    if torch.cuda.is_available():
        device = "cuda:0"
        if torch.cuda.device_count() > 1:
            model = nn.DataParallel(model)
    model.to(device)
    
    optimizer = optim.Adam(model.parameters(), lr=learning_rate)
    # optimizer = optim.Adam(model.parameters(), lr=learning_rate*batch_size/2)
    # optimizer = optim.SGD(model.parameters(), lr=learning_rate*batch_size/2, momentum=0.9)
    loss_g = custom_module.gradient_loss(dim=3)
    loss_xcorr_func = custom_module.xcorr_loss(dim=3)
    loss_mse_func = nn.MSELoss()
    train_loss = np.zeros((max_epoch))
    train_mse = np.zeros((max_epoch))
    train_xcorr = np.zeros((max_epoch))
    val_loss = np.zeros((max_epoch))
    val_mse = np.zeros((max_epoch))
    val_xcorr = np.zeros((max_epoch))
    val_mse = np.zeros((max_epoch))
    training_time = np.zeros((max_epoch))
    val_time = np.zeros((max_epoch))
    best_loss = 50000
    best_net = model

    trainset, valset = load_data(data_dir=data_dir)
    train_dataloader = torch.utils.data.DataLoader(trainset, 
        batch_size=batch_size, 
        shuffle=True,
        num_workers=0)
    
    val_dataloader = torch.utils.data.DataLoader(valset, 
        batch_size=batch_size, 
        shuffle=True,
        num_workers=0)

    start_time = time.time()
    print(time.ctime())
    for epoch in tqdm(range(max_epoch)):
        running_loss = 0.
        running_mse = 0.
        running_xcorr = 0.
        t = time.time()
        # torch.save(net.state_dict(), rf'C:\Users\Buslab\Desktop\Experiment data\DaMing\contusion\displacement_estimation\train_epoch{epoch}.pt')
        for img, label in train_dataloader:

            inputs = img.cuda()
            labels = label.cuda()
            # zero the parameter gradients
            optimizer.zero_grad()

            # forward + backward + optimize
            outputs = model(inputs)
            sample_xcorr = loss_xcorr_func(outputs, labels)
            sample_mse = loss_mse_func(outputs, labels)
            loss = 0.4*sample_xcorr + 0.6*sample_mse
            # record training loss
            running_loss += loss.item()
            running_mse += sample_mse.item()
            running_xcorr += sample_xcorr.item()
            # _, preds = torch.max(output, 1) #從
            loss.backward()
            optimizer.step()

        print(time.ctime())
        train_loss[epoch] = float(running_loss / len(train_dataloader))
        train_mse[epoch] = float(running_mse / len(train_dataloader))
        train_xcorr[epoch] = float(running_xcorr / len(train_dataloader))
        print(f"epoch: {epoch+1}, Train loss: {train_loss[epoch]}, train mse: {train_mse[epoch]}, train xcorr: {train_xcorr[epoch]}")
        training_time[epoch] = time.time()-t

        # Validation loss
        if is_run_test==False and ((epoch == 0) or (epoch % val_epoch_interval == val_epoch_interval-1)):
            t = time.time()
            model.eval()
            with torch.no_grad():
                running_loss = 0.
                running_mse = 0.
                running_xcorr = 0.
                
                for img, label in val_dataloader:
                    inputs = img.cuda()
                    labels = label.cuda()
                    outputs = model(inputs)
                    
                    sample_xcorr = loss_xcorr_func(outputs,labels) #計算loss
                    sample_mse = loss_mse_func(outputs,labels)
                    loss = 0.4*sample_xcorr + 0.6*sample_mse
                    running_loss += loss.item()
                    running_xcorr += sample_xcorr.item()
                    running_mse += sample_mse.item()

                val_time[epoch] = time.time()-t
                val_loss[epoch] = float(running_loss / len(val_dataloader))
                val_mse[epoch] = float(running_mse / len(val_dataloader))
                val_xcorr[epoch] = float(running_xcorr / len(val_dataloader))
                # val_mse_without_push[epoch] = float(val_acc_without_push / len(val_dataloader))
                # torch.save(net.state_dict(), rf'C:\Users\Buslab\Desktop\Experiment data\DaMing\contusion\displacement_estimation\test_epoch{epoch}.pt')

                if val_mse[epoch] < best_loss:
                    best_loss = val_mse[epoch]
                    best_net = model

            print(f'Validation Loss:{val_loss[epoch]}, Validation mse:{val_mse[epoch]}, Validation xcorr:{val_xcorr[epoch]}')
            # print(f"Validation Loss without Push: {val_mse_without_push[epoch]}")
            # Acc = float(val_acc / (len(valset)))
            # print("Validation Accuracy:%.9f; correct number:%d; total: %d" % (Acc, val_acc, len(valset)))
            model.train()
    print("Finished")

    # with tune.checkpoint_dir(0) as checkpoint_dir:
    #     path = os.path.join(checkpoint_dir, "checkpoint")
    #     torch.save((net.state_dict(), optimizer.state_dict()), path)
    
    file_copy = 1
    file_name = (rf'D:\DaMing\sw_data\model_output' + 
    rf'\model_2layer_kernel{kernel_height}_linearscale_{losspow}powloss_lr{learning_rate}_bs{batch_size}_{file_copy}')
    while os.path.exists(rf'{file_name}.pt')==True :
        file_copy += 1
        file_name = (rf'D:\DaMing\sw_data\model_output' + 
        rf'\model_2layer_kernel{kernel_height}_linearscale_{losspow}powloss_lr{learning_rate}_bs{batch_size}_{file_copy}')

    myLib.output_data({'train_loss': train_loss, 'train_mse': train_mse, 'train_xcorr': train_xcorr,
    'val_loss': val_loss, 'val_mse': val_mse, 'val_xcorr': val_xcorr,
    'training_time': training_time, 'val_time': val_time}, file_name=file_name)
    torch.save(best_net.state_dict(), f'{file_name}.pt')
    # tune.report(loss=val_mse[-1])

gpus_per_trial=1
data_dir = os.path.abspath(r"D:\DaMing\sw_data\demod_adjust")
train_cifar(config=config, data_dir=data_dir)


# scheduler = ASHAScheduler(
#     metric="loss",
#     mode="min",
#     max_t=max_num_epochs,
#     grace_period=1,
#     reduction_factor=2)
# reporter = CLIReporter(
#     # parameter_columns=["l1", "l2", "lr", "batch_size"],
#     metric_columns=["loss", "accuracy", "training_iteration"])
# result = tune.run(
#     partial(train_cifar, data_dir=data_dir, max_epoch=max_num_epochs),
#     resources_per_trial={"cpu": 8, "gpu": gpus_per_trial},
#     config=config,
#     num_samples=num_samples,
#     verbose = 0,
#     scheduler=scheduler,
#     progress_reporter=reporter)

# best_trial = result.get_best_trial("loss", "min", "last")
# print("Best trial config: {}".format(best_trial.config))
# print("Best trial final validation loss: {}".format(
#     best_trial.last_result["loss"]))


# %%
# import torch
# import numpy as np
# import scipy.io as matlabio
# import matplotlib.pyplot as plt
# import importlib
# import glob
# import myLib
# import custom_module
# importlib.reload(myLib)

# torch.cuda.empty_cache()
# print(torch.cuda.memory_allocated())
# print(torch.cuda.memory_reserved())
# print(torch.cuda.max_memory_reserved())

# data_path = glob.glob(r'C:\Users\Buslab\Desktop\Experiment data\DaMing\contusion\displacement_estimation\validation\data\*.npy')
# label_path = glob.glob(r'C:\Users\Buslab\Desktop\Experiment data\DaMing\contusion\displacement_estimation\validation\label\*.npy')
# mse_val = np.zeros(len(data_path))
# mse_without_push = np.zeros(len(data_path))
# for i in range(len(data_path)):
#     data = np.load(data_path[i]).reshape((1, 1, 1000, 995)).astype(np.float32)
#     data = torch.tensor(data)
#     output = net(data).detach().numpy()
#     label = np.load(label_path[i])
#     mse_val[i] = ((output-label)**2).mean(axis=1).mean()
#     output = np.reshape(output, (1000, 5, 199))
#     label = np.reshape(label, (1000, 5, 199))
#     output = output[:, 1:4, 15:].reshape((1000, 184*3))
#     label = label[:, 1:4, 15:].reshape((1000, 184*3))
#     mse_without_push[i] = ((output-label)**2).mean(axis=1).mean()
    
#%%
# print(mse_val[401:601].mean())
# print(mse_without_push[401:601].mean())
