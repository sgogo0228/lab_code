import torch.nn as nn
import torch
import sys
import numpy as np
from torch.autograd import Variable
from spatial_correlation_sampler import spatial_correlation_sample
import convGRU
import convGRU_1d

# get position embedding (PE) table based on sin&cos function
# reference: 'Attention is all you need'
def get_sin_cos_PE(emb_len=1000, emb_dim=20):
    pos_emb = torch.zeros(emb_len, emb_dim, dtype=torch.float)
    vel = torch.arange(emb_dim)*0.5
    t_interval = torch.tensor(1/2/1e9) * (1000 / emb_len)
    t = torch.cat((torch.arange(emb_len / 2 - 1, -1, -1), torch.arange(emb_len / 2)))*t_interval
    freq = 50
    wave_len = 1/freq
    torch_pi = torch.acos(torch.zeros(1)).item()
    for i in range(emb_dim):
        pos_emb[:, i] = torch.cos(vel[i]*t/wave_len*2*torch_pi)
    
    return pos_emb

class Attention1(nn.Module):
    def __init__(self, enc_hidden_size, dec_hidden_size):
        super(Attention1, self).__init__()
        
        self.enc_hidden_size = enc_hidden_size
        self.dec_hidden_size = dec_hidden_size
        
        self.linear_in = nn.Linear(enc_hidden_size, dec_hidden_size)     #主要應該還是為了匹配encoder&decoder兩邊hidden output的長度
        self.linear_out = nn.Linear(enc_hidden_size+dec_hidden_size, dec_hidden_size)
    
    def forward(self, output, encoder_output, time_lag):
        # output: [batch_size, seq_len_y-1, dec_hidden_size]  这个output 是decoder的每个时间步输出的隐藏状态
        # encoder_output: [batch_size, seq_len_x, 2*enc_hidden_size]
        
        encoder_output = self.linear_in(encoder_output)  # [batch_size*seq_len_x,dec_hidden_size]
        context_in = encoder_output.transpose(1, 2)   # [batch_size, dec_hidden_size, seq_len_x]
        attn = torch.bmm(output, context_in)  # [batch_size, seq_len_y-1, seq_len_x]
        # 这个东西就是求得当前时间步的输出output和所有输入相似性关系的一个得分score , 下面就是通过softmax把这个得分转成权重
        attn = nn.functional.softmax(attn, dim=2)    # 此时第二维度的数字全都变成了0-1之间的数， 越大表示当前的输出output与哪个相关程度越大
        
        context = torch.bmm(attn, encoder_output)   # [batch_size, seq_len_y-1, enc_hidden_size]
        
        output = torch.cat((context, output), dim=2)  # [batch_size, seq_len_y-1, 2*enc_hidden_size+dec_hidden_size]
        output = torch.tanh(self.linear_out(output))     # [batch_size*seq_len_y-1, dec_hidden_size]
        
        return output, attn

class Attention1_3(nn.Module):
    def __init__(self, enc_hidden_size, dec_hidden_size):
        super().__init__()
        
        self.enc_hidden_size = enc_hidden_size
        self.dec_hidden_size = dec_hidden_size
        
        # self.linear_in = nn.Linear(enc_hidden_size, dec_hidden_size)     #主要應該還是為了匹配encoder&decoder兩邊hidden output的長度
        self.linear_out = nn.Linear(enc_hidden_size+dec_hidden_size, dec_hidden_size)
        self.linear_query = nn.Linear(enc_hidden_size, dec_hidden_size)
    
    def forward(self, output, encoder_output, time_lag):
        # output: [batch_size, seq_len_y-1, dec_hidden_size]  这个output 是decoder的每个时间步输出的隐藏状态
        # encoder_output: [batch_size, seq_len_x, 2*enc_hidden_size]
        
        output1 = self.linear_query(output)
        # encoder_output = self.linear_in(encoder_output)  # [batch_size*seq_len_x,dec_hidden_size]
        context_in = encoder_output.transpose(1, 2)   # [batch_size, dec_hidden_size, seq_len_x
        
        attn = torch.bmm(output1, context_in)  # [batch_size, seq_len_y-1, seq_len_x]
        
        # 这个东西就是求得当前时间步的输出output和所有输入相似性关系的一个得分score , 下面就是通过softmax把这个得分转成权重
        attn = nn.functional.softmax(attn, dim=2)    # 此时第二维度的数字全都变成了0-1之间的数， 越大表示当前的输出output与哪个相关程度越大
        context = torch.bmm(attn, encoder_output)   # [batch_size, seq_len_y-1, enc_hidden_size]

        output = torch.cat((context, output), dim=2)  # [batch_size, seq_len_y-1, 2*enc_hidden_size+dec_hidden_size]
        output = torch.tanh(self.linear_out(output))     # [batch_size*seq_len_y-1, dec_hidden_size]
        
        return output, attn

class Attention2(nn.Module):
    def __init__(self, enc_hidden_size, dec_hidden_size):
        super(Attention2, self).__init__()
        
        self.enc_hidden_size = enc_hidden_size
        self.dec_hidden_size = dec_hidden_size
        
        self.score = torch.signal.windows.gaussian(7).cuda()
        self.linear_in = nn.Linear(enc_hidden_size, dec_hidden_size)     #主要應該還是為了匹配encoder&decoder兩邊hidden output的長度
        self.linear_out = nn.Linear(enc_hidden_size+dec_hidden_size, dec_hidden_size)
    
    def forward(self, output, encoder_output, time_lag):
        # output: [batch_size, seq_len_y-1, dec_hidden_size]  这个output 是decoder的每个时间步输出的隐藏状态
        # encoder_output: [batch_size, seq_len_x, 2*enc_hidden_size]
        batch_size = output.size(0)
        output_len = output.size(1)
        input_len = encoder_output.size(1)
        
        attn = torch.zeros((190)).cuda()
        attn[time_lag:time_lag+7] = self.score
        attn = attn[3:-3].repeat((batch_size, 1, 5))
        
        # context_in = self.linear_in(encoder_output.reshape(batch_size*input_len, -1))  # [batch_size*seq_len_x,dec_hidden_size]
        # context_in = context_in.reshape(batch_size, input_len, -1)  # [batch_size, seq_len_x, dec_hidden_size]
        # context_in = context_in.transpose(1, 2)   # [batch_size, dec_hidden_size, seq_len_x]
        # attn = torch.bmm(output, context_in)  # [batch_size, seq_len_y-1, seq_len_x]
        # 这个东西就是求得当前时间步的输出output和所有输入相似性关系的一个得分score , 下面就是通过softmax把这个得分转成权重
        attn = nn.functional.softmax(attn, dim=2)    # 此时第二维度的数字全都变成了0-1之间的数， 越大表示当前的输出output与哪个相关程度越大
        
        context = torch.bmm(attn, encoder_output)   # [batch_size, seq_len_y-1, 2*enc_hidden_size]
        
        output = torch.cat((context, output), dim=2)  # [batch_size, seq_len_y-1, 2*enc_hidden_size+dec_hidden_size]
        
        output = output.view(batch_size*output_len, -1)   # [batch_size*seq_len_y-1, 2*enc_hidden_size+dec_hidden_size]
        output = torch.tanh(self.linear_out(output).cuda())     # [batch_size*seq_len_y-1, dec_hidden_size]
        output = output.view(batch_size, output_len, -1)  # [batch_size, seq_len_y-1, dec_hidden_size]
        
        return output, attn

class myNet(nn.Module):
    def __init__(self, kernel=3):
        super(myNet, self).__init__()
        # net = models.vgg16_bn(pretrained=True)
        # for vgg net
        
        conv_1 = nn.Sequential(nn.Conv2d(1, 32, kernel_size=(kernel, kernel), stride=(1, 1), padding='same'), 
                                nn.BatchNorm2d(32), 
                                nn.ReLU(), 
                                nn.MaxPool2d(kernel_size=2, stride=2))
        conv_2 = nn.Sequential(nn.Conv2d(32, 64, kernel_size=(kernel, kernel), stride=(1, 1), padding='same'), 
                                nn.BatchNorm2d(64),
                                nn.ReLU(), 
                                nn.MaxPool2d(kernel_size=2, stride=2))
        conv_4 = nn.Sequential(nn.Conv2d(64, 128, kernel_size=(kernel, kernel), stride=(1, 1), padding='same'), 
                                nn.BatchNorm2d(128),
                                nn.ReLU())
        tran_conv_2 = nn.Sequential(nn.ConvTranspose2d(128, 64, kernel_size=(2, 2), stride=(2, 2)), 
                                nn.BatchNorm2d(64),
                                nn.ReLU())
        tran_conv_3 = nn.Sequential(nn.ConvTranspose2d(64, 32, kernel_size=(2, 2), stride=(2, 2)), 
                                nn.BatchNorm2d(32),
                                nn.ReLU())
        out = nn.Sequential(nn.Conv2d(32, 1, kernel_size=(3, 3), stride=(1, 1), padding=1), 
                                nn.ReLU())
        self.out = nn.Sequential(conv_1, conv_2, conv_4, tran_conv_2, tran_conv_3, out)

    def forward(self, x):
        x = self.out(x)
        return x

class myNet_3layer(nn.Module):
    def __init__(self, kernel=3):
        super().__init__()
        # net = models.vgg16_bn(pretrained=True)
        # for vgg net
        
        conv_1 = nn.Sequential(nn.Conv2d(1, 32, kernel_size=(kernel, kernel), stride=(1, 1), padding='same'), 
                                nn.ReLU(), 
                                nn.MaxPool2d(kernel_size=2, stride=2))
        conv_2 = nn.Sequential(nn.Conv2d(32, 64, kernel_size=(kernel, kernel), stride=(1, 1), padding='same'), 
                                nn.ReLU(), 
                                nn.MaxPool2d(kernel_size=2, stride=2))
        conv_3 = nn.Sequential(nn.Conv2d(64, 128, kernel_size=(kernel, kernel), stride=(1, 1), padding='same'), 
                                nn.ReLU(), 
                                nn.MaxPool2d(kernel_size=2, stride=2))
        conv_4 = nn.Sequential(nn.Conv2d(128, 256, kernel_size=(kernel, kernel), stride=(1, 1), padding='same'), 
                                nn.ReLU())
        tran_conv_1 = nn.Sequential(nn.ConvTranspose2d(256, 128, kernel_size=(2, 2), stride=(2, 2)), 
                                nn.ReLU())
        tran_conv_2 = nn.Sequential(nn.ConvTranspose2d(128, 64, kernel_size=(2, 2), stride=(2, 2)), 
                                nn.ReLU())
        tran_conv_3 = nn.Sequential(nn.ConvTranspose2d(64, 32, kernel_size=(2, 2), stride=(2, 2)), 
                                nn.ReLU())
        out = nn.Sequential(nn.Conv2d(32, 1, kernel_size=(3, 3), stride=(1, 1), padding=1), 
                                nn.ReLU())
        self.out = nn.Sequential(conv_1, conv_2, conv_3, conv_4, tran_conv_1, tran_conv_2, tran_conv_3, out)

    def forward(self, x):
        x = self.out(x)
        return x

class test_net(nn.Module):
    def __init__(self, kernel=3):
        super().__init__()
        # net = models.vgg16_bn(pretrained=True)
        # for vgg net
        
        conv_1 = nn.Sequential(nn.Conv2d(1, 32, kernel_size=(kernel, 3), stride=(1, 1), padding='same'), 
                                nn.ReLU(), 
                                nn.MaxPool2d(kernel_size=2, stride=2))
        conv_2 = nn.Sequential(nn.Conv2d(32, 64, kernel_size=(kernel, 3), stride=(1, 1), padding='same'), 
                                nn.ReLU(), 
                                nn.MaxPool2d(kernel_size=2, stride=2))
        conv_3 = nn.Sequential(nn.Conv2d(64, 128, kernel_size=(kernel, 3), stride=(1, 1), padding='same'), 
                                nn.ReLU(), 
                                nn.MaxPool2d(kernel_size=2, stride=2))
        conv_4 = nn.Sequential(nn.Conv2d(128, 256, kernel_size=(kernel, 3), stride=(1, 1), padding='same'), 
                                nn.ReLU())
        tran_conv_1 = nn.Sequential(nn.ConvTranspose2d(256, 128, kernel_size=(2, 2), stride=(2, 2)), 
                                nn.ReLU())
        tran_conv_2 = nn.Sequential(nn.ConvTranspose2d(128, 64, kernel_size=(2, 2), stride=(2, 2)), 
                                nn.ReLU())
        tran_conv_3 = nn.Sequential(nn.ConvTranspose2d(64, 32, kernel_size=(2, 2), stride=(2, 2)), 
                                nn.ReLU())
        out = nn.Sequential(nn.Conv2d(32, 1, kernel_size=(3, 3), stride=(1, 1), padding=1), 
                                nn.ReLU())
        self.out = nn.Sequential(conv_1, conv_2, conv_3, conv_4, tran_conv_1, tran_conv_2, tran_conv_3, out)

    def forward(self, x):
        x = self.out(x)
        return x

class myLSTM(nn.Module):
#建立LSTM class
    def __init__(self,input_feature_dim,hidden_feature_dim,hidden_layer_num):
        super().__init__()
        self.input_feature_dim=input_feature_dim
        self.hidden_feature_dim=hidden_feature_dim
        self.hidden_layer_num=hidden_layer_num

        #初始化LSTM
        self.lstm=nn.LSTM(input_feature_dim,hidden_feature_dim,hidden_layer_num, batch_first=True).cuda()
        #LSTM的輸出藉由單層的線性神經網路層分類~
        self.linear1=nn.Linear(hidden_feature_dim,hidden_feature_dim).cuda()

    def forward(self,input):
        temp,(hn,cn)=self.lstm(input)
        output = torch.zeros(list(temp.size())).cuda()
        for seq in range(0, temp.size(dim=1)):
            output[:, seq, :]=self.linear1(temp[:, seq, :])
        return output,(hn,cn)
        
        # temp,(hn,cn)=self.lstm(input)
        # output=self.linear1(temp[:, -1, :])
        # return output,(hn,cn)

class myLSTM_encode_decode(nn.Module):
#建立LSTM class
    def __init__(self,encode_input_channel, encode_output_channel, encode_layer_num, decode_input_channel, decode_output_channel, decode_layer_num):
        super().__init__()
        self.encode_input_channel=encode_input_channel
        self.encode_output_channel=encode_output_channel
        self.encode_layer_num=encode_layer_num
        self.decode_input_channel=decode_input_channel
        self.decode_output_channel=decode_output_channel
        self.decode_layer_num=decode_layer_num

        #初始化LSTM
        self.lstm1=nn.LSTM(self.encode_input_channel, self.encode_output_channel, self.encode_layer_num, batch_first=True)
        # self.lstm2=nn.LSTM(output_feature_dim,output_feature_dim,decode_layer_num, batch_first=True)
        self.lstm2=nn.LSTM(self.decode_input_channel, self.decode_output_channel, self.decode_layer_num, batch_first=True)

    def forward(self,input, corr_fun, output_time_steps):
        time_steps = input.size()[1]
        output = torch.zeros(list(input.size())).cuda()
        temp, (hn, cn) = self.lstm1(input)
        latency_vector=temp[:, -1:, :]
        # output[:, 0:1, :], (hn, cn) = self.lstm2(latency_vector, (hn, cn))
        output[:, 0:1, :], (hn, cn) = self.lstm2(torch.cat((latency_vector, corr_fun[:, 0:1, :]), dim=2), (hn, cn))
        for t in range(1, time_steps):
            # output[:, t:t+1, :], (hn, cn) = self.lstm2(output[:, t-1:t, :], (hn, cn))
            output[:, t:t+1, :], (hn, cn) = self.lstm2(torch.cat((output[:, t-1:t, :], corr_fun[:, t:t+1, :]), dim=2), (hn, cn))
        return output,(hn,cn)
    
class myGRU_encode_decode(nn.Module):
#建立LSTM class
    def __init__(self,encode_input_channel, encode_output_channel, encode_layer_num, decode_input_channel, decode_output_channel, decode_layer_num):
        super().__init__()
        self.encode_input_channel=encode_input_channel
        self.encode_output_channel=encode_output_channel
        self.encode_layer_num=encode_layer_num
        self.decode_input_channel=decode_input_channel
        self.decode_output_channel=decode_output_channel
        self.decode_layer_num=decode_layer_num

        #初始化LSTM
        self.gru1=nn.GRU(self.encode_input_channel, self.encode_output_channel, self.encode_layer_num, batch_first=True).cuda()
        self.gru2=nn.GRU(self.decode_input_channel, self.decode_output_channel, self.decode_layer_num, batch_first=True).cuda()

    def forward(self,input, corr_fun=None, is_cuda=True):
        time_steps = input.size()[1]
        output = torch.zeros(list(input.size())).cuda()
        h0 = torch.zeros(list([1*self.encode_layer_num, input.size(0), self.encode_output_channel])).cuda()
        temp, hn = self.gru1(input, h0)
        latency_vector=temp[:, -1:, :]
        if is_cuda is False:
            corr_fun = corr_fun.cpu()
            output = output.cpu()
            h0 = h0.cpu()
        output[:, 0:1, :], hn = self.gru2(torch.cat((latency_vector, corr_fun[:, 0:1, :]), dim=2), hn)
        for t in range(1, time_steps):
            output[:, t:t+1, :], hn = self.gru2(torch.cat((output[:, t-1:t, :], corr_fun[:, t:t+1, :]), dim=2), hn)
                
        return output,hn

class myGRU_encode_decode_bi(nn.Module):
#建立LSTM class
    def __init__(self,encode_input_channel, encode_output_channel, encode_layer_num, decode_input_channel, decode_output_channel, decode_layer_num):
        super().__init__()
        self.encode_input_channel=encode_input_channel
        self.encode_output_channel=encode_output_channel
        self.encode_layer_num=encode_layer_num
        self.decode_input_channel=decode_input_channel
        self.decode_output_channel=decode_output_channel
        self.decode_layer_num=decode_layer_num

        #初始化LSTM
        self.gru1=nn.GRU(self.encode_input_channel, self.encode_output_channel, self.encode_layer_num, batch_first=True, dropout=0.2, bidirectional=True).cuda()
        self.gru2=nn.GRU(self.decode_input_channel, self.decode_output_channel, self.decode_layer_num, batch_first=True, dropout=0.2).cuda()

    def forward(self,input, corr_fun=None, is_cuda=True):
        time_steps = input.size()[1]
        output = torch.zeros(list(input.size())).cuda()
        h0 = torch.zeros(list([2*self.encode_layer_num, input.size(0), self.encode_output_channel])).cuda()
        temp, hn = self.gru1(input, h0)
        latency_vector=temp[:, -1:, 0:self.encode_output_channel] + temp[:, -1:, self.encode_output_channel:self.encode_output_channel*2]
        hn = hn[2:, :, :]
        if is_cuda is False:
            corr_fun = corr_fun.cpu()
            output = output.cpu()
            h0 = h0.cpu()
        output[:, 0:1, :], hn = self.gru2(torch.cat((latency_vector, corr_fun[:, 0:1, :]), dim=2), hn)
        for t in range(1, time_steps):
            output[:, t:t+1, :], hn = self.gru2(torch.cat((output[:, t-1:t, :], corr_fun[:, t:t+1, :]), dim=2), hn)
                
        return output,hn

class myGRU_encode_decode_bi_IQ(nn.Module):
#建立LSTM class，事實上可以取代myGRU_encode_decode_bi
    def __init__(self,encode_input_channel, encode_output_channel, encode_layer_num, decode_input_channel, decode_output_channel, decode_layer_num):
        super().__init__()
        self.encode_input_channel=encode_input_channel
        self.encode_output_channel=encode_output_channel
        self.encode_layer_num=encode_layer_num
        self.decode_input_channel=decode_input_channel
        self.decode_output_channel=decode_output_channel
        self.decode_layer_num=decode_layer_num

        #初始化LSTM
        self.gru1=nn.GRU(self.encode_input_channel, self.encode_output_channel, self.encode_layer_num, batch_first=True, dropout=0.2, bidirectional=True).cuda()
        self.gru2=nn.GRU(self.decode_input_channel, self.decode_output_channel, self.decode_layer_num, batch_first=True, dropout=0.2).cuda()

    def forward(self,input, corr_fun, output_time_steps):
        output = torch.zeros((input.size(0), output_time_steps, self.decode_output_channel)).cuda()
        h0 = torch.zeros((2*self.encode_layer_num, input.size(0), self.encode_output_channel)).cuda()
        temp, hn = self.gru1(input, h0)
        latency_vector=temp[:, -1:, 0:self.encode_output_channel] + temp[:, -1:, self.encode_output_channel:self.encode_output_channel*2]
        hn = hn[self.encode_layer_num:, :, :]
        output[:, 0:1, :], hn = self.gru2(torch.cat((latency_vector, corr_fun[:, 0:1, :]), dim=2), hn)
        for t in range(1, output_time_steps):
            output[:, t:t+1, :], hn = self.gru2(torch.cat((output[:, t-1:t, :], corr_fun[:, t:t+1, :]), dim=2), hn)
                
        return output,hn
    
class myGRU_with_attention(nn.Module):
#建立LSTM class，事實上可以取代myGRU_encode_decode_bi
    def __init__(self,encode_input_channel, encode_output_channel, encode_layer_num, decode_input_channel, decode_output_channel, decode_layer_num):
        super().__init__()
        self.encode_input_channel=encode_input_channel
        self.encode_output_channel=encode_output_channel
        self.encode_layer_num=encode_layer_num
        self.decode_input_channel=decode_input_channel
        self.decode_output_channel=decode_output_channel
        self.decode_layer_num=decode_layer_num
        self.attn = Attention1(encode_output_channel, decode_output_channel).cuda()
        #初始化LSTM
        self.gru1=nn.GRU(self.encode_input_channel, self.encode_output_channel, self.encode_layer_num, batch_first=True, dropout=0.2, bidirectional=True).cuda()
        self.gru2=nn.GRU(self.decode_input_channel, self.decode_output_channel, self.decode_layer_num, batch_first=True, dropout=0.2).cuda()

    def forward(self,input, corr_fun, output_time_steps):
        output = torch.zeros((input.size(0), output_time_steps, self.decode_output_channel)).cuda()
        h0 = torch.zeros((2*self.encode_layer_num, input.size(0), self.encode_output_channel)).cuda()
        encoder_output, hn = self.gru1(input, h0)
        latency_vector=encoder_output[:, -1:, 0:self.encode_output_channel]+encoder_output[:, -1:, self.encode_output_channel:]
        hn = hn[self.encode_layer_num:, :, :]
        encoder_output = encoder_output[:, :, 0:self.encode_output_channel]+encoder_output[:, :, self.encode_output_channel:]
        # latency_vector=encoder_output[:, -1:, :]    #將encoder hidden output設為一半，這樣雙向的rnn output就會剛好符合endocder input的需求，不需要把他們加起來
        # hn = torch.cat((hn[[0, 2], :, :], hn[[1, 3], :, :]), dim=2)
        # hn.permute((1, 0, 2)).reshape((-1, 2, 1000)).permute((1, 0, 2))
        
        output[:, 0:1, :], hn = self.gru2(torch.cat((latency_vector, corr_fun[:, 0:1, :]), dim=2), hn)
        for t in range(1, output_time_steps):
            temp, hn = self.gru2(torch.cat((output[:, t-1:t, :], corr_fun[:, t:t+1, :]), dim=2), hn)
            output[:, t:t+1, :], _ = self.attn(temp, encoder_output.contiguous(), t)
                
        return output,hn
    
class myGRU_with_attention2(nn.Module):
#建立LSTM class，事實上可以取代myGRU_encode_decode_bi
    def __init__(self,encode_input_channel, encode_output_channel, encode_layer_num, decode_input_channel, decode_output_channel, decode_layer_num):
        super().__init__()
        self.encode_input_channel=encode_input_channel
        self.encode_output_channel=encode_output_channel
        self.encode_layer_num=encode_layer_num
        self.decode_input_channel=decode_input_channel
        self.decode_output_channel=decode_output_channel
        self.decode_layer_num=decode_layer_num
        self.attn = Attention2(encode_output_channel, decode_output_channel).cuda()
        #初始化LSTM
        self.gru1=nn.GRU(self.encode_input_channel, self.encode_output_channel, self.encode_layer_num, batch_first=True, dropout=0.2, bidirectional=True).cuda()
        self.gru2=nn.GRU(self.decode_input_channel, self.decode_output_channel, self.decode_layer_num, batch_first=True, dropout=0.2).cuda()

    def forward(self,input, corr_fun, output_time_steps):
        output = torch.zeros((input.size(0), output_time_steps, self.decode_output_channel)).cuda()
        h0 = torch.zeros((2*self.encode_layer_num, input.size(0), self.encode_output_channel)).cuda()
        # encoder_output: (bs, seq, bi*encode_hid_size)
        #  hn: (bi*encode_layer, bs, encode_hid_size)
        encoder_output, hn = self.gru1(input, h0)
        
        #雙向結構會讓output有兩組，之前都是用這段將兩組加起來
        latency_vector=encoder_output[:, -1:, 0:self.encode_output_channel]+encoder_output[:, -1:, self.encode_output_channel:]
        hn = hn[self.encode_layer_num:, :, :]
        encoder_output = encoder_output[:, :, 0:self.encode_output_channel]+encoder_output[:, :, self.encode_output_channel:]
        
        # #雙向結構會讓output有兩組，嘗試過將gru encoder output設為一半長度並串接，但gru_2000_2layer_bs10_epoch200_groupwidth1000_2說明並沒有比加起來好
        # latency_vector=encoder_output[:, -1:, :]    #將encoder hidden output設為一半，這樣雙向的rnn output就會剛好符合endocder input的需求，不需要把他們加起來
        # hn = torch.cat((hn[[0, 2], :, :], hn[[1, 3], :, :]), dim=2)
        # hn.permute((1, 0, 2)).reshape((-1, 2, 1000)).permute((1, 0, 2))
        
        output[:, 0:1, :], hn = self.gru2(torch.cat((latency_vector, corr_fun[:, 0:1, :]), dim=2), hn)
        for t in range(1, output_time_steps):
            temp, hn = self.gru2(torch.cat((output[:, t-1:t, :], corr_fun[:, t:t+1, :]), dim=2), hn)
            output[:, t:t+1, :], _ = self.attn(temp, encoder_output.contiguous(), t)
                
        return output,hn

class myGRU_with_attention3(nn.Module):
#建立LSTM class，事實上可以取代myGRU_encode_decode_bi
    def __init__(self,encode_input_channel, encode_output_channel, encode_layer_num, decode_input_channel, decode_output_channel, decode_layer_num):
        super().__init__()
        self.encode_input_channel=encode_input_channel
        self.encode_output_channel=encode_output_channel
        self.encode_layer_num=encode_layer_num
        self.decode_input_channel=decode_input_channel
        self.decode_output_channel=decode_output_channel
        self.decode_layer_num=decode_layer_num
        self.attn = Attention2(encode_output_channel, decode_output_channel).cuda()
        #初始化LSTM
        self.gru1=nn.GRU(self.encode_input_channel, self.encode_output_channel, self.encode_layer_num, batch_first=True, dropout=0.2, bidirectional=True).cuda()
        self.gru2=nn.GRU(self.decode_input_channel, self.decode_output_channel, self.decode_layer_num, batch_first=True, dropout=0.2).cuda()
        self.linear_out = nn.Linear(184*2, 184);
        self.linear_out2 = nn.Linear(184, 184);

    def forward(self,input, corr_fun, xcorr_max_idx, output_time_steps):
        output = torch.zeros((input.size(0), output_time_steps, self.decode_output_channel)).cuda()
        h0 = torch.zeros((2*self.encode_layer_num, input.size(0), self.encode_output_channel)).cuda()
        # encoder_output: (bs, seq, bi*encode_hid_size)
        #  hn: (bi*encode_layer, bs, encode_hid_size)
        encoder_output, hn = self.gru1(input, h0)
        
        #雙向結構會讓output有兩組，之前都是用這段將兩組加起來
        latency_vector=encoder_output[:, -1:, 0:self.encode_output_channel]+encoder_output[:, -1:, self.encode_output_channel:]
        hn = hn[self.encode_layer_num:, :, :]
        encoder_output = encoder_output[:, :, 0:self.encode_output_channel]+encoder_output[:, :, self.encode_output_channel:]
        
        # #雙向結構會讓output有兩組，嘗試過將gru encoder output設為一半長度並串接，但gru_2000_2layer_bs10_epoch200_groupwidth1000_2說明並沒有比加起來好
        # latency_vector=encoder_output[:, -1:, :]    #將encoder hidden output設為一半，這樣雙向的rnn output就會剛好符合endocder input的需求，不需要把他們加起來
        # hn = torch.cat((hn[[0, 2], :, :], hn[[1, 3], :, :]), dim=2)
        # hn.permute((1, 0, 2)).reshape((-1, 2, 1000)).permute((1, 0, 2))
        
        output[:, 0:1, :], hn = self.gru2(torch.cat((latency_vector, corr_fun[:, 0:1, :]), dim=2), hn)
        for t in range(1, output_time_steps):
            temp, hn = self.gru2(torch.cat((output[:, t-1:t, :], corr_fun[:, t:t+1, :]), dim=2), hn)
            output[:, t:t+1, :], _ = self.attn(temp, encoder_output.contiguous(), t)
        output = output[:, :, 490:510].mean(dim=2)
        output = torch.cat((output, xcorr_max_idx), dim=1)
        output = self.linear_out2(self.linear_out(output))
        
        return output,hn

class myGRU_with_attention4(nn.Module):
#建立LSTM class，事實上可以取代myGRU_encode_decode_bi
    def __init__(self,encode_input_channel, encode_output_channel, encode_layer_num, decode_input_channel, decode_output_channel, decode_layer_num):
        super().__init__()
        self.encode_input_channel=encode_input_channel
        self.encode_output_channel=encode_output_channel
        self.encode_layer_num=encode_layer_num
        self.decode_input_channel=decode_input_channel
        self.decode_output_channel=decode_output_channel
        self.decode_layer_num=decode_layer_num
        self.attn = Attention2(encode_output_channel, decode_output_channel).cuda()
        #初始化LSTM
        self.gru1=nn.GRU(self.encode_input_channel, self.encode_output_channel, self.encode_layer_num, batch_first=True, dropout=0.2, bidirectional=True).cuda()
        self.gru2=nn.GRU(self.decode_input_channel, self.decode_output_channel, self.decode_layer_num, batch_first=True, dropout=0.2).cuda()
        self.linear_out = nn.Linear(184*2, 184);
        self.linear_out2 = nn.Linear(184, 184);

    def forward(self,input, corr_fun, xcorr_max_idx, output_time_steps):
        output = torch.zeros((input.size(0), output_time_steps, self.decode_output_channel)).cuda()
        h0 = torch.zeros((2*self.encode_layer_num, input.size(0), self.encode_output_channel)).cuda()
        # encoder_output: (bs, seq, bi*encode_hid_size)
        #  hn: (bi*encode_layer, bs, encode_hid_size)
        encoder_output, hn = self.gru1(input, h0)
        
        #雙向結構會讓output有兩組，之前都是用這段將兩組加起來
        latency_vector=encoder_output[:, -1:, 0:self.encode_output_channel]+encoder_output[:, -1:, self.encode_output_channel:]
        hn = hn[self.encode_layer_num:, :, :]
        encoder_output = encoder_output[:, :, 0:self.encode_output_channel]+encoder_output[:, :, self.encode_output_channel:]
        
        # #雙向結構會讓output有兩組，嘗試過將gru encoder output設為一半長度並串接，但gru_2000_2layer_bs10_epoch200_groupwidth1000_2說明並沒有比加起來好
        # latency_vector=encoder_output[:, -1:, :]    #將encoder hidden output設為一半，這樣雙向的rnn output就會剛好符合endocder input的需求，不需要把他們加起來
        # hn = torch.cat((hn[[0, 2], :, :], hn[[1, 3], :, :]), dim=2)
        # hn.permute((1, 0, 2)).reshape((-1, 2, 1000)).permute((1, 0, 2))
        
        output[:, 0:1, :], hn = self.gru2(torch.cat((latency_vector, corr_fun[:, 0:1, :]), dim=2), hn)
        for t in range(1, output_time_steps):
            temp, hn = self.gru2(torch.cat((output[:, t-1:t, :], corr_fun[:, t:t+1, :]), dim=2), hn)
            output[:, t:t+1, :], _ = self.attn(temp, encoder_output.contiguous(), t)
            
        output = output.permute((0, 2, 1)).reshape((-1, output_time_steps))
        xcorr_max_idx = xcorr_max_idx[:, None, :].repeat((1, self.decode_output_channel, 1)).reshape((-1, output_time_steps))
        output = torch.cat((output, xcorr_max_idx), dim=1)
        output = self.linear_out2(self.linear_out(output))
        output = output.reshape((-1, self.decode_output_channel, output_time_steps)).permute((0, 2, 1))
        
        return output,hn
    
class myGRU_with_attention5(nn.Module):
#建立LSTM class，事實上可以取代myGRU_encode_decode_bi
    def __init__(self,encode_input_channel, encode_output_channel, encode_layer_num, decode_input_channel, decode_output_channel, decode_layer_num):
        super().__init__()
        self.encode_input_channel=encode_input_channel
        self.encode_output_channel=encode_output_channel
        self.encode_layer_num=encode_layer_num
        self.decode_input_channel=decode_input_channel
        self.decode_output_channel=decode_output_channel
        self.decode_layer_num=decode_layer_num
        self.attn = Attention2(encode_output_channel, decode_output_channel)
        #初始化LSTM
        self.gru1=nn.GRU(self.encode_input_channel, self.encode_output_channel, self.encode_layer_num, batch_first=True, dropout=0.2, bidirectional=True).cuda()
        self.gru2=nn.GRU(self.decode_input_channel, self.decode_output_channel, self.decode_layer_num, batch_first=True, dropout=0.2).cuda()
        self.position_emb = nn.Embedding(1000, 10)
        self.linear_emb = nn.Linear(20, 184)
        self.linear_out = nn.Linear(184*3, 184*2)
        self.linear_out2 = nn.Linear(184*2, 184)
        self.linear_out3 = nn.Linear(184, 184)
        

    def forward(self,input, corr_fun, xcorr_max_idx, output_time_steps):
        output = torch.zeros((input.size(0), output_time_steps, self.decode_output_channel)).cuda()
        h0 = torch.zeros((2*self.encode_layer_num, input.size(0), self.encode_output_channel)).cuda()
        # encoder_output: (bs, seq, bi*encode_hid_size)
        #  hn: (bi*encode_layer, bs, encode_hid_size)
        encoder_output, hn = self.gru1(input, h0)
        
        #雙向結構會讓output有兩組，之前都是用這段將兩組加起來
        latency_vector=encoder_output[:, -1:, 0:self.encode_output_channel]+encoder_output[:, -1:, self.encode_output_channel:]
        hn = hn[self.encode_layer_num:, :, :]
        encoder_output = encoder_output[:, :, 0:self.encode_output_channel]+encoder_output[:, :, self.encode_output_channel:]
        
        # #雙向結構會讓output有兩組，嘗試過將gru encoder output設為一半長度並串接，但gru_2000_2layer_bs10_epoch200_groupwidth1000_2說明並沒有比加起來好
        # latency_vector=encoder_output[:, -1:, :]    #將encoder hidden output設為一半，這樣雙向的rnn output就會剛好符合endocder input的需求，不需要把他們加起來
        # hn = torch.cat((hn[[0, 2], :, :], hn[[1, 3], :, :]), dim=2)
        # hn.permute((1, 0, 2)).reshape((-1, 2, 1000)).permute((1, 0, 2))
        
        output[:, 0:1, :], hn = self.gru2(torch.cat((latency_vector, corr_fun[:, 0:1, :]), dim=2), hn)
        for t in range(1, output_time_steps):
            temp, hn = self.gru2(torch.cat((output[:, t-1:t, :], corr_fun[:, t:t+1, :]), dim=2), hn)
            output[:, t:t+1, :], _ = self.attn(temp, encoder_output.contiguous(), t)
        
        output = output.permute((0, 2, 1))
        # pos_info = torch.tensor(np.arange(1000)).cuda()
        pos_info = get_sin_cos_PE(self.decode_output_channel).cuda()
        pos_info = self.linear_emb(pos_info)[None, :, :].repeat((output.size(0), 1, 1))
        
        # pos_info = self.position_emb(pos_info)[None, :, :].repeat((output.size(0), 1, 1))
        xcorr_max_idx = xcorr_max_idx[:, None, :].repeat((1, self.decode_output_channel, 1))
        output = torch.cat((output, xcorr_max_idx, pos_info), dim=2)
        output = output.reshape((-1, output_time_steps*3))
        output = self.linear_out3(self.linear_out2(self.linear_out(output)))
        output = output.reshape((-1, self.decode_output_channel, output_time_steps)).permute((0, 2, 1))
        
        return output,hn

class myGRU2_with_attention5(nn.Module):
#建立LSTM class，事實上可以取代myGRU_encode_decode_bi
    def __init__(self,encode_input_channel, encode_output_channel, encode_layer_num, decode_input_channel, decode_output_channel, decode_layer_num):
        super().__init__()
        self.encode_input_channel=encode_input_channel
        self.encode_output_channel=encode_output_channel
        self.encode_layer_num=encode_layer_num
        self.decode_input_channel=decode_input_channel
        self.decode_output_channel=decode_output_channel
        self.decode_layer_num=decode_layer_num
        self.attn = Attention2(encode_output_channel, decode_output_channel).cuda()
        #初始化LSTM
        self.gru1=nn.GRU(self.encode_input_channel, self.encode_output_channel, self.encode_layer_num, batch_first=True, dropout=0.2, bidirectional=True).cuda()
        self.gru2=nn.GRU(self.decode_input_channel, self.decode_output_channel, self.decode_layer_num, batch_first=True, dropout=0.2).cuda()
        self.position_emb = nn.Embedding(1000, 10).cuda()
        self.linear_emb = nn.Linear(20, 184).cuda()
        self.linear_out = nn.Linear(184*3, 184*2).cuda()
        self.linear_out2 = nn.Linear(184*2, 184).cuda()
        self.linear_out3 = nn.Linear(184, 184).cuda()
        

    def forward(self,input, corr_fun, xcorr_max_idx, output_time_steps):
        output = torch.zeros((input.size(0), output_time_steps, self.decode_output_channel)).cuda()
        h0 = torch.zeros((2*self.encode_layer_num, input.size(0), self.encode_output_channel)).cuda()
        # encoder_output: (bs, seq, bi*encode_hid_size)
        #  hn: (bi*encode_layer, bs, encode_hid_size)
        encoder_output, hn = self.gru1(input, h0)
        
        #雙向結構會讓output有兩組，之前都是用這段將兩組加起來
        latency_vector=encoder_output[:, -1:, 0:self.encode_output_channel]+encoder_output[:, -1:, self.encode_output_channel:]
        hn = hn[self.encode_layer_num:, :, :]
        encoder_output = encoder_output[:, :, 0:self.encode_output_channel]+encoder_output[:, :, self.encode_output_channel:]
        
        # #雙向結構會讓output有兩組，嘗試過將gru encoder output設為一半長度並串接，但gru_2000_2layer_bs10_epoch200_groupwidth1000_2說明並沒有比加起來好
        # latency_vector=encoder_output[:, -1:, :]    #將encoder hidden output設為一半，這樣雙向的rnn output就會剛好符合endocder input的需求，不需要把他們加起來
        # hn = torch.cat((hn[[0, 2], :, :], hn[[1, 3], :, :]), dim=2)
        # hn.permute((1, 0, 2)).reshape((-1, 2, 1000)).permute((1, 0, 2))
        
        output[:, 0:1, :], hn = self.gru2(torch.cat((latency_vector, latency_vector, corr_fun[:, 0:1, :]), dim=2), hn)
        for t in range(1, output_time_steps):
            temp, hn = self.gru2(torch.cat((output[:, t-1:t, :], latency_vector, corr_fun[:, t:t+1, :]), dim=2), hn)
            output[:, t:t+1, :], _ = self.attn(temp, encoder_output.contiguous(), t)
        
        output = output.permute((0, 2, 1))
        # pos_info = torch.tensor(np.arange(1000)).cuda()
        pos_info = get_sin_cos_PE().cuda()
        pos_info = self.linear_emb(pos_info)[None, :, :].repeat((output.size(0), 1, 1))
        
        # pos_info = self.position_emb(pos_info)[None, :, :].repeat((output.size(0), 1, 1))
        xcorr_max_idx = xcorr_max_idx[:, None, :].repeat((1, self.decode_output_channel, 1))
        output = torch.cat((output, xcorr_max_idx, pos_info), dim=2)
        output = output.reshape((-1, output_time_steps*3))
        output = self.linear_out3(self.linear_out2(self.linear_out(output)))
        output = output.reshape((-1, self.decode_output_channel, output_time_steps)).permute((0, 2, 1))
        
        return output,hn

class myGRU_with_attention6(nn.Module):
#建立LSTM class，事實上可以取代myGRU_encode_decode_bi
    def __init__(self,encode_input_channel, encode_output_channel, encode_layer_num, decode_input_channel, decode_output_channel, decode_layer_num):
        super().__init__()
        self.encode_input_channel=encode_input_channel
        self.encode_output_channel=encode_output_channel
        self.encode_layer_num=encode_layer_num
        self.decode_input_channel=decode_input_channel
        self.decode_output_channel=decode_output_channel
        self.decode_layer_num=decode_layer_num
        self.attn = Attention2(encode_output_channel, decode_output_channel).cuda()
        #初始化LSTM
        self.gru1=nn.GRU(self.encode_input_channel, self.encode_output_channel, self.encode_layer_num, batch_first=True, dropout=0.2, bidirectional=True).cuda()
        self.gru2=nn.GRU(self.decode_input_channel, self.decode_output_channel, self.decode_layer_num, batch_first=True, dropout=0.2).cuda()
        self.position_emb = nn.Linear(1, 10).cuda()
        self.linear_out = nn.Linear(184*2+10, 184*2).cuda()
        self.linear_out2 = nn.Linear(184*2, 184).cuda()
        self.linear_out3 = nn.Linear(184, 184).cuda()
        

    def forward(self,input, corr_fun, xcorr_max_idx, output_time_steps):
        output = torch.zeros((input.size(0), output_time_steps, self.decode_output_channel)).cuda()
        h0 = torch.zeros((2*self.encode_layer_num, input.size(0), self.encode_output_channel)).cuda()
        # encoder_output: (bs, seq, bi*encode_hid_size)
        #  hn: (bi*encode_layer, bs, encode_hid_size)
        encoder_output, hn = self.gru1(input, h0)
        
        #雙向結構會讓output有兩組，之前都是用這段將兩組加起來
        latency_vector=encoder_output[:, -1:, 0:self.encode_output_channel]+encoder_output[:, -1:, self.encode_output_channel:]
        hn = hn[self.encode_layer_num:, :, :]
        encoder_output = encoder_output[:, :, 0:self.encode_output_channel]+encoder_output[:, :, self.encode_output_channel:]
        
        # #雙向結構會讓output有兩組，嘗試過將gru encoder output設為一半長度並串接，但gru_2000_2layer_bs10_epoch200_groupwidth1000_2說明並沒有比加起來好
        # latency_vector=encoder_output[:, -1:, :]    #將encoder hidden output設為一半，這樣雙向的rnn output就會剛好符合endocder input的需求，不需要把他們加起來
        # hn = torch.cat((hn[[0, 2], :, :], hn[[1, 3], :, :]), dim=2)
        # hn.permute((1, 0, 2)).reshape((-1, 2, 1000)).permute((1, 0, 2))
        
        output[:, 0:1, :], hn = self.gru2(torch.cat((latency_vector, corr_fun[:, 0:1, :]), dim=2), hn)
        for t in range(1, output_time_steps):
            temp, hn = self.gru2(torch.cat((output[:, t-1:t, :], corr_fun[:, t:t+1, :]), dim=2), hn)
            output[:, t:t+1, :], _ = self.attn(temp, encoder_output.contiguous(), t)
        
        output = output.permute((0, 2, 1))
        pos_info = torch.tensor(np.arange(1000))[:, None].float().cuda()
        pos_info = self.position_emb(pos_info)[None, :, :].repeat((output.size(0), 1, 1))
        xcorr_max_idx = xcorr_max_idx[:, None, :].repeat((1, self.decode_output_channel, 1))
        output = torch.cat((output, xcorr_max_idx, pos_info), dim=2)
        output = output.reshape((-1, output_time_steps*2+10))
        output = self.linear_out3(self.linear_out2(self.linear_out(output)))
        output = output.reshape((-1, self.decode_output_channel, output_time_steps)).permute((0, 2, 1))
        
        return output,hn

class myGRU_with_attention7(nn.Module):
#建立LSTM class，事實上可以取代myGRU_encode_decode_bi
    def __init__(self,encode_input_channel, encode_output_channel, encode_layer_num, decode_input_channel, decode_output_channel, decode_layer_num):
        super().__init__()
        self.encode_input_channel=encode_input_channel
        self.encode_output_channel=encode_output_channel
        self.encode_layer_num=encode_layer_num
        self.decode_input_channel=decode_input_channel
        self.decode_output_channel=decode_output_channel
        self.decode_layer_num=decode_layer_num
        self.attn = Attention1_3(encode_output_channel, decode_output_channel)
        #初始化LSTM
        self.gru1 = nn.GRU(self.encode_input_channel, 500, self.encode_layer_num, batch_first=True, dropout=0.2, bidirectional=True).cuda()
        
        
        self.gru2=nn.GRU(self.decode_input_channel, self.decode_output_channel, self.decode_layer_num, batch_first=True, dropout=0.2).cuda()
        self.key = nn.Linear(encode_output_channel, decode_output_channel)
        self.position_emb = nn.Embedding(1000, 10)
        self.linear_emb = nn.Linear(20, 184)
        self.linear_out = nn.Linear(184*3, 184*2)
        self.linear_out2 = nn.Linear(184*2, 184)
        self.linear_out3 = nn.Linear(184, 184)
        

    def forward(self,input, encoder_input, xcorr_max_idx, output_time_steps):
        output = torch.zeros((input.size(0), output_time_steps, self.decode_output_channel)).cuda()
        h0 = torch.zeros((2*self.encode_layer_num, input.size(0), self.encode_output_channel)).cuda()
        # encoder_output: (bs, seq, bi*encode_hid_size)
        #  hn: (bi*encode_layer, bs, encode_hid_size)
        encoder_output, hn = self.gru1(input, h0)
        
        #雙向結構會讓output有兩組，之前都是用這段將兩組加起來
        latency_vector=encoder_output[:, -1:, 0:self.encode_output_channel]+encoder_output[:, -1:, self.encode_output_channel:]
        hn = hn[self.encode_layer_num:, :, :]
        encoder_output = encoder_output[:, :, 0:self.encode_output_channel]+encoder_output[:, :, self.encode_output_channel:]
        encoder_output = self.key(encoder_output)
        
        # #雙向結構會讓output有兩組，嘗試過將gru encoder output設為一半長度並串接，但gru_2000_2layer_bs10_epoch200_groupwidth1000_2說明並沒有比加起來好
        # latency_vector=encoder_output[:, -1:, :]    #將encoder hidden output設為一半，這樣雙向的rnn output就會剛好符合endocder input的需求，不需要把他們加起來
        # hn = torch.cat((hn[[0, 2], :, :], hn[[1, 3], :, :]), dim=2)
        # hn.permute((1, 0, 2)).reshape((-1, 2, 1000)).permute((1, 0, 2))
        
        output[:, 0:1, :], hn = self.gru2(torch.cat((latency_vector, encoder_input[:, 0:1, :]), dim=2), hn)
        for t in range(1, output_time_steps):
            temp, hn = self.gru2(torch.cat((output[:, t-1:t, :], encoder_input[:, t:t+1, :]), dim=2), hn)
            output[:, t:t+1, :], _ = self.attn(temp, encoder_output.contiguous(), t)
        
        output = output.permute((0, 2, 1))
        # pos_info = torch.tensor(np.arange(1000)).cuda()
        pos_info = get_sin_cos_PE().cuda()
        pos_info = self.linear_emb(pos_info)[None, :, :].repeat((output.size(0), 1, 1))
        
        # pos_info = self.position_emb(pos_info)[None, :, :].repeat((output.size(0), 1, 1))
        xcorr_max_idx = xcorr_max_idx[:, None, :].repeat((1, self.decode_output_channel, 1))
        output = torch.cat((output, xcorr_max_idx, pos_info), dim=2)
        output = output.reshape((-1, output_time_steps*3))
        output = self.linear_out3(self.linear_out2(self.linear_out(output)))
        output = output.reshape((-1, self.decode_output_channel, output_time_steps)).permute((0, 2, 1))
        
        return output,hn

# modify from myGRU_with_attention5
class my_pyramid_GRU(nn.Module):
#建立LSTM class，事實上可以取代myGRU_encode_decode_bi
    def __init__(self,encode_input_channel, encode_output_channel, encode_layer_num, decode_input_channel, decode_output_channel, decode_layer_num):
        super().__init__()
        self.encode_input_channel=encode_input_channel
        self.encode_output_channel=encode_output_channel
        self.encode_layer_num=encode_layer_num
        self.decode_input_channel=decode_input_channel
        self.decode_output_channel=decode_output_channel
        self.decode_layer_num=decode_layer_num
        self.attn = Attention2(encode_output_channel, decode_output_channel)
        #初始化LSTM
        self.gru1=nn.GRU(self.encode_input_channel, self.encode_output_channel, self.encode_layer_num, batch_first=True, dropout=0.2, bidirectional=True).cuda()
        self.gru2=nn.GRU(self.decode_input_channel, self.decode_output_channel, self.decode_layer_num, batch_first=True, dropout=0.2).cuda()
        self.position_emb = nn.Embedding(1000, 10)
        self.linear_emb = nn.Linear(20, 184)
        self.linear_out = nn.Linear(184*3, 184*2)
        self.linear_out2 = nn.Linear(184*2, 184)
        self.linear_out3 = nn.Linear(184, 184)
        

    def forward(self,input, corr_fun, xcorr_max_idx, output_time_steps):
        output = torch.zeros((input.size(0), output_time_steps, self.decode_output_channel)).cuda()
        h0 = torch.zeros((2*self.encode_layer_num, input.size(0), self.encode_output_channel)).cuda()
        # encoder_output: (bs, seq, bi*encode_hid_size)
        #  hn: (bi*encode_layer, bs, encode_hid_size)
        encoder_output, hn = self.gru1(input, h0)
        
        #雙向結構會讓output有兩組，之前都是用這段將兩組加起來
        latency_vector=encoder_output[:, -1:, 0:self.encode_output_channel]+encoder_output[:, -1:, self.encode_output_channel:]
        hn = hn[self.encode_layer_num:, :, :]
        encoder_output = encoder_output[:, :, 0:self.encode_output_channel]+encoder_output[:, :, self.encode_output_channel:]
        
        # #雙向結構會讓output有兩組，嘗試過將gru encoder output設為一半長度並串接，但gru_2000_2layer_bs10_epoch200_groupwidth1000_2說明並沒有比加起來好
        # latency_vector=encoder_output[:, -1:, :]    #將encoder hidden output設為一半，這樣雙向的rnn output就會剛好符合endocder input的需求，不需要把他們加起來
        # hn = torch.cat((hn[[0, 2], :, :], hn[[1, 3], :, :]), dim=2)
        # hn.permute((1, 0, 2)).reshape((-1, 2, 1000)).permute((1, 0, 2))
        
        output[:, 0:1, :], hn = self.gru2(torch.cat((latency_vector, corr_fun[:, 0:1, :]), dim=2), hn)
        for t in range(1, output_time_steps):
            temp, hn = self.gru2(torch.cat((output[:, t-1:t, :], corr_fun[:, t:t+1, :]), dim=2), hn)
            output[:, t:t+1, :], _ = self.attn(temp, encoder_output.contiguous(), t)
        
        output = output.permute((0, 2, 1))
        # pos_info = torch.tensor(np.arange(1000)).cuda()
        pos_info = get_sin_cos_PE().cuda()
        pos_info = self.linear_emb(pos_info)[None, :, :].repeat((output.size(0), 1, 1))
        
        # pos_info = self.position_emb(pos_info)[None, :, :].repeat((output.size(0), 1, 1))
        xcorr_max_idx = xcorr_max_idx[:, None, :].repeat((1, self.decode_output_channel, 1))
        output = torch.cat((output, xcorr_max_idx, pos_info), dim=2)
        output = output.reshape((-1, output_time_steps*3))
        output = self.linear_out3(self.linear_out2(self.linear_out(output)))
        output = output.reshape((-1, self.decode_output_channel, output_time_steps)).permute((0, 2, 1))
        
        return output,hn

class myGRU_atten2_IQdecoder(nn.Module):
#建立LSTM class，事實上可以取代myGRU_encode_decode_bi
    def __init__(self,encode_input_channel, encode_output_channel, encode_layer_num, decode_input_channel, decode_output_channel, decode_layer_num):
        super().__init__()
        self.encode_input_channel=encode_input_channel
        self.encode_output_channel=encode_output_channel
        self.encode_layer_num=encode_layer_num
        self.decode_input_channel=decode_input_channel
        self.decode_output_channel=decode_output_channel
        self.decode_layer_num=decode_layer_num
        self.attn = Attention2(encode_output_channel, decode_output_channel).cuda()
        #初始化LSTM
        self.gru1=nn.GRU(self.encode_input_channel, self.encode_output_channel, self.encode_layer_num, batch_first=True, dropout=0.2, bidirectional=True).cuda()
        self.gru2=nn.GRU(self.decode_input_channel, self.decode_output_channel, self.decode_layer_num, batch_first=True, dropout=0.2).cuda()

    def forward(self,input, corr_fun, IQ, output_time_steps):
        output = torch.zeros((input.size(0), output_time_steps, self.decode_output_channel)).cuda()
        h0 = torch.zeros((2*self.encode_layer_num, input.size(0), self.encode_output_channel)).cuda()
        encoder_output, hn = self.gru1(input, h0)
        
        #雙向結構會讓output有兩組，之前都是用這段將兩組加起來
        latency_vector=encoder_output[:, -1:, 0:self.encode_output_channel]+encoder_output[:, -1:, self.encode_output_channel:]
        hn = hn[self.encode_layer_num:, :, :]
        # encoder_output: (bs, seq, bi*encode_hid_size)
        #  hn: (bi*encode_layer, bs, encode_hid_size)
        encoder_output = encoder_output[:, :, 0:self.encode_output_channel]+encoder_output[:, :, self.encode_output_channel:]
        
        # #雙向結構會讓output有兩組，嘗試過將gru encoder output設為一半長度並串接，但gru_2000_2layer_bs10_epoch200_groupwidth1000_2說明並沒有比加起來好
        # latency_vector=encoder_output[:, -1:, :]    #將encoder hidden output設為一半，這樣雙向的rnn output就會剛好符合endocder input的需求，不需要把他們加起來
        # hn = torch.cat((hn[[0, 2], :, :], hn[[1, 3], :, :]), dim=2)
        # hn.permute((1, 0, 2)).reshape((-1, 2, 1000)).permute((1, 0, 2))
        
        output[:, 0:1, :], hn = self.gru2(torch.cat((latency_vector, corr_fun[:, 0:1, :], IQ[:, 0:1, :]), dim=2), hn)
        for t in range(1, output_time_steps):
            temp, hn = self.gru2(torch.cat((output[:, t-1:t, :], corr_fun[:, t:t+1, :], IQ[:, t:t+1, :]), dim=2), hn)
            output[:, t:t+1, :], _ = self.attn(temp, encoder_output.contiguous(), t)
                
        return output,hn

def conv(in_planes, out_planes, kernel_size=3, padding=1, stride=1, dilation=1, bias=True):   
    return nn.Sequential(
            nn.Conv1d(in_planes, out_planes, kernel_size=kernel_size, stride=stride, 
                        padding=padding, dilation=dilation, bias=bias),
            nn.LeakyReLU(0.1))

def predict_flow(in_planes,bias=True):
    return nn.Conv1d(in_planes,1,kernel_size=3,stride=1,padding=1,bias=bias)

def deconv(in_planes, out_planes, kernel_size=4, stride=2, padding=1, bias=True):
    return nn.ConvTranspose1d(in_planes, out_planes, kernel_size, stride, padding, bias=bias)

def crop_like(input, target):
    if input.size()[2] == target.size()[2]:
        return input
    else:
        return input[:, :, :target.size(2)]
    
def correlate(input1, input2, md=5):
    input1 = input1[:, :, None, :]
    input2 = input2[:, :, None, :]
    out_corr = spatial_correlation_sample(input1,
                                          input2,
                                          kernel_size=1,
                                          patch_size=md*2+1,
                                          stride=1,
                                          padding=0,
                                          dilation_patch=1)
    # collate dimensions 1 and 2 in order to be treated as a
    # regular 4D tensor
    b, ph, pw, h, w = out_corr.size()
    out_corr = out_corr[:, md, :, :, :]/input1.size(1)
    # out_corr = out_corr.view(b, ph * pw, h, w)/input1.size(1)
    return nn.functional.leaky_relu_(out_corr, 0.1).squeeze()

def load_my_state_dict(model, pretrained_dict):
    model_dict = model.state_dict()

    # 1. filter out unnecessary keys
    pretrained_dict = {k: v for k, v in pretrained_dict.items() if k in model_dict}
    # 2. overwrite entries in the existing state dict
    model_dict.update(pretrained_dict) 
    # 3. load the new state dict
    model.load_state_dict(model_dict)

class Correlation1d(nn.Module):
    def __init__(self, pad_size=None, kernel_size=None, max_displacement=None,
            stride1=None, stride2=None, corr_multiply=None):
        super(Correlation1d, self).__init__()
        self.pad_size = pad_size
        self.kernel_size = kernel_size
        self.half_ks = int((kernel_size-1)/2)
        self.max_displacement = max_displacement
        self.stride1 = stride1
        self.stride2 = stride2
        self.corr_multiply = corr_multiply
        
    def xcorr_loss(self, output, target, dim=0):
        output = output.detach().cpu()
        target = target.detach().cpu()
        mean_o = output.mean(axis=dim)
        mean_t = target.mean(axis=dim)
        mean_ot = torch.mul(output, target).mean(axis=dim)
        mean_square_o = output.pow(2).mean(axis=dim)
        mean_square_t = target.pow(2).mean(axis=dim)
        den = torch.mul(torch.sqrt(mean_square_o - mean_o.pow(2)), torch.sqrt(mean_square_t - mean_t.pow(2)))
        xcorr = 1-torch.div(mean_ot-torch.mul(mean_o, mean_t), den+1e-5)
        return xcorr.mean(1).cuda()
    
    def forward(self, input1, input2):
        output = torch.zeros((input1.size(0), self.max_displacement*2+1, input1.size(2))).cuda()
        for d in range(-self.max_displacement, self.max_displacement+1):
            for i in range(self.half_ks+self.max_displacement, input1.size(2)-self.half_ks-self.max_displacement):
                output[:, d+self.max_displacement, i] = self.xcorr_loss(input1[:, :, i-self.half_ks:i+self.half_ks+1], 
                                                                 input2[:, :, i+d-self.half_ks:i+d+self.half_ks+1], dim=2)
        return output

class my_1DPWC_net(nn.Module):
    def __init__(self, md=3):
        """
        input: md --- maximum displacement (for correlation. default: 4), after warpping

        """
        super(my_1DPWC_net,self).__init__()

        self.conv1a  = conv(1,   16, kernel_size=3, stride=2)
        self.conv1aa = conv(16,  16, kernel_size=3, stride=1)
        self.conv1b  = conv(16,  16, kernel_size=3, stride=1)
        self.conv2a  = conv(16,  32, kernel_size=3, stride=2)
        self.conv2aa = conv(32,  32, kernel_size=3, stride=1)
        self.conv2b  = conv(32,  32, kernel_size=3, stride=1)
        self.conv3a  = conv(32,  64, kernel_size=3, stride=2)
        self.conv3aa = conv(64,  64, kernel_size=3, stride=1)
        self.conv3b  = conv(64,  64, kernel_size=3, stride=1)
        self.conv4a  = conv(64,  96, kernel_size=3, stride=2)
        self.conv4aa = conv(96,  96, kernel_size=3, stride=1)
        self.conv4b  = conv(96,  96, kernel_size=3, stride=1)
        self.conv5a  = conv(96, 128, kernel_size=3, stride=2)
        self.conv5aa = conv(128,128, kernel_size=3, stride=1)
        self.conv5b  = conv(128,128, kernel_size=3, stride=1)
        self.conv6aa = conv(128,196, kernel_size=3, stride=2)
        self.conv6a  = conv(196,196, kernel_size=3, stride=1)
        self.conv6b  = conv(196,196, kernel_size=3, stride=1)


        self.corr    = Correlation1d(pad_size=md, kernel_size=11, max_displacement=md, stride1=1, stride2=1, corr_multiply=1)
        self.leakyRELU = nn.LeakyReLU(0.1)
        
        nd = 2*md+1
        dd = np.cumsum([128,128,96,64,32])

        od = nd
        self.conv6_0 = conv(od,      128, kernel_size=3, stride=1)
        self.conv6_1 = conv(od+dd[0],128, kernel_size=3, stride=1)
        self.conv6_2 = conv(od+dd[1],96,  kernel_size=3, stride=1)
        self.conv6_3 = conv(od+dd[2],64,  kernel_size=3, stride=1)
        self.conv6_4 = conv(od+dd[3],32,  kernel_size=3, stride=1)        
        self.predict_flow6 = predict_flow(od+dd[4])
        self.deconv6 = deconv(1, 1, kernel_size=4, stride=2, padding=1) 
        self.upfeat6 = deconv(od+dd[4], 1, kernel_size=4, stride=2, padding=1) 
        
        od = nd+128+2
        self.conv5_0 = conv(od,      128, kernel_size=3, stride=1)
        self.conv5_1 = conv(od+dd[0],128, kernel_size=3, stride=1)
        self.conv5_2 = conv(od+dd[1],96,  kernel_size=3, stride=1)
        self.conv5_3 = conv(od+dd[2],64,  kernel_size=3, stride=1)
        self.conv5_4 = conv(od+dd[3],32,  kernel_size=3, stride=1)
        self.predict_flow5 = predict_flow(od+dd[4]) 
        self.deconv5 = deconv(1, 1, kernel_size=4, stride=2, padding=1) 
        self.upfeat5 = deconv(od+dd[4], 1, kernel_size=4, stride=2, padding=1) 
        
        od = nd+96+2
        self.conv4_0 = conv(od,      128, kernel_size=3, stride=1)
        self.conv4_1 = conv(od+dd[0],128, kernel_size=3, stride=1)
        self.conv4_2 = conv(od+dd[1],96,  kernel_size=3, stride=1)
        self.conv4_3 = conv(od+dd[2],64,  kernel_size=3, stride=1)
        self.conv4_4 = conv(od+dd[3],32,  kernel_size=3, stride=1)
        self.predict_flow4 = predict_flow(od+dd[4]) 
        self.deconv4 = deconv(1, 1, kernel_size=4, stride=2, padding=1) 
        self.upfeat4 = deconv(od+dd[4], 1, kernel_size=4, stride=2, padding=1) 
        
        od = nd+64+2
        self.conv3_0 = conv(od,      128, kernel_size=3, stride=1)
        self.conv3_1 = conv(od+dd[0],128, kernel_size=3, stride=1)
        self.conv3_2 = conv(od+dd[1],96,  kernel_size=3, stride=1)
        self.conv3_3 = conv(od+dd[2],64,  kernel_size=3, stride=1)
        self.conv3_4 = conv(od+dd[3],32,  kernel_size=3, stride=1)
        self.predict_flow3 = predict_flow(od+dd[4]) 
        self.deconv3 = deconv(1, 1, kernel_size=4, stride=2, padding=1) 
        self.upfeat3 = deconv(od+dd[4], 1, kernel_size=4, stride=2, padding=1) 
        
        od = nd+32+2
        self.conv2_0 = conv(od,      128, kernel_size=3, stride=1)
        self.conv2_1 = conv(od+dd[0],128, kernel_size=3, stride=1)
        self.conv2_2 = conv(od+dd[1],96,  kernel_size=3, stride=1)
        self.conv2_3 = conv(od+dd[2],64,  kernel_size=3, stride=1)
        self.conv2_4 = conv(od+dd[3],32,  kernel_size=3, stride=1)
        self.predict_flow2 = predict_flow(od+dd[4]) 
        self.deconv2 = deconv(1, 1, kernel_size=4, stride=2, padding=1) 
        
        self.dc_conv1 = conv(od+dd[4], 128, kernel_size=3, stride=1, padding=1,  dilation=1)
        self.dc_conv2 = conv(128,      128, kernel_size=3, stride=1, padding=2,  dilation=2)
        self.dc_conv3 = conv(128,      128, kernel_size=3, stride=1, padding=4,  dilation=4)
        self.dc_conv4 = conv(128,      96,  kernel_size=3, stride=1, padding=8,  dilation=8)
        self.dc_conv5 = conv(96,       64,  kernel_size=3, stride=1, padding=16, dilation=16)
        self.dc_conv6 = conv(64,       32,  kernel_size=3, stride=1, padding=1,  dilation=1)
        self.dc_conv7 = predict_flow(32)

        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d):
                nn.init.kaiming_normal(m.weight.data, mode='fan_in')
                if m.bias is not None:
                    m.bias.data.zero_()


    def warp(self, x, flo):
        """
        warp an image/tensor (im2) back to im1, according to the optical flow

        x: [B, C, H, W] (im2)
        flo: [B, 2, H, W] flow

        """
        x = x[:, :, None, :]
        flo = flo[:, :, None, :]
        print(x.size(), flo.size())
        
        B, C, H, W = x.size()
        # mesh grid 
        xx = torch.arange(0, W).view(1,-1).repeat(H,1)
        yy = torch.arange(0, H).view(-1,1).repeat(1,W)
        xx = xx.view(1,1,H,W).repeat(B,1,1,1)
        yy = yy.view(1,1,H,W).repeat(B,1,1,1)
        grid = torch.cat((xx,yy),1).float()

        if x.is_cuda:
            grid = grid.cuda()
            
        vgrid = Variable(grid) + flo

        # scale grid to [-1,1] 
        vgrid[:,0,:,:] = 2.0*vgrid[:,0,:,:].clone() / max(W-1,1)-1.0
        vgrid[:,1,:,:] = 2.0*vgrid[:,1,:,:].clone() / max(H-1,1)-1.0

        vgrid = vgrid.permute(0,2,3,1)        
        output = nn.functional.grid_sample(x, vgrid)
        mask = torch.autograd.Variable(torch.ones(x.size())).cuda()
        mask = nn.functional.grid_sample(mask, vgrid)
        

        # if W==128:
            # np.save('mask.npy', mask.cpu().data.numpy())
            # np.save('warp.npy', output.cpu().data.numpy())
        
        mask[mask<0.9999] = 0
        mask[mask>0] = 1
        
        return (output*mask).squeeze()


    def forward(self,inputs):
        im1 = inputs[:, :, :, 0:-1].permute((0, 3, 1, 2)).reshape((inputs.size(0)*(inputs.size(3)-1), inputs.size(1), inputs.size(2)))
        im2 = inputs[:, :, :, 1:].permute((0, 3, 1, 2)).reshape((inputs.size(0)*(inputs.size(3)-1), inputs.size(1), inputs.size(2)))
        c11 = self.conv1b(self.conv1aa(self.conv1a(im1)))
        c21 = self.conv1b(self.conv1aa(self.conv1a(im2)))
        c12 = self.conv2b(self.conv2aa(self.conv2a(c11)))
        c22 = self.conv2b(self.conv2aa(self.conv2a(c21)))
        c13 = self.conv3b(self.conv3aa(self.conv3a(c12)))
        c23 = self.conv3b(self.conv3aa(self.conv3a(c22)))
        c14 = self.conv4b(self.conv4aa(self.conv4a(c13)))
        c24 = self.conv4b(self.conv4aa(self.conv4a(c23)))
        c15 = self.conv5b(self.conv5aa(self.conv5a(c14)))
        c25 = self.conv5b(self.conv5aa(self.conv5a(c24)))
        c16 = self.conv6b(self.conv6a(self.conv6aa(c15)))
        c26 = self.conv6b(self.conv6a(self.conv6aa(c25)))


        corr6 = self.corr(c16, c26) 
        corr6 = self.leakyRELU(corr6)   


        x = torch.cat((self.conv6_0(corr6), corr6),1)
        x = torch.cat((self.conv6_1(x), x),1)
        x = torch.cat((self.conv6_2(x), x),1)
        x = torch.cat((self.conv6_3(x), x),1)
        x = torch.cat((self.conv6_4(x), x),1)
        flow6 = self.predict_flow6(x)
        up_flow6 = self.deconv6(flow6)
        up_feat6 = self.upfeat6(x)

        
        warp5 = self.warp(c25, up_flow6*0.625)
        corr5 = self.corr(c15, warp5) 
        corr5 = self.leakyRELU(corr5)
        x = torch.cat((corr5, c15, up_flow6, up_feat6), 1)
        x = torch.cat((self.conv5_0(x), x),1)
        x = torch.cat((self.conv5_1(x), x),1)
        x = torch.cat((self.conv5_2(x), x),1)
        x = torch.cat((self.conv5_3(x), x),1)
        x = torch.cat((self.conv5_4(x), x),1)
        flow5 = self.predict_flow5(x)
        up_flow5 = self.deconv5(flow5)
        up_feat5 = self.upfeat5(x)

       
        warp4 = self.warp(c24, up_flow5*1.25)
        corr4 = self.corr(c14, warp4)  
        corr4 = self.leakyRELU(corr4)
        x = torch.cat((corr4, c14, up_flow5, up_feat5), 1)
        x = torch.cat((self.conv4_0(x), x),1)
        x = torch.cat((self.conv4_1(x), x),1)
        x = torch.cat((self.conv4_2(x), x),1)
        x = torch.cat((self.conv4_3(x), x),1)
        x = torch.cat((self.conv4_4(x), x),1)
        flow4 = self.predict_flow4(x)
        up_flow4 = self.deconv4(flow4)
        up_feat4 = self.upfeat4(x)


        warp3 = self.warp(c23, up_flow4*2.5)
        corr3 = self.corr(c13, warp3) 
        corr3 = self.leakyRELU(corr3)
        

        x = torch.cat((corr3, c13, up_flow4, up_feat4), 1)
        x = torch.cat((self.conv3_0(x), x),1)
        x = torch.cat((self.conv3_1(x), x),1)
        x = torch.cat((self.conv3_2(x), x),1)
        x = torch.cat((self.conv3_3(x), x),1)
        x = torch.cat((self.conv3_4(x), x),1)
        flow3 = self.predict_flow3(x)
        up_flow3 = self.deconv3(flow3)
        up_feat3 = self.upfeat3(x)


        warp2 = self.warp(c22, up_flow3*5.0) 
        corr2 = self.corr(c12, warp2)
        corr2 = self.leakyRELU(corr2)
        x = torch.cat((corr2, c12, up_flow3, up_feat3), 1)
        x = torch.cat((self.conv2_0(x), x),1)
        x = torch.cat((self.conv2_1(x), x),1)
        x = torch.cat((self.conv2_2(x), x),1)
        x = torch.cat((self.conv2_3(x), x),1)
        x = torch.cat((self.conv2_4(x), x),1)
        flow2 = self.predict_flow2(x)
 
        x = self.dc_conv4(self.dc_conv3(self.dc_conv2(self.dc_conv1(x))))
        flow2 = flow2 + self.dc_conv7(self.dc_conv6(self.dc_conv5(x)))
        
        if self.training:
            return flow2,flow3,flow4,flow5,flow6
        else:
            return flow2

class my_1DPWC_3layer_net(nn.Module):
    def __init__(self, md=3, is_corr_func_calc=False):
        """
        input: md --- maximum displacement (for correlation. default: 4), after warpping

        """
        super(my_1DPWC_3layer_net,self).__init__()

        self.conv1a  = conv(1,   16, kernel_size=3, stride=2)
        self.conv1aa = conv(16,  16, kernel_size=3, stride=1)
        self.conv1b  = conv(16,  16, kernel_size=3, stride=1)
        self.conv2a  = conv(16,  32, kernel_size=3, stride=2)
        self.conv2aa = conv(32,  32, kernel_size=3, stride=1)
        self.conv2b  = conv(32,  32, kernel_size=3, stride=1)
        self.conv3a  = conv(32,  64, kernel_size=3, stride=2)
        self.conv3aa = conv(64,  64, kernel_size=3, stride=1)
        self.conv3b  = conv(64,  64, kernel_size=3, stride=1)

        self.corr    = Correlation1d(pad_size=md, kernel_size=11, max_displacement=md, stride1=1, stride2=1, corr_multiply=1)
        self.leakyRELU = nn.LeakyReLU(0.1)
        
        nd = 2*md+1
        dd = np.cumsum([128,128,96,64,32])

        od = nd
        self.conv3_0 = conv(od,      128, kernel_size=3, stride=1)
        self.conv3_1 = conv(od+dd[0],128, kernel_size=3, stride=1)
        self.conv3_2 = conv(od+dd[1],96,  kernel_size=3, stride=1)
        self.conv3_3 = conv(od+dd[2],64,  kernel_size=3, stride=1)
        self.conv3_4 = conv(od+dd[3],32,  kernel_size=3, stride=1)        
        self.predict_flow3 = predict_flow(od+dd[4])
        self.deconv3 = deconv(1, 1, kernel_size=4, stride=2, padding=1) 
        self.upfeat3 = deconv(od+dd[4], 1, kernel_size=4, stride=2, padding=1) 
        
        od = nd+32+2
        self.conv2_0 = conv(od,      128, kernel_size=3, stride=1)
        self.conv2_1 = conv(od+dd[0],128, kernel_size=3, stride=1)
        self.conv2_2 = conv(od+dd[1],96,  kernel_size=3, stride=1)
        self.conv2_3 = conv(od+dd[2],64,  kernel_size=3, stride=1)
        self.conv2_4 = conv(od+dd[3],32,  kernel_size=3, stride=1)
        self.predict_flow2 = predict_flow(od+dd[4]) 
        # self.deconv2 = deconv(2, 2, kernel_size=4, stride=2, padding=1)
        
        self.dc_conv1 = conv(od+dd[4], 128, kernel_size=3, stride=1, padding=1,  dilation=1)
        self.dc_conv2 = conv(128,      128, kernel_size=3, stride=1, padding=2,  dilation=2)
        self.dc_conv3 = conv(128,      128, kernel_size=3, stride=1, padding=4,  dilation=4)
        self.dc_conv4 = conv(128,      96,  kernel_size=3, stride=1, padding=8,  dilation=8)
        self.dc_conv5 = conv(96,       64,  kernel_size=3, stride=1, padding=16, dilation=16)
        self.dc_conv6 = conv(64,       32,  kernel_size=3, stride=1, padding=1,  dilation=1)
        self.dc_conv7 = predict_flow(32)

        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d):
                nn.init.kaiming_normal(m.weight.data, mode='fan_in')
                if m.bias is not None:
                    m.bias.data.zero_()


    def warp(self, x, flo):
        """
        warp an image/tensor (im2) back to im1, according to the optical flow

        x: [B, C, H, W] (im2)
        flo: [B, 2, H, W] flow

        """
        x = x[:, :, None, :]
        flo = flo[:, :, None, :]
        flo = torch.cat((flo, torch.zeros((flo.size())).cuda()), 1)
        
        B, C, H, W = x.size()
        # mesh grid 
        xx = torch.arange(0, W).view(1,-1).repeat(H,1)
        yy = torch.arange(0, H).view(-1,1).repeat(1,W)
        xx = xx.view(1,1,H,W).repeat(B,1,1,1)
        yy = yy.view(1,1,H,W).repeat(B,1,1,1)
        grid = torch.cat((xx,yy),1).float()

        if x.is_cuda:
            grid = grid.cuda()
            
        vgrid = Variable(grid) + flo

        # scale grid to [-1,1] 
        vgrid[:,0,:,:] = 2.0*vgrid[:,0,:,:].clone() / max(W-1,1)-1.0
        
        # 這邊不-1.0是為讓y軸的grid維持0，否則會讓他變成-1
        vgrid[:,1,:,:] = 2.0*vgrid[:,1,:,:].clone() / max(H-1,1)

        vgrid = vgrid.permute(0,2,3,1)        
        output = nn.functional.grid_sample(x, vgrid)
        mask = torch.autograd.Variable(torch.ones(x.size())).cuda()
        mask = nn.functional.grid_sample(mask, vgrid)
        

        # if W==128:
            # np.save('mask.npy', mask.cpu().data.numpy())
            # np.save('warp.npy', output.cpu().data.numpy())
        
        mask[mask<0.9999] = 0
        mask[mask>0] = 1
        
        return (output*mask).squeeze()


    def forward(self,inputs):
        im1 = inputs[:, :, :, 0:-1].permute((0, 3, 1, 2)).reshape((inputs.size(0)*(inputs.size(3)-1), inputs.size(1), inputs.size(2)))
        im2 = inputs[:, :, :, 1:].permute((0, 3, 1, 2)).reshape((inputs.size(0)*(inputs.size(3)-1), inputs.size(1), inputs.size(2)))
        c11 = self.conv1b(self.conv1aa(self.conv1a(im1)))
        c21 = self.conv1b(self.conv1aa(self.conv1a(im2)))
        c12 = self.conv2b(self.conv2aa(self.conv2a(c11)))
        c22 = self.conv2b(self.conv2aa(self.conv2a(c21)))
        c13 = self.conv3b(self.conv3aa(self.conv3a(c12)))
        c23 = self.conv3b(self.conv3aa(self.conv3a(c22)))

        corr3 = self.corr(c13, c23) 
        corr3 = self.leakyRELU(corr3)

        x = torch.cat((self.conv3_0(corr3), corr3),1)
        x = torch.cat((self.conv3_1(x), x),1)
        x = torch.cat((self.conv3_2(x), x),1)
        x = torch.cat((self.conv3_3(x), x),1)
        x = torch.cat((self.conv3_4(x), x),1)
        flow3 = self.predict_flow3(x)
        up_flow3 = self.deconv3(flow3)
        up_feat3 = self.upfeat3(x)
        
        warp2 = self.warp(c22, up_flow3*0.625)
        corr2 = self.corr(c12, warp2)
        x = torch.cat((corr2, c12, up_flow3, up_feat3), 1)
        x = torch.cat((self.conv2_0(x), x),1)
        x = torch.cat((self.conv2_1(x), x),1)
        x = torch.cat((self.conv2_2(x), x),1)
        x = torch.cat((self.conv2_3(x), x),1)
        x = torch.cat((self.conv2_4(x), x),1)
        flow2 = self.predict_flow2(x)
 
        x = self.dc_conv4(self.dc_conv3(self.dc_conv2(self.dc_conv1(x))))
        flow2 = flow2 + self.dc_conv7(self.dc_conv6(self.dc_conv5(x)))
        
        # return flow2
        if self.training:
            return flow2, flow3
        else:
            return flow2
        
class flownetS(nn.Module):
    expansion = 1
    def __init__(self,batchNorm=True):
        super(flownetS, self).__init__()
        self.batchNorm = batchNorm
        self.conv1   = conv(2,   64, kernel_size=7, padding=3, stride=2, bias=False)
        self.conv2   = conv(64,  128, kernel_size=5, padding=2, stride=2, bias=False)
        self.conv3   = conv(128,  256, kernel_size=5, padding=2, stride=2, bias=False)
        self.conv3_1 = conv(256,  256, bias=False)
        self.conv4   = conv(256,  512, stride=2, bias=False)
        self.conv4_1 = conv(512,  512, bias=False)
        self.conv5   = conv(512,  512, stride=2, bias=False)
        self.conv5_1 = conv(512,  512, bias=False)
        self.conv6   = conv(512, 1024, stride=2, bias=False)
        self.conv6_1 = conv(1024, 1024, bias=False)

        self.deconv5 = deconv(1024,512, bias=False)
        self.deconv4 = deconv(1025,256, bias=False)
        self.deconv3 = deconv(769,128, bias=False)
        self.deconv2 = deconv(385,64, bias=False)

        self.predict_flow6 = predict_flow(1024, bias=False)
        self.predict_flow5 = predict_flow(1025, bias=False)
        self.predict_flow4 = predict_flow(769, bias=False)
        self.predict_flow3 = predict_flow(385, bias=False)
        self.predict_flow2 = predict_flow(193, bias=False)

        self.upsampled_flow6_to_5 = nn.ConvTranspose1d(1, 1, 4, 2, 1, bias=False)
        self.upsampled_flow5_to_4 = nn.ConvTranspose1d(1, 1, 4, 2, 1, bias=False)
        self.upsampled_flow4_to_3 = nn.ConvTranspose1d(1, 1, 4, 2, 1, bias=False)
        self.upsampled_flow3_to_2 = nn.ConvTranspose1d(1, 1, 4, 2, 1, bias=False)

        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d):
                nn.init.kaiming_normal_(m.weight, 0.1)
                if m.bias is not None:
                    nn.init.constant_(m.bias, 0)
            elif isinstance(m, nn.BatchNorm1d):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)

    def forward(self, inputs):
        im1 = inputs[:, :, :, 0:-1].permute((0, 3, 1, 2)).reshape((inputs.size(0)*(inputs.size(3)-1), inputs.size(1), inputs.size(2)))
        im2 = inputs[:, :, :, 1:].permute((0, 3, 1, 2)).reshape((inputs.size(0)*(inputs.size(3)-1), inputs.size(1), inputs.size(2)))
        x = torch.cat((im1, im2), 1)
        out_conv2 = self.conv2(self.conv1(x))
        out_conv3 = self.conv3_1(self.conv3(out_conv2))
        out_conv4 = self.conv4_1(self.conv4(out_conv3))
        out_conv5 = self.conv5_1(self.conv5(out_conv4))
        out_conv6 = self.conv6_1(self.conv6(out_conv5))

        flow6       = self.predict_flow6(out_conv6)
        flow6_up    = crop_like(self.upsampled_flow6_to_5(flow6), out_conv5)
        out_deconv5 = crop_like(self.deconv5(out_conv6), out_conv5)

        concat5 = torch.cat((out_conv5,out_deconv5,flow6_up),1)
        flow5       = self.predict_flow5(concat5)
        flow5_up    = crop_like(self.upsampled_flow5_to_4(flow5), out_conv4)
        out_deconv4 = crop_like(self.deconv4(concat5), out_conv4)

        concat4 = torch.cat((out_conv4,out_deconv4,flow5_up),1)
        flow4       = self.predict_flow4(concat4)
        flow4_up    = crop_like(self.upsampled_flow4_to_3(flow4), out_conv3)
        out_deconv3 = crop_like(self.deconv3(concat4), out_conv3)

        concat3 = torch.cat((out_conv3,out_deconv3,flow4_up),1)
        flow3       = self.predict_flow3(concat3)
        flow3_up    = crop_like(self.upsampled_flow3_to_2(flow3), out_conv2)
        out_deconv2 = crop_like(self.deconv2(concat3), out_conv2)

        concat2 = torch.cat((out_conv2,out_deconv2,flow3_up),1)
        flow2 = self.predict_flow2(concat2)

        if self.training:
            return flow2,flow3,flow4,flow5,flow6
        else:
            return flow2

    def weight_parameters(self):
        return [param for name, param in self.named_parameters() if 'weight' in name]

    def bias_parameters(self):
        return [param for name, param in self.named_parameters() if 'bias' in name]
        
class FlowNetC(nn.Module):
    expansion = 1

    def __init__(self,md=5,batchNorm=True, is_corr_func_calc=False):
        super(FlowNetC,self).__init__()
        self.md = md
        self.batchNorm = batchNorm
        if is_corr_func_calc is True:
            self.conv1      = conv(2,   64, kernel_size=7, padding=3, stride=2, bias=False)
        else:
            self.conv1      = conv(1,   64, kernel_size=7, padding=3, stride=2, bias=False)
        self.conv2      = conv(64,  128, kernel_size=5, padding=2, stride=2, bias=False)
        self.conv3      = conv(128,  256, kernel_size=5, padding=2, stride=2, bias=False)
        self.conv_redir = conv(256,   32, kernel_size=1, padding=0, stride=1, bias=False)

        self.conv3_1 = conv((self.md*2+1)+32,  256, bias=False)
        self.conv4   = conv(256,  512, stride=2, bias=False)
        self.conv4_1 = conv(512,  512, bias=False)
        self.conv5   = conv(512,  512, stride=2, bias=False)
        self.conv5_1 = conv(512,  512, bias=False)
        self.conv6   = conv(512, 1024, stride=2, bias=False)
        self.conv6_1 = conv(1024, 1024, bias=False)

        self.deconv5 = deconv(1024,512, bias=False)
        self.deconv4 = deconv(1025,256, bias=False)
        self.deconv3 = deconv(769,128, bias=False)
        self.deconv2 = deconv(385,64, bias=False)

        self.predict_flow6 = predict_flow(1024, bias=False)
        self.predict_flow5 = predict_flow(1025, bias=False)
        self.predict_flow4 = predict_flow(769, bias=False)
        self.predict_flow3 = predict_flow(385, bias=False)
        self.predict_flow2 = predict_flow(193, bias=False)

        self.upsampled_flow6_to_5 = nn.ConvTranspose1d(1, 1, 4, 2, 1, bias=False)
        self.upsampled_flow5_to_4 = nn.ConvTranspose1d(1, 1, 4, 2, 1, bias=False)
        self.upsampled_flow4_to_3 = nn.ConvTranspose1d(1, 1, 4, 2, 1, bias=False)
        self.upsampled_flow3_to_2 = nn.ConvTranspose1d(1, 1, 4, 2, 1, bias=False)

        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d):
                nn.init.kaiming_normal_(m.weight, 0.1)
                if m.bias is not None:
                    nn.init.constant_(m.bias, 0)
            elif isinstance(m, nn.BatchNorm1d):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)

    def forward(self, inputs):
        im1 = inputs[:, :, :, 0:-1].permute((0, 3, 1, 2)).reshape((inputs.size(0)*(inputs.size(3)-1), inputs.size(1), inputs.size(2)))
        im2 = inputs[:, :, :, 1:].permute((0, 3, 1, 2)).reshape((inputs.size(0)*(inputs.size(3)-1), inputs.size(1), inputs.size(2)))

        out_conv1a = self.conv1(im1)
        out_conv2a = self.conv2(out_conv1a)
        out_conv3a = self.conv3(out_conv2a)

        out_conv1b = self.conv1(im2)
        out_conv2b = self.conv2(out_conv1b)
        out_conv3b = self.conv3(out_conv2b)

        out_conv_redir = self.conv_redir(out_conv3a)
        out_correlation = correlate(out_conv3a,out_conv3b,self.md)

        in_conv3_1 = torch.cat([out_conv_redir, out_correlation], dim=1)

        out_conv3 = self.conv3_1(in_conv3_1)
        out_conv4 = self.conv4_1(self.conv4(out_conv3))
        out_conv5 = self.conv5_1(self.conv5(out_conv4))
        out_conv6 = self.conv6_1(self.conv6(out_conv5))

        flow6       = self.predict_flow6(out_conv6)
        flow6_up    = crop_like(self.upsampled_flow6_to_5(flow6), out_conv5)
        out_deconv5 = crop_like(self.deconv5(out_conv6), out_conv5)

        concat5 = torch.cat((out_conv5,out_deconv5,flow6_up),1)
        flow5       = self.predict_flow5(concat5)
        flow5_up    = crop_like(self.upsampled_flow5_to_4(flow5), out_conv4)
        out_deconv4 = crop_like(self.deconv4(concat5), out_conv4)

        concat4 = torch.cat((out_conv4,out_deconv4,flow5_up),1)
        flow4       = self.predict_flow4(concat4)
        flow4_up    = crop_like(self.upsampled_flow4_to_3(flow4), out_conv3)
        out_deconv3 = crop_like(self.deconv3(concat4), out_conv3)

        concat3 = torch.cat((out_conv3,out_deconv3,flow4_up),1)
        flow3       = self.predict_flow3(concat3)
        flow3_up    = crop_like(self.upsampled_flow3_to_2(flow3), out_conv2a)
        out_deconv2 = crop_like(self.deconv2(concat3), out_conv2a)

        concat2 = torch.cat((out_conv2a,out_deconv2,flow3_up),1)
        flow2 = self.predict_flow2(concat2)

        if self.training:
            return flow2,flow3,flow4,flow5,flow6
        else:
            return flow2

    def weight_parameters(self):
        return [param for name, param in self.named_parameters() if 'weight' in name]

    def bias_parameters(self):
        return [param for name, param in self.named_parameters() if 'bias' in name]
    
class FlowNetC_hollow(nn.Module):
    expansion = 1

    def __init__(self,md=5,batchNorm=True, is_corr_func_calc=False):
        super(FlowNetC_hollow,self).__init__()
        self.md = md
        self.batchNorm = batchNorm
        if is_corr_func_calc is True:
            self.conv1      = conv(2,   32, kernel_size=7, padding=3, stride=2, bias=False)
        else:
            self.conv1      = conv(1,   32, kernel_size=7, padding=3, stride=2, bias=False)
        self.conv2      = conv(32,  64, kernel_size=5, padding=2, stride=2, bias=False)
        self.conv_redir = conv(64,   16, kernel_size=1, padding=0, stride=1, bias=False)

        self.conv2_1 = conv((self.md*2+1)+16,  128, bias=False)
        self.conv3   = conv(128,  256, stride=2, bias=False)
        self.conv3_1 = conv(256,  256, bias=False)

        self.deconv2 = deconv(256,64, bias=False)
        self.deconv1 = deconv(193,32, bias=False)

        self.predict_flow3 = predict_flow(256, bias=False)
        self.predict_flow2 = predict_flow(193, bias=False)
        self.predict_flow1 = predict_flow(65, bias=False)

        self.upsampled_flow3_to_2 = nn.ConvTranspose1d(1, 1, 4, 2, 1, bias=False)
        self.upsampled_flow2_to_1 = nn.ConvTranspose1d(1, 1, 4, 2, 1, bias=False)

        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d):
                nn.init.kaiming_normal_(m.weight, 0.1)
                if m.bias is not None:
                    nn.init.constant_(m.bias, 0)
            elif isinstance(m, nn.BatchNorm1d):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)

    def forward(self, inputs):
        im1 = inputs[:, :, :, 0:-1].permute((0, 3, 1, 2)).reshape((inputs.size(0)*(inputs.size(3)-1), inputs.size(1), inputs.size(2)))
        im2 = inputs[:, :, :, 1:].permute((0, 3, 1, 2)).reshape((inputs.size(0)*(inputs.size(3)-1), inputs.size(1), inputs.size(2)))

        out_conv1a = self.conv1(im1)
        out_conv2a = self.conv2(out_conv1a)

        out_conv1b = self.conv1(im2)
        out_conv2b = self.conv2(out_conv1b)

        out_conv_redir = self.conv_redir(out_conv2a)
        out_correlation = correlate(out_conv2a,out_conv2b,self.md)

        in_conv2_1 = torch.cat([out_conv_redir, out_correlation], dim=1)

        out_conv2 = self.conv2_1(in_conv2_1)
        out_conv3 = self.conv3_1(self.conv3(out_conv2))

        flow3       = self.predict_flow3(out_conv3)
        flow3_up    = crop_like(self.upsampled_flow3_to_2(flow3), out_conv2)
        out_deconv2 = crop_like(self.deconv2(out_conv3), out_conv2)

        concat2 = torch.cat((out_conv2,out_deconv2,flow3_up),1)
        flow2       = self.predict_flow2(concat2)
        flow2_up    = crop_like(self.upsampled_flow2_to_1(flow2), out_conv1a)
        out_deconv1 = crop_like(self.deconv1(concat2), out_conv1a)

        concat1 = torch.cat((out_conv1a,out_deconv1,flow2_up),1)
        flow1 = self.predict_flow1(concat1)

        if self.training:
            return flow1, flow2,flow3
        else:
            return flow1

    def weight_parameters(self):
        return [param for name, param in self.named_parameters() if 'weight' in name]

    def bias_parameters(self):
        return [param for name, param in self.named_parameters() if 'bias' in name]    
    
class FlowNetC_hollow_1(nn.Module):
    expansion = 1

    def __init__(self,md=5,batchNorm=True, is_corr_func_calc=False):
        super(FlowNetC_hollow_1,self).__init__()
        self.md = md
        self.batchNorm = batchNorm
        if is_corr_func_calc is True:
            self.conv1      = conv(2,   32, kernel_size=7, padding=3, stride=2, bias=False)
        else:
            self.conv1      = conv(1,   32, kernel_size=7, padding=3, stride=2, bias=False)
        self.conv2      = conv(32,  64, kernel_size=5, padding=2, stride=2, bias=False)
        self.conv3      = conv(64,  128, kernel_size=5, padding=2, stride=2, bias=False)
        self.conv_redir = conv(128,   16, kernel_size=1, padding=0, stride=1, bias=False)

        self.conv3_1 = conv((self.md*2+1)+16,  128, bias=False)
        self.conv4   = conv(128,  256, stride=2, bias=False)
        self.conv4_1 = conv(256,  256, bias=False)

        self.deconv3 = deconv(256,64, bias=False)
        self.deconv2 = deconv(193,32, bias=False)

        self.predict_flow4 = predict_flow(256, bias=False)
        self.predict_flow3 = predict_flow(193, bias=False)
        self.predict_flow2 = predict_flow(97, bias=False)

        self.upsampled_flow4_to_3 = nn.ConvTranspose1d(1, 1, 4, 2, 1, bias=False)
        self.upsampled_flow3_to_2 = nn.ConvTranspose1d(1, 1, 4, 2, 1, bias=False)

        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d):
                nn.init.kaiming_normal_(m.weight, 0.1)
                if m.bias is not None:
                    nn.init.constant_(m.bias, 0)
            elif isinstance(m, nn.BatchNorm1d):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)

    def forward(self, inputs):
        im1 = inputs[:, :, :, 0:-1].permute((0, 3, 1, 2)).reshape((inputs.size(0)*(inputs.size(3)-1), inputs.size(1), inputs.size(2)))
        im2 = inputs[:, :, :, 1:].permute((0, 3, 1, 2)).reshape((inputs.size(0)*(inputs.size(3)-1), inputs.size(1), inputs.size(2)))

        out_conv1a = self.conv1(im1)
        out_conv2a = self.conv2(out_conv1a)
        out_conv3a = self.conv3(out_conv2a)

        out_conv1b = self.conv1(im2)
        out_conv2b = self.conv2(out_conv1b)
        out_conv3b = self.conv3(out_conv2b)

        out_conv_redir = self.conv_redir(out_conv3a)
        out_correlation = correlate(out_conv3a,out_conv3b,self.md)

        in_conv3_1 = torch.cat([out_conv_redir, out_correlation], dim=1)

        out_conv3 = self.conv3_1(in_conv3_1)
        out_conv4 = self.conv4_1(self.conv4(out_conv3))

        flow4       = self.predict_flow4(out_conv4)
        flow4_up    = crop_like(self.upsampled_flow4_to_3(flow4), out_conv3)
        out_deconv3 = crop_like(self.deconv3(out_conv4), out_conv3)

        concat3 = torch.cat((out_conv3,out_deconv3,flow4_up),1)
        flow3       = self.predict_flow3(concat3)
        flow3_up    = crop_like(self.upsampled_flow3_to_2(flow3), out_conv2a)
        out_deconv2 = crop_like(self.deconv2(concat3), out_conv2a)

        concat2 = torch.cat((out_conv2a,out_deconv2,flow3_up),1)
        flow2 = self.predict_flow2(concat2)

        if self.training:
            return flow2, flow3,flow4
        else:
            return flow2

    def weight_parameters(self):
        return [param for name, param in self.named_parameters() if 'weight' in name]

    def bias_parameters(self):
        return [param for name, param in self.named_parameters() if 'bias' in name]        

class FlowNetC_hollow_2(nn.Module):
    expansion = 1

    def __init__(self,md=5,batchNorm=True, is_corr_func_calc=False):
        super(FlowNetC_hollow_2,self).__init__()
        self.md = md
        self.batchNorm = batchNorm
        if is_corr_func_calc is True:
            self.conv1      = conv(2,   32, kernel_size=7, padding=3, stride=2, bias=False)
        else:
            self.conv1      = conv(1,   32, kernel_size=7, padding=3, stride=2, bias=False)
        self.conv2      = conv(32,  64, kernel_size=5, padding=2, stride=2, bias=False)
        self.conv3      = conv(64,  128, kernel_size=5, padding=2, stride=2, bias=False)
        self.conv4      = conv(128,  256, kernel_size=5, padding=2, stride=2, bias=False)
        self.conv_redir = conv(256,   32, kernel_size=1, padding=0, stride=1, bias=False)

        self.conv4_1 = conv((self.md*2+1)+32,  128, bias=False)
        self.conv5   = conv(128,  256, stride=2, bias=False)
        self.conv5_1 = conv(256,  256, bias=False)

        self.deconv4 = deconv(256,64, bias=False)
        self.deconv3 = deconv(193,32, bias=False)
        self.deconv2 = deconv(161,16, bias=False)

        self.predict_flow5 = predict_flow(256, bias=False)
        self.predict_flow4 = predict_flow(193, bias=False)
        self.predict_flow3 = predict_flow(161, bias=False)
        self.predict_flow2 = predict_flow(81, bias=False)

        self.upsampled_flow5_to_4 = nn.ConvTranspose1d(1, 1, 4, 2, 1, bias=False)
        self.upsampled_flow4_to_3 = nn.ConvTranspose1d(1, 1, 4, 2, 1, bias=False)
        self.upsampled_flow3_to_2 = nn.ConvTranspose1d(1, 1, 4, 2, 1, bias=False)

        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d):
                nn.init.kaiming_normal_(m.weight, 0.1)
                if m.bias is not None:
                    nn.init.constant_(m.bias, 0)
            elif isinstance(m, nn.BatchNorm1d):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)

    def forward(self, inputs):
        im1 = inputs[:, :, :, 0:-1].permute((0, 3, 1, 2)).reshape((inputs.size(0)*(inputs.size(3)-1), inputs.size(1), inputs.size(2)))
        im2 = inputs[:, :, :, 1:].permute((0, 3, 1, 2)).reshape((inputs.size(0)*(inputs.size(3)-1), inputs.size(1), inputs.size(2)))

        out_conv1a = self.conv1(im1)
        out_conv2a = self.conv2(out_conv1a)
        out_conv3a = self.conv3(out_conv2a)
        out_conv4a = self.conv4(out_conv3a)

        out_conv1b = self.conv1(im2)
        out_conv2b = self.conv2(out_conv1b)
        out_conv3b = self.conv3(out_conv2b)
        out_conv4b = self.conv4(out_conv3b)

        out_conv_redir = self.conv_redir(out_conv4a)
        out_correlation = correlate(out_conv4a,out_conv4b,self.md)

        in_conv4_1 = torch.cat([out_conv_redir, out_correlation], dim=1)

        out_conv4 = self.conv4_1(in_conv4_1)
        out_conv5 = self.conv5_1(self.conv5(out_conv4))

        flow5       = self.predict_flow5(out_conv5)
        flow5_up    = crop_like(self.upsampled_flow5_to_4(flow5), out_conv4)
        out_deconv4 = crop_like(self.deconv4(out_conv5), out_conv4)
        
        concat4 = torch.cat((out_conv4,out_deconv4,flow5_up),1)
        flow4       = self.predict_flow4(concat4)
        flow4_up    = crop_like(self.upsampled_flow4_to_3(flow4), out_conv3a)
        out_deconv3 = crop_like(self.deconv3(concat4), out_conv3a)

        concat3 = torch.cat((out_conv3a,out_deconv3,flow4_up),1)
        flow3       = self.predict_flow3(concat3)
        flow3_up    = crop_like(self.upsampled_flow3_to_2(flow3), out_conv2a)
        out_deconv2 = crop_like(self.deconv2(concat3), out_conv2a)

        concat2 = torch.cat((out_conv2a,out_deconv2,flow3_up),1)
        flow2 = self.predict_flow2(concat2)

        if self.training:
            return flow2, flow3, flow4, flow5
        else:
            return flow2

    def weight_parameters(self):
        return [param for name, param in self.named_parameters() if 'weight' in name]

    def bias_parameters(self):
        return [param for name, param in self.named_parameters() if 'bias' in name]  

class FlowNetC_hollow_3(nn.Module):
    expansion = 1

    def __init__(self,md=5,batchNorm=True, is_corr_func_calc=False):
        super(FlowNetC_hollow_3,self).__init__()
        self.md = md
        self.batchNorm = batchNorm
        if is_corr_func_calc is True:
            self.conv1      = conv(2,   64, kernel_size=7, padding=3, stride=2, bias=False)
        else:
            self.conv1      = conv(1,   64, kernel_size=7, padding=3, stride=2, bias=False)
        self.conv2      = conv(64,  96, kernel_size=5, padding=2, stride=2, bias=False)
        self.conv3      = conv(96,  128, kernel_size=5, padding=2, stride=2, bias=False)
        self.conv_redir = conv(128,   16, kernel_size=1, padding=0, stride=1, bias=False)

        self.conv3_1 = conv((self.md*2+1)+16,  128, bias=False)
        self.conv4   = conv(128,  256, stride=2, bias=False)
        self.conv4_1 = conv(256,  256, bias=False)

        self.deconv3 = deconv(256,64, bias=False)
        self.deconv2 = deconv(193,32, bias=False)

        self.predict_flow4 = predict_flow(256, bias=False)
        self.predict_flow3 = predict_flow(193, bias=False)
        self.predict_flow2 = predict_flow(129, bias=False)

        self.upsampled_flow4_to_3 = nn.ConvTranspose1d(1, 1, 4, 2, 1, bias=False)
        self.upsampled_flow3_to_2 = nn.ConvTranspose1d(1, 1, 4, 2, 1, bias=False)

        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d):
                nn.init.kaiming_normal_(m.weight, 0.1)
                if m.bias is not None:
                    nn.init.constant_(m.bias, 0)
            elif isinstance(m, nn.BatchNorm1d):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)

    def forward(self, inputs):
        im1 = inputs[:, :, :, 0:-1].permute((0, 3, 1, 2)).reshape((inputs.size(0)*(inputs.size(3)-1), inputs.size(1), inputs.size(2)))
        im2 = inputs[:, :, :, 1:].permute((0, 3, 1, 2)).reshape((inputs.size(0)*(inputs.size(3)-1), inputs.size(1), inputs.size(2)))

        out_conv1a = self.conv1(im1)
        out_conv2a = self.conv2(out_conv1a)
        out_conv3a = self.conv3(out_conv2a)

        out_conv1b = self.conv1(im2)
        out_conv2b = self.conv2(out_conv1b)
        out_conv3b = self.conv3(out_conv2b)

        out_conv_redir = self.conv_redir(out_conv3a)
        out_correlation = correlate(out_conv3a,out_conv3b,self.md)

        in_conv3_1 = torch.cat([out_conv_redir, out_correlation], dim=1)

        out_conv3 = self.conv3_1(in_conv3_1)
        out_conv4 = self.conv4_1(self.conv4(out_conv3))

        flow4       = self.predict_flow4(out_conv4)
        flow4_up    = crop_like(self.upsampled_flow4_to_3(flow4), out_conv3)
        out_deconv3 = crop_like(self.deconv3(out_conv4), out_conv3)

        concat3 = torch.cat((out_conv3,out_deconv3,flow4_up),1)
        flow3       = self.predict_flow3(concat3)
        flow3_up    = crop_like(self.upsampled_flow3_to_2(flow3), out_conv2a)
        out_deconv2 = crop_like(self.deconv2(concat3), out_conv2a)

        concat2 = torch.cat((out_conv2a,out_deconv2,flow3_up),1)
        flow2 = self.predict_flow2(concat2)

        if self.training:
            return flow2, flow3,flow4
        else:
            return flow2

    def weight_parameters(self):
        return [param for name, param in self.named_parameters() if 'weight' in name]

    def bias_parameters(self):
        return [param for name, param in self.named_parameters() if 'bias' in name]        

class FlowNetC_slim_1(nn.Module):
    expansion = 1

    def __init__(self,md=5,batchNorm=True, is_corr_func_calc=False):
        super(FlowNetC_slim_1,self).__init__()
        self.md = md
        self.batchNorm = batchNorm
        if is_corr_func_calc is True:
            self.conv1      = conv(2,   16, kernel_size=7, padding=3, stride=2, bias=False)
        else:
            self.conv1      = conv(1,   16, kernel_size=7, padding=3, stride=2, bias=False)
        self.conv2      = conv(16,  32, kernel_size=5, padding=2, stride=2, bias=False)
        self.conv3      = conv(32,  64, kernel_size=5, padding=2, stride=2, bias=False)
        self.conv_redir = conv(64,   16, kernel_size=1, padding=0, stride=1, bias=False)

        self.conv3_1 = conv((self.md*2+1)+16,  32, bias=False)
        self.conv4   = conv(32,  64, stride=2, bias=False)
        self.conv4_1 = conv(64,  64, bias=False)
        self.conv5   = conv(64,  128, stride=2, bias=False)
        self.conv5_1 = conv(128,  128, bias=False)
        self.conv6   = conv(128, 256, stride=2, bias=False)
        self.conv6_1 = conv(256, 256, bias=False)

        self.deconv5 = deconv(256, 128, bias=False)
        self.deconv4 = deconv(257,64, bias=False)
        self.deconv3 = deconv(129,32, bias=False)
        self.deconv2 = deconv(65,16, bias=False)

        self.predict_flow6 = predict_flow(256, bias=False)
        self.predict_flow5 = predict_flow(257, bias=False)
        self.predict_flow4 = predict_flow(129, bias=False)
        self.predict_flow3 = predict_flow(65, bias=False)
        self.predict_flow2 = predict_flow(49, bias=False)

        self.upsampled_flow6_to_5 = nn.ConvTranspose1d(1, 1, 4, 2, 1, bias=False)
        self.upsampled_flow5_to_4 = nn.ConvTranspose1d(1, 1, 4, 2, 1, bias=False)
        self.upsampled_flow4_to_3 = nn.ConvTranspose1d(1, 1, 4, 2, 1, bias=False)
        self.upsampled_flow3_to_2 = nn.ConvTranspose1d(1, 1, 4, 2, 1, bias=False)

        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d):
                nn.init.kaiming_normal_(m.weight, 0.1)
                if m.bias is not None:
                    nn.init.constant_(m.bias, 0)
            elif isinstance(m, nn.BatchNorm1d):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)

    def forward(self, inputs):
        im1 = inputs[:, :, :, 0:-1].permute((0, 3, 1, 2)).reshape((inputs.size(0)*(inputs.size(3)-1), inputs.size(1), inputs.size(2)))
        im2 = inputs[:, :, :, 1:].permute((0, 3, 1, 2)).reshape((inputs.size(0)*(inputs.size(3)-1), inputs.size(1), inputs.size(2)))

        out_conv1a = self.conv1(im1)
        out_conv2a = self.conv2(out_conv1a)
        out_conv3a = self.conv3(out_conv2a)

        out_conv1b = self.conv1(im2)
        out_conv2b = self.conv2(out_conv1b)
        out_conv3b = self.conv3(out_conv2b)

        out_conv_redir = self.conv_redir(out_conv3a)
        out_correlation = correlate(out_conv3a,out_conv3b,self.md)

        in_conv3_1 = torch.cat([out_conv_redir, out_correlation], dim=1)

        out_conv3 = self.conv3_1(in_conv3_1)
        out_conv4 = self.conv4_1(self.conv4(out_conv3))
        out_conv5 = self.conv5_1(self.conv5(out_conv4))
        out_conv6 = self.conv6_1(self.conv6(out_conv5))

        flow6       = self.predict_flow6(out_conv6)
        flow6_up    = crop_like(self.upsampled_flow6_to_5(flow6), out_conv5)
        out_deconv5 = crop_like(self.deconv5(out_conv6), out_conv5)

        concat5 = torch.cat((out_conv5,out_deconv5,flow6_up),1)
        flow5       = self.predict_flow5(concat5)
        flow5_up    = crop_like(self.upsampled_flow5_to_4(flow5), out_conv4)
        out_deconv4 = crop_like(self.deconv4(concat5), out_conv4)

        concat4 = torch.cat((out_conv4,out_deconv4,flow5_up),1)
        flow4       = self.predict_flow4(concat4)
        flow4_up    = crop_like(self.upsampled_flow4_to_3(flow4), out_conv3)
        out_deconv3 = crop_like(self.deconv3(concat4), out_conv3)

        concat3 = torch.cat((out_conv3,out_deconv3,flow4_up),1)
        flow3       = self.predict_flow3(concat3)
        flow3_up    = crop_like(self.upsampled_flow3_to_2(flow3), out_conv2a)
        out_deconv2 = crop_like(self.deconv2(concat3), out_conv2a)

        concat2 = torch.cat((out_conv2a,out_deconv2,flow3_up),1)
        flow2 = self.predict_flow2(concat2)

        if self.training:
            return flow2,flow3,flow4,flow5,flow6
        else:
            return flow2

    def weight_parameters(self):
        return [param for name, param in self.named_parameters() if 'weight' in name]

    def bias_parameters(self):
        return [param for name, param in self.named_parameters() if 'bias' in name]

class FlowNetC_slim_2(nn.Module):
    expansion = 1

    def __init__(self,md=5,batchNorm=True, is_corr_func_calc=False):
        super(FlowNetC_slim_2,self).__init__()
        self.md = md
        self.batchNorm = batchNorm
        if is_corr_func_calc is True:
            self.conv1      = conv(2,   16, kernel_size=7, padding=3, stride=2, bias=False)
        else:
            self.conv1      = conv(1,   16, kernel_size=7, padding=3, stride=2, bias=False)
        self.conv2      = conv(16,  32, kernel_size=5, padding=2, stride=2, bias=False)
        self.conv3      = conv(32,  64, kernel_size=5, padding=2, stride=2, bias=False)
        self.conv_redir = conv(64,   16, kernel_size=1, padding=0, stride=1, bias=False)

        self.conv3_1 = conv((self.md*2+1)+16,  32, bias=False)
        self.conv4   = conv(32,  64, stride=2, bias=False)
        self.conv4_1 = conv(64,  64, bias=False)
        self.conv5   = conv(64,  128, stride=2, bias=False)
        self.conv5_1 = conv(128,  128, bias=False)

        self.deconv4 = deconv(128,64, bias=False)
        self.deconv3 = deconv(129,32, bias=False)
        self.deconv2 = deconv(65,16, bias=False)

        self.predict_flow5 = predict_flow(128, bias=False)
        self.predict_flow4 = predict_flow(129, bias=False)
        self.predict_flow3 = predict_flow(65, bias=False)
        self.predict_flow2 = predict_flow(49, bias=False)

        self.upsampled_flow5_to_4 = nn.ConvTranspose1d(1, 1, 4, 2, 1, bias=False)
        self.upsampled_flow4_to_3 = nn.ConvTranspose1d(1, 1, 4, 2, 1, bias=False)
        self.upsampled_flow3_to_2 = nn.ConvTranspose1d(1, 1, 4, 2, 1, bias=False)

        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d):
                nn.init.kaiming_normal_(m.weight, 0.1)
                if m.bias is not None:
                    nn.init.constant_(m.bias, 0)
            elif isinstance(m, nn.BatchNorm1d):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)

    def forward(self, inputs):
        im1 = inputs[:, :, :, 0:-1].permute((0, 3, 1, 2)).reshape((inputs.size(0)*(inputs.size(3)-1), inputs.size(1), inputs.size(2)))
        im2 = inputs[:, :, :, 1:].permute((0, 3, 1, 2)).reshape((inputs.size(0)*(inputs.size(3)-1), inputs.size(1), inputs.size(2)))

        out_conv1a = self.conv1(im1)
        out_conv2a = self.conv2(out_conv1a)
        out_conv3a = self.conv3(out_conv2a)

        out_conv1b = self.conv1(im2)
        out_conv2b = self.conv2(out_conv1b)
        out_conv3b = self.conv3(out_conv2b)

        out_conv_redir = self.conv_redir(out_conv3a)
        out_correlation = correlate(out_conv3a,out_conv3b,self.md)

        in_conv3_1 = torch.cat([out_conv_redir, out_correlation], dim=1)

        out_conv3 = self.conv3_1(in_conv3_1)
        out_conv4 = self.conv4_1(self.conv4(out_conv3))
        out_conv5 = self.conv5_1(self.conv5(out_conv4))


        flow5       = self.predict_flow5(out_conv5)
        flow5_up    = crop_like(self.upsampled_flow5_to_4(flow5), out_conv4)
        out_deconv4 = crop_like(self.deconv4(out_conv5), out_conv4)

        concat4 = torch.cat((out_conv4,out_deconv4,flow5_up),1)
        flow4       = self.predict_flow4(concat4)
        flow4_up    = crop_like(self.upsampled_flow4_to_3(flow4), out_conv3)
        out_deconv3 = crop_like(self.deconv3(concat4), out_conv3)

        concat3 = torch.cat((out_conv3,out_deconv3,flow4_up),1)
        flow3       = self.predict_flow3(concat3)
        flow3_up    = crop_like(self.upsampled_flow3_to_2(flow3), out_conv2a)
        out_deconv2 = crop_like(self.deconv2(concat3), out_conv2a)

        concat2 = torch.cat((out_conv2a,out_deconv2,flow3_up),1)
        flow2 = self.predict_flow2(concat2)

        if self.training:
            return flow2,flow3,flow4,flow5
        else:
            return flow2

    def weight_parameters(self):
        return [param for name, param in self.named_parameters() if 'weight' in name]

    def bias_parameters(self):
        return [param for name, param in self.named_parameters() if 'bias' in name]

class FlowNetC_slim_3(nn.Module):
    expansion = 1

    def __init__(self,md=5,batchNorm=True, is_corr_func_calc=False):
        super(FlowNetC_slim_3,self).__init__()
        self.md = md
        self.batchNorm = batchNorm
        if is_corr_func_calc is True:
            self.conv1      = conv(2,   16, kernel_size=7, padding=3, stride=2, bias=False)
        else:
            self.conv1      = conv(1,   16, kernel_size=7, padding=3, stride=2, bias=False)
        self.conv2      = conv(16,  32, kernel_size=5, padding=2, stride=2, bias=False)
        self.conv3      = conv(32,  64, kernel_size=5, padding=2, stride=2, bias=False)
        self.conv_redir = conv(64,   16, kernel_size=1, padding=0, stride=1, bias=False)

        self.conv3_1 = conv((self.md*2+1)+16,  32, bias=False)
        self.conv4   = conv(32,  64, stride=2, bias=False)
        self.conv4_1 = conv(64,  64, bias=False)

        self.deconv3 = deconv(64,32, bias=False)
        self.deconv2 = deconv(65,16, bias=False)

        self.predict_flow4 = predict_flow(64, bias=False)
        self.predict_flow3 = predict_flow(65, bias=False)
        self.predict_flow2 = predict_flow(49, bias=False)

        self.upsampled_flow4_to_3 = nn.ConvTranspose1d(1, 1, 4, 2, 1, bias=False)
        self.upsampled_flow3_to_2 = nn.ConvTranspose1d(1, 1, 4, 2, 1, bias=False)

        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d):
                nn.init.kaiming_normal_(m.weight, 0.1)
                if m.bias is not None:
                    nn.init.constant_(m.bias, 0)
            elif isinstance(m, nn.BatchNorm1d):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)

    def forward(self, inputs):
        im1 = inputs[:, :, :, 0:-1].permute((0, 3, 1, 2)).reshape((inputs.size(0)*(inputs.size(3)-1), inputs.size(1), inputs.size(2)))
        im2 = inputs[:, :, :, 1:].permute((0, 3, 1, 2)).reshape((inputs.size(0)*(inputs.size(3)-1), inputs.size(1), inputs.size(2)))

        out_conv1a = self.conv1(im1)
        out_conv2a = self.conv2(out_conv1a)
        out_conv3a = self.conv3(out_conv2a)

        out_conv1b = self.conv1(im2)
        out_conv2b = self.conv2(out_conv1b)
        out_conv3b = self.conv3(out_conv2b)

        out_conv_redir = self.conv_redir(out_conv3a)
        out_correlation = correlate(out_conv3a,out_conv3b,self.md)

        in_conv3_1 = torch.cat([out_conv_redir, out_correlation], dim=1)

        out_conv3 = self.conv3_1(in_conv3_1)
        out_conv4 = self.conv4_1(self.conv4(out_conv3))

        flow4       = self.predict_flow4(out_conv4)
        flow4_up    = crop_like(self.upsampled_flow4_to_3(flow4), out_conv3)
        out_deconv3 = crop_like(self.deconv3(out_conv4), out_conv3)

        concat3 = torch.cat((out_conv3,out_deconv3,flow4_up),1)
        flow3       = self.predict_flow3(concat3)
        flow3_up    = crop_like(self.upsampled_flow3_to_2(flow3), out_conv2a)
        out_deconv2 = crop_like(self.deconv2(concat3), out_conv2a)

        concat2 = torch.cat((out_conv2a,out_deconv2,flow3_up),1)
        flow2 = self.predict_flow2(concat2)

        if self.training:
            return flow2,flow3,flow4
        else:
            return flow2

    def weight_parameters(self):
        return [param for name, param in self.named_parameters() if 'weight' in name]

    def bias_parameters(self):
        return [param for name, param in self.named_parameters() if 'bias' in name]

class myCRNN(nn.Module):
    def __init__(self,encode_input_channel, encode_output_channel, encode_layer_num, decode_input_channel, decode_output_channel, decode_layer_num, is_corr_func_calc=False, md=5):
        super(myCRNN, self).__init__()
        self.encode_input_channel=encode_input_channel
        self.encode_output_channel=encode_output_channel
        self.encode_layer_num=encode_layer_num
        self.decode_input_channel=decode_input_channel
        self.decode_output_channel=decode_output_channel
        self.decode_layer_num=decode_layer_num
        self.is_corr_func_calc = is_corr_func_calc
        self.md = md
        
        if self.is_corr_func_calc is True:
            self.conv1      = conv(2,   32, kernel_size=7, padding=3, stride=2, bias=False)
        else:
            self.conv1      = conv(1,   32, kernel_size=7, padding=3, stride=2, bias=False)
        self.conv2      = conv(32,  64, kernel_size=5, padding=2, stride=2, bias=False)
        self.conv3      = conv(64,  128, kernel_size=5, padding=2, stride=2, bias=False)
        self.conv_redir = conv(128,   16, kernel_size=1, padding=0, stride=1, bias=False)

        self.conv3_1 = conv((self.md*2+1)+16,  128, bias=False)
        self.conv4   = conv(128,  256, stride=2, bias=False)
        self.conv4_1 = conv(256,  256, bias=False)

        self.deconv3 = deconv(256,64, bias=False)
        self.deconv2 = deconv(193,32, bias=False)

        self.predict_flow4 = predict_flow(256, bias=False)
        self.predict_flow3 = predict_flow(193, bias=False)
        self.predict_flow2 = predict_flow(97, bias=False)

        self.upsampled_flow4_to_3 = nn.ConvTranspose1d(1, 1, 4, 2, 1, bias=False)
        self.upsampled_flow3_to_2 = nn.ConvTranspose1d(1, 1, 4, 2, 1, bias=False)
        self.upsampled_flow2_to_1 = nn.ConvTranspose1d(1, 1, 4, 2, 1, bias=False)
        self.upsampled_flow1_to_0 = nn.ConvTranspose1d(1, 1, 4, 2, 1, bias=False)

        #初始化LSTM
        self.gru1=nn.GRU(self.encode_input_channel, self.encode_output_channel, self.encode_layer_num, batch_first=True)
        self.gru2=nn.GRU(self.decode_input_channel, self.decode_output_channel, self.decode_layer_num, batch_first=True)
        
        self.linear = nn.Linear(64*64, 64)
        
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d):
                nn.init.kaiming_normal_(m.weight, 0.1)
                if m.bias is not None:
                    nn.init.constant_(m.bias, 0)
            elif isinstance(m, nn.BatchNorm1d):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)

    # forward裡面好像無法使用if (always會進入)
    def forward(self, inputs, is_cuda=True):
        if self.is_corr_func_calc is True:
            corr_fun = inputs[:, 1, :, :].permute((0, 2, 1))
        
        im1 = inputs[:, :, :, 0:-1].permute((0, 3, 1, 2)).reshape((inputs.size(0)*(inputs.size(3)-1), inputs.size(1), inputs.size(2)))
        im2 = inputs[:, :, :, 1:].permute((0, 3, 1, 2)).reshape((inputs.size(0)*(inputs.size(3)-1), inputs.size(1), inputs.size(2)))
        out_conv1a = self.conv1(im1)
        out_conv2a = self.conv2(out_conv1a)
        out_conv3a = self.conv3(out_conv2a)

        out_conv1b = self.conv1(im2)
        out_conv2b = self.conv2(out_conv1b)
        out_conv3b = self.conv3(out_conv2b)

        out_conv_redir = self.conv_redir(out_conv3a)
        out_correlation = correlate(out_conv3a,out_conv3b,self.md)

        in_conv3_1 = torch.cat([out_conv_redir, out_correlation], dim=1)

        out_conv3 = self.conv3_1(in_conv3_1)
        out_conv4 = self.conv4_1(self.conv4(out_conv3))

        flow4       = self.predict_flow4(out_conv4)
        flow4_up    = crop_like(self.upsampled_flow4_to_3(flow4), out_conv3)
        out_deconv3 = crop_like(self.deconv3(out_conv4), out_conv3)

        concat3 = torch.cat((out_conv3,out_deconv3,flow4_up),1)
        flow3       = self.predict_flow3(concat3)
        flow3_up    = crop_like(self.upsampled_flow3_to_2(flow3), out_conv2a)
        out_deconv2 = crop_like(self.deconv2(concat3), out_conv2a)

        concat2 = torch.cat((out_conv2a,out_deconv2,flow3_up),1)
        flow2 = self.predict_flow2(concat2)
        flow1 = self.upsampled_flow2_to_1(flow2)
        flow0 = self.upsampled_flow1_to_0(flow1)
        
        gru_in_data = flow0.reshape((inputs.size(0), inputs.size(3)-1, -1))
        time_steps = gru_in_data.size()[1]
        output = torch.zeros(list(gru_in_data.size())).cuda()
        h0 = torch.zeros(list([1*self.encode_layer_num, gru_in_data.size(0), self.encode_output_channel])).cuda()
        temp, hn = self.gru1(gru_in_data, h0)
        latency_vector=temp[:, -1:, :]

        if self.is_corr_func_calc is False:
            output[:, 0:1, :], hn = self.gru2(latency_vector, hn)
            for t in range(1, time_steps):
                output[:, t:t+1, :], hn = self.gru2(output[:, t-1:t, :], hn)
        else:
            output[:, 0:1, :], hn = self.gru2(torch.cat((latency_vector, corr_fun[:, 0:1, :]), dim=2), hn)
            for t in range(1, time_steps):
                output[:, t:t+1, :], hn = self.gru2(torch.cat((output[:, t-1:t, :], corr_fun[:, t:t+1, :]), dim=2), hn)
                
        # output = self.linear(output)
        # output = self.linear(temp)

        return output

class CRNN_hollow1_gru(nn.Module):
    def __init__(self,encode_input_channel, encode_output_channel, encode_layer_num, decode_input_channel, decode_output_channel, decode_layer_num, is_corr_func_calc=False, md=5):
        super(CRNN_hollow1_gru, self).__init__()
        self.encode_input_channel=encode_input_channel
        self.encode_output_channel=encode_output_channel
        self.encode_layer_num=encode_layer_num
        self.decode_input_channel=decode_input_channel
        self.decode_output_channel=decode_output_channel
        self.decode_layer_num=decode_layer_num
        self.is_corr_func_calc = is_corr_func_calc
        self.md = md
        
        if self.is_corr_func_calc is True:
            self.conv1      = conv(2,   32, kernel_size=7, padding=3, stride=2, bias=False)
        else:
            self.conv1      = conv(1,   32, kernel_size=7, padding=3, stride=2, bias=False)
        self.conv2      = conv(32,  64, kernel_size=5, padding=2, stride=2, bias=False)
        self.conv3      = conv(64,  128, kernel_size=5, padding=2, stride=2, bias=False)
        self.conv_redir = conv(128,   16, kernel_size=1, padding=0, stride=1, bias=False)

        self.conv3_1 = conv((self.md*2+1)+16,  128, bias=False)
        self.conv4   = conv(128,  256, stride=2, bias=False)
        self.conv4_1 = conv(256,  256, bias=False)

        self.deconv3 = deconv(256,64, bias=False)
        self.deconv2 = deconv(193,32, bias=False)

        self.predict_flow4 = predict_flow(256, bias=False)
        self.predict_flow3 = predict_flow(193, bias=False)
        self.predict_flow2 = predict_flow(97, bias=False)

        self.upsampled_flow4_to_3 = nn.ConvTranspose1d(1, 1, 4, 2, 1, bias=False)
        self.upsampled_flow3_to_2 = nn.ConvTranspose1d(1, 1, 4, 2, 1, bias=False)
        self.upsampled_flow2_to_1 = nn.ConvTranspose1d(1, 1, 4, 2, 1, bias=False)
        self.upsampled_flow1_to_0 = nn.ConvTranspose1d(1, 1, 4, 2, 1, bias=False)

        #初始化LSTM
        self.gru1=nn.GRU(self.encode_input_channel, self.encode_output_channel, self.encode_layer_num, batch_first=True, dropout=0.2, bidirectional=True).cuda()
        self.gru2=nn.GRU(self.decode_input_channel, self.decode_output_channel, self.decode_layer_num, batch_first=True, dropout=0.2).cuda()
        
        self.linear = nn.Linear(64*64, 64)
        
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d):
                nn.init.kaiming_normal_(m.weight, 0.1)
                if m.bias is not None:
                    nn.init.constant_(m.bias, 0)
            elif isinstance(m, nn.BatchNorm1d):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)

    # forward裡面好像無法使用if (always會進入)
    def forward(self, inputs, IQ, output_time_steps, is_cuda=True):
        if self.is_corr_func_calc is True:
            corr_fun = inputs[:, 1, :, :].permute((0, 2, 1))
        
        im1 = inputs[:, :, :, 0:-1].permute((0, 3, 1, 2)).reshape((inputs.size(0)*(inputs.size(3)-1), inputs.size(1), inputs.size(2)))
        im2 = inputs[:, :, :, 1:].permute((0, 3, 1, 2)).reshape((inputs.size(0)*(inputs.size(3)-1), inputs.size(1), inputs.size(2)))
        out_conv1a = self.conv1(im1)
        out_conv2a = self.conv2(out_conv1a)
        out_conv3a = self.conv3(out_conv2a)

        out_conv1b = self.conv1(im2)
        out_conv2b = self.conv2(out_conv1b)
        out_conv3b = self.conv3(out_conv2b)

        out_conv_redir = self.conv_redir(out_conv3a)
        out_correlation = correlate(out_conv3a,out_conv3b,self.md)

        in_conv3_1 = torch.cat([out_conv_redir, out_correlation], dim=1)

        out_conv3 = self.conv3_1(in_conv3_1)
        out_conv4 = self.conv4_1(self.conv4(out_conv3))

        flow4       = self.predict_flow4(out_conv4)
        flow4_up    = crop_like(self.upsampled_flow4_to_3(flow4), out_conv3)
        out_deconv3 = crop_like(self.deconv3(out_conv4), out_conv3)

        concat3 = torch.cat((out_conv3,out_deconv3,flow4_up),1)
        flow3       = self.predict_flow3(concat3)
        flow3_up    = crop_like(self.upsampled_flow3_to_2(flow3), out_conv2a)
        out_deconv2 = crop_like(self.deconv2(concat3), out_conv2a)

        concat2 = torch.cat((out_conv2a,out_deconv2,flow3_up),1)
        flow2 = self.predict_flow2(concat2)
        flow1 = self.upsampled_flow2_to_1(flow2)
        flow0 = self.upsampled_flow1_to_0(flow1)
        
        gru_in_data = flow0.reshape((inputs.size(0), inputs.size(3)-1, -1))
        output = torch.zeros((inputs.size(0), output_time_steps, self.decode_output_channel)).cuda()
        h0 = torch.zeros([2*self.encode_layer_num, gru_in_data.size(0), self.encode_output_channel]).cuda()
        gru_in_data = torch.cat([gru_in_data, IQ[:, 1:, :]], dim=2)
        temp, hn = self.gru1(gru_in_data, h0)
        latency_vector=temp[:, -1:, 0:self.encode_output_channel] + temp[:, -1:, self.encode_output_channel:self.encode_output_channel*2]
        hn = hn[self.encode_layer_num:, :, :]

        if self.is_corr_func_calc is False:
            output[:, 0:1, :], hn = self.gru2(latency_vector, hn)
            for t in range(1, output_time_steps):
                output[:, t:t+1, :], hn = self.gru2(output[:, t-1:t, :], hn)
        else:
            output[:, 0:1, :], hn = self.gru2(torch.cat((latency_vector, corr_fun[:, 0:1, :]), dim=2), hn)
            for t in range(1, output_time_steps):
                output[:, t:t+1, :], hn = self.gru2(torch.cat((output[:, t-1:t, :], corr_fun[:, t:t+1, :]), dim=2), hn)
                
        # output = self.linear(output)
        # output = self.linear(temp)

        return output

class myCRNN_RNN_first(nn.Module):
    def __init__(self,encode_input_channel, encode_output_channel, encode_layer_num, decode_input_channel, decode_output_channel, decode_layer_num, md=5, is_corr_func_calc=False, is_cuda=True):
        super(myCRNN_RNN_first, self).__init__()
        self.encode_input_channel=encode_input_channel
        self.encode_output_channel=encode_output_channel
        self.encode_layer_num=encode_layer_num
        self.decode_input_channel=decode_input_channel
        self.decode_output_channel=decode_output_channel
        self.decode_layer_num=decode_layer_num
        self.is_corr_func_calc = is_corr_func_calc
        self.is_cuda = is_cuda
        self.md = md

        #初始化LSTM
        self.gru1=nn.GRU(self.encode_input_channel, self.encode_output_channel, self.encode_layer_num, batch_first=True)
        self.gru2=nn.GRU(self.decode_input_channel, self.decode_output_channel, self.decode_layer_num, batch_first=True)
        
        if self.is_corr_func_calc is True:
            self.conv1      = conv(2,   32, kernel_size=7, padding=3, stride=2, bias=False)
        else:
            self.conv1      = conv(1,   32, kernel_size=7, padding=3, stride=2, bias=False)
        self.conv2      = conv(32,  64, kernel_size=5, padding=2, stride=2, bias=False)
        self.conv3      = conv(64,  128, kernel_size=5, padding=2, stride=2, bias=False)
        self.conv_redir = conv(128,   16, kernel_size=1, padding=0, stride=1, bias=False)

        self.conv3_1 = conv((self.md*2+1)+16,  128, bias=False)
        self.conv4   = conv(128,  256, stride=2, bias=False)
        self.conv4_1 = conv(256,  256, bias=False)

        self.deconv3 = deconv(256,64, bias=False)
        self.deconv2 = deconv(193,32, bias=False)

        self.predict_flow4 = predict_flow(256, bias=False)
        self.predict_flow3 = predict_flow(193, bias=False)
        self.predict_flow2 = predict_flow(97, bias=False)

        self.upsampled_flow4_to_3 = nn.ConvTranspose1d(1, 1, 4, 2, 1, bias=False)
        self.upsampled_flow3_to_2 = nn.ConvTranspose1d(1, 1, 4, 2, 1, bias=False)
        self.upsampled_flow2_to_1 = nn.ConvTranspose1d(1, 1, 4, 2, 1, bias=False)
        self.upsampled_flow1_to_0 = nn.ConvTranspose1d(1, 1, 4, 2, 1, bias=False)
        
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d):
                nn.init.kaiming_normal_(m.weight, 0.1)
                if m.bias is not None:
                    nn.init.constant_(m.bias, 0)
            elif isinstance(m, nn.BatchNorm1d):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)

    # forward裡面好像無法使用if (always會進入)
    def forward(self, inputs):
        inputs = inputs.permute((0, 1, 3, 2))
        inputs, corr_fun = inputs[:, 0, :, :], inputs[:, 1, :, :]
        time_steps = inputs.size(1)
        corr_fun = corr_fun.cuda()
        output = torch.zeros(list(inputs.size())).cuda()
        h0 = torch.zeros(list([1*self.encode_layer_num, inputs.size(0), self.encode_output_channel])).cuda()
        if self.is_cuda is False:
            corr_fun = corr_fun.cpu()
            output = output.cpu()
            h0 = h0.cpu()
        temp, hn = self.gru1(inputs, h0)
        latency_vector=temp[:, -1:, :]
        output[:, 0:1, :], hn = self.gru2(torch.cat((latency_vector, corr_fun[:, 0:1, :]), dim=2), hn)
        for t in range(1, time_steps):
            output[:, t:t+1, :], hn = self.gru2(torch.cat((output[:, t-1:t, :], corr_fun[:, t:t+1, :]), dim=2), hn)
                
        # output = self.linear(output)
        # output = self.linear(temp)
        
        output = torch.cat([output[:, :, None, :], corr_fun[:, :, None, :]], dim=2)
        im1 = output[:, 0:-1, :, :].reshape((-1, output.size(2), output.size(3)))
        im2 = output[:, 1:, :, :].reshape((-1, output.size(2), output.size(3)))
        out_conv1a = self.conv1(im1)
        out_conv2a = self.conv2(out_conv1a)
        out_conv3a = self.conv3(out_conv2a)

        out_conv1b = self.conv1(im2)
        out_conv2b = self.conv2(out_conv1b)
        out_conv3b = self.conv3(out_conv2b)

        out_conv_redir = self.conv_redir(out_conv3a)
        out_correlation = correlate(out_conv3a,out_conv3b,self.md)

        in_conv3_1 = torch.cat([out_conv_redir, out_correlation], dim=1)

        out_conv3 = self.conv3_1(in_conv3_1)
        out_conv4 = self.conv4_1(self.conv4(out_conv3))

        flow4       = self.predict_flow4(out_conv4)
        flow4_up    = crop_like(self.upsampled_flow4_to_3(flow4), out_conv3)
        out_deconv3 = crop_like(self.deconv3(out_conv4), out_conv3)

        concat3 = torch.cat((out_conv3,out_deconv3,flow4_up),1)
        flow3       = self.predict_flow3(concat3)
        flow3_up    = crop_like(self.upsampled_flow3_to_2(flow3), out_conv2a)
        out_deconv2 = crop_like(self.deconv2(concat3), out_conv2a)

        concat2 = torch.cat((out_conv2a,out_deconv2,flow3_up),1)
        flow2 = self.predict_flow2(concat2)
        flow1 = self.upsampled_flow2_to_1(flow2)
        flow0 = self.upsampled_flow1_to_0(flow1)
        output = flow0.reshape((-1, time_steps-1, flow0.size(2)))
        
        # (batchSize, timestep, signalLen)
        return output

class myCRNN_RNN_first_atten2(nn.Module):
    def __init__(self,encode_input_channel, encode_output_channel, encode_layer_num, decode_input_channel, decode_output_channel, decode_layer_num, md=5, is_corr_func_calc=False):
        super().__init__()
        self.encode_input_channel=encode_input_channel
        self.encode_output_channel=encode_output_channel
        self.encode_layer_num=encode_layer_num
        self.decode_input_channel=decode_input_channel
        self.decode_output_channel=decode_output_channel
        self.decode_layer_num=decode_layer_num
        self.is_corr_func_calc = is_corr_func_calc
        self.md = md

        #初始化LSTM
        self.gru1=nn.GRU(self.encode_input_channel, self.encode_output_channel, self.encode_layer_num, batch_first=True, dropout=0.2, bidirectional=True).cuda()
        self.gru2=nn.GRU(self.decode_input_channel, self.decode_output_channel, self.decode_layer_num, batch_first=True, dropout=0.2).cuda()
        self.attn = Attention2(encode_output_channel, decode_output_channel).cuda()
        
        if self.is_corr_func_calc is True:
            self.conv1      = conv(2,   32, kernel_size=7, padding=3, stride=2, bias=False)
        else:
            self.conv1      = conv(1,   32, kernel_size=7, padding=3, stride=2, bias=False)
        self.conv2      = conv(32,  64, kernel_size=5, padding=2, stride=2, bias=False)
        self.conv3      = conv(64,  128, kernel_size=5, padding=2, stride=2, bias=False)
        self.conv_redir = conv(128,   16, kernel_size=1, padding=0, stride=1, bias=False)

        self.conv3_1 = conv((self.md*2+1)+16,  128, bias=False)
        self.conv4   = conv(128,  256, stride=2, bias=False)
        self.conv4_1 = conv(256,  256, bias=False)

        self.deconv3 = deconv(256,64, bias=False)
        self.deconv2 = deconv(193,32, bias=False)

        self.predict_flow4 = predict_flow(256, bias=False)
        self.predict_flow3 = predict_flow(193, bias=False)
        self.predict_flow2 = predict_flow(97, bias=False)

        self.upsampled_flow4_to_3 = nn.ConvTranspose1d(1, 1, 4, 2, 1, bias=False)
        self.upsampled_flow3_to_2 = nn.ConvTranspose1d(1, 1, 4, 2, 1, bias=False)
        self.upsampled_flow2_to_1 = nn.ConvTranspose1d(1, 1, 4, 2, 1, bias=False)
        self.upsampled_flow1_to_0 = nn.ConvTranspose1d(1, 1, 4, 2, 1, bias=False)
        
        for m in self.modules():
            if isinstance(m, nn.Conv1d) or isinstance(m, nn.ConvTranspose1d):
                nn.init.kaiming_normal_(m.weight, 0.1)
                if m.bias is not None:
                    nn.init.constant_(m.bias, 0)
            elif isinstance(m, nn.BatchNorm1d):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)

    def forward(self, inputs, corr_fun, output_time_steps):
        # inputs = inputs.permute((0, 1, 3, 2))
        corr_fun = corr_fun.cuda()
        
        h0 = torch.zeros((2*self.encode_layer_num, inputs.size(0), self.encode_output_channel)).cuda()
        output = torch.zeros((inputs.size(0), output_time_steps, self.decode_output_channel)).cuda()
        
        encoder_output, hn = self.gru1(inputs, h0)
        latency_vector=encoder_output[:, -1:, 0:self.encode_output_channel]+encoder_output[:, -1:, self.encode_output_channel:]
        hn = hn[self.encode_layer_num:, :, :]
        encoder_output = encoder_output[:, :, 0:self.encode_output_channel]+encoder_output[:, :, self.encode_output_channel:]
        
        output[:, 0:1, :], hn = self.gru2(torch.cat((latency_vector, corr_fun[:, 0:1, :]), dim=2), hn)
        for t in range(1, output_time_steps):
            temp, hn = self.gru2(torch.cat((output[:, t-1:t, :], corr_fun[:, t:t+1, :]), dim=2), hn)
            output[:, t:t+1, :], _ = self.attn(temp, encoder_output.contiguous(), t)
                
        # output = self.linear(output)
        # output = self.linear(temp)
        corr_fun = corr_fun.reshape((corr_fun.size(0), -1, output_time_steps, corr_fun.size(2))).mean(dim=1)
        output = torch.cat([output[:, :, None, :], corr_fun[:, :, None, :]], dim=2)
        im1 = output[:, 0:-1, :, :].reshape((-1, output.size(2), output.size(3)))
        im2 = output[:, 1:, :, :].reshape((-1, output.size(2), output.size(3)))
        out_conv1a = self.conv1(im1)
        out_conv2a = self.conv2(out_conv1a)
        out_conv3a = self.conv3(out_conv2a)

        out_conv1b = self.conv1(im2)
        out_conv2b = self.conv2(out_conv1b)
        out_conv3b = self.conv3(out_conv2b)

        out_conv_redir = self.conv_redir(out_conv3a)
        out_correlation = correlate(out_conv3a,out_conv3b,self.md)

        in_conv3_1 = torch.cat([out_conv_redir, out_correlation], dim=1)

        out_conv3 = self.conv3_1(in_conv3_1)
        out_conv4 = self.conv4_1(self.conv4(out_conv3))

        flow4       = self.predict_flow4(out_conv4)
        flow4_up    = crop_like(self.upsampled_flow4_to_3(flow4), out_conv3)
        out_deconv3 = crop_like(self.deconv3(out_conv4), out_conv3)

        concat3 = torch.cat((out_conv3,out_deconv3,flow4_up),1)
        flow3       = self.predict_flow3(concat3)
        flow3_up    = crop_like(self.upsampled_flow3_to_2(flow3), out_conv2a)
        out_deconv2 = crop_like(self.deconv2(concat3), out_conv2a)

        concat2 = torch.cat((out_conv2a,out_deconv2,flow3_up),1)
        flow2 = self.predict_flow2(concat2)
        flow1 = self.upsampled_flow2_to_1(flow2)
        flow0 = self.upsampled_flow1_to_0(flow1)
        output = flow0.reshape((-1, output_time_steps-1, flow0.size(2)))
        
        # (batchSize, timestep-1, signalLen)
        return output

class convGRU_endec(nn.Module):
    def __init__(self, input_data_size, kernel_size, dtype, encode_input_channel, encode_output_channel, encode_layer_num, decode_input_channel, decode_output_channel, decode_layer_num):
        super(convGRU_endec, self).__init__()
        self.decode_layer_num = decode_layer_num
        self.input_data_size = input_data_size
        self.convGRU1 = convGRU.ConvGRU(input_size=input_data_size,
                    input_dim=encode_input_channel,
                    hidden_dim=encode_output_channel,
                    kernel_size=kernel_size,
                    num_layers=encode_layer_num,
                    dtype=dtype,
                    batch_first=True,
                    bias = True,
                    return_all_layers = True)
        self.convGRU2 = convGRU.ConvGRU(input_size=input_data_size,
                    input_dim=decode_input_channel,
                    hidden_dim=decode_output_channel,
                    kernel_size=kernel_size,
                    num_layers=decode_layer_num,
                    dtype=dtype,
                    batch_first=True,
                    bias = True,
                    return_all_layers = True)
        
    def forward(self, input, corr_fun, output_time_steps):
        output = torch.zeros((input.size(0), output_time_steps, 1, 1, self.input_data_size[1])).cuda()
        temp, hn = self.convGRU1(input[:, :, None, None, :])
        latency_vector = temp[0][:, -1:, :, :, :]
        temp, hn = self.convGRU2(torch.cat((latency_vector, corr_fun[:, 0:1, None, None, :]), dim=2), hn)
        output[:, 0:1, :, :, :] = temp[0]
        for t in range(1, output_time_steps):
            temp, hn = self.convGRU2(torch.cat((output[:, t-1:t, :, :, :], corr_fun[:, t:t+1, None, None, :]), dim=2), hn)
            output[:, t:t+1, :, :, :] = temp[0]
        return output.squeeze()
        
class convGRU_endec_1d(nn.Module):
    def __init__(self, input_data_size, kernel_size, dtype, encode_input_channel, encode_output_channel, encode_layer_num, decode_input_channel, decode_output_channel, decode_layer_num):
        super(convGRU_endec_1d, self).__init__()
        self.decode_layer_num = decode_layer_num
        self.input_data_size = input_data_size
        self.convGRU1 = convGRU_1d.ConvGRU(input_size=input_data_size,
                    input_dim=encode_input_channel,
                    hidden_dim=encode_output_channel,
                    kernel_size=kernel_size,
                    num_layers=encode_layer_num,
                    dtype=dtype,
                    batch_first=True,
                    bias = True,
                    return_all_layers = True)
        self.convGRU2 = convGRU_1d.ConvGRU(input_size=input_data_size,
                    input_dim=decode_input_channel,
                    hidden_dim=decode_output_channel,
                    kernel_size=kernel_size,
                    num_layers=decode_layer_num,
                    dtype=dtype,
                    batch_first=True,
                    bias = True,
                    return_all_layers = True)
        
    def forward(self, input, corr_fun, output_time_steps):
        output = torch.zeros((input.size(0), output_time_steps, 1, self.input_data_size)).cuda()
        temp, hn = self.convGRU1(input[:, :, None, :])
        latency_vector = temp[0][:, -1:, :, :]
        temp, hn = self.convGRU2(torch.cat((latency_vector, corr_fun[:, 0:1, None, :]), dim=2), hn)
        output[:, 0:1, :, :] = temp[0]
        for t in range(1, output_time_steps):
            temp, hn = self.convGRU2(torch.cat((output[:, t-1:t, :, :], corr_fun[:, t:t+1, None, :]), dim=2), hn)
            output[:, t:t+1, :, :] = temp[0]
        return output.squeeze()
        
class convGRU_atten2(nn.Module):
    def __init__(self, input_data_size, kernel_size, dtype, encode_input_channel, encode_output_channel, encode_layer_num, decode_input_channel, decode_output_channel, decode_layer_num):
        # "input_size" in the parameters of ConvGRU is the input dimension in RNN
        # "input_dim" and "hidden_dim" in the parameters of ConvGRU refer to the channels in convolution operation, differing from the dim in GRU
        super().__init__()
        self.decode_layer_num = decode_layer_num
        self.input_data_size = input_data_size
        self.convGRU1 = convGRU_1d.ConvGRU(input_size=input_data_size,
                    input_dim=encode_input_channel,
                    hidden_dim=encode_output_channel,
                    kernel_size=kernel_size,
                    num_layers=encode_layer_num,
                    dtype=dtype,
                    batch_first=True,
                    bias = True,
                    return_all_layers = True)
        self.convGRU2 = convGRU_1d.ConvGRU(input_size=input_data_size,
                    input_dim=decode_input_channel,
                    hidden_dim=decode_output_channel,
                    kernel_size=kernel_size,
                    num_layers=decode_layer_num,
                    dtype=dtype,
                    batch_first=True,
                    bias = True,
                    return_all_layers = True)
        self.attn = Attention2(input_data_size, input_data_size).cuda()
        
    def forward(self, input, corr_fun, output_time_steps):
        output = torch.zeros((input.size(0), output_time_steps, 1, self.input_data_size)).cuda()
        # encoder_output: (bs, seq, encode_hid_size)
        # hn: (encode_layer, bs, encode_hid_size)
        encoder_output, hn = self.convGRU1(input)
        latency_vector = encoder_output[0][:, -1:, :, :]
        temp, hn = self.convGRU2(torch.cat((latency_vector, corr_fun[:, 0:1, None, :]), dim=2), hn)
        output[:, 0:1, :, :] = temp[0]
        for t in range(1, output_time_steps):
            temp, hn = self.convGRU2(torch.cat((output[:, t-1:t, :, :], corr_fun[:, t:t+1, None, :]), dim=2), hn)
            output[:, t:t+1, 0, :], _ = self.attn(temp[0][:, :, 0, :], encoder_output[0][:, :, 0, :].contiguous(), t)
        return output.squeeze()