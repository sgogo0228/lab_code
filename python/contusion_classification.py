#%%
from __future__ import print_function, division

import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torchvision import datasets, transforms
from torch.autograd import Variable
import numpy as np
from torchvision import models

import time
import scipy.io as io

batch_size = 16
learning_rate = 0.0002
epoch = 30
train_transforms = transforms.Compose([
    transforms.RandomHorizontalFlip(),
    transforms.ToTensor()
])
val_transforms = transforms.Compose([
    transforms.ToTensor()
])



class VGGNet(nn.Module):
    def __init__(self, num_classes=4):
        super(VGGNet, self).__init__()
        net = models.vgg19_bn(pretrained=True)
        # net.features = nn.Sequential(*list(net.features.children())[0:15])
        net.avgpool = nn.Sequential()
        net.classifier = nn.Sequential()
        self.features = net
        self.classifier = nn.Sequential(
                nn.Linear(512 * 4 * 16, 512),
                nn.ReLU(True),
                nn.Dropout(),
                nn.Linear(512, 128),
                nn.ReLU(True),
                nn.Dropout(),
                nn.Linear(128, num_classes),
        )

    def forward(self, x):
        x = self.features(x)
        print(x.size())
        x = self.classifier(x)
        return x

#--------------------訓練過程---------------------------------


confusion = np.zeros((9, 4, 4))
for fold_iter in range(1):
    train_dir = 'C:\\Users\\Buslab\\Desktop\\Experiment data\\DaMing\\contusion\\9-fold validation\\' + str(fold_iter+1) + '\\training'
    train_datasets = datasets.ImageFolder(train_dir, transform=train_transforms)
    train_dataloader = torch.utils.data.DataLoader(train_datasets, batch_size=batch_size, shuffle=True)

    val_dir = 'C:\\Users\\Buslab\\Desktop\\Experiment data\\DaMing\\contusion\\9-fold validation\\' + str(fold_iter+1) + '\\testing'
    val_datasets = datasets.ImageFolder(val_dir, transform=val_transforms)
    val_dataloader = torch.utils.data.DataLoader(val_datasets, batch_size=batch_size, shuffle=True)

    model = VGGNet()
    if torch.cuda.is_available():
        model.cuda()
    params = [{'params': md.parameters()} for md in model.children()
            if md in [model.classifier]]
    optimizer = optim.Adam(model.parameters(), lr=learning_rate)
    loss_func = nn.CrossEntropyLoss()

    start = time.time()
    model.train()
    for i in range(epoch):
        print('epoch {}'.format(i + 1))
        train_loss = 0.
        train_acc = 0.
        for img, label in train_dataloader:
            img = Variable(img).cuda()
            label = Variable(label).cuda()
            optimizer.zero_grad()
            output = model(img)
            # print(output.size())
            loss = loss_func(output, label)
            train_loss += loss.item()
            pred = torch.max(output, 1)[1]
            train_correct = (pred == label).sum()
            train_acc += train_correct.item()
            loss.backward()
            optimizer.step()
        Loss = float(train_loss / (len(train_datasets)))
        Acc = float(train_acc / (len(train_datasets)))
        print("Train Loss:%.9f" % Loss)
        print("Train Accuracy:%.9f" % Acc)

    end = time.time()
    print("Training Time: %.2f mins" % ((end-start)/60))
    print(fold_iter)

    model.eval()
    val_loss = 0.
    val_acc = 0.
    for img, label in val_dataloader:
        img = Variable(img).cuda()
        label = Variable(label).cuda()
        output = model(img)
        loss = loss_func(output, label)
        val_loss += loss.item()
        pred = torch.max(output, 1)[1]
        val_correct = (pred == label).sum()
        val_acc += val_correct.item()
        for idx in range(label.size(dim=0)):
            confusion[fold_iter, label[idx], pred[idx]] = confusion[fold_iter, label[idx], pred[idx]] + 1

    Loss = float(val_loss / (len(val_datasets)))
    Acc = float(val_acc / (len(val_datasets)))
    print("Validation Loss:%.9f" % Loss)
    print("Validation Accuracy:%.9f" % Acc)
    # print(confusion)



#%%
io.savemat('confusion_vgg19.mat', {'conf': confusion})

#%%
torch.cuda.empty_cache()
print(torch.cuda.memory_allocated())
print(torch.cuda.memory_cached())
print(torch.cuda.max_memory_reserved())
# %%
