from functools import partial
import numpy as np
import os, glob
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torch.utils.data as dataclass
import torchvision
import torchvision.transforms as transforms
from torchvision import models
from ray import tune
from ray.tune import CLIReporter
from ray.tune.schedulers import ASHAScheduler
from yaml import MarkedYAMLError
from tqdm import tqdm
import warnings
import myLib
import custom_module
import gc
from torchsummary import summary
import matplotlib.pyplot as plt
import time
import importlib
import disp_data_fetch
importlib.reload(myLib)
importlib.reload(custom_module)


warnings.filterwarnings('ignore')
gc.collect()
torch.cuda.empty_cache()

num_samples=1
max_num_epochs=50
config = {
    "lr": tune.grid_search([1e-4]),
    "batch_size": tune.grid_search([2])
}

def load_data(data_dir="./"):
    train_transform = transforms.Compose([
        transforms.ToTensor(),
        transforms.Resize([256, 920]),
        # transforms.Normalize(0, 0.1),
    ])
    val_transform = transforms.Compose([
        transforms.ToTensor(),
        transforms.Resize([256, 920]),
        # transforms.Normalize(0, 0.1),
    ])

    train_dir = data_dir + r'\training'
    train_datasets = disp_data_fetch.Displacement_dataset_IQ(root=train_dir, transform=train_transform)

    val_dir = data_dir + r'\validation'
    val_datasets = disp_data_fetch.Displacement_dataset_IQ(root=val_dir, transform=val_transform)

    return train_datasets, val_datasets

def train_cifar(config, checkpoint_dir=None, data_dir=None, max_epoch=200):
    data_depth = 256
    data_group_width = 256
    groups = np.int32(data_depth/data_group_width)
    encode_input_channel = data_group_width*2
    encode_output_channel = data_group_width
    encode_layer_num = 2
    decode_input_channel = data_group_width*2
    decode_output_channel = data_group_width
    decode_layer_num = 2
    learning_rate = 1e-4
    batch_size = 1
    time_steps= 920
    output_time_steps = 184
    val_epoch_interval = 5
    is_run_test = False
    is_corr_func_calc = True
    file_copy = 1
    net_name = 'CRNN_hollow1_gru'
    file_name_prefix = (rf'D:\DaMing\sw_data\model_output' + 
    rf'\gru_{encode_input_channel}_{encode_layer_num}layer_bs{batch_size}_epoch{max_epoch}_groupwidth{data_group_width}')
    while os.path.exists(rf'{file_name_prefix}_{file_copy}.pt')==True :
        file_copy += 1
    file_name = rf'{file_name_prefix}_{file_copy}'
    
    params = {'encode_input_channel':encode_input_channel,
              'encode_output_channel':encode_output_channel,
              'encode_layer_num':encode_layer_num,
              'decode_input_channel':decode_input_channel,
              'decode_output_channel':decode_output_channel,
              'decode_layer_num':decode_layer_num,
              'max_epoch':max_epoch,
              'learning_rate':learning_rate,
              'batch_size':batch_size,
              'time_steps':time_steps,
              'output_time_steps':output_time_steps,
              'val_epoch_interval':val_epoch_interval,
              'data_depth':data_depth,
              'data_group_width':data_group_width,
              'net_name':net_name}
    
    model = eval(rf'custom_module.{net_name}(encode_input_channel, encode_output_channel, encode_layer_num, decode_input_channel, decode_output_channel, decode_layer_num, True, 7)')
    storage_epoch = 100

    if torch.cuda.is_available():
        model.cuda()
    device = "cpu"
    if torch.cuda.is_available():
        device = "cuda:0"
        if torch.cuda.device_count() > 1:
            model = nn.DataParallel(model)
    model.to(device)
    
    optimizer = optim.Adam(model.parameters(), lr=learning_rate)
    # optimizer = optim.SGD(model.parameters(), lr=learning_rate*batch_size/2, momentum=0.9)
    loss_g = custom_module.gradient_loss(dim=1)
    loss_xcorr_func = custom_module.xcorr_loss(dim=1)
    loss_mse_func = nn.MSELoss()
    train_loss = np.zeros((max_epoch))
    train_mse = np.zeros((max_epoch))
    train_xcorr = np.zeros((max_epoch))
    val_loss = np.zeros((max_epoch))
    val_mse = np.zeros((max_epoch))
    val_xcorr = np.zeros((max_epoch))
    val_mse = np.zeros((max_epoch))
    training_time = np.zeros((max_epoch))
    val_time = np.zeros((max_epoch))
    best_loss = 50000
    best_net = model
    best_model_epoch = 0

    trainset, valset = load_data(data_dir=data_dir)
    train_dataloader = torch.utils.data.DataLoader(trainset, 
        batch_size=batch_size, 
        shuffle=True,
        num_workers=0)
    
    val_dataloader = torch.utils.data.DataLoader(valset, 
        batch_size=batch_size, 
        shuffle=True,
        num_workers=0)

    start_time = time.time()
    print(time.ctime())
    for epoch in tqdm(range(max_epoch)):
        running_loss = 0.
        running_mse = 0.
        running_xcorr = 0.
        t = time.time()
        # torch.save(net.state_dict(), rf'C:\Users\Buslab\Desktop\Experiment data\DaMing\contusion\displacement_estimation\train_epoch{epoch}.pt')
        for inputs, labels, I_inputs, Q_inputs in train_dataloader:
            inputs = inputs.reshape((-1, 1, data_group_width, time_steps)).cuda()
            I_inputs = I_inputs.reshape(-1, data_group_width, time_steps).permute((0, 2, 1)).cuda()
            Q_inputs = Q_inputs.reshape(-1, data_group_width, time_steps).permute((0, 2, 1)).cuda()
            IQ = torch.sqrt(I_inputs**2+Q_inputs**2)
            labels = labels.reshape(-1, data_depth, 5, output_time_steps).mean(dim=2).permute((0, 2, 1)).cuda()
            # zero the parameter gradients
            optimizer.zero_grad()

            # forward + backward + optimize
            local_bz = inputs.size(0)
            if is_corr_func_calc:
                xcorr_func = torch.zeros(local_bz, 1, time_steps, data_group_width)
                # 需更改model的in_feature_dim
                for bz in range(0, local_bz):
                    temp1 = inputs[bz:bz+1, 0, :, :].permute((0, 2, 1))
                    temp2 = inputs[bz:bz+1, 0, :, 0:1].repeat((1, 1, time_steps)).permute((2, 0, 1))
                    xcorr_func[bz, 0, :, :] = nn.functional.conv1d(temp1, temp2, groups=time_steps, padding='same')[0, :, :]
                    xcorr_func[bz, 0, :, :] = (xcorr_func[bz, :, :]-xcorr_func[bz, :, :].min())/(xcorr_func[bz, :, :].max()-xcorr_func[bz, :, :].min())
                xcorr_func = xcorr_func.permute((0, 1, 3, 2)).cuda()
                
                # # same as the above for_loop version but the normalization is not correct (xcorr_func2 = (xcorr_func2 - xcorr_func2.min()) / xcorr_func2.max() - xcorr_func2.min())
                # temp1 = inputs.permute((2, 0, 1))
                # temp2 = inputs[:, :, 0:1].permute((0, 2, 1))
                # xcorr_func2 = nn.functional.conv1d(temp1, temp2, groups=local_bz, padding='same')[None].permute((2, 0, 3, 1))
                # xcorr_func2 = (xcorr_func2 - xcorr_func2.min()) / xcorr_func2.max() - xcorr_func2.min()
                
                inputs = torch.cat([inputs, xcorr_func], dim=1)
                
            outputs = model(inputs, IQ, output_time_steps)
            # outputs = outputs.reshape((local_bz, groups, output_time_steps, data_group_width)).permute((0, ))
            
            sample_xcorr = loss_xcorr_func(outputs, labels)
            sample_mse = loss_mse_func(outputs, labels)
            loss = 0.4*sample_mse + 0.6*sample_xcorr
            # record training loss
            running_loss += loss.item()
            running_mse += sample_mse.item()
            running_xcorr += sample_xcorr.item()
            # _, preds = torch.max(output, 1) #從
            loss.backward()
            optimizer.step()

        print(time.ctime())
        train_loss[epoch] = float(running_loss / len(train_dataloader))
        train_mse[epoch] = float(running_mse / len(train_dataloader))
        train_xcorr[epoch] = float(running_xcorr / len(train_dataloader))
        print(f"epoch: {epoch+1}, Train loss: {train_loss[epoch]}, train mse: {train_mse[epoch]}, train xcorr: {train_xcorr[epoch]}")
        training_time[epoch] = time.time()-t

        # Validation loss
        if is_run_test==False and ((epoch == 0) or (epoch % val_epoch_interval == val_epoch_interval-1)):
            t = time.time()
            model.eval()
            with torch.no_grad():
                running_loss = 0.
                running_mse = 0.
                running_xcorr = 0.
                
                for inputs, labels, I_inputs, Q_inputs in val_dataloader:
                    inputs = inputs.reshape((-1, 1, data_group_width, time_steps)).cuda()
                    I_inputs = I_inputs.reshape(-1, data_group_width, time_steps).permute((0, 2, 1)).cuda()
                    Q_inputs = Q_inputs.reshape(-1, data_group_width, time_steps).permute((0, 2, 1)).cuda()
                    IQ = torch.sqrt(I_inputs**2+Q_inputs**2)
                    labels = labels.reshape(-1, data_depth, 5, output_time_steps).mean(dim=2).permute((0, 2, 1)).cuda()
                    local_bz = inputs.size(0)
                    if is_corr_func_calc:
                        xcorr_func = torch.zeros(local_bz, 1, time_steps, data_group_width)
                        # 需更改model的in_feature_dim
                        for bz in range(0, local_bz):
                            temp1 = inputs[bz:bz+1, 0, :, :].permute((0, 2, 1))
                            temp2 = inputs[bz:bz+1, 0, :, 0:1].repeat((1, 1, time_steps)).permute((2, 0, 1))
                            xcorr_func[bz, 0, :, :] = nn.functional.conv1d(temp1, temp2, groups=time_steps, padding='same')[0, :, :]
                            xcorr_func[bz, 0, :, :] = (xcorr_func[bz, :, :]-xcorr_func[bz, :, :].min())/(xcorr_func[bz, :, :].max()-xcorr_func[bz, :, :].min())
                        xcorr_func = xcorr_func.permute((0, 1, 3, 2)).cuda()
                        inputs = torch.cat([inputs, xcorr_func], dim=1)
                    outputs = model(inputs, IQ, output_time_steps)
                    
                    # labels = labels[:, :, :, 1:].permute((0, 1, 3, 2)).reshape((-1, labels.size(3)-1, labels.size(2)))
                    sample_xcorr = loss_xcorr_func(outputs, labels)
                    sample_mse = loss_mse_func(outputs, labels)
                    loss = 0.4*sample_mse + 0.6*sample_xcorr
                    running_loss += loss.item()
                    running_xcorr += sample_xcorr.item()
                    running_mse += sample_mse.item()

                val_time[epoch] = time.time()-t
                val_loss[epoch] = float(running_loss / len(val_dataloader))
                val_mse[epoch] = float(running_mse / len(val_dataloader))
                val_xcorr[epoch] = float(running_xcorr / len(val_dataloader))
                # val_mse_without_push[epoch] = float(val_acc_without_push / len(val_dataloader))
                # torch.save(net.state_dict(), rf'C:\Users\Buslab\Desktop\Experiment data\DaMing\contusion\displacement_estimation\test_epoch{epoch}.pt')

                if val_mse[epoch] < best_loss:
                    best_model_epoch = epoch
                    best_loss = val_mse[epoch]
                    best_net = model

            print(f'Validation Loss:{val_loss[epoch]}, Validation mse:{val_mse[epoch]}, Validation xcorr:{val_xcorr[epoch]}')
            # print(f"Validation Loss without Push: {val_mse_without_push[epoch]}")
            # Acc = float(val_acc / (len(valset)))
            # print("Validation Accuracy:%.9f; correct number:%d; total: %d" % (Acc, val_acc, len(valset)))
            model.train()
            if epoch%storage_epoch == storage_epoch-1:
                myLib.output_data({'train_loss': train_loss, 'train_mse': train_mse, 'train_xcorr': train_xcorr,
                'val_loss': val_loss, 'val_mse': val_mse, 'val_xcorr': val_xcorr,
                'training_time': training_time, 'val_time': val_time, 'best_model_epoch':best_model_epoch, 'params':params}, file_name=file_name)
                torch.save(best_net.state_dict(), f'{file_name}.pt')
    print("Finished")

gpus_per_trial=1
data_dir = os.path.abspath(r"D:\DaMing\sw_data\train_data_with_IQ")
train_cifar(config=config, data_dir=data_dir)


# scheduler = ASHAScheduler(
#     metric="loss",
#     mode="min",
#     max_t=max_num_epochs,
#     grace_period=1,
#     reduction_factor=2)
# reporter = CLIReporter(
#     # parameter_columns=["l1", "l2", "lr", "batch_size"],
#     metric_columns=["loss", "accuracy", "training_iteration"])
# result = tune.run(
#     partial(train_cifar, data_dir=data_dir, max_epoch=max_num_epochs),
#     resources_per_trial={"cpu": 8, "gpu": gpus_per_trial},
#     config=config,
#     num_samples=num_samples,
#     verbose = 0,
#     scheduler=scheduler,
#     progress_reporter=reporter)

# best_trial = result.get_best_trial("loss", "min", "last")
# print("Best trial config: {}".format(best_trial.config))
# print("Best trial final validation loss: {}".format(
#     best_trial.last_result["loss"]))
