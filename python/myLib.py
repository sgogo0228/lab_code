import numpy as np
import scipy.io as matlabio
import torch.nn as nn
import glob
import shutil
import re
import matplotlib.pyplot as plt
import scipy.signal as sig

def headFile_NI(filename):
    name = filename.strip()
    with open(name + '.dat', 'rb') as f:
        rf = np.fromfile(f, dtype=np.int8)
    # data_dir = 'E:/temp'
    # mat = matlabio.loadmat(data_dir + '/30MHz_dist0to10_depth9to14k_amp600_cycle5100_1_SW0')
    # mat = mat['temp'][0][0][0]
    ScanSpeed=rf[3]*16129+rf[4]*127+rf[5]
    Aline=rf[6]*16129+rf[7]*127+rf[8]
    DataLength=rf[9]*16129+rf[10]*127+rf[11]
    SamplingRate=rf[12]*16129+rf[13]*127+rf[14]
    Delay=rf[15]*16129+rf[16]*127+rf[17]
    Vpp=rf[18]*16129+rf[19]*127+rf[20]
    XInterval=rf[21]*16129+rf[22]*127+rf[23]
    YInterval=rf[24]*16129+rf[25]*127+rf[26]
    MoveTimes=rf[27]*16129+rf[28]*127+rf[29]
    Doppler=rf[30]*16129+rf[31]*127+rf[32]
    rf = rf[33:]/255*Vpp
    rf = rf.reshape(Aline, DataLength, int(rf.size/DataLength/Aline))
    rf = np.transpose(rf, (1, 0, 2))
    out = {'ScanSpeed':ScanSpeed,
    'Aline': Aline,
    'DataLength': DataLength,
    'SamplingRate': SamplingRate,
    'Delay': Delay,
    'Vpp': Vpp,
    'XInterval': XInterval,
    'YInterval': YInterval,
    'MoveTimes': MoveTimes,
    'Doppler': Doppler,
    'rf': rf}

    return out

def IQ_demod(input, fc, fs, dim=0):
    sig_len, time_step = input.shape
    t = np.arange(0, sig_len/fs, 1/fs)
    t = np.repeat(t[:, None], time_step, axis=1)
    I = 2*input*np.cos(2*np.pi*fc*t)
    Q = 2*input*np.sin(2*np.pi*fc*t)
    b, a = sig.butter(5, fc*2/fs)
    I = sig.filtfilt(b, a, I, axis=dim)
    Q = sig.filtfilt(b, a, Q, axis=dim)
    return I, Q

def output_data(data, file_name='./temp'):
    matlabio.savemat(f'{file_name}.mat', data)
    return

def init_weights(m):
    if isinstance(m, nn.Conv2d) or isinstance(m, nn.BatchNorm2d) or isinstance(m, nn.ConvTranspose2d):
        nn.init.normal_(m.weight, mean=0, std=0.2)
        m.bias.data.fill_(0.1)

def xcorr_coef_calc():
    data_path = glob.glob(r'D:\daming' +
    r'\training_demod_adjust\data\*.npy')
    label_path = glob.glob(r'D:\daming' +
    r'\training_demod_adjust\label\*.npy')
    file_index = re.search("data", data_path[0]).start()
    data_path = sorted(data_path, key=lambda x: int(x[file_index+5:-4]))
    label_path = sorted(label_path, key=lambda x: int(x[file_index+6:-4]))
    sum_xcorr_coef = np.zeros((len(label_path), 1000))

    for f in range(0, len(label_path)):
        fname = data_path[f][file_index+5:]
        label_data = np.load(label_path[f]).astype(np.float32).reshape(1000, 5, 184)
        for d in range(0, 1000):
            sum_xcorr_coef[f, d] = np.corrcoef(label_data[d, :, :]).sum()
        if ((sum_xcorr_coef[f, :]-5)/20).mean() > 0.6:
            shutil.copyfile(data_path[f], rf'D:\daming\training_demod_adjust\high_xcorr\data\{fname}')
            shutil.copyfile(label_path[f], rf'D:\daming\training_demod_adjust\high_xcorr\label\{fname}')

def next_power2(x):  
    return 1 if x == 0 else 2**(x - 1).bit_length()

def show_training_result(output, label, ymin, ymax, cc, mse, yinterval=0.2):
    cc = cc
    mse = mse
    font_style = {'size': 24, 
        'family': 'Times New Roman', 
        'weight': 'bold'}
    label_style = {'labelsize': 30, 
        'labelweight': 'bold', 
        'linewidth': 3}
    data_style = {'linewidth': 3}
    legend_style = {'labelspacing': 0.3, 
        'edgecolor':'none', 
        'facecolor':'none', 
        'fontsize':20, 
        'loc': 'upper right'}
    plt.rc('font', **font_style)
    plt.rc('axes', **label_style)
    plt.rc('lines', **data_style)
    plt.rc('legend', **legend_style)

    plt.figure(figsize=(8,3))
    # x_axis = 0.1*np.arange(0, output.shape[0])
    plt.plot(0.1*np.arange(0, output.shape[0]), output)
    plt.plot(0.1*np.arange(0, label.shape[0]), label, 'r')
    plt.legend(['predicted', 'labeled'])

    plt.xlabel('Time (ms)')
    plt.ylabel('Phase (rad)')
    temp = plt.yticks(np.arange(ymin, ymax, yinterval))
    temp = plt.ylim([ymin, ymax])
    plt.title(f'CC: {np.round(cc, 3)}, MSE: {np.round(mse, 3)}', fontweight='bold')
