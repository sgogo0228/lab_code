classdef extend_mutiplication_layer < nnet.layer.Layer
    % Example custom weighted addition layer.
    methods
        function layer = extend_mutiplication_layer(numInputs,name) 
            % layer = weightedAdditionLayer(numInputs,name) creates a
            % weighted addition layer and specifies the number of inputs
            % and the layer name.

            % Set number of inputs.
            layer.NumInputs = numInputs;

            % Set layer name.
            layer.Name = name;

            % Set layer description.
            layer.Description = "Weighted addition of " + numInputs +  ... 
                " inputs";
        
            % Initialize layer weights.
%             layer.Weights = rand(1,numInputs); 
        end
        
        function Z = predict(layer, x1, x2)
            % Z = predict(layer, X1, ..., Xn) forwards the input data X1,
            % ..., Xn through the layer and outputs the result Z.
            if size(x1, 3)==1 && size(x2, 3)~=1
                Z = repmat(x1, 1, 1, size(x2, 3)).*x2;
            elseif size(x1, 3)~=1 && size(x2, 3)==1
                Z = x1.*repmat(x2, 1, 1, size(x1, 3));
            else
                Z = x1.*x2;
            end
        end
    end
end