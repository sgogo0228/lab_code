%{
paramin
    'is_save' 是否要執行figure並儲存在硬碟中，default=1
    'dynamic_range' DR，default=42
    'output_range' 指定顯示的x軸與z軸範圍，default=[min_y, max_y]
'data_file_range'   如果掃的frame太多可能會導致out of memory，可以設定B或C的編號範圍
%}
function [us_image, us_rf, env_rf, x_axis, y_axis, I, Q] = get_us_data(fullPath, varargin)
%     close all
    paramin = parse_fun(fullPath, varargin{:});
    [dirPath, fileName, ~] = fileparts(strtrim(fullPath));
    batchSize = dir([dirPath '\' fileName(1:end-1) '*.dat']);
    batchSize = regexp({batchSize.name}, [fileName(1:end-1) '\d+\.dat']);
    batchSize = cellfun(@isempty, batchSize);
    batchSize = length(batchSize) - sum(batchSize);
    firstParam = headFile_NI([dirPath '\' fileName '.dat']);
    us_rf = firstParam.rf;
    
    %如果是Bmode會從B0開始，如果是Cmode會從C1開始，兩參數需做調整，因為B和C起始檔案號碼不同，需要對batchsize做調整
    first_batch_num = str2double(fileName(end));
    if ~first_batch_num
        batchSize = batchSize-1;
    end
    
    if isnan(paramin.data_file_range)
        paramin.data_file_range = [first_batch_num batchSize];
    else
        if paramin.data_file_range(2)>batchSize
            paramin.data_file_range(2)=batchSize;
        end
    end
    
    for b = paramin.data_file_range(1):paramin.data_file_range(2)
        param = headFile_NI([dirPath '\' fileName(1:end-1) num2str(b) '.dat']);
        us_rf(:, :, 1+size(firstParam.rf, 3)*(b-paramin.data_file_range(1)):size(param.rf, 3)+size(firstParam.rf, 3)*(b-paramin.data_file_range(1))) = param.rf;
    end
    us_rf(:, :, 2:2:end) = fliplr(us_rf(:, :, 2:2:end));
    x_axis = (1:firstParam.Aline)*firstParam.XInterval/1000;
    y_axis = ((1:firstParam.DataLength)+firstParam.Delay)/2/(firstParam.SamplingRate*1e6)*1540*1000;
    if isnan(paramin.output_range) 
        paramin.output_range = [x_axis(1) x_axis(end) y_axis(1) y_axis(end)];
    end
    env_rf=abs(hilbert(us_rf));

    % 用demodulator實現demodulation
    I = zeros(size(us_rf));
    Q = zeros(size(us_rf));
    for i=1:size(us_rf, 3)
        [I(:, :, i), Q(:, :, i)] = demod(us_rf(:, :, i), paramin.transducer_freqeuncy, firstParam.SamplingRate*1e6,'qam');
    end
    env_rf = abs(complex(I, Q));
    log_rf=20*log10(env_rf./max(env_rf, [], [1 2]));
    us_image=(log_rf+paramin.dynamic_range)/paramin.dynamic_range*255;

    if paramin.is_save
        figure('position', [100 100 800 400], 'color', 'white'); 
        for i=1:size(us_image, 3)
            image(x_axis, y_axis, us_image(:, :, i))
            set(gca, 'FontSize', 16, 'FontName', 'Times', 'FontWeight', 'bold', 'XTick', round(x_axis(1)):1:x_axis(end), 'YTick', round(y_axis(1)):1:y_axis(end))
            xlabel('Distance (mm)', 'FontSize', 20, 'FontName', 'Times', 'FontWeight', 'bold')
            ylabel('Depth (mm)', 'FontSize', 20, 'FontName', 'Times', 'FontWeight', 'bold');
            colormap(gray(255))
%             colorbar
            axis equal tight
            axis(paramin.output_range)
            [X,Map] = rgb2ind(frame2im(getframe(gcf)), 256);
            savePath = [dirPath '\us_image\' fileName '_' num2str(i) '.png'];
            if exist([dirPath '\us_image'], 'dir')==0
                mkdir([dirPath '\us_image']);
            end
            if exist(savePath, 'file')==2
                delete(savePath);
            end

            imwrite(X, Map, savePath, 'PNG');
        end
    end
end

function [param_out] = parse_fun(fullPath, varargin)
    p = inputParser;
    
    addRequired(p, 'file_path', @ischar);
    addOptional(p, 'is_save', 1, @isnumeric);
    addParameter(p, 'dynamic_range', 42, @isnumeric);
    addParameter(p, 'output_range', nan);
    addParameter(p, 'data_file_range', nan);
    addParameter(p, 'transducer_freqeuncy', 30e6, @isnumeric);
    
    parse(p, fullPath, varargin{:});
    param_out = p.Results;
end
