function [output, origSignal] = shearWaveAnalyze(I, Q, depth, time)
    close all
    % 取相位角
    s=-atan(Q./I);
    % tan相位角限定在-pi到pi之間，因此可能造成不連續(如：連續兩個點相位角為90和91會變成90和-89)
    % 若判定跟前一個點相差太多，就把它調回來
    
    for j=5:time
        if(abs(s(depth, j-1)-s(depth, j))>1)
            if(s(depth, j)<s(depth, j-1))
                s(depth, j:end) = s(depth, j:end)+pi;
            else
                s(depth, j:end) = s(depth, j:end)-pi;
            end
        end
    end

    % 求s平均並和s相減可消掉載波相位的影響
    meanValue = mean(s(depth, 5:end));
    % 理論上y已和fast time(縱向深度)無關，因此取fast time之平均獲得待測物振動之相位角
    kalmanInput = s(depth, :)-meanValue;
    
%     subplot(2, 1, 2)
%     plot(kalmanInput)
%     kalmanInput = reshape(kalmanInput(1201:16000), 400, 37)';
%     kalmanInput = mean(kalmanInput);
%     time = 2000;
%     for i=401:400:time
%         kalmanInput(i:i+399) = kalmanInput(1:400);
%     end
%     subplot(2, 1, 2)
%     plot(kalmanInput)
    
    % 各種參數初始化
    % 此部分參照Kalman Filter Motion Detection for Vibro-acoustography using Pulse Echo Ultrasound 
    detectSamplingRate = 20000;
    t = [0:time-1]/detectSamplingRate;
    signalNum = 4;
    X = zeros(2*signalNum, time);
%     X(1, 1) = 1;
%     X(3, 1) = 1;
%     X(5, 1) = 1;
%     X(7, 1) = 1;
    P = 20*eye(2*signalNum);
    F = eye(2*signalNum);
    H = zeros(time, 2*signalNum);
    for i=1:signalNum
        H(:, 2*i-1) = sin(2*pi*100*i*t)';
        H(:, 2*i) = cos(2*pi*100*i*t)';
    end
    Q = 0.0001*eye(2*signalNum);
    R = 10;

    % kalmanfilter
    for i=2:time
        xMinus = F*X(:, i-1);
        pMinus = F*P*F'+Q;
        G = pMinus*H(i, :)'/(H(i, :)*pMinus*H(i, :)'+R);
        X(:, i) = xMinus+G*(kalmanInput(i)-H(i, :)*xMinus);
        P = (eye(2*signalNum)-G*H(i, :))*pMinus;
    end
    filteredSignal = zeros(1, time);
    for i=1:2:(2*signalNum-1)
        filteredSignal = filteredSignal+H(:, i)'.*X(i, :)+H(:, i+1)'.*X(i+1, :);
    end
    output = X;
    origSignal = kalmanInput;
    
    figure
    subplot(2, 1, 1)
    plot(kalmanInput);
    title('original signal without noise')
    ylabel('phase (rad)')

    subplot(2, 1, 2)
    plot(t, filteredSignal)
    title('filtered signal')
    xlabel('time (s)')
end