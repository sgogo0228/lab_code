clear
clc
close all

filedir = 'D:\子悠_Journal\子悠資料_contusion\子悠實驗資料\實驗data\20190915_skin_TranX\wirephantom';
filename = '30MHz#2_filter25to35_amp6p5_freq20';
imageSet = dir([filedir '\' filename '_B*.dat']);
imgNum = length(imageSet);
% firstparam = headFile_NI_forCscan_Wulab([filedir '\' filename '_B1.dat']);
firstparam = headFile_NI([filedir '\' filename '_B0.dat']);
avg_rf = zeros(firstparam.DataLength, firstparam.Aline);
for i=0:imgNum-1
%     param = headFile_NI_forCscan_Wulab([filedir '\' filename '_B' num2str(i) '.dat']);
    param = headFile_NI([filedir '\' filename '_B' num2str(i) '.dat']);
    if mod(i, 2)==0
        rf = param.rf;
    else
        rf = fliplr(param.rf);
    end
    avg_rf = avg_rf+rf/imgNum;
end

env_rf = abs(hilbert(avg_rf));
log_rf = 20*log10(env_rf/max(max(env_rf)));
% dynamicRange = 42;
% img = 255*(log_rf+dynamicRange)/dynamicRange;