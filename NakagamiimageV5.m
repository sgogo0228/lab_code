% figure
%  axial resolution: 25.41  70MHz -6dB pulse length(um)   
%  axial resolution: 31.96  70MHz -10dB pulse length(um)
%  noise at focal zone: mean-> -46.8575 dB 70MHz var->2.3181 
% clc
% clear
% close all

%% 如果要單跑
file_set = char('D:\子悠_Journal\子悠資料_contusion\子悠實驗資料\實驗data\contusion05(G5_back)\20180411\right\right_day1_C1.dat');

% noiseFile = 'D:\子悠_Journal\子悠資料_contusion\子悠實驗資料\實驗data\contusion05(G5_back)\20180411\right\right_day1_C1.dat';
% noiseRange = [2900 770 200 50]; % [pos_y pos_x H W]
transducer_freq = 30;
dynamicRange = 42;
resolutionAxial = 0.0459; %mm
resolutionLateral = 0.0459;
windowFold=[11;14;17;20];
AxialROIFold = windowFold;
LateralROIFold = windowFold;
img_id_for_nakagami = (15:15);  %計算nakagami image的影像序列編號(因為時間考量，可能file_set裡的每個.dat檔裡面只有特定幾張要計算nakagami img)
% estimateNoiseLevel;
filter_range = [25 38.5];

%%
for f = 1:size(file_set)
    if ~exist(file_set(f, :), 'file')
        disp(file_set(f, :));
        return;
    end
end

param = headFile_NI(strtrim(file_set(1, :)));
% nimgs = zeros(param.DataLength, param.Aline, length(img_id_for_nakagami), size(file_set, 1));
counter = zeros(length(img_id_for_nakagami), size(file_set, 1));
for f = 1:size(file_set, 1)
    fullFilename = strtrim(file_set(f, :));
    batchSize = length(dir([fullFilename(1:end-5) '*.dat']));
    param = headFile_NI(fullFilename);
    us_rf = param.rf;
    for b = str2double(fullFilename(end-4))+1:batchSize
        param = headFile_NI([fullFilename(1:end-5) num2str(b) '.dat']);
        us_rf(:, :, size(us_rf, 3)+1:size(us_rf, 3)+size(param.rf, 3)) = param.rf;
    end
    us_rf(:, :, 2:2:size(us_rf, 3)) = fliplr(us_rf(:, :, 2:2:size(us_rf, 3)));
    
    for i=1:length(img_id_for_nakagami)
        rf = us_rf(:, :, img_id_for_nakagami(i));
        [b, a] = butter(3, 2*filter_range/param.SamplingRate);
        rf = filter(b, a, rf);
%         env_rf = abs(hilbert(rf));
        [I, Q] = demod(rf, transducer_freq*1e6, param.SamplingRate*1e6,'qam');
        env_rf = abs(complex(I, Q));
        log_rf=20*log10(env_rf/max(max(env_rf)));
        img=255*(log_rf+dynamicRange)/dynamicRange;

        xAxis = (0:param.Aline-1)*param.XInterval/1000;
        yAxis = ((1:param.DataLength)+param.Delay)/2/(param.SamplingRate*1e6)*1540*1000;
%         noiseLevel = mean(mean(log_rf(noiseRange(1):noiseRange(1)+noiseRange(3)-1, noiseRange(2):noiseRange(2)+noiseRange(4)-1)));
        nakagamiImg = zeros(param.DataLength, param.Aline, length(windowFold))-1;
        snr_img = zeros(param.DataLength, param.Aline, length(windowFold));
        
        for wf = 1:length(windowFold)
            windowZ = round(2*resolutionAxial/1000/1540*param.SamplingRate*1e6*AxialROIFold(wf));
            windowX =  round(resolutionLateral/(param.XInterval*1e-3)*LateralROIFold(wf));
            off_set_z = round(windowZ/2);
            off_set_x = round(windowX/2);
% % computations take place here
            tempImg = zeros(param.DataLength, param.Aline)-1;
            temp_snr = zeros(param.DataLength, param.Aline);
%           四個角落中訊號最小的當作雜訊
            roi_noise_lu = rf(1:windowZ, 1:windowX);
            roi_noise_ru = rf(1:windowZ, end-windowX+1:end);
            roi_noise_ld = rf(end-windowZ+1:end, 1:windowX);
            roi_noise_rd = rf(end-windowZ+1:end, end-windowX+1:end);
            roi_noise = min([sum(sum(roi_noise_lu.^2)) sum(sum(roi_noise_ru.^2)) sum(sum(roi_noise_ld.^2)) sum(sum(roi_noise_rd.^2))]);
            for jj = 1+off_set_z:param.DataLength-off_set_z
                for ii = 1+off_set_x:param.Aline-off_set_x
                    roi_rf = rf((jj-off_set_z):(jj+off_set_z)-1,(ii-off_set_x):(ii+off_set_x)-1);
                    roi_env_rf = env_rf((jj-off_set_z):(jj+off_set_z)-1,(ii-off_set_x):(ii+off_set_x)-1);
                    roi_SNR = 10*log10(sum(sum(roi_rf.^2))/roi_noise);
                    if(roi_SNR < 15)
                        mu = 0;
                    else
                        roi_env_rf_reshape = roi_env_rf(:);
                        mu = (mean((roi_env_rf_reshape).^2).^2)/mean((roi_env_rf_reshape.^2-mean(roi_env_rf_reshape.^2)).^2);
                    end
                    tempImg(jj, ii) = mu;
                    temp_snr(jj, ii) = roi_SNR;
                end
            end
            nakagamiImg(:, :,wf) = tempImg;
            snr_img(:, :,wf) = temp_snr;
        end
        counter(i, f) = toc;
        
        [fdir, fname, ~] = fileparts(fullFilename);
        
%         nimgs(:, :, i, f) = mean(nakagamiImg, 3);

% %         output nakagami image
        if ~exist([fdir '\nakagami'], 'dir')
            mkdir([fdir '\nakagami']);
        end
        figure('color', 'white')
        imagesc(xAxis, yAxis, mean(nakagamiImg, 3));
        colormap(jet(255))
        axis image
%         title('Nakagami image', 'fontSize', 20, 'fontWeight', 'bold', 'fontName', 'Times')
        xlabel('Distance (mm)', 'fontSize', 24, 'fontWeight', 'bold', 'fontName', 'Times')
        ylabel('Depth (mm)', 'fontSize', 24, 'fontWeight', 'bold', 'fontName', 'Times')
        colorbar
        set(gca, 'FontSize', 16, 'FontName', 'Times', 'FontWeight', 'bold', 'XTick',0:1:15, 'YTick',5:1:12)
        caxis([0 1])
        axis([1 14 5.1 yAxis(end)])
        [X,Map] = rgb2ind(frame2im(getframe(gcf)), 256);
        imwrite(X, Map, [fdir '\nakagami\' fname(1:end-2) num2str(img_id_for_nakagami(i)) '.jpg'], 'JPEG');
        close all
    end
end
delete(gcp('nocreate'));
