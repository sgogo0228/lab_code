function noiseNum = nakagamiImgNoise(block_struct, noiseLevel, imgHeight, imgWidth, windowZ, windowX)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
    if(block_struct.location(1)<windowZ/2 || block_struct.location(2)<windowX/2 || block_struct.location(1)>imgHeight-windowZ/2 || block_struct.location(2)>imgWidth-windowX/2)
        noiseNum = false;
    else
        noiseNum = (length(find(block_struct.data < 0.95*noiseLevel))<0.2*windowZ*windowX);
    end
end

