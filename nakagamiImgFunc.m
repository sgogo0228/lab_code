function [mu] = nakagamiImgFunc(block_struct, noiseNum)
% figure
%  axial resolution: 25.41  70MHz -6dB pulse length(um)   
%  axial resolution: 31.96  70MHz -10dB pulse length(um)
%  noise at focal zone: mean-> -46.8575 dB 70MHz var->2.3181
    roi = reshape(block_struct.data, [], 1);
    if(noiseNum(block_struct.location(1), block_struct.location(2)) > 0.2*numel(roi))
        mu = 0;
    else
        env_roi = abs(hilbert(roi));
        env_roi = env_roi/max(max(env_roi))*255;
        mu = (mean((env_roi).^2).^2)/mean((env_roi.^2-mean(env_roi.^2)).^2);
    end
end