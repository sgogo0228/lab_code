clear nClass trainedData indices net
%% prepare the input dataset
nClass = length(unique(T{:, end}));
trainedData = T{:, 1:end-1}';
classes = zeros(nClass, size(T, 1));
for i=1:size(T, 1)
    if strcmp(T{i, end}, 'normal')
        classes(1, i) = 1;
    elseif strcmp(T{i, end}, 'inflammation')
        classes(2, i) = 1;
    elseif strcmp(T{i, end}, 'week1')
        classes(3, i) = 1;
    elseif strcmp(T{i, end}, 'week2')
        classes(4, i) = 1;
    else
        classes(5, i) = 1;
    end
end

% trainedData = trainedData(1:3, :);

% net = feedforwardnet([10 10]);
% net = train(net,trainedData,classes);
% view(net)
% y = net(trainedData);
% perf = perform(net,y,classes)
% plotconfusion(classes,y)
%% NN and validation
indices = crossvalind('Kfold',T{:, end},10);
trainAcc = zeros(10, 1);
testAcc = zeros(10, 1);
tic
% pool = parpool(4);
% parfor k = 1:10
for k = 1:10
%     net = patternnet([1]);
%     net.layers{1}.transferFcn = 'purelin';
%     net.layers{2}.transferFcn = 'purelin';
%     net.outputs{2}.processParams{2}.ymin = -1;
%     net.trainFcn = 'traincgf';
%     net.performFcn='mse';
    net = feedforwardnet([10 20 20], 'traincgf');
    
%     net.outputs{2}.processFcns = {};
%      net.outputs{4}.processFcns = {};
%     net.outputs{4}.processParams{2}.ymin = 0;
%     net.inputs{1}.processParams{2}
%     net.inputs{1}.processParams{2}.ymin = 0;
%     net.inputs{1}.processParams{2}.ymax = 1;
%     net.layers{1}.transferFcn = 'purelin';
%     net.layers{2}.transferFcn = 'tansig';
%     net.layers{3}.transferFcn = 'logsig';
%     net.layers{4}.transferFcn = 'tansig';
%     net.layers{4}.transferFcn = 'logsig';
    net.divideFcn = 'dividetrain';
    net.trainParam.epochs = 1000;
    net.trainParam.goal = 1e-6;
    net.trainParam.time = 3600;
%     net.trainParam.min_grad = 1e-6;
    

    testing = (indices == k);
    testX = trainedData(:, find(testing));
    testY = classes(:, find(testing));
    trainX = trainedData(:, find(~testing));
    trainY = classes(:, find(~testing));
    
    net = configure(net, trainX, trainY);
    [net tr] = train(net,trainX,trainY);
    
    predicted = net(trainX);
    [~, labeled] = max(trainY);
    [~, predicted] = max(predicted);
    cm = confusionmat(labeled, predicted);
    for c=1:nClass
        trainAcc(k) = trainAcc(k) + cm(c, c)/sum(sum(cm));
    end
    
    predicted = net(testX);
    [~, labeled] = max(testY);
    [~, predicted] = max(predicted);
    cm = confusionmat(labeled, predicted);
    for c=1:nClass
        testAcc(k) = testAcc(k) + cm(c, c)/sum(sum(cm));
    end
end
% delete(pool);
disp(['training accuracy: ' num2str(mean(trainAcc))])
disp(['testing accuracy: ' num2str(mean(testAcc))])
toc