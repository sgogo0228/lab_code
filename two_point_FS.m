clear
clc
% close all
%% 
unitAline = 199;
pushNum = 5;
distNum = 10;
depth = [1 5000];
% CImage = '30MHz_dist0to5_depth5to14';
analyzeFilePath = ['H:\school\shearWaveData\20180409_contusion_d0\rat#head_normal_left'];
file = char('H:\school\shearWaveData\20180409_contusion_d0\rat#head_normal_left\30MHz_dist0to5_depth9to14k_amp600_cycle5100_1',...
    'H:\school\shearWaveData\20180410_contusion_d0\rat#back_contusion_right\30MHz_dist0to5_depth9to14k_amp600_cycle5100_1');
pushNoise=10;
detectSamplingRate=10000;   %千萬不要打錯!! 是指detect探頭的PRF
hormonicNum = 5;
baseFrequency=50;
modelParams = struct('unitAline', unitAline, 'pushNum', pushNum, 'usedCycle', 3, 'frontNoise', pushNoise-3, 'backNoise', 3, 'damping', 0);
slopeFitDotNum = [6 8];
analysisDistRange = [6 15];

fnum = size(file, 1);
bandFilter = [baseFrequency*0.7 baseFrequency*6];
overallDepth = depth(2)-depth(1)+1;
modifiedUnitAline = (modelParams.unitAline-modelParams.frontNoise-modelParams.backNoise-modelParams.damping);
unusedCycle=modelParams.pushNum-modelParams.usedCycle;

analyzeInterval = 250;  %% 經驗上分析肌肉的SW 100或200(以上也可以500)都可以，太少變異大
for i=1:overallDepth/analyzeInterval
    range(i, 1) = (i-1)*analyzeInterval+1;
    range(i, 2) = i*analyzeInterval;
end
% xcorrDepth = 3;
sgn = zeros(fnum, distNum, analysisDistRange(2)-analysisDistRange(1)+1, unitAline*pushNum);

for f = 1:fnum
    filename = [strtrim(file(f, :)) '_SW0.dat'];
    if ~exist(filename, 'file')
        f
        disp(filename);
        return;
    end
end

pool = parpool(6);
try
    for f=1:fnum
        demodSignal = zeros(distNum, overallDepth, unitAline*pushNum);
        parfor j=0:distNum-1
            filename = [strtrim(file(f, :)) '_SW' num2str(j) '.dat'];
            [sgnParam demodSignal(j+1, :, :)]=shearWaveByDemod20170327(filename, depth, unitAline, pushNum, pushNoise);
        end
        for r = 1:analysisDistRange(2)-analysisDistRange(1)+1
            sgn(f, :, r, :) = squeeze(mean(demodSignal(:, range(r, 1):range(r, 2), :), 2));
        end
    end
catch
    delete(gcp('nocreate'));
end
delete(gcp('nocreate'));

temp = sgn;


sgn = temp;
timeAxis = (0:(modelParams.pushNum*modelParams.unitAline-1))/detectSamplingRate;
% 去除push造成的雜訊
for i=pushNum-1:-1:0
    shift = modelParams.unitAline*i;
    sgn(:, :, :, (1+modelParams.unitAline-modelParams.damping)+shift:modelParams.unitAline+shift) = [];
    sgn(:, :, :, 1+shift:(modelParams.frontNoise+modelParams.backNoise)+shift) = [];
    timeAxis(1+(modelParams.unitAline-modelParams.backNoise-modelParams.damping)+shift:modelParams.unitAline+shift) = [];
    timeAxis(1+shift:(modelParams.frontNoise)+shift) = [];
end
timeAxis = timeAxis(1:modifiedUnitAline*3);
tempSgn = permute(sgn, [4 3 2 1]);
tempSgn = smoothdata(tempSgn, 'movmean', 5);
sgn = permute(tempSgn, [4 3 2 1]);
clear tempSgn phase_50hz

% 去除前後較不穩定的訊號(刪除第一個和最後一個shearwave)
sgn(:, :, :, 1:modifiedUnitAline) = [];
sgn(:, :, :, end-(unusedCycle-1)*modifiedUnitAline+1:end) = [];

% % 將剩餘的較穩定的訊號做平均作為kalman filter輸出
for f=1:fnum
    for i=1:distNum
        for d = 1:analysisDistRange(2)-analysisDistRange(1)+1
            tempSgn = mean(reshape(sgn(f, i, d, :), modifiedUnitAline, modelParams.usedCycle), 2)';
            sgn(f, i, d, 1:modifiedUnitAline) = tempSgn;
        end
    end
end
sgn(:, :, :, modifiedUnitAline+1:end) = [];

SW_attenuation = zeros(10, 10, 3, fnum);
for fn = 1:fnum
    for dp = 5:7
        signal = squeeze(squeeze(sgn(fn, :, dp, :))');

        % % 單純是為了show Spatiotemporal影像
        img = (signal-min(min(signal)))/(max(max(signal))-min(min(signal)))*64;
        [iX,iY] = meshgrid((0:distNum-1)/distNum*5, (0:modifiedUnitAline-1)*0.1);
        [Xq,Yq] = meshgrid((0:199)/200*4.5, (0:modifiedUnitAline-1)*0.1);
        Vq = interp2(iX,iY,img,Xq,Yq);
        figure
        x = (0:distNum-1)/distNum*5;
        y = (0:modifiedUnitAline-1)*0.1;
        image(y, x, Vq')
        set(gca,'YDir','normal')

        % % 對原始Spatiotemporal影像(沒有內插)進行2D FFT取得k1
        nfftx = 8192;
        nffty = 512;
        imgFFT = abs(fftshift(fft2(signal, nfftx, nffty)))';
        figure
        x = ((0:nfftx-1)-(nfftx/2))/nfftx*10000;
        y = ((0:nffty-1)-(nffty/2))/nffty*12566;
        imagesc(x, y, flip(imgFFT))
        set(gca,'YDir','normal')
        axis([0 400 0 2000])

        [max_y_value, max_y_index] = max(imgFFT(1:256, 4097:end));
        [~, max_x_index] = max(max_y_value);
        max_y_index = max_y_index(max_x_index);
        max_x_index = max_x_index+4096;

        % % 將訊號fit到gamma dist
        nfft = 8192;
        signal_fft = abs(fft(signal, nfft));
        f = (0:nfft-1)'/nfft*detectSamplingRate;
        % plot(f, signal_fft(:, 3))
        % log_spectrum = 20*log10(spectrum./max(spectrum));
        gammafun = @(param, xdata) param(1)*(xdata.^param(2)).*exp(-xdata/param(3));
        fit_param = zeros(3, distNum);
        fit_curve = zeros(size(signal_fft));
        for d=1:distNum
            fit_param(:, d) = lsqcurvefit(gammafun, [1 0.1 1], f, signal_fft(:, d));
            fit_curve(:, d) = fit_param(1, d)*(f.^fit_param(2, d)).*exp(-f/fit_param(3, d));
        end
        % % 用fit過的curve找中心頻率
        fit_curve(1, :) = 0.001;    %gamma fit完後第一個值是0，在壓縮時會有問題
        central_frequency = zeros(distNum, 1);
        compressed_curve = 20*log10(fit_curve./max(fit_curve));
        for d=1:distNum
            for i=1:nfft-1
                if compressed_curve(i, d)<-6 && compressed_curve(i+1, d)>-6
                    bandwith_left = i;
                end
                if compressed_curve(i, d)>-6 && compressed_curve(i+1, d)<-6
                    bandwith_right = i;
                    break;
                end
            end
            central_frequency(d) = (f(bandwith_left)+f(bandwith_right))/2;
        end


        for d=2:distNum
            maxCCindex = zeros(distNum, 1);
            maxCCindex(1) = size(sgn, 4);
            xcr = zeros(distNum, 2*size(sgn, 4)-1);
            for d=2:distNum
                xcr(d, :) = xcorr(signal(:, 1), signal(:, d));
                [~, I] = findpeaks(squeeze(xcr(d, :)));
                [~, tempIdx] = min(abs(I-maxCCindex(d-1)));
                maxCCindex(d) = I(tempIdx);
            end
            maxCCindex = maxCCindex-size(sgn, 4);
        end

        coef = zeros(10);
        for i=1:9
            for j=i+1:10
                n_bar = (fit_param(2, i) + fit_param(2, j))/2;
                source_fb_bar = central_frequency(i)/(n_bar+1);
                attenuated_fb_bar = central_frequency(j)/(n_bar+1);
                R_bar = (pi*(maxCCindex(i)-maxCCindex(j))*0.1*0.001*source_fb_bar*attenuated_fb_bar)/source_fb_bar-attenuated_fb_bar;
                k1 = y(nffty-max_y_index);
                k2 = k1*(R_bar-(1+R_bar^2)^0.5);
                coef(i, j) = 2*k2/(central_frequency(i)-central_frequency(j));
            end
        end
        SW_attenuation(:, :, dp-4, fn) = coef;
    end
end

