%% 用_A或_B計算探頭的解析度

% wirephantom_file = "K:\黃大銘實驗資料\skin\20190915_skin_TranX\wirephantom\30MHz#2_filter25to35_amp6p5_freq20_B1.dat";
wirephantom_file = "D:\子悠_Journal\子悠資料_contusion\子悠實驗資料\實驗data\20190915_skin_TranX\wirephantom\30MHz#2_filter25to35_amp6p5_freq20_B0.dat";
param = headFile_NI(wirephantom_file);
rf = param.rf;

[~, temp] = max(rf, [], 'all', 'linear');
[max_i, max_j] = ind2sub(size(rf), temp);

compressed_rf = abs(hilbert(rf));
compressed_rf = 20*log10(compressed_rf/compressed_rf(max_i, max_j));
search_direction = [-1 0; 1 0; 0 -1; 0 1];
for i=1:4
    index = [max_i, max_j];
    while compressed_rf(index(1), index(2)) > -6
        index = index + search_direction(i, :);
    end
    index_pre = index - search_direction(i, :);
%   用線性內插法找到-6db的點
    border(i, :) = index_pre+search_direction(i, :)*(-6-compressed_rf(index_pre(1), index_pre(2)))/(compressed_rf(index(1), index(2))-compressed_rf(index_pre(1), index_pre(2)));
end

resolution_axial = 0.5*(border(2, 1)-border(1, 1))/(param.SamplingRate*1e6)*1500*1000  %mm
resolution_lateral = (border(4, 2)-border(3, 2))*param.XInterval/1000  %mm