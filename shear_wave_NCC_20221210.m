function [params, idx] = shear_wave_NCC_20221210(filename, depth, interp_time_fold)
    % nfft = 4096;
    % 讀取檔案
    params = headFile_NI(filename);
    data = params.rf;
    data = data*params.Vpp/255;
    quantify_range = round([9/1540/1000*1e9*2-params.Delay-depth 9/1540/1000*1e9*2-params.Delay+depth]);
    
    [paramB, paramA] = butter(5, [25e6 35e6]/(1e9/2));    %第一個參數須視情況調整
    data = filtfilt(paramB, paramA, data);
    [X, Y] = meshgrid(1:params.Aline, 1:params.DataLength);
    [xq, yq] = meshgrid(1:params.Aline, linspace(1, params.DataLength, params.DataLength*interp_time_fold));
    interp_data = interp2(X, Y, data, xq, yq, 'cubic');

    quantify_range = [quantify_range(1)*interp_time_fold+1 quantify_range(2)*interp_time_fold];
    idx = zeros(quantify_range(2)-quantify_range(1)+1, params.Aline);
    w = 50*interp_time_fold;
% tic
    for i=1:params.Aline
        for j=quantify_range(1):quantify_range(2)
            [~, idx(j-quantify_range(1)+1, i)] = max(xcorr(interp_data(j-w:j+w, 1), interp_data(j-w:j+w, i)));
        end
    end
    idx = mean(cat(3, idx(1:2:end, :), idx(2:2:end, :)), 3);
% toc

end

