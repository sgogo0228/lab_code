% % 此code能輸出IB, GLCM feature, 1-4階momentum, snr, nakagami, wmc_nakagami

% #主要輸出為QUS

%% 單跑
% % 只需要輸入第一筆(_C1.dat或_B0.dat)的檔名就好

% noiseFile = 'I:\黃大銘實驗資料\skinflap\20200615_transducer_char\waternoise\30MHz_eng1_dmp1_newwire_B1.dat';
% % 計算WMC各window size的雜訊(這個方法是將打水的訊號做為雜訊，目前都是用四個角落的方法取雜訊)
% estimateNoiseLevel;
% noise4IB = noiseSquareSum(1);

% file = char('D:\子悠_Journal\子悠資料_contusion\子悠實驗資料\實驗data\contusion05(G5_back)\20180410\right\right_day0_C1.dat');
% stainlessBlockData = 'F:\子悠_Journal\子悠資料_contusion\子悠實驗資料\實驗data\20190915_skin_TranX\pulseecho\30MHz#2_filter25to35_amp0p5_freq20_attn6_B0.dat';    %skin跟contusion是用同一顆
% dynamic_range = 42;
% WMC_window_fold=[9; 11; 13; 15];
% ROI_window_fold = [30 60];
% resolutionAxial = 0.0459; %mm
% resolutionLateral = 0.0743;
% % estimateNoiseLevel;
% nfft = 4096;
% filter_range = [25 38.5];
% glcm_dist = [1;2;3;4;5;6;7;8;9;10;11;12;13;14;15;16;17;18;19;20];
% glcm_dist = [glcm_dist*10 glcm_dist*10];
% glcm_dist = [1 1; 2 2; 3 3; 4 4; 5 5; 7 7; 10 10; 15 15; 20 20; 25 25; 30 30; 40 40; 50 50; 60 60; 2 13; 4 26; 6 39; 8 52; 10 65];


%% #程式
clear QUS
figure;
full_file_name = strtrim(file);
us_rf = [];
log_rf = [];
attenuator = 6;  %如果檔名顯示有衰減過，需要在讀檔時補償回去
QUS(10) = struct();
% QUS(size(us_rf, 3)) = struct();
% % 計算鋼塊頻譜
param = headFile_NI(stainlessBlockData);
% % 根據是否有加上衰減器對rf做補償
stainlessBlockFFT = mean(abs(fft(param.rf*10^(attenuator/20), nfft)), 2);
param = headFile_NI(full_file_name);

[us_image, us_rf, ~, x_axis, y_axis] = get_us_data(full_file_name, struct('is_save', 0, 'dynamic_range', dynamic_range));

% [paramB, paramA] = butter(3, filter_range*1e6/(param.SamplingRate*1e6/2));    %第一個參數須視情況調整(-10db)
% us_rf = filtfilt(paramB, paramA, us_rf);


for f=int32(size(us_rf, 3))/2-4:int32(size(us_rf, 3)/2)+5
    if f<0 || f>size(us_rf, 3)
        QUS(1).roi = nan;
        QUS(1).nakagami = nan;
        QUS(1).IB = nan;
        % QUS(1).glcm = nan;
        % QUS(1).glcm_feature = nan;
        QUS(1).hist_mean = nan;
        QUS(1).hist_var = nan;
        QUS(1).hist_skew = nan;
        QUS(1).hist_kurt = nan;
        QUS(1).SNR = nan;
        QUS(1).WMCNakagami = nan;
        break;
    end
%     #會打在這邊是因為算WMC時的window size會不一樣
    AxialROIFold = ROI_window_fold(1); %AxialROIFold = 30;
    LateralROIFold = ROI_window_fold(2); %LateralROIFold = 60;
    
%     #___找出ROI的座標
    temp_rf = us_rf(:, :, f);
    temp_img = us_image(:, :, f);
    image(x_axis, y_axis, temp_img)
    temp = drawrectangle('Position', temp_pos.roi_pos, 'RotationAngle', temp_pos.roi_degree, 'Color', 'y', 'InteractionsAllowed', 'translate', 'Rotatable', true);
    % temp = drawrectangle('Position', temp_pos(f).roi_pos, 'RotationAngle', temp_pos(f).roi_degree, 'Color', 'y', 'InteractionsAllowed', 'translate', 'Rotatable', true);
    temp_BW = createMask(temp);
    temp_BW_index = find(temp_BW);
    
%     #___因為ROI有可能轉一個角度，某些aline只有很少數的點在ROI內，在統計上可能會有問題，所以要找出那些aline
    BW_nonzero_num = sum(temp_BW, 1);
    temp_fft_rf = [];
    for i=1:length(BW_nonzero_num)
        % %如果aline上被判定為ROI的點多於0.95*ROI高度(ROI縱向的點數)，才做FFT
        if BW_nonzero_num(i) > resolutionAxial*AxialROIFold/1000/1540*param.SamplingRate*1e6*2*0.95
            temp_fft_rf = cat(2, temp_fft_rf, abs(fft(temp_rf(temp_BW(:, i), i), nfft)));
        end
    end
       
%     #___將非ROI區域的部分在image上剪掉
    temp = temp_img.*temp_BW;
    roi_img =  temp(any(temp,2),:);
    roi_img = roi_img(:,any(temp,1));
    roi_img = uint8(roi_img);
    QUS(f).roi = roi_img;
    
%     #___算IB
    temp_fft_rf = mean(temp_fft_rf, 2);
    temp_log_fft = 20*log10(temp_fft_rf/max(temp_fft_rf(20:end-20)));
    [~, index] = max(temp_log_fft(20:end/2)); % #Find center freq.
    index = index+19;   %上面為了避免找到index=1所以從第20個點開始找，因此要加回來
    x1 = index; x2 = index;
    while temp_log_fft(x1) > -6 && x1>1
       x1 = x1-1;
    end
    while temp_log_fft(x2) > -6 && x2<length(temp_log_fft)
       x2 = x2+1;
    end
    s1 = sum(temp_fft_rf(x1+1:x2));
    s2 = sum(stainlessBlockFFT(x1+1:x2));
    QUS(f).IB = 20*log10((s1/s2)/(x2-x1));  %鋼塊 bandwidth：12  取得點數要相同

%     #___算GLCM參數
    % for n=1:length(glcm_dist)
    %     temp = get_glcm(roi_img, glcm_dist(n, 1), glcm_dist(n, 2));
    %     % QUS(f).glcm(n).array = temp;
    %     QUS(f).glcm_feature(n) = GLCM_Features(temp, 0);
    % end
    
%     #___算統計參數
    temp = mean(roi_img, 'all');
    temp2 = var(double(roi_img), 0, 'all');
    QUS(f).hist_mean = mean(roi_img, 'all');
    QUS(f).hist_var = var(double(roi_img), 0, 'all');
    QUS(f).hist_skew = (sum((roi_img-temp).^3, 'all')./numel(roi_img)) ./ (temp2.^1.5);
    QUS(f).hist_kurt = (sum((roi_img-temp).^4, 'all')./numel(roi_img)) ./ (temp2.^2);
    
%     #___算SNR和Nakagami參數
    roi_rf = temp_rf(temp_BW_index);
    [roi_index_row, roi_index_column] = ind2sub(size(temp_rf), temp_BW_index);
    % %這一段是為取與ROI同樣大小和角度的雜訊，直接將ROI的區域平移到影像的邊緣(此段是移到右上角)
    noise_region_shift = size(temp_rf, 1)*(size(temp_rf, 2)-max(roi_index_column))-(min(roi_index_row)-1);
%     % %這一段是為取與ROI同樣大小和角度的雜訊，直接將ROI的區域平移到影像的邊緣(此段是移到左上角)
%     noise_region_shift = -(size(temp_rf, 1)*(min(roi_index_column)-1)+(min(roi_index_row)-1));
    noise_index = temp_BW_index+noise_region_shift;
    roi_noise = temp_rf(noise_index);
    QUS(f).SNR = 10*log10(sum(roi_rf.^2)/sum(roi_noise.^2));
    env_roi = abs(hilbert(roi_rf));
    env_roi = env_roi/max(env_roi, [], 'all')*255;
    nakagami_param = mle(env_roi, 'distribution', 'nakagami');
    QUS(f).nakagami = nakagami_param(1);
%     figure; hist = histogram(env_roi,'BinWidth' , 9.9);

% % #做WMC nakagami parameter
    AxialROIFold = WMC_window_fold;
    LateralROIFold = WMC_window_fold;
    Nakagami_image_v8;
    nakagami_img(nakagami_img==0) = nan;
    nakagami_img = mean(nakagami_img, 3, 'omitnan');
    QUS(f).WMCNakagami = mean(nakagami_img(temp_BW_index), 'omitnan');
end
clear temp_rf temp_log temp_nakagami nakagamiImg temp_BW temp_fft_rf temp_log_fft
close all

