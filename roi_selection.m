% % figure
% % close all
% % clear;
% % clc

% % 只需要輸入第一筆(_C1.dat或_B0.dat)的檔名就好
% % 如果檔名顯示有衰減過，需要在讀檔時補償回去
% file = char('D:\子悠_Journal\子悠資料_contusion\子悠實驗資料\實驗data\contusion05(G5_back)\20180410\right\right_day0_C1.dat');

for f = 1:size(file, 1)
    if ~exist(file(f, :), 'file')
        f
        disp(file(f, :));
        return;
    end
end
% dynamicRange = 42;
% resolutionAxial = 0.0459; %mm
% resolutionLateral = 0.0743;
% AxialROIFold = 30;
% LateralROIFold = 60;
nfft = 4096;

% % 根據是否有加上衰減器對rf做補償
% stainlessBlockSgn = mean(param.rf/10^-(0.5), 2);
% stainlessBlockFFT = abs(fft(stainlessBlockSgn, nfft)).^2;

us_rf = [];
log_rf = [];
for f = 1:size(file, 1)
    full_file_name = strtrim(file(f, :));
    batch_size = length(dir([full_file_name(1:end-5) '*dat']));
    for b = 1:batch_size
        param = headFile_NI([full_file_name(1:end-5) num2str(b) '.dat']);
        us_rf = cat(3, us_rf, param.rf);
    end

us_rf(:, :, 2:2:end) = fliplr(us_rf(:, :, 2:2:end));
env_rf = abs(hilbert(us_rf));
cmp_rf = 20*log10(env_rf./max(env_rf, [], [1 2]));
us_image = (cmp_rf++dynamic_range)/dynamic_range*255;

clear us_rf env_rf cmp_rf
%% select ROI
    x_axis = (1:param.Aline)*param.XInterval/1000;
    y_axis = ((1:param.DataLength)+param.Delay)/2/(param.SamplingRate*1e6)*1540*1000;
    clear roi_info
    for i = 1:size(us_image, 3)
        temp_img = us_image(:, :, i);

        figure('color', 'white', 'position', [500 200 1000 500])
        image(x_axis, y_axis, temp_img)
        colormap(gray(255))
        axis equal tight
        title(i)
        xlabel('Distance (mm)', 'fontSize', 28, 'fontWeight', 'bold', 'fontName', 'Times')
        ylabel('Depth (mm)', 'fontSize', 28, 'fontWeight', 'bold', 'fontName', 'Times')
        set(gca, 'FontSize', 16, 'FontName', 'Times', 'FontWeight', 'bold', 'XTick', round(x_axis(1)):2:x_axis(end), 'YTick', round(y_axis(1)):1:y_axis(end))
        hold on
        plot([x_axis(1) x_axis(end)], [y_axis(length(y_axis)/2) y_axis(length(y_axis)/2)], 'y-', 'LineWidth', 2)
        plot([x_axis(length(x_axis)/2) x_axis(length(x_axis)/2)], [y_axis(1) y_axis(end)], 'y-', 'LineWidth', 2)

        origY = (1+param.Delay)/(param.SamplingRate*10^6)*1540*1000/2;
        if (i>1)
    %         h = drawrectangle('Position', roi_info(f-1).roi_pos, 'RotationAngle', roi_info(f-1).roi_degree, 'Color', 'y', 'InteractionsAllowed', 'translate', 'Rotatable', true);
            h = drawrectangle('Position', [roi_info(i-1, f).roi_pos(1), roi_info(i-1, f).roi_pos(2), roi_info(i-1, f).roi_pos(3), roi_info(i-1, f).roi_pos(4)], 'RotationAngle', roi_info(i-1).roi_degree, 'Color', 'y', 'InteractionsAllowed', 'translate', 'Rotatable', true);
        else
            h = drawrectangle('Position', [0 origY resolutionLateral*LateralROIFold resolutionAxial*AxialROIFold], 'Color', 'y', 'InteractionsAllowed', 'translate', 'Rotatable', true);
        end
        roi_info(i, f) = custom_wait_for_ROI(h);
        close(gcf);
    end
end
clear temp_img us_image
