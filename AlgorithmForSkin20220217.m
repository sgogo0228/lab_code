% clear
clc
% close all
%%
% 能夠一次分析好幾組shear wave

fileSet = readcell('K:\temp\path.txt', 'Delimiter', ',');
% fileSet = {'L:\黃大銘實驗資料\contusion\45cm\20200918_shearwave_#5\day0\right\30MHz_dist0to5_depth7to16k_amp600_cycle1700_1_SW0'}';
distPostfix_skin = {'0', '0p5', '1', '1p5', '2', '2p5', '3', '3p5', '4', '4p5'};
distPostfix_muscle = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};

params = headFile_NI([strtrim(fileSet{1}) '.dat']);
push_num = 5;                           %在固定的蒐集時間內(params.aline)，push打了幾次，跟Push的PRF有關
position_num = 10;                      %蒐集多少個position的signal
interp_dist_fold = 10;                  %內差的倍數
interp_time_fold = 1;
interp_depth_fold = 3;
interp_dist_pts = (length(distPostfix_skin)-1) * interp_dist_fold;
push_noise=15;                          %push干擾detect的點數
baseFrequency=50;
hormonicNum = 5;                      %kalman filter故估計頻率的上限(如果push的PRF是50Hz，那就會最高抓到50*此變數)
% slopeFitDotNum = [6 8];               %最後計算斜率時參考的點數
depth = [1 5000];
detect_sampling_rate=10000;   %千萬不要打錯!! 是指detect探頭的PRF
analyzeInterval = 250;                  % 經驗上分析肌肉的SW 100或200(以上也可以500)都可以，太少變異大
W0 = 30*1e6;
unitAline = params.Aline/push_num;      %push打一次所需要的時間
modelParams = struct('unitAline', unitAline, 'push_num', push_num, 'usedCycle', 3, 'front_noise', push_noise-3, 'backNoise', 3, 'damping', 0);


overall_depth = depth(2)-depth(1)+1;
fnum = size(fileSet, 1);
bandFilter = [baseFrequency*0.7 baseFrequency*6];
modifiedUnitAline = (modelParams.unitAline-modelParams.front_noise-modelParams.backNoise-modelParams.damping);
unusedCycle=modelParams.push_num-modelParams.usedCycle;

for f = 1:fnum
    filename = [strtrim(fileSet{f}) '.dat'];
    if ~exist(filename, 'file')
        f
        disp(filename);
        return;
    end
end

% gcp
D = size(fnum, 1);
sgn = zeros(modifiedUnitAline, overall_depth/analyzeInterval*interp_depth_fold, interp_dist_pts, fnum);
h = waitbar(1/position_num, ['f = 1 (1/' num2str(position_num) ')']);
for f = 1:fnum
    params = headFile_NI([strtrim(fileSet{f}) '.dat']);
    if params.Delay > 8000
        depth = [1 5000];
    else
        depth = [2001 7000];
    end

%     temp.sgn = zeros(depth(2)-depth(1)+1, 995, position_num);
%     temp.sgn = zeros(modifiedUnitAline*push_num*interp_time_fold, overall_depth, position_num);
    waitbar(1/position_num, h, ['f = ' num2str(f) '(1/' num2str(position_num) ')']);
    for j=1:position_num
        waitbar(j/position_num, h, ['f = ' num2str(f) '(' num2str(j) '/' num2str(position_num) ')']);

%         %  --for skin scanning protocol
%         filename = [regexprep(strtrim(fileSet{f}), 'dist0', ['dist' num2str(distPostfix_skin{j})]) '.dat'];
 
        %   --for muscle scanning protocol
        filename = [regexprep(strtrim(fileSet{f}), 'SW0', ['SW' num2str(distPostfix_muscle{j})]) '.dat'];
        [sgnParam, temp.sgn(:, :, j)]= shearWaveByDemod20220211(filename, depth, unitAline, push_num, push_noise, W0, interp_time_fold);
        
    end
    [paramB, paramA] = butter(5, bandFilter(2)/(interp_time_fold*detect_sampling_rate/2));    %第一個參數須視情況調整
    temp.sgn = filtfilt(paramB, paramA, temp.sgn);

    temp.sgn = reshape(temp.sgn, modifiedUnitAline, push_num, analyzeInterval, overall_depth/analyzeInterval, position_num);
    temp.sgn = squeeze(mean(temp.sgn(:, 2:4, :, :, :), [2, 3]));

    [temp.X,temp.Y,temp.Z] = meshgrid(1:size(temp.sgn, 2), 1:size(temp.sgn, 1), 1:size(temp.sgn, 3));
    [temp.Xq,temp.Yq,temp.Zq] = meshgrid(linspace(1, size(temp.sgn, 2), size(temp.sgn, 2)*interp_depth_fold), 1:size(temp.sgn, 1), linspace(1, size(temp.sgn, 3), interp_dist_pts));
    sgn(:, :, :, f) = interp3(temp.X, temp.Y, temp.Z, temp.sgn, temp.Xq, temp.Yq, temp.Zq, "cubic");

%     sgn = sgn - min(sgn, [], [1]);
%     sgn = sgn ./ max(sgn, [], [1]);
    
% %     --為計算以voltage為單位的SW振福，用到的參數beta、W0、D都跟2007這篇一樣
%     temp.beta = max(sgn(:, :, :, f));
%     D(f) = temp.beta*1540/2/W0;
end
close(h);
delete(gcp('nocreate'));
clear temp
backup = sgn;
sgn = backup;

%%
% clear x y
% interp_time_fold = 3;
% CC_window = [round(size(sgn, 1)*interp_time_fold*0.8):round(size(sgn, 1)*interp_time_fold*1.2)];
% anal_pos = [1:size(sgn, 3)];
% anal_depth = [1:size(sgn, 2)];
% for f=1:fnum
%     clear C
%     [temp.X,temp.Y,temp.Z] = meshgrid(1:size(sgn, 2), 1:size(sgn, 1), 1:size(sgn, 3));
%     [temp.Xq,temp.Yq,temp.Zq] = meshgrid(1:size(sgn, 2), linspace(1, size(sgn, 1), size(sgn, 1)*interp_time_fold), 1:size(sgn, 3));
%     temp.interp_sgn = interp3(temp.X,temp.Y,temp.Z, sgn(:, :, :, f), temp.Xq,temp.Yq,temp.Zq, "cubic");
%     phaseAngle = zeros(interp_dist_pts, size(sgn, 2), hormonicNum);
%     for r=1:size(sgn, 2)
%         temp.xcr = xcorr(squeeze(temp.interp_sgn(:, r, :)));
%         temp.xcr = temp.xcr(:, 1:interp_dist_pts);
%         [~, temp.I(:, r)] = max(temp.xcr(CC_window, :), [], 1);
%     end
%     temp.maxCCindex = temp.I - size(sgn, 1)*interp_time_fold + CC_window(1) - 1;
%     phaseAngle(:, :, 1) = temp.maxCCindex / interp_time_fold * 0.1;     %一個點代表0.1 ms
% 
%     [x, y(:, :, f)] = linearFitForStatistic20220216(phaseAngle, anal_pos, anal_depth);
%     
% %     [path, ~, ~] = fileparts(strtrim(fileSet{f}));
% %     if exist([path(1:end-10) '\avtech_30MHz_amp1p5_freq2_pos6_B0.dat'])
% % 
% %         [imgs, ~, ~, xAxis, yAxis] = get_us_data([path(1:end-10) '\avtech_30MHz_amp1p5_freq2_pos6_B0'], 'dynamic_range', 45);
% % %         image(xAxis, yAxis, imgs(:, :, 1))
% %         hold on
% %         xAxis = linspace(5, 9.5, size(sgn, 1)*interp_time_fold);
% %         yAxis = ((depth(1):depth(2))+params.Delay)/params.SamplingRate/1e6/2*1540*1000;
% %         h = image(xAxis, yAxis+0.2, 257 + abs(y(:, :, f))'/4 * 256);
% %         set(h, 'AlphaData', 0.4);
% %         colormap([gray(256); turbo(256)])
% %         colorbar
% %         axis image
% %         axis([4 11 7 12])
% %         [X,Map] = rgb2ind(frame2im(getframe(gcf)), 256);
% %         savePath = ['L:\黃大銘實驗資料\skin\temp\' num2str(f) '_withoutWindow.png'];
% %         imwrite(X, Map, savePath, 'PNG');
% %     end
% %     xAxis = linspace(5, 9.5, size(sgn, 1)*interp_time_fold);
% %     yAxis = ((depth(1):depth(2))+params.Delay)/params.SamplingRate/1e6/2*1540*1000;
% 
%     C{1, 1} = 'position';
%     for d=1:length(anal_depth)
%         d_mm = round((((d - 1) * analyzeInterval / interp_depth_fold) + depth(1) + params.Delay - 1) / 2 / params.SamplingRate / 1e6 * 1000 * 1540, 2);
%         C{1, d+1} = ['depth_' num2str(d_mm)];
%     end
%     C = [C; num2cell([x' abs(y(:, :, f))])];
% 
%     [path, name, ~] = fileparts(strtrim(fileSet{f}));
%     if exist([path '_' name '_velocity.xlsx'])
%         delete([path '_' name '_velocity.xlsx']);
%     end
% %     if exist(['L:\黃大銘實驗資料\skin\temp\' num2str(f) '_withoutWindow.xlsx'])
% %         delete(['L:\黃大銘實驗資料\skin\temp\' num2str(f) '_withoutWindow.xlsx']);
% %     end
%     writecell(C, [path '_' name '_velocity.xlsx']);
% %     writecell(C, ['L:\黃大銘實驗資料\skin\temp\' num2str(f) '_withoutWindow.xlsx']);
%     close all
% end

%% show spatiotemporal image
interp_time_fold = 3;
moving_window = false;
window_width = 3.5; %unit: mm
window_width_pts = round(window_width / 0.5) * interp_dist_fold;
analysis_region = 31:40;   %可能不會想整個深度都分析，可自設區間
depth_num = analysis_region(2)-analysis_region(1)+1;
rsd_threshold = 10;
result = zeros(2, fnum);
% [43 44 45 88 89 90 282 283 284 321 322 323 361 362 363];
for f = 1:fnum
    params = headFile_NI([strtrim(fileSet{f}) '.dat']);
    if params.Delay > 8000
        depth = [1 5000];
    else
        depth = [2001 7000];
    end

    [temp_X,temp_Y,temp_Z] = meshgrid(1:size(sgn, 2), 1:size(sgn, 1), 1:size(sgn, 3));
    [temp_Xq,temp_Yq,temp_Zq] = meshgrid(1:size(sgn, 2), linspace(1, size(sgn, 1), size(sgn, 1)*interp_time_fold), 1:size(sgn, 3));
    temp_interp_sgn = interp3(temp_X,temp_Y,temp_Z, sgn(:, :, :, f), temp_Xq,temp_Yq,temp_Zq, "cubic");
    [dirPath, fileName, ~] = fileparts(strtrim(fileSet{f}));
    velocity = zeros(1, size(temp_interp_sgn, 2));
    attenuation = zeros(1, size(temp_interp_sgn, 2));
    for dp = analysis_region
        temp_st_image = squeeze(temp_interp_sgn(:, dp, :));

        % -這邊先輸出spatiotemporal image
        figure('color', 'white', 'position', [500 200 1000 500])
        x = (0:size(temp_st_image, 2)-1) / size(temp_st_image, 2) * 4.5;
        y = (0:size(temp_st_image, 1)-1) * 0.1 / interp_time_fold;
        image(y, x, (64-(temp_st_image-min(min(temp_st_image)))/(max(max(temp_st_image))-min(min(temp_st_image)))*64)')
        set(gca,'YDir','normal')
        colormap jet(64)
        colorbar('Ticks', [1 12.8 25.6 38.4 51.2 64], 'TickLabels', {'-0.3','-0.2','-0.1','0','0.1','0.2'})
        set(gca, 'FontSize', 16, 'FontName', 'Times', 'FontWeight', 'bold', 'XTick', round(y(1)):2:y(end), 'YTick', round(x(1)):1:x(end))
        xlabel('Time (ms), T', 'FontSize', 20, 'FontName', 'Times', 'FontWeight', 'bold', 'fontName', 'Times')
        ylabel('Distance (mm), D', 'FontSize', 20, 'FontName', 'Times', 'FontWeight', 'bold', 'fontName', 'Times');
        
        [X,Map] = rgb2ind(frame2im(getframe(gcf)), 256);
        savePath = [dirPath '\spatiotemporal\' fileName '_' num2str(dp) '.jpg'];
        if exist([dirPath '\spatiotemporal\'], 'dir')==0
            mkdir([dirPath '\spatiotemporal\']);
        end
        imwrite(X, Map, savePath, 'JPEG');
        
%         % -找出st image中的峰值追蹤該條shear wave的位移
        max_point_num = 3;
        max_point = zeros(3, window_width_pts * max_point_num);
        for i = 1:window_width_pts
            [value, index] = maxk(-temp_st_image(:, i), max_point_num);
            max_point(:, (i-1)*max_point_num+1:(i-1)*max_point_num+max_point_num) = [ones(1, max_point_num)*i;index';value'];
        end

        hold on
        scatter((max_point(2, :)-1) * 0.1 / interp_time_fold, (max_point(1, :)-1) / interp_dist_pts * 4.5, 'k')

        cluster_num = 1;
        while (var(max_point(2, :)) / mean(max_point(2, :)) > rsd_threshold)
            cluster_num = cluster_num + 1;
            [temp_idx, temp_C] = kmeans(max_point(2:3, :)', cluster_num);
            [~, temp_min_cluster_id] = min(temp_C(:, 1));
            max_point = max_point(:, (temp_idx==temp_min_cluster_id));
        end

        hold on
        scatter((max_point(2, :)-1) * 0.1 / interp_time_fold, (max_point(1, :)-1) / interp_dist_pts * 4.5, 'r')
        
        max_point_avg = zeros(3, interp_dist_pts);
        for i=1:interp_dist_pts
            if(sum(max_point(1, :)==i) > 0)
            % -這邊是要確定對應的測量點有峰值沒有被刪光，才進行取平均
                max_point_avg(1, i) = i;
                max_point_avg(2, i) = mean(max_point(2, (max_point(1, :)==i)));
                max_point_avg(3, i) = mean(max_point(3, (max_point(1, :)==i)));
            end
        end

%         % -對dist&amplitude與time做fit
        valid_dist = round(window_width / 4.5 * interp_dist_pts);
        fit_x = max_point_avg(1, 1:valid_dist)' * 4.5 / interp_dist_pts;
        fit_y1 = max_point_avg(2, 1:valid_dist)' * 0.1 / interp_time_fold;
        fit_y2 = max_point_avg(3, 1:valid_dist)';
        fit_y1(fit_x == 0) = [];
        fit_y2(fit_x == 0) = [];
        fit_y2 = log(fit_y2/fit_y2(1));
        fit_x(fit_x == 0) = [];
        slope = [polyfit(fit_x, fit_y1, 1); polyfit(fit_x, fit_y2, 1)];
        velocity(dp) = 1/slope(1, 1); %m/s
        attenuation(dp) = -slope(2, 1);
        close all
    end
    result(:, f) = [mean(velocity(analysis_region)); mean(attenuation(analysis_region))];
    C = ['depth', num2cell(round(((((analysis_region)-1) * analyzeInterval / interp_depth_fold) + depth(1)...
        + params.Delay - 1) / 2 / params.SamplingRate / 1e6 * 1000 * 1540, 2))];
    C = [C; 'velocity', num2cell(velocity(analysis_region))];
    C = [C; 'attenuation', num2cell(attenuation(analysis_region))];
    writecell(C, [dirPath '\vel_and_atten_' fileName '.xlsx']);
    clear C
end
% delete(gcp('nocreate'));