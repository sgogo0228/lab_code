function [params, idx] = shear_wave_NCC_20230703(filename, depth, unitAline, push_num, push_noise, window_width, interp_time_fold)
    % nfft = 4096;
    % 讀取檔案
    params = headFile_NI(filename);
    data = params.rf;

    % tic

    is_push_noise = [];
    for i = 0 : push_num - 1
        is_push_noise = [is_push_noise, (1:push_noise) + i * unitAline];
    end
    
    [paramB, paramA] = butter(5, [25e6 35e6]/(params.SamplingRate*1e6/2));    %第一個參數須視情況調整
    data = filtfilt(paramB, paramA, data);

    idx = zeros(params.Aline, depth(2) - depth(1) + 1);
    quantify_range = depth;
    if quantify_range(1) < window_width+1
        quantify_range(1) = window_width+1;
    end
    if quantify_range(2) > size(data, 1)-window_width
        quantify_range(2) = size(data, 1)-window_width;
    end
    depth_len = quantify_range(2) - quantify_range(1) + 1;
    idx(is_push_noise, :) = [];
    data(:, is_push_noise) = [];
    lower_bias = depth(1)-window_width-1;
    upper_bias = depth(1)+window_width-1;
    depth_start = quantify_range(1) - depth(1) + 1;
    depth_end = quantify_range(2) - depth(1) + 1;
    parfor i=1:size(data, 2)
        for j=depth_start:depth_end
            [~, idx(i, j)] = max(xcorr(data(j+lower_bias:j+upper_bias, 1), data(j+lower_bias:j+upper_bias, i)));
        end
    end

    % toc
end
