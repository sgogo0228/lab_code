% file_path = 'D:\daming\skinflap\skinflap in wang lab\rat#1\day3\30MHz_9kto16k_0to5mm_C1.dat';
% target_size = [256 256];
% is_save = 0   是否要儲存
% target_dir = 'D:\daming\skinflap\skinflap in wang lab\rat#1\day3\resize_image_256';
% 'crop_region' = [700 2500];
% 'is_filter'= 1    如果想要輸出濾波&resize影像並且下面三個參數都要設定
% 'filter_range'=[25 35]    濾波範圍
% 'sampling_rate'=1000
% 'dynamic_range'=45        濾完波後輸出影像

function [output_image] = image_resize(file_path, target_size, varargin)
    
    param = parse_fun(file_path, target_size, varargin{:});
    if param.is_filter && (~isnumeric(param.filter_range) || ~isnumeric(param.sampling_rate))
        disp('Filter parameters are lost.')
        return;
    end
    [us_image, us_rf] = get_us_data(param.file_path, 'is_save', 0, 'dynamic_range', 45);
    
    if param.is_filter
        [paramB, paramA] = butter(3, param.filter_range/(param.sampling_rate/2));    %第一個參數須視情況調整
        temp = filtfilt(paramB, paramA, us_rf);
        temp = abs(hilbert(temp));
        temp = 20*log10(temp/max(temp, [], 'all'));
        us_image = (temp+param.dynamic_range)/param.dynamic_range*255;
    end
    
    us_image = uint8(us_image);
    file_num = size(us_image, 3);
    
    if ~param.is_save
        if isnan(param.target_dir)
            param.target_dir = fileparts(param.file_path);
        end

        output_image_index = 1;
        if(~exist(param.target_dir))
            mkdir(param.target_dir);
        else
            output_image_index = size(dir(param.target_dir), 1)-1;
        end

        for i=1:file_num
            I = us_image(:, :, i);
            if ~isnan(param.crop_region)
                I = I(param.crop_region(1):param.crop_region(2), :);
            end
            output_image(:, :, i) = imresize(I, param.target_size, 'method', 'bilinear');
            imwrite(output_image(:, :, i), [param.target_dir '\' num2str(output_image_index) '.png'], 'PNG');
            output_image_index = output_image_index+1;
        end
    end
end

function [param_out] = parse_fun(file_path, target_size, varargin)
    p = inputParser;
    
    addRequired(p, 'file_path', @ischar);
    addRequired(p, 'target_size', @isnumeric);
    addOptional(p, 'is_save', 0, @isnumeric);
    addOptional(p, 'target_dir', nan, @ischar);
    addParameter(p, 'crop_region', nan);
    addParameter(p, 'is_filter', 'test', @isnumeric);
    addParameter(p, 'filter_range', nan, @isnumeric);
    addParameter(p, 'sampling_rate', nan, @isnumeric);
    addParameter(p, 'dynamic_range', 42, @isnumeric);
    
    parse(p, file_path, target_size, varargin{:});
    param_out = p.Results;
end