close all
pd = fitdist(env_roi,'Nakagami');
x_axis = 0:255;
y_axis = pdf(pd, x_axis);
figure('Color','white')
hold on
histogram(env_roi/255, 'Normalization','probability')
plot(linspace(0, 1, 256), y_axis, 'r', 'linewidth', 2)
set(gca, 'FontSize', 20, 'FontName', 'times', 'xtick', [0, 0.2, 0.4, 0.6, 0.8, 1], 'LineWidth', 1.5)
xlabel('Normalized envelope', 'FontWeight','bold')
ylabel('Probability (%)', 'FontWeight','bold')
box on