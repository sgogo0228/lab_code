% Cross-section of sound field
clear
clc
% close all

Path='D:\school\shearWaveData\20180114_broadsoundTranX_2';
n = char('2D_dist30mm',...
        '2D_dist30_5mm',...
        '2D_dist31mm',...
        '2D_dist31_5mm',...
        '2D_dist32mm',...
        '2D_dist32_5mm',...
        '2D_dist33mm',...
        '2D_dist33_5mm');
Image = zeros(size(n, 1), 21, 21);
for i=1:size(n, 1)
    Name=[strtrim(n(i, :)) '.dat'];
    fid=fopen([Path '\' Name]);
    Data=fread(fid,'double');
    fclose(fid);
    ScanPlane=Data(1);% 0=XZ, 1=XY
    XMoveDistance=Data(2);% mm
    YZMoveDistance=Data(3);% mm
    XResolution=Data(5);% mm
    YZResolution=Data(6);% mm

    Xnum = XMoveDistance/XResolution+1;
    YZnum = YZMoveDistance/YZResolution+1;
    Image(i, :, :)=reshape(Data(8:end),Xnum, YZnum);
end
globalMax = max(max(max(Image)));
globalMin = min(min(min(Image)));
Image = (Image-globalMin)/(globalMax-globalMin)*255;

DistanceX= (0:Xnum)*XResolution;
DistanceYZ=(0:YZnum)*YZResolution;

% DistanceX=(-XMoveDistance/2:XMoveDistance/2)*XResolution;
% DistanceYZ=(-YZMoveDistance/2:YZMoveDistance/2)*YZResolution;

figure;
for i=1:size(n, 1)
   
    subplot(2, 4, i);
    image(DistanceX,DistanceYZ,squeeze(Image(i, :, :)))
    colormap(jet(255))
    axis image
    title(['Distance to transducer:' num2str(n(i)/10) '(mm)'], 'FontSize', 13)
    xlabel('Distance (mm)', 'FontSize', 14)
    ylabel('Distance (mm)', 'FontSize', 14)
end

