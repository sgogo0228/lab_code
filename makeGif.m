prefix = 'Bmode_avtech';    %每個圖片檔名中一定會有的字串
fileDir = 'D:\daming\DrTang\20191209\20191209_zebrafish\fish#2';   %圖片檔的資料夾
fileMatched = findMatchedFile(fileDir, prefix);
sortCell;
img = imread([fileDir '\' fileMatched(1).name]);
imwrite(double(img(:, :, 1)), [fileDir '\' prefix '.gif'], 'GIF', 'WriteMode', 'overwrite', 'DelayTime', 0.33);
for i=2:length(fileMatched)
    img = imread([fileDir '\' fileMatched(i).name]);
    imwrite(double(img(:, :, 1)), [fileDir '\' prefix '.gif'], 'GIF', 'WriteMode', 'append', 'DelayTime', 0.33);
end