% % 此code能輸出IB, 1-4階momentum, snr, nakagami, wmc_nakagami
% % figure
% % close all
% % clear;
% % clc

%% 單跑
clear QUS
% % 只需要輸入第一筆(_C1.dat或_B0.dat)的檔名就好
% % 如果檔名顯示有衰減過，需要在讀檔時補償回去
% noiseFile = 'I:\黃大銘實驗資料\skinflap\20200615_transducer_char\waternoise\30MHz_eng1_dmp1_newwire_B1.dat';
% file = char('K:\黃大銘實驗資料\skinflap\G5(female rats)\20190624_skinflap_post\no1\30MHz#2_0to6mm_C1.dat',...
%     'K:\黃大銘實驗資料\skinflap\G5(female rats)\20190624_skinflap_post\no1\30MHz#2_6to12mm_C1.dat',...
%     'K:\黃大銘實驗資料\skinflap\G5(female rats)\20190624_skinflap_post\no1\30MHz#2_12to18mm_C1.dat',...
%     'K:\黃大銘實驗資料\skinflap\G5(female rats)\20190624_skinflap_post\no1\30MHz#2_18to24mm_C1.dat',...
%     'K:\黃大銘實驗資料\skinflap\G5(female rats)\20190624_skinflap_post\no1\30MHz#2_24to30mm_C1.dat');
% r_iter = 1;
% d_iter = 1;

% % % 計算雜訊
% estimateNoiseLevel;
% noise4IB = noiseSquareSum(1);

% dynamicRange = 42;
% windowFold=[2;3;4;5]*3; %windowFold=[6; 9; 12; 15];
% AxialROIFold = windowFold;
% LateralROIFold = windowFold;
% resolutionAxial = 0.0591; %mm resolutionAxial = 0.0459; %mm
% resolutionLateral = 0.0591;% resolutionLateral = 0.0743;
% % estimateNoiseLevel;
% nfft = 4096;
% filter_range = [25 38.5];


for f = 1:size(file)
    if ~exist(file(f, :), 'file')
        f
        disp(file(f, :));
        return;
    end
end

% % 計算鋼塊頻譜
param = headFile_NI(stainlessBlockData);
% % 根據是否有加上衰減器對rf做補償
attenuator = 10;  %衰減器6 db
stainlessBlockFFT = mean(abs(fft(param.rf*10^(attenuator/20), nfft)), 2);
figure;
us_rf = [];
log_rf = [];

for f = 1:size(file, 1)
    full_file_name = strtrim(file(f, :));
    batch_size = length(dir([full_file_name(1:end-5) '*dat']));
    for b = 1:batch_size
        param = headFile_NI([full_file_name(1:end-5) num2str(b) '.dat']);
        us_rf = cat(3, us_rf, param.rf);
%         temp = abs(hilbert(param.rf));
%         log_rf = cat(3, log_rf, 20*log10(temp/max(temp, [], 'all')));
    end
end
clear temp

[paramB, paramA] = butter(3, filter_range*1e6/(param.SamplingRate*1e6/2));    %第一個參數須視情況調整(-10db)
us_rf = filtfilt(paramB, paramA, us_rf);
log_rf = abs(hilbert(us_rf));
log_rf = 20*log10(log_rf./max(log_rf, [], [1 2]));

QUS(size(us_rf, 3)) = struct();

tic
% parfor f=1:size(us_rf, 3)
for f=1:size(us_rf, 3)
    AxialROIFold = 10; %AxialROIFold = 30;
    LateralROIFold = 40; %LateralROIFold = 60;
    
    if ~mod(f, 2)
        temp_rf = fliplr(us_rf(:, :, f));
        temp_log = fliplr(log_rf(:, :, f));
    else
        temp_rf = us_rf(:, :, f);
        temp_log = log_rf(:, :, f);
    end
    temp_img=255*(temp_log+dynamicRange)/dynamicRange;
    x_axis = (1:param.Aline)*param.XInterval/1000;
    y_axis = ((1:param.DataLength)+param.Delay)/2/(param.SamplingRate*1e6)*1540*1000;
    image(x_axis, y_axis, temp_img)
    
    temp = drawrectangle('Position', roi_position(f, d_iter, r_iter).roi_pos, 'RotationAngle', roi_position(f, d_iter, r_iter).roi_degree, 'Color', 'y', 'InteractionsAllowed', 'translate', 'Rotatable', true);
    temp_BW = createMask(temp);
    temp_BW_index = find(temp_BW);
    
    % %因為ROI有可能轉移個角度，某些aline只有很少數的點在ROI內，在統計上可能會有問題，所以要找出那些aline
    BW_nonzero_num = sum(temp_BW, 1);
    temp_fft_rf = [];
    for i=1:length(BW_nonzero_num)
        % %如果aline上被判定為ROI的點多於0.95*ROI高度(ROI縱向的點數)，才做FFT
        if BW_nonzero_num(i) > resolutionAxial*AxialROIFold/1000/1540*param.SamplingRate*1e6*2*0.95
            temp_fft_rf = cat(2, temp_fft_rf, abs(fft(temp_rf(temp_BW(:, i), i), nfft)));
        end
    end
    temp_fft_rf = mean(temp_fft_rf, 2);
    temp_log_fft = 20*log10(temp_fft_rf/max(temp_fft_rf(20:end-20)));
    [~, index] = max(temp_log_fft(20:end/2)); % Find center freq.
    index = index+19;
    x1 = index; x2 = index;
    while temp_log_fft(x1) > -6 && x1>1
       x1 = x1-1;
    end
    while temp_log_fft(x2) > -6 && x2<length(temp_log_fft)
       x2 = x2+1;
    end
    s1 = sum(temp_fft_rf(x1+1:x2));
    s2 = sum(stainlessBlockFFT(x1+1:x2));
    QUS(f).IB = 20*log10((s1/s2)/(x2-x1));  %鋼塊 bandwidth：12  取得點數要相同

    temp = temp_img.*temp_BW;
    %這兩行是要將非ROI區域的部分剪掉
    roi_img =  temp(any(temp,2),:);
    roi_img = roi_img(:,any(temp,1));
    
    roi_img = uint8(roi_img);
%     roi_img = fliplr(roi_img);
%     roi_img = flipud(roi_img);
    
%     roi_img = histeq(uint8(roi_img));
% %     計算glcm
    glcm_dist = [1 1; 2 2; 3 3; 4 4; 5 5; 7 7; 10 10; 15 15; 20 20; 25 25; 30 30; 40 40; 50 50; 60 60; 2 13; 4 26; 6 39; 8 52; 10 65];
    for n=1:19
        temp = get_glcm(roi_img, glcm_dist(n, 1), glcm_dist(n, 2));
%         QUS(f).glcm(n).array = temp;
        QUS(f).glcm_feature(n) = GLCM_Features(temp, 0);
    end
    
%     roi_img = reshape(roi_img, [], 1);
    temp = mean(roi_img, 'all');
    temp2 = var(double(roi_img), 0, 'all');
    QUS(f).hist_mean = mean(roi_img, 'all');
    QUS(f).hist_var = var(double(roi_img), 0, 'all');
    QUS(f).hist_skew = (sum((roi_img-temp).^3, 'all')./numel(roi_img)) ./ (temp2.^1.5);
    QUS(f).hist_kurt = (sum((roi_img-temp).^4, 'all')./numel(roi_img)) ./ (temp2.^2);
    
    roi_rf = temp_rf(temp_BW_index);
    [roi_index_row, roi_index_column] = ind2sub(size(temp_rf), temp_BW_index);
    % %這一段是為取與ROI同樣大小和角度的雜訊，直接將ROI的區域平移到影像的邊緣(此段是移到右上角)
    noise_region_shift = size(temp_rf, 1)*(size(temp_rf, 2)-max(roi_index_column))-(min(roi_index_row)-1);
%     % %這一段是為取與ROI同樣大小和角度的雜訊，直接將ROI的區域平移到影像的邊緣(此段是移到左上角)
%     noise_region_shift = -(size(temp_rf, 1)*(min(roi_index_column)-1)+(min(roi_index_row)-1));
    noise_index = temp_BW_index+noise_region_shift;
    roi_noise = temp_rf(noise_index);
    QUS(f).SNR = 10*log10(sum(roi_rf.^2)/sum(roi_noise.^2));
    env_roi = abs(hilbert(roi_rf));
    env_roi = env_roi/max(env_roi, [], 'all')*255;
    nakagami_param = mle(env_roi, 'distribution', 'nakagami');
    QUS(f).nakagami = nakagami_param(1);
%     figure; hist = histogram(env_roi,'BinWidth' , 9.9);

%     AxialROIFold = windowFold;
%     LateralROIFold = windowFold;
%     nakagami_img = zeros(length(temp_BW_index), length(windowFold));
%     parfor wf = 1:length(windowFold)
%         windowZ = round(2*resolutionAxial/1000/1540*param.SamplingRate*1e6*AxialROIFold(wf));
%         windowX =  round(resolutionLateral/(param.XInterval*1e-3)*LateralROIFold(wf));
%         off_set_z = round(windowZ/2);
%         off_set_x = round(windowX/2);
%         temp_nakagami = zeros(length(temp_BW_index), 1);
% % % computations take place here
%         for idx = 1:length(temp_BW_index)
%             [jj, ii] = ind2sub(size(temp_rf), temp_BW_index(idx));
%             if jj-off_set_z<1 || jj+off_set_z>size(temp_rf, 1) || ii-off_set_x<1 || ii+off_set_x-1>size(temp_rf, 2)
%                 roi = [];
%                 roi_SNR = inf;
%             else
%                 roi = temp_rf((jj-off_set_z):(jj+off_set_z)-1,(ii-off_set_x):(ii+off_set_x)-1);
%                 roi_SNR = 10*log10(sum(sum(roi.^2))/noise4WMC(wf));
%             end
% %             noiseNum = length(find(log_rf((j-off_set_z):(j+off_set_z),(i-off_set_x):(i+off_set_x)) < 0.8*noiseLevel));
%             if roi_SNR < 15 || roi_SNR==inf
% %                     if(noiseNum > 0.2*numel(roi))
%                 mu = 0;
%             else
%                 roi_reshape=reshape(roi, [], 1);
%                 env_roi = abs(hilbert(roi_reshape));
%                 env_roi = env_roi/max(max(env_roi))*255;
%                 mu = (mean((env_roi).^2).^2)/mean((env_roi.^2-mean(env_roi.^2)).^2);
%             end
%             temp_nakagami(idx) = mu;
%         end
%         nakagami_img(:, wf) = temp_nakagami;
%     end
%     QUS(f).WMCNakagami = mean(nakagami_img(nakagami_img>0), 'all');
end
    toc
clear temp_rf temp_log temp_nakagami nakagamiImg temp_BW temp_fft_rf temp_log_fft
close all
% a=c

