% clear
clc
% close all
%%
% 能夠一次分析好幾組shear wave
fileSet = char('L:\黃大銘實驗資料\skin\G7\20190607_ratskin_d0\no1\shearwave\avtech_30MHz_dist0_depth9to17k_amp600_cycle5100_1_SW0',...
    'L:\黃大銘實驗資料\skin\G7\20190614_ratskin_d7\no1\shearwave\avtech_30MHz_dist0_depth9to17k_amp600_cycle5100_1_SW0',...
    'L:\黃大銘實驗資料\skin\G7\20190614_ratskin_d7\no1\shearwave\avtech_30MHz_dist0_depth9to17k_amp600_cycle5100_2_SW0',...
    'L:\黃大銘實驗資料\skin\G7\20190614_ratskin_d7\no1\shearwave\avtech_30MHz_dist0_depth9to17k_amp600_cycle5100_3_SW0',...
    'L:\黃大銘實驗資料\skin\G7\20190618_ratskin_d10\no1\shearwave\avtech_30MHz_dist0_depth9to17k_amp600_cycle5100_1_SW0',...
    'L:\黃大銘實驗資料\skin\G7\20190618_ratskin_d10\no1\shearwave\avtech_30MHz_dist0_depth9to17k_amp600_cycle5100_2_SW0',...
    'L:\黃大銘實驗資料\skin\G7\20190618_ratskin_d10\no1\shearwave\avtech_30MHz_dist0_depth9to17k_amp600_cycle5100_3_SW0',...
    'L:\黃大銘實驗資料\skin\G7\20190621_ratskin_d14\no1\shearwave\avtech_30MHz_dist0_depth9to17k_amp600_cycle5100_1_SW0',...
    'L:\黃大銘實驗資料\skin\G7\20190621_ratskin_d14\no1\shearwave\avtech_30MHz_dist0_depth9to17k_amp600_cycle5100_2_SW0',...
    'L:\黃大銘實驗資料\skin\G7\20190621_ratskin_d14\no1\shearwave\avtech_30MHz_dist0_depth9to17k_amp600_cycle5100_4_SW0',...
    'L:\黃大銘實驗資料\skin\G7\20190628_ratskin_d21\no1_butt\shearwave\avtech_30MHz_dist0_depth9to17k_amp600_cycle5100_1_SW0',...
    'L:\黃大銘實驗資料\skin\G7\20190628_ratskin_d21\no1_butt\shearwave\avtech_30MHz_dist0_depth9to17k_amp600_cycle5100_2_SW0',...
    'L:\黃大銘實驗資料\skin\G7\20190628_ratskin_d21\no1_butt\shearwave\avtech_30MHz_dist0_depth9to17k_amp600_cycle5100_3_SW0',...
    'L:\黃大銘實驗資料\skin\G7\20190706_ratskin_d28\no1_butt\shearwave\avtech_30MHz_dist0_depth9to17k_amp600_cycle5100_1_SW0',...
    'L:\黃大銘實驗資料\skin\G7\20190706_ratskin_d28\no1_butt\shearwave\avtech_30MHz_dist0_depth9to17k_amp600_cycle5100_2_SW0',...
    'L:\黃大銘實驗資料\skin\G7\20190706_ratskin_d28\no1_butt\shearwave\avtech_30MHz_dist0_depth9to17k_amp600_cycle5100_3_SW0');

params = headFile_NI([strtrim(fileSet(1, :)) '.dat']);
push_num = 5;                           %在固定的蒐集時間內(params.aline)，push打了幾次，跟Push的PRF有關
position_num = 10;                      %蒐集多少個position的signal
interp_dist_pts = 100;                        %內差的倍數
interp_depth_fold = 3;
push_noise=15;                          %push干擾detect的點數
baseFrequency=50;
hormonicNum = 5;                      %kalman filter故估計頻率的上限(如果push的PRF是50Hz，那就會最高抓到50*此變數)
% slopeFitDotNum = [6 8];               %最後計算斜率時參考的點數
unitAline = params.Aline/push_num;      %push打一次所需要的時間
depth = [3501 5000];
detect_sampling_rate=10000;   %千萬不要打錯!! 是指detect探頭的PRF
modelParams = struct('unitAline', unitAline, 'push_num', push_num, 'usedCycle', 3, 'front_noise', push_noise-3, 'backNoise', 3, 'damping', 0);
analyzeInterval = 100;                  % 經驗上分析肌肉的SW 100或200(以上也可以500)都可以，太少變異大

overall_depth = depth(2)-depth(1)+1;
analysis_region = [1 overall_depth/analyzeInterval];   %可能不會想整個深度都分析，可自設區間
depth_num = analysis_region(2)-analysis_region(1)+1;
fnum = size(fileSet, 1);
bandFilter = [baseFrequency*0.7 baseFrequency*6];
modifiedUnitAline = (modelParams.unitAline-modelParams.front_noise-modelParams.backNoise-modelParams.damping);
unusedCycle=modelParams.push_num-modelParams.usedCycle;

distPostfix = {'0', '0p5', '1', '1p5', '2', '2p5', '3', '3p5', '4', '4p5'};

for f = 1:fnum
    filename = [strtrim(fileSet(f, :)) '.dat'];
    if ~exist(filename, 'file')
        f
        disp(filename);
        return;
    end
end


% gcp
sgn = zeros(modifiedUnitAline, overall_depth/analyzeInterval*interp_depth_fold, interp_dist_pts, fnum);
h = waitbar(1/position_num, ['f = 1 (1/' num2str(position_num) ')']);
for f=1:fnum
    temp.sgn = zeros(modifiedUnitAline*push_num, overall_depth, position_num);
    waitbar(1/position_num, h, ['f = ' num2str(f) '(1/' num2str(position_num) ')']);
    for j=1:position_num
        waitbar(j/position_num, h, ['f = ' num2str(f) '(' num2str(j) '/' num2str(position_num) ')']);
        filename = [regexprep(strtrim(fileSet(f, :)), 'dist0', ['dist' num2str(distPostfix{j})]) '.dat'];
        [sgnParam, temp.sgn(:, :, j)]= shearWaveByDemod20220211(filename, depth, unitAline, push_num, push_noise);
    end
    [paramB, paramA] = butter(5, bandFilter/(detect_sampling_rate/2));    %第一個參數須視情況調整
    temp.sgn = filtfilt(paramB, paramA, temp.sgn);

    temp.sgn = reshape(temp.sgn, modifiedUnitAline, push_num, analyzeInterval, overall_depth/analyzeInterval, position_num);
    temp.sgn = squeeze(mean(temp.sgn(:, 2:4, :, :, :), [2, 3]));

    [temp.X,temp.Y,temp.Z] = meshgrid(1:size(temp.sgn, 2), 1:size(temp.sgn, 1), 1:size(temp.sgn, 3));
    [temp.Xq,temp.Yq,temp.Zq] = meshgrid(linspace(1, size(temp.sgn, 2), size(temp.sgn, 2)*interp_depth_fold), 1:size(temp.sgn, 1), linspace(1, size(temp.sgn, 3), interp_dist_pts));
    sgn(:, :, :, f) = interp3(temp.X, temp.Y, temp.Z, temp.sgn, temp.Xq, temp.Yq, temp.Zq, "cubic");
end
close(h);
delete(gcp('nocreate'));
clear temp
backup = sgn;
sgn = backup;

%% 輸出SW image
% sgn(時間, 深度, detect距離, 檔案) 
clear imgSgn
interp_time_fold = 1;
figure('color', 'white')
graphInterval = 3;
colorScale = [0 1];
y_lim = 14.5;


% 輸出2D的SW圖
for f=1:1
    i=1;
    imgSgn(:, :, :, f) = 5*(sgn{f}-min(sgn{f}, [], [1]));
    [filepath, filename, ~] = fileparts(fileSet{f});
    savePath = [filepath '\gif_' strtrim(filename) '.gif'];
    param = headFile_NI([strtrim(fileSet{f}) '.dat']);
    xAxis = linspace(0, y_lim, size(sgn{f}, 1)*interp_time_fold);
    yAxis = (((depth(1):depth(2))+param.Delay)/(param.SamplingRate*1e6)/2*1540*1000);
    if exist(savePath, 'file')==2
        delete(savePath);
    end

    videoPath = [filepath '\video_' strtrim(filename) '.avi'];
    v = VideoWriter(videoPath);
    v.FrameRate = 10;
    open(v);

    while i<=modifiedUnitAline                % 任何一個 loop 
        imagesc(xAxis, yAxis, squeeze((imgSgn(i, :, :, f))));
%         axis
        colormap jet(256)
        caxis(colorScale)
        title([num2str(i*0.1) 'ms'])
        set(gca, 'FontSize', 24, 'FontName', 'Times', 'FontWeight', 'bold')
        xlabel('Distance (mm)', 'FontSize', 28, 'FontWeight', 'bold')
        ylabel('Depth (mm)', 'FontSize', 28, 'FontWeight', 'bold')
        axis image
        [X,Map] = rgb2ind(frame2im(getframe(gcf)), 256);
        %在 loop內 的writemode 須改為 append ,這時候frame才會不斷的增加進去
        if i==1
            imwrite(X, Map, savePath, 'GIF', 'WriteMode', 'overwrite', 'DelayTime', 0, 'LoopCount', Inf);
        else
            imwrite(X, Map, savePath,'GIF','WriteMode','append','DelayTime', 0);
        end
        frame = getframe(gcf);
        writeVideo(v, frame);
        i = i+graphInterval;
    end
    close(v)
end

%% 輸出3D的SW圖
colorScale = [0 0.3];
temp.b = permute(movmedian(permute(imgSgn, [2 1 3 4]), 11), [2 1 3 4]);

figure('position', [900 200 670 500], 'color', 'white')

for f=1:fnum
    i=1;
    [filepath, filename, ~] = fileparts(fileSet(f, :));
    savePath = [filepath '\3Dgif_' strtrim(filename) '.gif'];
    if exist(savePath, 'file')==2
        delete(savePath);
    end
    while i<=modifiedUnitAline                % 任何一個 loop
        surf(squeeze((temp.b(i, :, :, f)))', 'EdgeColor', 'black', 'LineStyle', ':', 'FaceColor', 'interp');
        zlim(colorScale)
        caxis(colorScale)
        view(-45, -60);
        title(num2str(i))
        xlabel('Distance (mm)', 'FontSize', 14)
        ylabel('Depth (mm)', 'FontSize', 14)
        pause(0.5)

        [X,Map] = rgb2ind(frame2im(getframe(gcf)), 256);
        if i==1
            imwrite(X, Map, savePath, 'GIF', 'WriteMode', 'overwrite', 'DelayTime', 0, 'LoopCount', Inf);
        else
            imwrite(X, Map, savePath, 'GIF', 'WriteMode', 'append','DelayTime', 0);
        end
        i = i+graphInterval;
    end
end