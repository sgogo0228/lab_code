clear
clc
% close all
%%
% 能夠一次分析好幾組shear wave
unitAline = 199;
pushNum = 5;
distNum = 10;
depth = [1 8000];
% CImage = '30MHz_dist0to5_depth5to14';
analyzeFilePath = ['L:\黃大銘實驗資料\skin\G6\'];
fileSet = char('K:\黃大銘資料夾20180420前\school\shearWaveData\20151119_shearWave\gelatin3_dist');
pushNoise=50;
detectSamplingRate=10000;
hormonicNum = 5;
baseFrequency=50;
analyzeInterval = 400;
modelParams = struct('unitAline', unitAline, 'pushNum', pushNum, 'usedCycle', 3, 'frontNoise', pushNoise-3, 'backNoise', 3, 'damping', 49);
slopeFitDotNum = [6 8];
analysisDistRange = [6 12];

bandFilter = [baseFrequency*0.5 baseFrequency*6];
fnum = size(fileSet, 1);
overallDepth = depth(2)-depth(1)+1;
modifiedUnitAline = (modelParams.unitAline-modelParams.frontNoise-modelParams.backNoise-modelParams.damping);
unusedCycle=modelParams.pushNum-modelParams.usedCycle;
distPostfix = {'0', '1', '2', '3', '4'};

for i=analysisDistRange(1):analysisDistRange(2)
    range(i-analysisDistRange(1)+1, :) = [(i-1)*analyzeInterval+1, i*analyzeInterval];
end
sgn = zeros(fnum, distNum, analysisDistRange(2)-analysisDistRange(1)+1, unitAline*pushNum);

for f = 1:fnum
    filename = [strtrim(fileSet(f, :)) '_SW0.dat'];
    if ~exist(filename, 'file')
        f
        disp(filename);
        return;
    end
end

pool = parpool(6);
try
    for f=1:fnum
%         waitbar(f/fnum)
        demodSignal = zeros(distNum, overallDepth, unitAline*pushNum);
        tempidx = regexp(fileSet(f, :), 'dist0');
%         for j=1:distNum
        parfor j=1:distNum
            filename = [strtrim(fileSet(f, 1:tempidx+3)) distPostfix{j} strtrim(fileSet(f, tempidx+5:end)) '_SW0.dat'];
            demodSignal(j, :, :)=shearWaveByDemod20170327(filename, depth, unitAline, pushNum, pushNoise);
        end
        for r = 1:analysisDistRange(2)-analysisDistRange(1)+1
            sgn(f, :, r, :) = squeeze(mean(demodSignal(:, range(r, 1):range(r, 2), :), 2));
        end
    end
catch
    'Some errors happened'
    delete(pool);
end
delete(pool);
temp = sgn;

Macro_SWanalysis20181210;
% showCscanImage(path, CImage, 42);
a = b;