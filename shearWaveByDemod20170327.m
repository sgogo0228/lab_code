function [params, demodSignal] = shearWaveByDemod20170327(filename, depth, unitAline, pushNum, pushNoise)
    aline = unitAline*pushNum;
    demodSignal = zeros(depth(2)-depth(1)+1, aline);
%     nfft = 4096;
    % 讀取檔案
    params = headFile_NI(filename);
    data = params.rf;
    data = data*params.Vpp/255;

    % demodulation
    [I, Q] = demod(data,30*1e6,1e9,'qam');
    
    for d=depth(1):depth(2)
%         d=5000;
        % 取IQ的相位角可得到shear wave
%         Q(d, :) = datasmooth(Q(d,:), 5);
%         I(d, :) = datasmooth(I(d,:), 5);
        s=atan(Q(d, :)./I(d, :));
        
        if(size(find(abs(s)>1.5), 2)>20)
            for i=1:size(s, 2)
                if(Q(d, i)>0&&I(d, i)<0)
                    s(i) = s(i)+pi;
                end
                if(Q(d, i)<0&&I(d, i)<0)
                    s(i) = s(i)-pi;
                end
            end
        end
        s=-s;
%         figure
        if(size(find(abs(s)>3), 2)>20)
            for j=pushNoise+2:aline
                if(mod(j, unitAline)==pushNoise+1)
                    if(abs(s(j-pushNoise-1)-s(j))>5)
                        if(s(j)<s(j-pushNoise-1))
                            s(j) = s(j)+2*pi;
                        else
                            s(j) = s(j)-2*pi;
                        end
                    end
                end
                if(mod(j, unitAline)>pushNoise+1)
                    if(abs(s(j-1)-s(j))>5)
                        if(s(j)<s(j-1))
                            s(j) = s(j)+2*pi;
                        else
                            s(j) = s(j)-2*pi;
                        end
                    end
                end
%                 hold off
%                 plot(s)
%                 hold on
%                 plot(j, s(j), 'ro')
%                 pause(0.01)
            end
        end
        
        % 求s平均並和s相減可消掉載波相位的影響
        meanValue = mean(s(pushNoise+1:end));
        s = s(1:aline);

        % 理論上y已和fast time(縱向深度)無關，因此取fast time之平均獲得待測物振動之相位角
        demodSignal(d-depth(1)+1, :) = s-meanValue;
%         [b, a] = butter(5, 30/(10000/2), 'high');
%         demodSignal(d-depth(1)+1, :) = filtfilt(b, a, demodSignal(d-depth(1)+1, :));
    end
end

