clc
close all
param = headFile_NI(strtrim(fileset(1, :)));
x_interval = 0.05;
y_interval = 0.2;
z_interval = 1/250e6*1540/2*1000;

% x_interval = 0.05;
% y_interval = 0.1;
% z_interval = 1/1000e6*1540/2*1000;

delay = param.Delay;   %pts
img_start_z = 7;  %mm
img_end_z = 11;
resize_ratio = 0.5;
rf_for_thickness = [107 116];

img_start_pts = round(img_start_z/1000/1540*250e6*2)-delay;  %pts
img_end_pts = round(img_end_z/1000/1540*250e6*2)-delay;

%% 影像smoothing
H = fspecial('average',21);
smoothing_image = zeros(size(us_image));
for f=1:size(us_image, 1)
    smoothing_image(f, :, :) = imfilter(squeeze(us_image(f, :, :)), H, 'replicate');
end
clear H

%% 找出前景(ACM)
skin_bound = ones(2, rf_for_thickness(2)-rf_for_thickness(1)+1, size(smoothing_image, 1));
front_mask = false(size(smoothing_image));
tic
% gcp;
parfor f=1:size(smoothing_image, 1)
    temp = squeeze(smoothing_image(f, :, :));
%     figure('Position', [0 100 1500 500]); subplot(1, 3, 1); image(xAxis, yAxis, temp); axis image;
%     temp(temp>=20) = 256;
    temp(temp<20) = 0;
    
% % 膨脹與侵蝕(好像對3D建模沒什麼影響)
%     se = strel('rectangle', [5 5]);
%     temp = imerode(temp, se);
%     subplot(1, 3, 2); image(xAxis, yAxis, temp); axis image;
%     temp = imdilate(temp, se);
%     subplot(1, 3, 3); image(xAxis, yAxis, temp); axis image;
%     pause(0.5)
%     close all;

% % active contour model
    mask = zeros(size(temp));
    mask(img_start_pts:img_end_pts, :) = 1;
    BW = activecontour(temp,mask, 1000);
    front_mask(f, :, :) = BW;
end
% delete(gcp('nocreate'));
toc
clear temp mask BW mask;

%% 計算厚度
for f=1:size(front_mask, 1)
    signal = squeeze(front_mask(f, :, rf_for_thickness(1):rf_for_thickness(2)));
    for i=1:size(signal, 2)
%         high_amp_signal = signal(:, i)>5;
        is_find_up_bound = 0;
        for j=1:size(signal, 1)-49
            if sum(signal(j:j+49, i))>35 && is_find_up_bound == 0
                is_find_up_bound = 1;
                skin_bound(1, i, f) = j;
            end
            if sum(signal(j:j+49, i))<35 && is_find_up_bound == 1
                is_find_up_bound = 0;
                skin_bound(2, i, f) = j;
                break;
            end
        end
    end
end
avg_skin_height = round(mean(squeeze(skin_bound(1, :, :)), 1));
avg_skin_height = avg_skin_height-avg_skin_height(1);
avg_skin_thickness = z_interval*round(mean(squeeze(skin_bound(2, :, :)-skin_bound(1, :, :)), 1));

%% 根據表皮位置(avg_skin_thickness)校正影像
shift_image = zeros(size(us_image));
for f = 1:size(us_image, 1)
    if avg_skin_height(f) > 0
        shift_image(f, 1:end-avg_skin_height(f), :) = squeeze(smoothing_image(f, avg_skin_height(f)+1:end, :));
    else
        shift_image(f, abs(avg_skin_height(f))+1:end, :) = squeeze(smoothing_image(f, 1:end-abs(avg_skin_height(f)), :));
    end
end

i=1;
rsz_image = [];
% for f = 109:116
for f = 1:size(shift_image)
    rsz_image(i, :, :) = imresize(squeeze(shift_image(f, img_start_pts:img_end_pts, :)), resize_ratio);
    i = i+1;
end

rsz_image = permute(rsz_image, [1 3 2]);
rsz_image = squeeze(rsz_image);
rsz_image = padarray(rsz_image,[3 3 3],'both');
rsz_image = smooth3(rsz_image);   %因為呼吸會導致皮膚上下起伏，且X軸上每個點的起伏都不是固定值，有可能左邊多右邊少，導致在3D image中只有校正區是平整的，而需要再Y軸上做一次smoothing
%% Create an isosurface
surface = isosurface(rsz_image, 35);
surface.vertices(:, 1) = surface.vertices(:, 1)*x_interval/resize_ratio;
surface.vertices(:, 2) = surface.vertices(:, 2)*y_interval;
surface.vertices(:, 3) = (surface.vertices(:, 3)+(delay+img_start_pts)*resize_ratio)*z_interval/resize_ratio;
% Display the surface
figure('position', [100 100 900 350], 'color', 'white');
hiso = patch('Vertices',surface.vertices,...
            'Faces',surface.faces,...
            'FaceColor',[1,.75,.65],...
              'EdgeColor','none');
set(gca, 'ZDir', 'reverse')
xlabel('Distance (mm), X', 'Position', [-2.3 10 15], 'FontSize', 20, 'FontName', 'Times', 'FontWeight', 'bold')
ylabel('Distance (mm), Y', 'Position', [0 23 15], 'FontSize', 20, 'FontName', 'Times', 'FontWeight', 'bold')
zlabel('Depth (mm), Z', 'FontSize', 20, 'FontName', 'Times', 'FontWeight', 'bold')
view(45, 10)
axis image;
set(gca, 'XLim', [0 9], 'XTick', [2 4 6 8], 'YLim', [0 32], 'YTick', [0 5 10 15 20 25 30], 'YTickLabel', {'30','25','20','15','10','5','0'}, 'ZLim', [7.7 11.5], 'ZTick', [8 9 10 11], 'FontSize', 16, 'FontName', 'Times', 'FontWeight', 'bold')
hold on


lightangle(45,-45); 
set(gcf,'Renderer','zbuffer'); lighting phong
% isonormals(rsz_image, hWiso)
set(hiso, 'SpecularColorReflectance',0,'SpecularExponent',50)
set(gca,'Position', [0.08 0.13 0.85 0.85])

% % %用slice顯示3D image
% figure('color', 'white', 'Position', [500 200 600 100])
% xAxis = (1:size(rsz_image, 2))*x_interval/resize_ratio;
% yAxis = (1:size(rsz_image, 1))*y_interval;
% zAxis = ((1:size(rsz_image, 3))+(delay+img_start_pts)*resize_ratio)*z_interval/resize_ratio;
% temp = rsz_image;
% temp(temp<35) = nan;
% % h = slice(temp, [], 1:size(temp, 1), []);
% h = slice(xAxis, yAxis, zAxis, temp, xAxis, yAxis, []);
% set(h, 'EdgeColor','none', 'FaceColor','interp')
% alpha(.1)
% view(90, 0)
% axis image;
% set(gca, 'YLim', [0 32], 'YTick', [0 15 30], 'YTickLabel', {'Distal','Middle','Proximal'})
% set(gca, 'ZLim', [7.7 11.5], 'ZTick', [8 9 10 11], 'ZDir', 'reverse') 
% set(gca, 'FontName', 'Times', 'FontWeight', 'bold', 'CLim', [0 255])
% xlabel('Distance (mm), X')
% ylabel('Position, Y')
% zlabel('Depth (mm), Z')
% cb = colorbar;
% set(gca, 'Position', [0.07 0.2 0.8 0.8])
% set(cb, 'Position', [0.9 0.2 0.03 0.7], 'Ticks', [0 50 100 150 200 250], 'TickLabels', {'0', '50', '100', '150', '200', '250'})

clear shift_image
a=xyx;