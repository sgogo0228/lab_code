function CAMshow(im, CAM, weight, bias)
    imSize = size(im);
    CAM = imresize(CAM,imSize(1:2));
    for i=1:size(weight, 1)
        CAM(:, :, i) =  CAM(:, :, i)*weight(i);
    end
    CAM = normalizeImage(sum(CAM, 3)+bias);
    figure
    imshow(CAM)
    CAM(CAM<0.2) = 0;
    cmap = jet(255).*linspace(0,1,255)';
    CAM = ind2rgb(uint8(CAM*255),cmap)*255;
    figure('Color', 'white')
    imshow(im);
    hold on
    overlay_image = ishow(uint8(CAM));
    hold off
    alpha(overlay_image, 0.35)
    axis image
    set(gca, 'XTick', {}, 'YTick', {});
%   combinedImage = double(rgb2gray(im))/2 + CAM;
%    combinedImage = normalizeImage(combinedImage)*255;
%   imshow(uint8(combinedImage));
end
function N = normalizeImage(I)
minimum = min(I(:));
maximum = max(I(:));
N = (I-minimum)/(maximum-minimum);
end