clear all;
clc;

times = 0;
feature_con = zeros (1, 20);
feature_hom = zeros (1, 20);
feature_cor = zeros (1, 20);
feature_ene = zeros (1, 20);
feature_ent = zeros (1, 20);
feature_cshad = zeros (1, 20);
feature_cprom = zeros (1, 20);

posROI = zeros(2, 20);
IB = zeros(1, 20); 
rfSNR = zeros(1, 20);
nakagami_m = zeros(1, 20);
result = zeros(12, 20);
r_squared = zeros(1, 20);

for n = 8:8
    %% Read Data
    str_no = sprintf('%d', n);
    r = fopen(['C:\Users\Ula\Desktop\Analysis_result\RF signals\contusion08(G6_1_head)\20180611\right\right_day21_C' str_no '.dat'], 'rb'); %**************************************
    %r = fopen(['D:\LabExpData_zy\ZY_Data\20171102_water\30MHz_water_noise_B0.dat'], 'rb'); % noise:water訊號
    rf = fread(r, 'int8');
    fclose(r);
    
    if rf(3) == 0
        pulser = '5900PR';
    else
        pulser = 'AVB2-TB-C';
    end
    scan_speed = rf(4)*16129+rf(5)*127+rf(6); 
    aline = rf(7)*16129+rf(8)*127+rf(9); 
    datalength = rf(10)*16129+rf(11)*127+rf(12);
    sampling_rate = rf(13)*16129+rf(14)*127+rf(15); % MHz
    delay = rf(16)*16129+rf(17)*127+rf(18);
    vpp = rf(19)*16129+rf(20)*127+rf(21);   % mV
    x_interval = rf(22)*16129+rf(23)*127+rf(24);    % μm
    y_interval = rf(25)*16129+rf(26)*127+rf(27);
    move_times = rf(28)*16129+rf(29)*127+rf(30);    % μm
    Doppler = rf(31)*16129+rf(32)*127+rf(33);

    rf(1:33) = [];
    frameNum = length(rf)/(datalength*aline);
    
    distance = linspace(0, (x_interval*10^(-3)*aline), aline);
    depth = (delay+(1:datalength))/sampling_rate*1500*1000/2*10^(-6);   % (mm)
       
    for f = 1:frameNum % c-scan
       times = times+1;
       if mod(f,2)==0
            rf1 = fliplr(reshape(rf(datalength*aline*(f-1)+1:datalength*aline*f), datalength, aline)); % fliplr：左右翻轉
       else
            rf1 = reshape(rf(datalength*aline*(f-1)+1:datalength*aline*f), datalength, aline);
       end 
        %% Bandpass Filter
        fs = sampling_rate*10^(3); % sample rate (kHz)
        order = 2;   % order of filter
        lowcut = 25*10^(3);   % low cut frequency (kHz)
        highcut = 40*10^(3);   % high cut frequency (kHz)
        [b,a] = butter(order, [lowcut, highcut]/(fs/2), 'bandpass'); %-6db bandwidth
        rf_filter = filter(b, a, rf1);
        %% 訊號濾波前後比較作圖
%         figure(99)
%         sig = abs(fft(rf1)); % Fast Fourier transform
%         x = [1:length(sig)]*(sampling_rate/datalength);
%         bar(x, sig, 'b');     
%         xlabel('Frequency(MHz)','FontSize',15,'FontName','Times New Roman')      
%         figure(100)
%         sig2 = abs(fft(rf_filter)); %Fast Fourier transform
%         bar(x, sig2, 'b');
%         xlabel('Frequency(MHz)','FontSize',15,'FontName','Times New Roman')
        %% Image Compression
        Dr = 40; 
        env = abs(hilbert(rf_filter));

        Log_Env = 20*log10(env/0.0119); % env/min(min(envo))
        Log_Env = Log_Env-max(max(Log_Env))+Dr;

        Log_Env = Log_Env - min(Log_Env(:))/8; 
        im = Log_Env/max(max(Log_Env))*255;
        %% Select ROI 
        if(n==8 && f==1)
        figure(98);
        image(distance, depth, im);
        set(gca,'FontSize',15,'FontName','Times New Roman');
        xlabel('Distance (mm)','FontSize',15,'FontName','Times New Roman');
        ylabel('Depth (mm)','FontSize',15,'FontName','Times New Roman');
        colormap(gray(255));
        axis image;
        h = imrect(gca,[3.6 8.4 4.5 1.5]);
        %h = imrect(gca,[3.6 8.1 3 1.5]);
        
        k = waitforbuttonpress;
        api = iptgetapi(h); 
        api.addNewPositionCallback(@(p) title(mat2str(p,3)));  %得到最新座標值
        fcn = makeConstrainToRectFcn('imrect',get(gca,'XLim'),get(gca,'YLim'));  %不能將框挪至影像以外區域
        api.setPositionConstraintFcn(fcn);  %顯示

        k = waitforbuttonpress;
        pos = getPosition(h); %record position
        fprintf('ROI location = (%g, %g)\n', pos(1), pos(2));
        posROI(1, 2*(n-1)+f) = pos(1);
        posROI(2, 2*(n-1)+f) = pos(2);

        % '座標位置'換算成'訊號點數'
        posROI_x = ceil(pos(1)*aline/(x_interval*10^(-3)*aline));
        posROI_y = ceil(pos(2)/1000*2/1500*sampling_rate*10^6-delay); 
        posROI_width = ceil((pos(1)+pos(3))*aline/(x_interval*10^(-3)*aline));
        posROI_height = ceil((pos(2)+pos(4))/1000*2/1500*sampling_rate*10^6-delay); %ceil：無條件進位
        end
        imROI = rf_filter(posROI_y:posROI_height, posROI_x:posROI_width); %1110 60
        env_m = env(posROI_y:posROI_height, posROI_x:posROI_width);  % For Nakagami Parameter & Histogram
        imGLCM = im(posROI_y:posROI_height, posROI_x:posROI_width); % For GLCM
        %% 畫ROI & 存圖
        figure(2*(n-1)+f);
        image(distance, depth(1:4000), im(1:4000, :));
        %image(distance, depth(501:4500), im(501:4500, :));
        rectangle('Position', [pos(1), pos(2), pos(3), pos(4)], 'LineWidth', 1.5, 'LineStyle', '-', 'EdgeColor', 'y')  
        set(gca,'FontSize',15,'FontName','Times New Roman');
        xlabel('Distance (mm)','FontSize',15,'FontName','Times New Roman');
        ylabel('Depth (mm)','FontSize',15,'FontName','Times New Roman');
        colormap(gray(255));
        axis image;
        print('-djpeg', ['G6_1_head_right_', num2str(2*(n-1)+f),'.jpg']); %****************************************************************************
           %% save ROI image
        ROI = figure(20+2*n-1);   
        image(distance(posROI_x:posROI_width), depth(posROI_y:posROI_height), im(posROI_y:posROI_height, posROI_x:posROI_width)); 
        colormap(gray(255));
        set(gca, 'position', [0 0 1 1]);
        set(ROI,'visible','off');
        sstr = sprintf('%d', 2*n-1);
        saveas(ROI, ['roi_G6_1_head_right_' sstr],'jpg'); %*******************************************
        %% SNR
        signal = (reshape(imROI, 1, numel(imROI)))';
        rfSNR(2*(n-1)+f) = 20*log10(rms(signal)/0.2014);
        %rsSNR = 20*log10(rms(signal)/rms(noise)); %rfSNR = snr(signal,noise)
        %% IB 
        % 1.訊號做傅立葉轉換換成頻域
        % 2.把頻域用dB表示, 得到頻寬範圍(-6dB)
        % 3.再回沒轉換成dB之前的頻域抓頻寬內的點的值
        % IB公式：signal(點1)^2 /steelsignal(點1)^2+signal(點2)^2/steelsignal(點2)^2+....
        %rfIB = (reshape(imROI, 1, numel(imROI)))';
        datalengthROI = posROI_height-posROI_y+1;
        alineROI = posROI_width-posROI_x+1; 
        
        rfIB = imROI*vpp/255;
        envIB = abs(fft(rfIB)).^2;
        for i=1:datalengthROI
            envIB_avg(i,1) = mean(envIB(i,:));
        end
        Log_envIB = 20*log10(envIB_avg/max(envIB_avg));
        
%        figure(1);
%        bar(envIB_avg);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              

%       figure(2)
       [value index] = max(Log_envIB(1:end/2)); % Find center freq.
       x2 = [index-6:index+5]*(sampling_rate/datalengthROI);   
%        plot(x2, Log_envIB(index-6:index+5));  
       
       s1 = sum(envIB_avg(index-6:index+5));  
       IB(2*(n-1)+f) = 20*log10((s1/1.4870e+09)/12);  %鋼塊 bandwidth：12  取得點數要相同  
       
        %% Nakagami
        env_mle = (reshape (env_m, 1, numel(env_m)))';
        nakagami_mle = mle(env_mle, 'distribution', 'nakagami'); 
        nakagami_m(2*(n-1)+f) = nakagami_mle(1);
        pd_mle = makedist('nakagami', 'mu', nakagami_mle(1), 'omega', nakagami_mle(2));
        pdf_values = pdf(pd_mle, 0:1:255);
        % nakagami sliding window
%         tic
%         pulselength = 0.15; % (mm)
%         window_x = ceil(3*pulselength*aline/(x_interval*10^(-3)*aline)); % 3倍 pulselength
%         window_y = ceil(3*pulselength/1000*2/1500*sampling_rate*10^6);           
%         m = 0;
%         for x = 1:(posROI_width-posROI_x+1-window_x)
%            for y = 1:(posROI_height-posROI_y+1-+window_y)
%                 m = m+1;
%                 env_window = reshape(env_m(y:y+window_y-1, x:x+window_x-1), 1, window_x*window_y);
%                 nakagami_mle = mle(env_window, 'distribution', 'nakagami'); 
%                 nakagami_window(m) = nakagami_mle(1);
%            end
%         end
%         toc
%         nakagami_m(2*(n-1)+f) = mean(nakagami_window);
        %% Histogram & PDF of Nakagami distribution
        figure(40+2*(n-1)+f);
        env_h = reshape(env_m, numel(env_m), 1); 
        h_values = zeros(1, 256);
        for i = 1:length(env_h)
            if(round(env_h(i)) >= 256)
                continue;
            end
                h_values(round(env_h(i))+1) = h_values(round(env_h(i)) + 1) + 1;
        end
        h_values = h_values / length(env_h);
        mdl_m = fitlm(h_values(2:256), pdf_values(2:256)); 
        r_squared(2*(n-1)+f) = mdl_m.Rsquared.Ordinary;
        %p_mle(2*(n-1)+f) = mdl_m.Coefficients.pValue(1);
        %rmse_mle(2*(n-1)+f) = mdl_m.RMSE;
        
        bar_width = 0.7;
        bar(h_values, bar_width);
        xlim([0 80]);
        ylim([0 0.35]);
        xlabel('Histogram of the envelope','FontSize',14);       
        ylabel('Probability','FontSize',14); 
        title('Probability Density Function');        
        hold on
        %pd = fitdist(h,'Nakagami');   
        plot(1:256, pdf_values, 'r', 'LineWidth', 1);
        hold off        
        l = legend('Envelope signals',['Nakagami distribution (m = ',num2str(nakagami_m(2*(n-1)+f),2), ')',sprintf('\n'), 'R-squared = ', num2str(r_squared(2*(n-1)+f),4)]);
        set(l,'Fontsize',10);
        legend('boxoff');       
%% GLCM (20張*10隻)       
        %減少計算量，對原始圖像graylevel壓縮
        % [M,N,O] = size(imGLCM);
        % for i = 1:M
        %     for j = 1:N
        %         for p = 1:256/128
        %             if ((p-1)*128 <= imGLCM(i,j) && imGLCM(i,j) < (p-1)*128+128)
        %                 imGLCM(i,j) = p-1;
        %             end
        %         end
        %     end
        % end
        
        glcm_0 = graycomatrix(imGLCM, 'offset', [0 1], 'NumLevels', 256,'GrayLimits', [0 255]); %灰階值256   
        figure(80+2*n-1);
        image(glcm_0);
        colormap(gray(255));
        title('Co-occurrence Matrix');
        xlabel('Gray Level');
        ylabel('Gray Level');     
        a = colorbar;
        set(a, 'YLim', [0 (1/sum(sum(glcm_0)))]);
        title(a,'PR');            
        
        % glcm_0 = graycomatrix(imGLCM, 'offset', [0 1], 'NumLevels', 16,'GrayLimits', [0 15]); %灰階16
        % glcm_45 = graycomatrix(imGLCM, 'offset', [-1 1], 'NumLevels', 16,'GrayLimits', [0 15]);
        % glcm_90 = graycomatrix(imGLCM, 'offset', [-1 0], 'NumLevels', 16,'GrayLimits', [0 15]);
        % glcm_135 = graycomatrix(imGLCM, 'offset', [-1 -1], 'NumLevels', 16,'GrayLimits', [0 15]);
        
        %Symmetric:true (是Haralick (1973)描述的GLCM)
        %glcm = graycomatrix(imGLCM(1:3,1:3),'offset', [0 1;-1 1;-1 0;-1 -1] , 'GrayLimits', [0 15], 'Symmetric', false) 
        
        % 內建function
        % range: contrast(0~(size(glcm,1)-1)^2) correlation(-1~1) energy(0~1) homogeneity(0~1)
        stats_0 = graycoprops(glcm_0, {'contrast', 'homogeneity', 'correlation', 'energy'});
        feature_con(2*(n-1)+f) = stats_0.Contrast;
        feature_hom(2*(n-1)+f) = stats_0.Homogeneity;
        feature_cor(2*(n-1)+f) = stats_0.Correlation;
        feature_ene(2*(n-1)+f) = stats_0.Energy;
        
        % 算entropy，呼叫function
        GLCM_Features(glcm_0, 0); %對稱 pairs給1
        feature_ent(2*(n-1)+f) = ans.entro;
        feature_cshad(2*(n-1)+f) = ans.cshad; % Cluster Shade
        feature_cprom(2*(n-1)+f) = ans.cprom; % Cluster Prominence 
        
        % stats_0 = graycoprops(glcm_0, {'contrast', 'homogeneity', 'correlation', 'energy'});
        % stats_45 = graycoprops(glcm_45, {'contrast', 'homogeneity', 'correlation', 'energy'});
        % stats_90 = graycoprops(glcm_90, {'contrast', 'homogeneity', 'correlation', 'energy'});
        % stats_135 = graycoprops(glcm_135, {'contrast', 'homogeneity', 'correlation', 'energy'});
        
        % 平均4個角度
        % contrast_sum(n) = (stats_0.Contrast + stats_45.Contrast + stats_90.Contrast + stats_135.Contrast)/4;
        % homogeneity_sum(n) = (stats_0.Homogeneity + stats_45.Homogeneity + stats_90.Homogeneity + stats_135.Homogeneity)/4;
        % correlation_sum(n) = (stats_0.Correlation + stats_45.Correlation + stats_90.Correlation + stats_135.Correlation)/4;
        % energy_sum(n) = (stats_0.Energy + stats_45.Energy + stats_90.Energy + stats_135.Energy)/4;       
    end
end
ib_avg = sum(IB)/times;
nakagami_avg = sum(nakagami_m)/times;       

result(1, 2*n-times+1:2*n) = rfSNR(2*n-times+1:2*n); 
result(2, 2*n-times+1:2*n) = IB(2*n-times+1:2*n); 
result(3, 2*n-times+1:2*n) = nakagami_m(2*n-times+1:2*n);
result(4, 2*n-times+1:2*n) = feature_con(2*n-times+1:2*n); 
result(5, 2*n-times+1:2*n) = feature_hom(2*n-times+1:2*n); 
result(6, 2*n-times+1:2*n) = feature_cor(2*n-times+1:2*n);
result(7, 2*n-times+1:2*n) = feature_ene(2*n-times+1:2*n);
result(8, 2*n-times+1:2*n) = feature_ent(2*n-times+1:2*n);
result(9, 2*n-times+1:2*n) = feature_cshad(2*n-times+1:2*n);        
result(10, 2*n-times+1:2*n) = feature_cprom(2*n-times+1:2*n);
result(11, 2*n-times+1:2*n) = posROI(1 ,2*n-times+1:2*n);
result(12, 2*n-times+1:2*n) = posROI(2, 2*n-times+1:2*n);

% xlswrite('contusion05(G5_back)_right.xlsx', result, ['day',num2str(21)]); %*************************************************************************************
