% figure
close all
clear;
clc

%  axial resolution: 25.41  70MHz -6dB pulse length(um)   
%  axial resolution: 31.96  70MHz -10dB pulse length(um)
%  noise at focal zone: mean-> -46.8575 dB 70MHz var->2.3181 
dynamicRange = 42;
resolutionAxial = 0.03; %mm
resolutionLateral = 0.09;
noiseLevel = -40;
windowFold=[3 4 5 6 7 8];
fullFilename = 'F:\skin\G2\20181121_ratskin#2_d14\avtech_70MHz_amp1p5_freq4_pos3_B0.dat';
param = headFile_NI(fullFilename);
xAxis = (1:param.Aline)*param.XInterval/1000;
yAxis = ((1:param.DataLength)+param.Delay)/2/(param.SamplingRate*1e6)*1540*1000;

BmodeDirection = regexp(regexp(fullFilename, '_B\d+.dat', 'match'), '\d+', 'match'); %找出該影像是否需要左右相反(B後面是奇數)
if mod(str2num(BmodeDirection{1}{1}), 2)==1
    rf = fliplr(param.rf);
else
    rf = param.rf;
end

% filePath = 'D:\school\shearWaveData\皮膚掃描\20180926_mouseSkin_d21';
% fileName = '70MHz_amp1p5_freq4_filter27to98';
% file = dir([filePath '\' fileName '_C*']);
% firstParam = headFile_NI_forCscan([filePath '\' fileName '_C1.dat']);
% imgNumPerData = size(firstParam.rf, 3);
% frameNum = 1;
% for f=1:length(file)
%     param = headFile_NI_forCscan([filePath '\' fileName '_C' num2str(f) '.dat']);
%     rf(:, :, frameNum:frameNum+size(param.rf, 3)-1) = param.rf;
%     frameNum = frameNum+size(param.rf, 3);
% end
% % rf = rf(:, :, 9);
% rf = fliplr(rf(:, :, 1));

env_rf=abs(hilbert(rf));
log_rf=20*log10(env_rf/max(max(env_rf)));
img=255*(log_rf+dynamicRange)/dynamicRange;
% xAxis = (1:param.Aline)*param.XInterval/1000;
% yAxis = ((1:param.DataLength)+param.Delay)/2/(param.SamplingRate*1e6)*1540*1000;
% figure('color', 'white', 'position', [0 200 1500 300])
% image(xAxis, yAxis, img)
% colormap(gray(255))
% axis equal tight
% xlabel('Distance (mm)', 'FontSize', 14, 'FontName', 'Arial')
% ylabel('Depth (mm)', 'FontSize', 14, 'FontName', 'Arial');

% for f=1:frameNum %c-scan
% for f=1:1 %c-scan
%     cal_times = cal_times+1;
% 	if mod(f,2)==0       
%         rf1 = fliplr(reshape(rf(datalength*aline*(f-1)+1:datalength*aline*f), datalength, aline)); % fliplr：左右翻轉
%     else
%         rf1 = reshape(rf(datalength*aline*(f-1)+1:datalength*aline*f), datalength, aline);
%     end
% 	%% Bandpass Filter
% 	fs = sampling_rate*10^(3); % sample rate (kHz)
% 	order = 2;   % order of filter
% 	lowcut = 25*10^(3);   % low cut frequency (kHz)
%  	highcut = 40*10^(3);   % high cut frequency (kHz)
%  	[b,a] = butter(order, [lowcut, highcut]/(fs/2), 'bandpass'); %-6db bandwidth
%  	rf_filter = filter(b, a, rf1);       

nakagamiImg = zeros(param.DataLength, param.Aline, length(windowFold))-1;
% env_rf(log_rf<=noiseLevel) = nan;
% env_rf = 255*env_rf/max(max(env_rf));
% pool = parpool(4);
% for wf = 1:1
for wf = 1:length(windowFold)
    windowZ = round(resolutionAxial/1000/1540*param.SamplingRate*1e6*windowFold(wf));
%     windowX =  windowZ;
    windowX =  round(resolutionLateral/(param.XInterval*1e-3)*windowFold(wf));
    
    % computations take place here
%     waitbar(wf/length(windowFold)) 
        
    for jj = 1:param.DataLength-windowZ;
        for iii = 1:param.Aline-windowX;
%             if jj==1502 && iii==352
%                 jj
%             end
            roi = env_rf(jj:floor(windowZ+jj),iii:floor(windowX+iii-1));
            roi_reshape=reshape(roi, 1, [])';
            roi_reshape(isnan(roi_reshape)) = [];
            mu = (mean((roi_reshape).^2).^2)/mean((roi_reshape.^2-mean(roi_reshape.^2)).^2);
            nakagamiImg(floor((1+jj+windowZ+jj)/2),floor((1+iii+windowX+iii)/2),wf) = mu;

        end
    end
end
% delete(pool);