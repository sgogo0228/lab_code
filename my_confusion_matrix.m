function [out] = my_confusion_matrix(target, predicted)
    if length(target) ~= length(predicted)
        print('Inconsistant length');
        return;
    end
    out = zeros(4, 4);
    for i=1:length(target)
        out(target(i)+1, predicted(i)+1) = out(target(i)+1, predicted(i)+1) + 1;
    end
end