function [params, demodSignal] = shearWaveByDemod20220211(filename, depth, unitAline, push_noise, carrier_freq, interp_time_fold)
    % nfft = 4096;
    % 讀取檔案
    params = headFile_NI(filename);
    data = params.rf;
    data = data*params.Vpp/255;
%     % demodulation
    [I, Q] = demod(data, carrier_freq,1e9,'qam');


%   --跟Zheng, Y.; Chen, S.; Tan, W.; Greenleaf, J.F. Kalman filter motion detection for vibro-acoustography using pulse echo ultrasound. 
%       In Proceedings of the IEEE Symposium on Ultrasonics, 2003, 2003; pp. 1812-1815. 
%       中要減掉S平均值的意思是一樣的。但是直接減掉phi_f和phi_o並套用積化合差公式
    temp = -atan(Q./I);
    shift_I = zeros(size(I));
    shift_Q = zeros(size(Q));

    shift_phase = mean(rmoutliers(temp(d, :), 2, 'ThresholdFactor',2));
    shift_I = I.*cos(shift_phase)-Q.*sin(shift_phase);
    shift_Q = Q.*cos(shift_phase)+I.*sin(shift_phase);
    temp = permute(-atan(shift_Q./shift_I), [2 1]);

    temp = reshape(temp, unitAline*interp_time_fold, [], size(temp, 2));
    temp(1:push_noise*interp_time_fold, :, :) = [];
    demodSignal = reshape(temp, size(temp, 1)*size(temp, 2), size(temp, 3));
    
    gradient_y = demodSignal(2:end, :) - demodSignal(1:end-1, :);
    gradient_y(abs(gradient_y) <= pi/2) = 0;
    gradient_y(gradient_y > 0.3) = pi;
    gradient_y(gradient_y < -0.3) = -pi;
    grad_cumsum = cumsum(gradient_y);
    demodSignal(2:end, :) = demodSignal(2:end, :) - grad_cumsum;
    demodSignal = demodSignal - mean(demodSignal, 1);
    demodSignal = demodSignal(:, depth(1):depth(2));
end

