% dist代表用於計算斜率的點數，如果是[8 20]則代表用前8個點與前20個點計算斜率
function [slope_50hz] = linearFitForStatistic20181029(filePath, fileName, fileRange, phaseAngle, dist)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
    xlsFile = [filePath '/' fileName '.xlsx'];
    fileSize = fileRange(2)-fileRange(1)+1;
     xaxis = (0:1:19)';
%     xaxis = (0:0.5:9.5)';
    slopeNum = length(dist);
    slope = zeros(slopeNum, fileSize*6-1);
    slope_50hz = zeros(slopeNum, fileSize);
    data = zeros(21+slopeNum, 6*fileRange(2)-1);
    for i=1:fileSize
        data(1:20, (1+(i-1)*6)) = xaxis;
        data(22:22+slopeNum-1, (i-1)*6+1) = dist';
        for j=1:4
            yaxis = phaseAngle(:, j, i)*180/pi;
            for k=1:slopeNum
                slope(k, (i-1)*6+1) = dist(k);
                slope(k, (i-1)*6+j+1) = xaxis(1:dist(k))\yaxis(1:dist(k));
            end
            data(1:20, ((i-1)*6+j+1)) = yaxis;
            data(22:22+slopeNum-1, (i-1)*6+j+1) = slope(:, (i-1)*6+j+1);
        end
        slope_50hz(i) = slope(1, (i-1)*6+2);
    end
    %   為了方便NCC統計用code
    
    xlswrite(xlsFile, data, 'phaseAngle', 'A23');
    
    xlsData(6:6+fileSize-1) = num2cell(slope_50hz);
    [tempStart tempEnd] = regexp(fileName,'\(\d+\)');
%     myXlswrite([filePath '/variousRangedDepth.xlsx' ], xlsData, 'velocity', ['B' fileName(tempStart+1:tempEnd-1)]);
% 
%     myXlswrite(xlsFile, slope, 'phaseAngle', 'A44');
%     velocity_50hz = num2cell(velocity_50hz);
end

