%解調法計算位移量
function [params, demodSignal] = shear_wave_demod_20221210(filename, depth, unitAline, push_num, push_noise, carrier_freq, interp_time_fold)
    % nfft = 4096;
    % 讀取檔案
    params = headFile_NI(filename);
    data = params.rf;
    data = data*params.Vpp/255;
    % demodulation
    [I, Q] = demod(data, carrier_freq,params.SamplingRate*1e6,'qam');
    is_push_noise = [];
    for i = 0 : push_num - 1
        is_push_noise = [is_push_noise, (1:push_noise) + i * unitAline];
    end


    %--跟Zheng, Y.; Chen, S.; Tan, W.; Greenleaf, J.F. Kalman filter motion detection for vibro-acoustography using pulse echo ultrasound. 
    %    In Proceedings of the IEEE Symposium on Ultrasonics, 2003, 2003; pp. 1812-1815. 
    %    中要減掉S平均值的意思是一樣的。但是直接減掉phi_f和phi_o並套用積化合差公式
    demodSignal = -atan(Q./I);
    
    phase_adjust = zeros(size(Q));
    phase_adjust(I<0 & Q>0) = pi;
    phase_adjust(I<0 & Q<0) = -pi;
    demodSignal = demodSignal - phase_adjust;
    
    demodSignal(:, is_push_noise) = [];
    demodSignal = demodSignal';
    
    %處理atan超出定量圍的問題，因為tan函數周期是-pi/2~pi/2，所以在做atan時低於-pi/2的部分會變成pi/2，反之亦然，
    % 在demodSignal上就會是一個pi的差值需要校正回來
    % 這邊
    gradient_y = demodSignal(2:end, :) - demodSignal(1:end-1, :);
    gradient_y(abs(gradient_y) <= 2*pi/3) = 0;
    gradient_y(gradient_y > 0) = 2*pi;
    gradient_y(gradient_y < 0) = -2*pi;
    grad_cumsum = cumsum(gradient_y);
    demodSignal(2:end, :) = demodSignal(2:end, :) - grad_cumsum;
    demodSignal = demodSignal - mean(demodSignal, 1);
    demodSignal = demodSignal(:, depth(1):depth(2));
    demodSignal = filloutliers(demodSignal,'linear');
end