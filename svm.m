%% train SVM model
nClass = unique(T{:, end});
trainedData = T{1:72, 1:end-1};
classes = T{1:72, end};

% trainedData = [T{1:18, 1:end-1}; T{19:144, 1:end-1}];
% classes = [T{1:18, end}; T{19:144, end}];
rng(1);
linearSVMmd = fitcsvm(trainedData, classes, 'BoxConstraint', 5, 'KernelScale', 'auto' , 'Standardize',true, 'ClassNames',{'normal','inflammation'});
polySVMmd = fitcsvm(trainedData, classes, 'BoxConstraint', 1, 'KernelFunction', 'polynomial', 'KernelScale', 'auto', 'Standardize',true, 'PolynomialOrder',3, 'ClassNames',{'normal','inflammation'});
gausSVMmd = fitcsvm(trainedData, classes, 'BoxConstraint', 1, 'KernelFunction', 'gaussian', 'KernelScale', 'auto', 'Standardize',true, 'ClassNames',{'normal','inflammation'});
rbfSVMmd = fitcsvm(trainedData, classes, 'BoxConstraint', 1, 'KernelFunction', 'rbf', 'KernelScale', 'auto' , 'Standardize',true, 'ClassNames',{'normal','inflammation'});

% %% show the trained hyperplane
% figure
% hold on
% gscatter(trainedData(:, 1), trainedData(:, 2), classes(:, 1), 'rb');
% % plot(trainedData(:,1),trainedData(:,2),'ko','MarkerSize',10)
% interval = 0.01;
% [xgrid, ygrid] = meshgrid(min(trainedData(:, 1)):interval:max(trainedData(:, 1)), min(trainedData(:, 2)):interval:max(trainedData(:, 2)));
% [~,scores] = predict(polySVMmd, [xgrid(:), ygrid(:)]);
% [C,h] = contour(xgrid, ygrid, reshape(scores(:, 2), size(xgrid)), [0 0], 'k');
% h.LineWidth = 1.5;
% xlabel('SD')
% ylabel('Mean')
% legend({'normal','contusion', 'hyperplane'})
% set(gca, 'FontSize', 14 , 'FontWeight', 'bold', 'LineWidth', 3, 'FontName', 'Times');
% legend('boxoff')

%% cross-validation
acc = zeros(4, 10);
for i=1:1
%     acc(1, i) = 1-kfoldLoss(crossval(linearSVMmd));
    acc(2, i) = 1-kfoldLoss(crossval(polySVMmd));
    acc(3, i) = 1-kfoldLoss(crossval(gausSVMmd));
    acc(4, i) = 1-kfoldLoss(crossval(rbfSVMmd));
end
disp(acc(:, 1));
disp(mean(acc, 2));

%% 
[~,polyScore] = predict(polySVMmd, trainedData);
% linearSVMmd = fitPosterior(linearSVMmd);
% polySVMmd = fitPosterior(polySVMmd);
% gausSVMmd = fitPosterior(gausSVMmd);
% rbfSVMmd = fitPosterior(rbfSVMmd);
% [~,linearScore] = resubPredict(linearSVMmd);
% [~,polyScore] = resubPredict(polySVMmd);
% [~,gausScore] = resubPredict(gausSVMmd);
% [~,rbfScore] = resubPredict(rbfSVMmd);

[Xsvm,Ysvm,Tsvm,polyAUC] = perfcurve(classes,polyScore(:,2),'inflammation');
plot(Xsvm, Ysvm)