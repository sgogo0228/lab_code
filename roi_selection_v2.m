% figure
% close all
% clear;
% clc

% #只需要輸入第一筆(_C1.dat或_B0.dat)的檔名就好
% #如果檔名顯示有衰減過，需要在讀檔時補償回去
% 
% #主要輸出為roi_info(batch_num)

%% #單跑
file = char('F:\daming\contusion\20171120_ratleg#3_contusionD0\left\30MHz_dist0to5_depth10kto15k_C1.dat');
dynamic_range = 42;
resolutionAxial = 0.0459; %mm
resolutionLateral = 0.0743;
% ROI_window_fold = [30 60];
ROI_window_fold = [20 45];


%% #程式本體
clear roi_info
full_file_name = strtrim(file);
AxialROIFold = ROI_window_fold(1);
LateralROIFold = ROI_window_fold(2);

% #計算所有會用到的bmode影像，並指讀取其中一張(為了加快框選速度)
[a, b, ~] = fileparts(full_file_name);
c = dir([a '\' b(1:end-1) '*']);
file_num = size(c, 1);
[us_image, us_rf, us_env, x_axis, y_axis] = get_one_us_data([a '\' b(1:end-1) num2str(int32(file_num/2)) '.dat'], struct('is_save', 0, 'dynamic_range', dynamic_range));
if size(us_image, 3)>1
    us_image = us_image(:, :, int32(size(us_image, 3)/2));
end

% [us_image, ~, ~, x_axis, y_axis] = get_us_data(full_file_name, struct('is_save', 0, 'dynamic_range', dynamic_range));
param = headFile_NI(full_file_name);

% #select ROI
for i = 1:size(us_image, 3)
    temp_img = us_image(:, :, i);

    figure('color', 'white', 'position', [500 200 1000 500])
    image(x_axis, y_axis, temp_img)
    colormap(gray(255))
    axis equal tight
    title(i)
    xlabel('Distance (mm)', 'fontSize', 28, 'fontWeight', 'bold', 'fontName', 'Times')
    ylabel('Depth (mm)', 'fontSize', 28, 'fontWeight', 'bold', 'fontName', 'Times')
    set(gca, 'FontSize', 16, 'FontName', 'Times', 'FontWeight', 'bold', 'XTick', round(x_axis(1)):2:x_axis(end), 'YTick', round(y_axis(1)):1:y_axis(end))
    hold on
    % plot([x_axis(1) x_axis(end)], [9 9], 'y-', 'LineWidth', 2)
    % plot([x_axis(int32(length(x_axis)/2)) x_axis(int32(length(x_axis)/2))], [y_axis(1) y_axis(end)], 'y-', 'LineWidth', 2)

    origY = (1+param.Delay)/(param.SamplingRate*10^6)*1540*1000/2;
    if (i>1)
        h = drawrectangle('Position', [roi_info(i-1).roi_pos(1), roi_info(i-1).roi_pos(2), roi_info(i-1).roi_pos(3), roi_info(i-1).roi_pos(4)], 'RotationAngle', roi_info(i-1).roi_degree, 'Color', 'y', 'InteractionsAllowed', 'translate', 'Rotatable', true);
    else
        h = drawrectangle('Position', [0 origY resolutionLateral*LateralROIFold resolutionAxial*AxialROIFold], 'Color', 'y', 'InteractionsAllowed', 'translate', 'Rotatable', true);
    end
    roi_info(i) = custom_wait_for_ROI(h);
    close(gcf);
end
clear temp_img us_image
