stop_point = [1 1];
frame_interval = 5;
dynamic_range = 45;
bandFilter = [25 35];
detectSamplingRate = 1000;
file_set = char('D:\daming\skinflap\skinflap in wang lab\rat#1\day3\30MHz_9kto16k_0to5mm_C1.dat',...
    'D:\daming\skinflap\skinflap in wang lab\rat#1\day3\30MHz_9kto16k_5to10mm_C1.dat',...
    'D:\daming\skinflap\skinflap in wang lab\rat#1\day3\30MHz_9kto16k_10to15mm_C1.dat',...
    'D:\daming\skinflap\skinflap in wang lab\rat#1\day3\30MHz_9kto16k_15to20mm_C1.dat',...
    'D:\daming\skinflap\skinflap in wang lab\rat#1\day3\30MHz_9kto16k_20to25mm_C1.dat',...
    'D:\daming\skinflap\skinflap in wang lab\rat#1\day3\30MHz_9kto16k_25to30mm_C1.dat',...
    'D:\daming\skinflap\skinflap in wang lab\rat#1\day3\30MHz_9kto16k_30to35mm_C1.dat',...
    'D:\daming\skinflap\skinflap in wang lab\rat#1\day3\30MHz_9kto16k_35to40mm_C1.dat');

%% 手動框選並儲存的程式
% clear roi_info
i_start = 1;
for f = stop_point(1):size(file_set, 1)
% for f = 1:size(file_set, 1)
    [~, us_rf, ~, x_axis, y_axis] = get_us_data(strtrim(file_set(f, :)), 0, 'data_file_range', [21 29]);
    i_start = 1;
    if f == stop_point(1)
        i_start = stop_point(2);
    end
    for i = i_start:frame_interval:size(us_rf, 3)
        [paramB, paramA] = butter(3, bandFilter/(detectSamplingRate/2));    %第一個參數須視情況調整
        temp = filtfilt(paramB, paramA, us_rf(:, :, i));
        temp = abs(hilbert(temp));
        temp = 20*log10(temp/max(temp, [], 'all'));
        temp = (temp+dynamic_range)/dynamic_range*255;
        figure('color', 'white', 'position', [1500 50 700 400])
        image(x_axis, y_axis, temp)
%         axis([0 10 7.5 10.5])
        colormap(gray(256))
        axis equal tight
        
        figure('color', 'white', 'position', [1300 400 1000 500])
        image(x_axis, y_axis, temp)
        colormap(jet(255))
        axis equal tight
%         axis([0 10 7.5 10.5])
        title(i)
        xlabel('Distance (mm)', 'fontSize', 28, 'fontWeight', 'bold', 'fontName', 'Times')
        ylabel('Depth (mm)', 'fontSize', 28, 'fontWeight', 'bold', 'fontName', 'Times')
        set(gca, 'FontSize', 16, 'FontName', 'Times', 'FontWeight', 'bold', 'XTick', round(x_axis(1)):2:x_axis(end), 'YTick', round(y_axis(1)):1:y_axis(end))
        hold on
        h = drawpolygon('Color','yellow', 'LineWidth', 1);
        roi_info(f, i) = custom_wait_for_ROI(h);
        close all;
    end
end

%% 根據上面那段code得出的roi輸出對應的train and label image
clear target_image_dir target_label_dir;
target_size = [128 128];
target_image_dir = 'C:\Users\sgogo\Desktop\filtered_data\image_128';
target_label_dir = 'C:\Users\sgogo\Desktop\filtered_data\ground_128';
overlay_dir = 'C:\Users\sgogo\Desktop\filtered_data\overlaydata';
frame_interval = 1;
% img_index = 1381;
for f = 1:size(file_set, 1)
    file_path = strtrim(file_set(f, :));
    [~, us_rf, ~, x_axis, y_axis] = get_us_data(file_path, 'is_save', 0, 'dynamic_range', 45);
    us_rf(:, :, 2:2:size(us_rf, 3)) = fliplr(us_rf(:, :, 2:2:size(us_rf, 3)));
    if f==1
        img_index = size(dir(target_image_dir), 1)-1
    end

    [dir_path, ~, ~] = fileparts(file_path);
    if exist([dir_path '\labeldata'], 'dir')==0
        mkdir([dir_path '\labeldata']);
    end
    if exist([dir_path '\traindata'], 'dir')==0
        mkdir([dir_path '\traindata']);
    end
    if exist([dir_path '\overlaydata'], 'dir')==0
        mkdir([dir_path '\overlaydata']);
    end
    
    for i = 1:frame_interval:size(us_rf, 3)
        [paramB, paramA] = butter(3, bandFilter/(detectSamplingRate/2));    %第一個參數須視情況調整
        temp = filtfilt(paramB, paramA, us_rf(:, :, i));
        temp = abs(hilbert(temp));
        temp = 20*log10(temp/max(temp, [], 'all'));
        temp = (temp+dynamic_range)/dynamic_range*255;
        
        figure('color', 'white', 'position', [50 50 1400 700])
        image(x_axis, y_axis, us_image(:, :, i));
        BW = createMask(drawpolygon('Position', roi_info(f, i).roi_pos));
% 下面是在將圖片中坐左邊座右邊沒有label到的地方補上(僅適用於早期的skinflap)
%         [max_value, max_index] = maxk(roi_info(f, i).roi_pos(:, 1), 2);
%         max_y1 = roi_info(f, i).roi_pos(max_index(1), 2);
%         max_y2 = roi_info(f, i).roi_pos(max_index(2), 2);
%         if (roi_info(f, i).roi_pos(max_index(1), 1)<roi_info(f, i).roi_pos(max_index(2), 1))
%             max_x1 = roi_info(f, i).roi_pos(max_index(1), 1);
%         else
%             max_x1 = roi_info(f, i).roi_pos(max_index(2), 1);
%         end
%         BW(max_y2:max_y1, max_x1:end) = 1;
%         [min_value, min_index] = min(roi_info(f, i).roi_pos(:, 1), 2);
%         min_y1 = roi_info(f, i).roi_pos(min_index(1), 2);
%         min_y2 = roi_info(f, i).roi_pos(min_index(2), 2);
%         if (roi_info(f, i).roi_pos(min_index(1), 1)<roi_info(f, i).roi_pos(min_index(2), 1))
%             min_x1 = roi_info(f, i).roi_pos(min_index(2), 1);
%         else
%             min_x1 = roi_info(f, i).roi_pos(min_index(1), 1);
%         end
%         BW(min_y1:min_y2, 1:min_x1) = 1;
        
        BW = imresize(BW, target_size, 'method', 'bilinear');
        BW = BW>0.5;
        imwrite(BW*255, [target_label_dir '\' num2str(img_index) '.png'], 'PNG');
        
        temp_img = imresize(uint8(temp), target_size, 'method', 'bilinear');
        imwrite(temp_img, [target_image_dir '\' num2str(img_index) '.png'], 'PNG');
        
        overlay_image = labeloverlay(temp_img, BW);
        imwrite(overlay_image, [overlay_dir '\' num2str(img_index) '.png'], 'PNG');
        
        img_index = img_index + 1;
        
        close(gcf);
    end
end