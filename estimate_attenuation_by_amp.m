clear
clc
% close all
%% 
unitAline = 199;
pushNum = 5;
distNum = 10;
depth = [1 5000];
% CImage = '30MHz_dist0to5_depth5to14';
analyzeFilePath = ['H:\school\shearWaveData'];
file = char('H:\school\shearWaveData\20180106_DM_w0\head_right\30MHz_dist0to10_depth10to15k_amp600_cycle5100_1',...
    'H:\school\shearWaveData\20180106_DM_w0\head_right\30MHz_dist0to10_depth10to15k_amp600_cycle5100_2',...
    'H:\school\shearWaveData\20180106_DM_w0\head_right\30MHz_dist0to10_depth10to15k_amp600_cycle5100_3',...
    'H:\school\shearWaveData\20180106_DM_w0\back_right\30MHz_dist0to10_depth9to14k_amp600_cycle5100_1',...
    'H:\school\shearWaveData\20180106_DM_w0\back_right\30MHz_dist0to10_depth9to14k_amp600_cycle5100_2',...
    'H:\school\shearWaveData\20180106_DM_w0\back_right\30MHz_dist0to10_depth9to14k_amp600_cycle5100_3',...
    'H:\school\shearWaveData\20180125_DM_w1\rat#2_withDM_head\30MHz_dist0to10_depth9to14k_amp600_cycle5100_1',...
    'H:\school\shearWaveData\20180125_DM_w1\rat#2_withDM_head\30MHz_dist0to10_depth9to14k_amp600_cycle5100_2',...
    'H:\school\shearWaveData\20180125_DM_w1\rat#2_withDM_head\30MHz_dist0to10_depth9to14k_amp600_cycle5100_3',...
    'H:\school\shearWaveData\20180125_DM_w1\rat#2_withDM_righthand\30MHz_dist0to10_depth9to14k_amp600_cycle5100_1',...
    'H:\school\shearWaveData\20180125_DM_w1\rat#2_withDM_righthand\30MHz_dist0to10_depth9to14k_amp600_cycle5100_2',...
    'H:\school\shearWaveData\20180125_DM_w1\rat#2_withDM_righthand\30MHz_dist0to10_depth9to14k_amp600_cycle5100_3',...
    'H:\school\shearWaveData\20180130_DM_w2\rat#2_withDM_head\30MHz_dist0to5_depth9to14k_amp600_cycle5100_1',...
    'H:\school\shearWaveData\20180130_DM_w2\rat#2_withDM_head\30MHz_dist0to5_depth9to14k_amp600_cycle5100_2',...
    'H:\school\shearWaveData\20180130_DM_w2\rat#2_withDM_head\30MHz_dist0to5_depth9to14k_amp600_cycle5100_3',...
    'H:\school\shearWaveData\20180130_DM_w2\rat#2_withDM_back(righthand)\30MHz_dist0to5_depth9to14k_amp600_cycle5100_1',...
    'H:\school\shearWaveData\20180130_DM_w2\rat#2_withDM_back(righthand)\30MHz_dist0to5_depth9to14k_amp600_cycle5100_2',...
    'H:\school\shearWaveData\20180130_DM_w2\rat#2_withDM_back(righthand)\30MHz_dist0to5_depth9to14k_amp600_cycle5100_3',...
    'H:\school\shearWaveData\20180209_DM_w3\rat#2_withDM_head\30MHz_dist0to5_depth9to14k_amp600_cycle5100_1',...
    'H:\school\shearWaveData\20180209_DM_w3\rat#2_withDM_head\30MHz_dist0to5_depth9to14k_amp600_cycle5100_2',...
    'H:\school\shearWaveData\20180209_DM_w3\rat#2_withDM_head\30MHz_dist0to5_depth9to14k_amp600_cycle5100_3',...
    'H:\school\shearWaveData\20180209_DM_w3\rat#2_withDM_back(righthand)\30MHz_dist0to5_depth9to14k_amp600_cycle5100_1',...
    'H:\school\shearWaveData\20180209_DM_w3\rat#2_withDM_back(righthand)\30MHz_dist0to5_depth9to14k_amp600_cycle5100_2',...
    'H:\school\shearWaveData\20180209_DM_w3\rat#2_withDM_back(righthand)\30MHz_dist0to5_depth9to14k_amp600_cycle5100_3',...
    'H:\school\shearWaveData\20180212_DM_w4\rat#2_withDM_head\30MHz_dist0to5_depth9to14k_amp600_cycle5100_1',...
    'H:\school\shearWaveData\20180212_DM_w4\rat#2_withDM_head\30MHz_dist0to5_depth9to14k_amp600_cycle5100_2',...
    'H:\school\shearWaveData\20180212_DM_w4\rat#2_withDM_head\30MHz_dist0to5_depth9to14k_amp600_cycle5100_3',...
    'H:\school\shearWaveData\20180212_DM_w4\rat#2_withDM_back(righthand)\30MHz_dist0to5_depth9to14k_amp600_cycle5100_1',...
    'H:\school\shearWaveData\20180212_DM_w4\rat#2_withDM_back(righthand)\30MHz_dist0to5_depth9to14k_amp600_cycle5100_2',...
    'H:\school\shearWaveData\20180212_DM_w4\rat#2_withDM_back(righthand)\30MHz_dist0to5_depth9to14k_amp600_cycle5100_3',...
    'H:\school\shearWaveData\20180219_DM_w5\rat#2_withDM_head\30MHz_dist0to5_depth9to14k_amp600_cycle5100_1',...
    'H:\school\shearWaveData\20180219_DM_w5\rat#2_withDM_head\30MHz_dist0to5_depth9to14k_amp600_cycle5100_2',...
    'H:\school\shearWaveData\20180219_DM_w5\rat#2_withDM_head\30MHz_dist0to5_depth9to14k_amp600_cycle5100_3',...
    'H:\school\shearWaveData\20180226_DM_w6\rat#2_withDM_head\30MHz_dist0to5_depth9to14k_amp600_cycle5100_1',...
    'H:\school\shearWaveData\20180226_DM_w6\rat#2_withDM_head\30MHz_dist0to5_depth9to14k_amp600_cycle5100_2',...
    'H:\school\shearWaveData\20180226_DM_w6\rat#2_withDM_head\30MHz_dist0to5_depth9to14k_amp600_cycle5100_3',...
    'H:\school\shearWaveData\20180305_DM_w7\rat#2_withDM_head\30MHz_dist0to5_depth9to14k_amp600_cycle5100_1',...
    'H:\school\shearWaveData\20180305_DM_w7\rat#2_withDM_head\30MHz_dist0to5_depth9to14k_amp600_cycle5100_2',...
    'H:\school\shearWaveData\20180305_DM_w7\rat#2_withDM_head\30MHz_dist0to5_depth9to14k_amp600_cycle5100_3');
pushNoise=20;
detectSamplingRate=10000;   %千萬不要打錯!! 是指detect探頭的PRF
hormonicNum = 5;
baseFrequency=50;
modelParams = struct('unitAline', unitAline, 'pushNum', pushNum, 'usedCycle', 3, 'frontNoise', pushNoise-3, 'backNoise', 3, 'damping', 0);
slopeFitDotNum = [6 8];
analysisDistRange = [6 15];

fnum = size(file, 1);
bandFilter = [baseFrequency*0.7 baseFrequency*6];
overallDepth = depth(2)-depth(1)+1;
modifiedUnitAline = (modelParams.unitAline-modelParams.frontNoise-modelParams.backNoise-modelParams.damping);
unusedCycle=modelParams.pushNum-modelParams.usedCycle;

analyzeInterval = 250;  %% 經驗上分析肌肉的SW 100或200(以上也可以500)都可以，太少變異大
for i=1:overallDepth/analyzeInterval
    range(i, 1) = (i-1)*analyzeInterval+1;
    range(i, 2) = i*analyzeInterval;
end
% xcorrDepth = 3;
sgn = zeros(fnum, distNum, analysisDistRange(2)-analysisDistRange(1)+1, unitAline*pushNum);

for f = 1:fnum
    filename = [strtrim(file(f, :)) '_SW0.dat'];
    if ~exist(filename, 'file')
        f
        disp(filename);
        return;
    end
end

for f=1:fnum
    demodSignal = zeros(distNum, overallDepth, unitAline*pushNum);
    parfor j=0:distNum-1
        filename = [strtrim(file(f, :)) '_SW' num2str(j) '.dat'];
        [sgnParam demodSignal(j+1, :, :)]=shearWaveByDemod20170327(filename, depth, unitAline, pushNum, pushNoise);
    end
    for r = 1:analysisDistRange(2)-analysisDistRange(1)+1
        sgn(f, :, r, :) = squeeze(mean(demodSignal(:, range(r, 1):range(r, 2), :), 2));
    end
end
delete(gcp('nocreate'));

temp = sgn;


sgn = temp;
timeAxis = (0:(modelParams.pushNum*modelParams.unitAline-1))/detectSamplingRate;
% 去除push造成的雜訊
for i=pushNum-1:-1:0
    shift = modelParams.unitAline*i;
    sgn(:, :, :, (1+modelParams.unitAline-modelParams.damping)+shift:modelParams.unitAline+shift) = [];
    sgn(:, :, :, 1+shift:(modelParams.frontNoise+modelParams.backNoise)+shift) = [];
    timeAxis(1+(modelParams.unitAline-modelParams.backNoise-modelParams.damping)+shift:modelParams.unitAline+shift) = [];
    timeAxis(1+shift:(modelParams.frontNoise)+shift) = [];
end
timeAxis = timeAxis(1:modifiedUnitAline*3);
tempSgn = permute(sgn, [4 3 2 1]);
tempSgn = smoothdata(tempSgn, 'movmean', 5);
sgn = permute(tempSgn, [4 3 2 1]);
clear tempSgn phase_50hz

% 去除前後較不穩定的訊號(刪除第一個和最後一個shearwave)
sgn(:, :, :, 1:modifiedUnitAline) = [];
sgn(:, :, :, end-(unusedCycle-1)*modifiedUnitAline+1:end) = [];

% % 將剩餘的較穩定的訊號做平均作為kalman filter輸出
for f=1:fnum
    for i=1:distNum
        for d = 1:analysisDistRange(2)-analysisDistRange(1)+1
            tempSgn = mean(reshape(sgn(f, i, d, :), modifiedUnitAline, modelParams.usedCycle), 2)';
            sgn(f, i, d, 1:modifiedUnitAline) = tempSgn;
        end
    end
end
sgn(:, :, :, modifiedUnitAline+1:end) = [];

%% show spatiotemporal image
acustic_param = zeros(3, fnum, 2);   % (3深度, 檔案數量, 2參數)
interp_ratio = 10;
% color_interval = max(Vq, [], 'all')-min(Vq, [], 'all');
for fn = 1:fnum
    for dp = 5:7
        signal = squeeze(sgn(fn, :, dp, :))';

        % % 單純是為了show Spatiotemporal影像
%         signal_interp = 64-(signal-min(min(signal)))/(max(max(signal))-min(min(signal)))*64;
        [iX,iY] = meshgrid((0:distNum-1)/distNum*5, (0:modifiedUnitAline-1)*0.1);
        [Xq,Yq] = meshgrid((0:99)/100*4.5, (0:modifiedUnitAline-1)*0.1);
        Vq = interp2(iX,iY,signal,Xq,Yq);
        
        figure('color', 'white', 'position', [500 200 1000 500])
        x = (0:distNum-1)/distNum*5;
        y = (0:modifiedUnitAline-1)*0.1;
        image(y, x, (64-(Vq-min(min(Vq)))/(max(max(Vq))-min(min(Vq)))*64)')
        set(gca,'YDir','normal')
        colormap jet
        axis([0 10 0 4.5])
        (64-[0 64])/64*(max(max(Vq))-min(min(Vq)))+min(min(Vq));
        colorbar('Ticks', [1 12.8 25.6 38.4 51.2 64], 'TickLabels', {'-0.3','-0.2','-0.1','0','0.1','0.2'})
        set(gca, 'FontSize', 16, 'FontName', 'Times', 'FontWeight', 'bold', 'XTick', round(y(1)):2:y(end), 'YTick', round(x(1)):1:x(end))
        xlabel('Time (ms), T', 'FontSize', 20, 'FontName', 'Times', 'FontWeight', 'bold', 'fontName', 'Times')
        ylabel('Distance (mm), D', 'FontSize', 20, 'FontName', 'Times', 'FontWeight', 'bold', 'fontName', 'Times');
%         [dirPath, fileName, ~] = fileparts(strtrim(file(fn, :)));
%         [X,Map] = rgb2ind(frame2im(getframe(gcf)), 256);
%         savePath = [dirPath '\spatiotemporal\' fileName '_' num2str(dp) '.jpg'];
%         if exist([dirPath '\spatiotemporal\'], 'dir')==0
%             mkdir([dirPath '\spatiotemporal\']);
%         end
%         imwrite(X, Map, savePath, 'JPEG');
%         close all
        
        max_point_num = 3;
        max_point = zeros(3, 100*max_point_num);
        for i = 1:size(Vq, 2)
            [value, index] = maxk(-Vq(:, i), max_point_num);
            max_point(:, (i-1)*max_point_num+1:(i-1)*max_point_num+max_point_num) = [ones(1, max_point_num)*i;index';value'];
        end
        
        map_point_outlier = isoutlier(max_point(2, :));
        max_point(:, map_point_outlier==true) = [];
%         hold on
%         scatter((max_point(2, :)-1)*0.1, (max_point(1, :)-1)*0.045, 'k')
        max_point_avg = zeros(3, 100);
        for i=1:100
            max_point_avg(1, i) = i;
            max_point_avg(2, i) = mean(max_point(2, (max_point(1, :)==i)));
            max_point_avg(3, i) = mean(max_point(3, (max_point(1, :)==i)));
        end
%         is_max_point = zeros(189, interp_ratio*10);
%         is_max_point((max_point(1, :)-1)*189+max_point(2, :)) = 64;

        % % 如果要用connected component找出outlier(但只適用於取最大的10個值時，否則每個最大值之間不會連起來)
%         is_max_point = zeros(189, 200);
%         is_max_point((max_point(1, :)-1)*189+max_point(2, :)) = 1;
%         cc = bwconncomp(is_max_point);
%         max_point= [ceil(cc.PixelIdxList{1,1}/189) mod(cc.PixelIdxList{1,1},189)]';

        valid_dist = 3.5;
        fit_y = max_point_avg(1, 1:valid_dist*20)';
        fit_x = [ones(valid_dist*20, 1) max_point_avg(2, 1:valid_dist*20)'];
        slope = regress(fit_y, fit_x);
        velocity = slope(2)*0.5; %m/s

        % % 同時plot出fit結果跟實際值
        % % scatter(max_point_avg(2, :), max_point_avg(1, :))
        % % hold on
        % % plot((40:60), (40:60)*slope(2)+slope(1))

        fit_y = max_point_avg(3, 1:valid_dist*20)';
        fit_x = [ones(valid_dist*20, 1) max_point_avg(1, 1:valid_dist*20)'];
        is_positive = fit_y > 0;
        fit_y = 20*log10(fit_y(is_positive)/max(fit_y));
        fit_x = fit_x(is_positive, :);
        slope = regress(fit_y, fit_x);
        attenuation = -slope(2)*200; %db/cm
        acustic_param(dp-4, fn, :) = [velocity; attenuation];
        close all
    end
end

