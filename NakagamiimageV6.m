% figure
%  axial resolution: 25.41  70MHz -6dB pulse length(um)   
%  axial resolution: 31.96  70MHz -10dB pulse length(um)
%  noise at focal zone: mean-> -46.8575 dB 70MHz var->2.3181 
% clc
% clear
% close all

%% 如果要單跑
% file_set = char('D:\子悠_Journal\子悠資料_contusion\子悠實驗資料\實驗data\contusion08(G6_1_head)\20180523_day0\right\right_day0_C1.dat');
% 
% % noiseFile = 'D:\子悠_Journal\子悠資料_contusion\子悠實驗資料\實驗data\contusion05(G5_back)\20180411\right\right_day1_C1.dat';
% % noiseRange = [2900 770 200 50]; % [pos_y pos_x H W]
% transducer_freq = 30;
% dynamic_range = 42;
% resolutionAxial = 0.0459; %mm
% resolutionLateral = 0.0459;
% WMC_window_fold=[9; 11; 13; 15];
% 
% img_id_for_nakagami = (10:10);  %計算nakagami image的影像序列編號(因為時間考量，可能file_set裡的每個.dat檔裡面只有特定幾張要計算nakagami img)
% % estimateNoiseLevel;
% filter_range = [25 38.5];

%%
AxialROIFold = WMC_window_fold;
LateralROIFold = WMC_window_fold;
for f = 1:size(file_set)
    if ~exist(file_set(f, :), 'file')
        disp(file_set(f, :));
        return;
    end
end
% parpool(6);
param = headFile_NI(strtrim(file_set(1, :)));
tic
for f = 1:size(file_set, 1)
    fullFilename = strtrim(file_set(f, :));
    batchSize = length(dir([fullFilename(1:end-5) '*.dat']));
    param = headFile_NI(fullFilename);
    us_rf = param.rf;
    for b = str2double(fullFilename(end-4))+1:batchSize
        param = headFile_NI([fullFilename(1:end-5) num2str(b) '.dat']);
        us_rf(:, :, size(us_rf, 3)+1:size(us_rf, 3)+size(param.rf, 3)) = param.rf;
    end
    us_rf(:, :, 2:2:size(us_rf, 3)) = fliplr(us_rf(:, :, 2:2:size(us_rf, 3)));
    
    for i=1:length(img_id_for_nakagami)
        nakagami_img = zeros(param.DataLength, param.Aline, length(WMC_window_fold));
        rf = us_rf(:, :, img_id_for_nakagami(i));
        [b, a] = butter(3, 2*filter_range/param.SamplingRate);
        rf = filter(b, a, rf);
%         env_rf = abs(hilbert(rf));
        [I, Q] = demod(rf, transducer_freq*1e6, param.SamplingRate*1e6,'qam');
        env_rf = abs(complex(I, Q));
        log_rf=20*log10(env_rf/max(max(env_rf)));
        img=255*(log_rf+dynamic_range)/dynamic_range;

        xAxis = (0:param.Aline-1)*param.XInterval/1000;
        yAxis = ((1:param.DataLength)+param.Delay)/2/(param.SamplingRate*1e6)*1540*1000;
%         noiseLevel = mean(mean(log_rf(noiseRange(1):noiseRange(1)+noiseRange(3)-1, noiseRange(2):noiseRange(2)+noiseRange(4)-1)));
        snr_img = zeros(param.DataLength, param.Aline, length(WMC_window_fold));
        
        for wf = 1:length(WMC_window_fold)
            windowZ = round(2*resolutionAxial/1000/1540*param.SamplingRate*1e6*AxialROIFold(wf));
            windowX =  round(resolutionLateral/(param.XInterval*1e-3)*LateralROIFold(wf));
            off_set_z = round(windowZ/2);
            off_set_x = round(windowX/2);
% % computations take place here
            tempImg = zeros(param.DataLength, param.Aline)-1;
            temp_snr = zeros(param.DataLength, param.Aline);
% %           四個角落中訊號最小的當作雜訊
            roi_noise_lu = rf(1:windowZ, 1:windowX);
            roi_noise_ru = rf(1:windowZ, end-windowX+1:end);
            roi_noise_ld = rf(end-windowZ+1:end, 1:windowX);
            roi_noise_rd = rf(end-windowZ+1:end, end-windowX+1:end);
            roi_noise = min([sum(sum(roi_noise_lu.^2)) sum(sum(roi_noise_ru.^2)) sum(sum(roi_noise_ld.^2)) sum(sum(roi_noise_rd.^2))]);
            
% %             用convolution方法實現sliding window
            sum_kernel = ones(windowZ, windowX);    %windowZ:縱向資料點數 windowX:橫向資料的點數
            avg_kernel = sum_kernel/(windowZ*windowX);
            numerator = conv2(env_rf.^2, avg_kernel, 'same').^2;    %nakagami公式中的分子(E(R^2))^2
            denominator = conv2(env_rf.^4, avg_kernel, 'same')-numerator; %nakagami公式中的分母(E(R^4)-(E(R^2))^2)=var(R^2)
            temp = numerator./denominator;
            snr_img =10*log10(conv2(rf.^2, sum_kernel, 'same')/roi_noise);  %SNR
            nakagami_img(:, :, wf) = temp.*(snr_img>15);  %用SNR濾掉不要的值
        end
        [fdir, fname, ~] = fileparts(fullFilename);

% %         output nakagami image
        if ~exist([fdir '\nakagami'], 'dir')
            mkdir([fdir '\nakagami']);
        end
        figure('color', 'white')
        imagesc(xAxis, yAxis, mean(nakagami_img, 3));
        colormap(jet(255))
        axis image
%         title('Nakagami image', 'fontSize', 20, 'fontWeight', 'bold', 'fontName', 'Times')
        xlabel('Distance (mm)', 'fontSize', 24, 'fontWeight', 'bold', 'fontName', 'Times')
        ylabel('Depth (mm)', 'fontSize', 24, 'fontWeight', 'bold', 'fontName', 'Times')
        colorbar
        set(gca, 'FontSize', 16, 'FontName', 'Times', 'FontWeight', 'bold', 'XTick',0:1:15, 'YTick',5:1:12)
        caxis([0 1])
        axis([1 14 5.1 yAxis(end)])
        [X,Map] = rgb2ind(frame2im(getframe(gcf)), 256);
        imwrite(X, Map, [fdir '\nakagami\' fname(1:end-2) num2str(img_id_for_nakagami(i)) '.jpg'], 'JPEG');
        close all
    end
end
toc
delete(gcp('nocreate'));
