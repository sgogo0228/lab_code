function [output] = get_glcm(roi_for_glcm, Dx, Dy)
    output = graycomatrix(roi_for_glcm, 'offset', [0 Dx; -Dy Dx; -Dy 0; -Dy -Dx], 'NumLevels', 64,'GrayLimits', [0 255]); %d(1, 0)
%     output(:, :, 2) = graycomatrix(roi_for_glcm, 'offset', [], 'NumLevels', 32,'GrayLimits', [0 255]); %d(1, 45)
%     output(:, :, 3) = graycomatrix(roi_for_glcm, 'offset', [], 'NumLevels', 32,'GrayLimits', [0 255]); %d(1, 90)
%     output(:, :, 4) = graycomatrix(roi_for_glcm, 'offset', [], 'NumLevels', 32,'GrayLimits', [0 255]); %d(1, 135)
end