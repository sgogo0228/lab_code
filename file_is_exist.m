function [is_exist] = file_is_exist(file_set)
    is_exist = 1;
    for f=1:size(file_set, 1)
        if ~exist(file_set(f, :), 'file')
            f
            disp(file_set(f, :));
            is_exist = 0;
            return;
        end
    end
end

