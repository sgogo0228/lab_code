% % figure
% % close all
% % clear;
% % clc

% % 只需要輸入第一筆(_C1.dat或_B0.dat)的檔名就好
% % 如果檔名顯示有衰減過，需要在讀檔時補償回去
stainlessBlockData = 'K:\黃大銘實驗資料\skinflap\20200615_transducer_char\pulseecho\30MHz_eng1_dmp1_atten10dBatlimiter_B1.dat';
% file = char('K:\黃大銘實驗資料\skinflap\G5(female rats)\20190624_skinflap_post\no1\30MHz#2_0to6mm_C1.dat',...
%     'K:\黃大銘實驗資料\skinflap\G5(female rats)\20190624_skinflap_post\no1\30MHz#2_6to12mm_C1.dat',...
%     'K:\黃大銘實驗資料\skinflap\G5(female rats)\20190624_skinflap_post\no1\30MHz#2_12to18mm_C1.dat',...
%     'K:\黃大銘實驗資料\skinflap\G5(female rats)\20190624_skinflap_post\no1\30MHz#2_18to24mm_C1.dat',...
%     'K:\黃大銘實驗資料\skinflap\G5(female rats)\20190624_skinflap_post\no1\30MHz#2_24to30mm_C1.dat');
% r_iter = 1;
% d_iter = 1;

for f = 1:size(file)
    if ~exist(file(f, :), 'file')
        f
        disp(file(f, :));
        return;
    end
end

% resolutionAxial = 0.0591; %mm
% resolutionLateral = 0.1003;
% AxialROIFold = 10;
% LateralROIFold = 40;
% 
% % % 計算雜訊
% estimateNoiseLevel;
% noise4IB = noiseSquareSum(1);

dynamicRange = 42;
windowFold=[2;3;4;5]*3;
AxialROIFold = windowFold;
LateralROIFold = windowFold;
resolutionAxial = 0.0591; %mm
resolutionLateral = 0.0591;
estimateNoiseLevel;
noise4WMC = noiseSquareSum;
nfft = 4096;

% % 計算鋼筷頻譜
param = headFile_NI(stainlessBlockData);
% % 根據是否有加上衰減器對rf做補償
stainlessBlockFFT = mean(abs(fft(param.rf/10^-(0.5), nfft)), 2);

us_rf = [];
log_rf = [];
for f = 1:size(file, 1)
    full_file_name = strtrim(file(f, :));
    batch_size = length(dir([full_file_name(1:end-5) '*dat']));
    for b = 1:batch_size
        param = headFile_NI([full_file_name(1:end-5) num2str(b) '.dat']);
        us_rf = cat(3, us_rf, param.rf);
        temp = abs(hilbert(param.rf));
        log_rf = cat(3, log_rf, 20*log10(temp/max(temp, [], 'all')));
    end
end
clear temp

QUS = struct();
    tic
for f=1:size(us_rf, 3)
    dynamicRange = 42;
    resolutionAxial = 0.0591; %mm
    resolutionLateral = 0.1003;
    AxialROIFold = 10;
    LateralROIFold = 40;
    nfft = 4096;
    
    if ~mod(f, 2)
        temp_rf = fliplr(us_rf(:, :, f));
        temp_log = fliplr(log_rf(:, :, f));
    else
        temp_rf = us_rf(:, :, f);
        temp_log = log_rf(:, :, f);
    end
    temp_img=255*(temp_log+dynamicRange)/dynamicRange;
    temp_BW = false(size(temp_rf));
    temp_BW(roi_position(f, r_iter, d_iter).roi_index) = true;
    BW_nonzero_num = sum(temp_BW);
    
    j=1;
    temp_fft_rf = [];
    for i=1:length(BW_nonzero_num)
        if BW_nonzero_num(i) > resolutionAxial*AxialROIFold/1000/1540*param.SamplingRate*1e6*2*0.95
            temp_fft_rf(:, j) = abs(fft(temp_rf(temp_BW(:, i), i), nfft));
            j = j+1;
        end
    end
    temp_fft_rf = mean(temp_fft_rf, 2);
    temp_log_fft = 20*log10(temp_fft_rf/max(temp_fft_rf(20:end-20)));
    [~, index] = max(temp_log_fft(20:end/2)); % Find center freq.
    x1 = index; x2 = index;
    while temp_log_fft(x1) > -6 && x1>1
       x1 = x1-1;
    end
    while temp_log_fft(x2) > -6 && x2<length(temp_log_fft)
       x2 = x2+1;
    end
    s1 = sum(temp_fft_rf(x1+1:x2));
    s2 = sum(stainlessBlockFFT(x1+1:x2));
    QUS(f).IB = 20*log10((s1/s2)/(x2-x1));  %鋼塊 bandwidth：12  取得點數要相同

%     roi_img = temp_img(roi_position(f, r_iter, d_iter).roi_index);
%     roi_img(roi_img<0) = 0;
%     roi_img(roi_img>255) = 255;
%     QUS(f).hist_mean = mean(roi_img);
%     QUS(f).hist_var = var(roi_img);
%     QUS(f).hist_skew = (sum((roi_img-QUS(f).hist_mean).^3)./length(roi_img)) ./ (QUS(f).hist_var.^1.5);
%     QUS(f).hist_kurt = (sum((roi_img-QUS(f).hist_mean).^4)./length(roi_img)) ./ (QUS(f).hist_var.^2);
% %     
%     roi_rf = temp_rf(roi_position(f, r_iter, d_iter).roi_index);
%     [roi_index_row, roi_index_column] = ind2sub(size(temp_rf), roi_position(f, r_iter, d_iter).roi_index);
%     noise_region_shift = max(roi_index_row)+size(temp_rf, 1)*(min(roi_index_column)-2);
%     noise_index = roi_position(f, r_iter, d_iter).roi_index-noise_region_shift;
%     roi_noise = temp_rf(noise_index);
%     QUS(f).SNR = 10*log10(sum(roi_rf.^2)/sum(roi_noise.^2));
%     env_roi = abs(hilbert(roi_rf));
%     env_roi = env_roi/max(env_roi, [], 'all')*255;
%     nakagami_param = mle(env_roi, 'distribution', 'nakagami');
%     QUS(f).nakagami = nakagami_param(1);
%         figure; hist = histogram(env_roi,'BinWidth' , 9.9);

%     windowFold=[2;3;4;5]*3;
%     AxialROIFold = windowFold;
%     LateralROIFold = windowFold;
%     resolutionAxial = 0.0591; %mm
%     resolutionLateral = 0.0591;
%     nakagami_img = zeros(length(roi_position(f, r_iter, d_iter).roi_index), length(windowFold));
%     parfor wf = 1:length(windowFold)
%         windowZ = round(2*resolutionAxial/1000/1540*param.SamplingRate*1e6*AxialROIFold(wf));
%         windowX =  round(resolutionLateral/(param.XInterval*1e-3)*LateralROIFold(wf));
%         off_set_z = round(windowZ/2);
%         off_set_x = round(windowX/2);
%         temp_nakagami = zeros(length(roi_position(f, r_iter, d_iter).roi_index), 1);
% % % computations take place here
%         for idx = 1:length(roi_position(f, r_iter, d_iter).roi_index)
%             [jj, ii] = ind2sub(size(temp_rf), roi_position(f, r_iter, d_iter).roi_index(idx));
%             if jj-off_set_z<1 || jj+off_set_z>size(temp_rf, 1) || ii-off_set_x<1 || ii+off_set_x-1>size(temp_rf, 2)
%                 roi = [];
%                 roi_SNR = inf;
%             else
%                 roi = temp_rf((jj-off_set_z):(jj+off_set_z)-1,(ii-off_set_x):(ii+off_set_x)-1);
%                 roi_SNR = 10*log10(sum(sum(roi.^2))/noise4WMC(wf));
%             end
% %             noiseNum = length(find(log_rf((j-off_set_z):(j+off_set_z),(i-off_set_x):(i+off_set_x)) < 0.8*noiseLevel));
%             if roi_SNR < 20 || roi_SNR==inf
% %                     if(noiseNum > 0.2*numel(roi))
%                 mu = 0;
%             else
%                 roi_reshape=reshape(roi, [], 1);
%                 env_roi = abs(hilbert(roi_reshape));
%                 env_roi = env_roi/max(max(env_roi))*255;
%                 mu = (mean((env_roi).^2).^2)/mean((env_roi.^2-mean(env_roi.^2)).^2);
%             end
%             temp_nakagami(idx) = mu;
%         end
%         nakagami_img(:, wf) = temp_nakagami;
%     end
%     QUS(f).WMCNakagami = mean(nakagami_img(nakagami_img>0), 'all');
end
    toc
clear temp_rf temp_log temp_nakagami nakagamiImg temp_BW temp_fft_rf temp_log_fft
% a=c

