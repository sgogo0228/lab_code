% figure
%  axial resolution: 25.41  70MHz -6dB pulse length(um)   
%  axial resolution: 31.96  70MHz -10dB pulse length(um)
%  noise at focal zone: mean-> -46.8575 dB 70MHz var->2.3181 
% clc
% clear
% close all

%% 如果要單跑
% file_set = char('D:\子悠_Journal\子悠資料_contusion\子悠實驗資料\實驗data\contusion08(G6_1_head)\20180523_day0\right\right_day0_C1.dat');
% 
% % noiseFile = 'D:\子悠_Journal\子悠資料_contusion\子悠實驗資料\實驗data\contusion05(G5_back)\20180411\right\right_day1_C1.dat';
% % noiseRange = [2900 770 200 50]; % [pos_y pos_x H W]
% transducer_freq = 30;
% dynamic_range = 42;
% resolutionAxial = 0.0459; %mm
% resolutionLateral = 0.0459;
% WMC_window_fold=[9; 11; 13; 15];
% 
% img_id_for_nakagami = (10:10);  %計算nakagami image的影像序列編號(因為時間考量，可能file_set裡的每個.dat檔裡面只有特定幾張要計算nakagami img)
% % estimateNoiseLevel;
% filter_range = [25 38.5];

%%

% parpool(6);

% tic

% param = headFile_NI(full_file_name);
% us_rf = get_us_data(full_file_name);

nakagami_img = zeros(param.DataLength, param.Aline, length(WMC_window_fold));
% rf = us_rf(:, :, img_id_for_nakagami(i));
% [b, a] = butter(3, 2*filter_range/param.SamplingRate);
% rf = filter(b, a, rf);
%         env_rf = abs(hilbert(rf));

[I, Q] = demod(temp_rf, transducer_freq*1e6, param.SamplingRate*1e6,'qam');
env_rf = abs(complex(I, Q));
log_rf=20*log10(env_rf/max(max(env_rf)));
img=255*(log_rf+dynamic_range)/dynamic_range;

xAxis = (0:param.Aline-1)*param.XInterval/1000;
yAxis = ((1:param.DataLength)+param.Delay)/2/(param.SamplingRate*1e6)*1540*1000;
%         noiseLevel = mean(mean(log_rf(noiseRange(1):noiseRange(1)+noiseRange(3)-1, noiseRange(2):noiseRange(2)+noiseRange(4)-1)));
snr_img = zeros(param.DataLength, param.Aline, length(WMC_window_fold));

parfor wf = 1:length(WMC_window_fold)
    windowZ = round(2*resolutionAxial/1000/1540*param.SamplingRate*1e6*AxialROIFold(wf));
    windowX =  round(resolutionLateral/(param.XInterval*1e-3)*LateralROIFold(wf));
    off_set_z = floor(windowZ/2);
    off_set_x = floor(windowX/2);

%             以4*4影像I作為例子[0 1 1 0]*I*[0 1 1 0]'就可以求出中間2*2區塊的加總
%             前面的[0 1 1 0]代表Z軸上要計算平均值對應的座標點，後面的[0 1 1 0]代表X軸
%             透過這套方法可以用matrix mutiple求出sliding window mean

    depth_shitf_matrix = zeros(param.DataLength, param.DataLength);
    for idx = 1:param.DataLength
        head = max([1, idx-off_set_z]);
        tail = min([idx+off_set_z-1, param.DataLength]);
        % length = min([idx+off_set_z-1, windowZ, off_set_z+(5000-idx)+1]);
        depth_shitf_matrix(idx, head:tail) = 1;
    end
    width_shitf_matrix = zeros(param.Aline, param.Aline);
    for idx = 1:param.Aline
        head = max([1, idx-off_set_x]);
        tail = min([idx+off_set_x-1, param.Aline]);
        % length = min([idx+off_set_x-1, windowX, off_set_x+(1500-idx)+1]);
        width_shitf_matrix(idx, head:tail) = 1;
    end
    width_shitf_matrix = width_shitf_matrix';
    numerator = (depth_shitf_matrix*(env_rf.^2)*width_shitf_matrix/windowX/windowZ).^2;
    denominator = depth_shitf_matrix*(env_rf.^4)*width_shitf_matrix/windowX/windowZ - numerator;
    temp = numerator./denominator;

% %           四個角落中訊號最小的當作雜訊
    roi_noise_lu = temp_rf(1:windowZ, 1:windowX);
    roi_noise_ru = temp_rf(1:windowZ, end-windowX+1:end);
    roi_noise_ld = temp_rf(end-windowZ+1:end, 1:windowX);
    roi_noise_rd = temp_rf(end-windowZ+1:end, end-windowX+1:end);
    roi_noise = min([sum(sum(roi_noise_lu.^2)) sum(sum(roi_noise_ru.^2)) sum(sum(roi_noise_ld.^2)) sum(sum(roi_noise_rd.^2))]);
    
    snr_img =10*log10((depth_shitf_matrix*(temp_rf.^2)*width_shitf_matrix)/roi_noise);  %SNR
    nakagami_img(:, :, wf) = temp.*(snr_img>15);  %用SNR濾掉不要的值
end
%         [fdir, fname, ~] = fileparts(fullFilename);
% 
% % %         output nakagami image
%         if ~exist([fdir '\nakagami'], 'dir')
%             mkdir([fdir '\nakagami']);
%         end
%         figure('color', 'white')
%         imagesc(xAxis, yAxis, mean(nakagami_img, 3));
%         colormap(jet(255))
%         axis image
% %         title('Nakagami image', 'fontSize', 20, 'fontWeight', 'bold', 'fontName', 'Times')
%         xlabel('Distance (mm)', 'fontSize', 24, 'fontWeight', 'bold', 'fontName', 'Times')
%         ylabel('Depth (mm)', 'fontSize', 24, 'fontWeight', 'bold', 'fontName', 'Times')
%         colorbar
%         set(gca, 'FontSize', 16, 'FontName', 'Times', 'FontWeight', 'bold', 'XTick',0:1:15, 'YTick',5:1:12)
%         caxis([0 1])
%         axis([1 14 5.1 yAxis(end)])
%         [X,Map] = rgb2ind(frame2im(getframe(gcf)), 256);
%         imwrite(X, Map, [fdir '\nakagami\' fname(1:end-2) num2str(img_id_for_nakagami(i)) '.jpg'], 'JPEG');
%         close all
% toc
% delete(gcp('nocreate'));
