transducerFocus = 9;    % mm
AxialROIFold = ROI_window_fold(1);
LateralROIFold = ROI_window_fold(2);

param = headFile_NI(noiseFile);
% [path, filename, ~] = fileparts(noiseFile);
% imageGroup = dir([filename(1:end-5) '*']);
windowSize = [round(resolutionAxial*AxialROIFold/1000/1540*param.SamplingRate*1e6*2) round(resolutionLateral*LateralROIFold/param.XInterval*1000)];
% [paramB, paramA] = butter(3, [23.5 32.5]*1e6/(250*1e6/2));    %第一個參數須視情況調整
% noise_rf = filtfilt(paramB, paramA, param.rf);
for wf = 1:length(AxialROIFold)
    position(wf, :) = [round(transducerFocus/1000/1500*param.SamplingRate*1e6*2)-param.Delay round(param.Aline/2-windowSize(wf, 2)/2) windowSize(wf, 1) windowSize(wf, 2)];
    noiseRoi = param.rf(position(wf, 1):position(wf, 1)+position(wf, 3)-1, position(wf, 2):position(wf, 2)+position(wf, 4)-1);
    noise4WMC(wf) = sum(sum(noiseRoi.^2));
end
