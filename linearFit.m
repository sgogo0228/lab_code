function [] = linearFit(filePath, fileName, dataRange, dist)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
    xlsFile = [filePath '/' fileName '.xlsx'];
    dataSize = dataRange(2)-dataRange(1)+1;
    [number, text, rawData] = xlsread(xlsFile);
    xaxis = (0:0.5:9.5)';
    slopeNum = size(dist, 2);
    slope = zeros(slopeNum, dataSize*6-1);
    for i=1:dataSize
        for j=1:4
            yaxis = number(22:41, (i-1)*6+j+1);
            for k=1:slopeNum
                slope(k, (i-1)*6+1) = (dist(k)-1)/2;
                slope(k, (i-1)*6+j+1) = xaxis(1:dist(k))\yaxis(1:dist(k));
            end
        end
    end
    xlswrite(xlsFile, slope, 'phaseAngle', 'A44');
end

