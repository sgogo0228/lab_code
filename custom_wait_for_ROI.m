function [out] = custom_wait_for_ROI(hROI)
            
        % Listen for mouse clicks on the ROI
        l = addlistener(hROI,'ROIClicked',@clickCallback);
        
        % Block program execution
        uiwait;
        
        % Remove listener
        delete(l);
        
%         out = hROI;
        % Return the points of current mask
        if isprop(hROI, 'RotationAngle')
            out = struct('roi_pos', hROI.Position, 'roi_degree', hROI.RotationAngle);
        else
            out = struct('roi_pos', hROI.Position);
        end
end
        
function clickCallback(~,evt)
        
        if strcmp(evt.SelectionType,'ctrl')
            uiresume;
        end
end
