%% 參數設定

resolutionAxial = 0.0591; %mm
resolutionLateral = 0.1003;
AxialROIFold = 10;
LateralROIFold = 40;
r_num = 1;
d_num = 1;
img_num_per_section = 30;
pos_num_per_day = 5;
dynamic_range = 42;

fileset = char('E:\黃大銘實驗資料\skinflap\G6\20190710_skinflap_d7\no1(butt)\30MHz#2_0to6mm_C1.dat',...
    'E:\黃大銘實驗資料\skinflap\G6\20190710_skinflap_d7\no1(butt)\30MHz#2_6to12mm_C1.dat',...
    'E:\黃大銘實驗資料\skinflap\G6\20190710_skinflap_d7\no1(butt)\30MHz#2_12to18mm_C1.dat',...
    'E:\黃大銘實驗資料\skinflap\G6\20190710_skinflap_d7\no1(butt)\30MHz#2_18to24mm_C1.dat',...
    'E:\黃大銘實驗資料\skinflap\G6\20190710_skinflap_d7\no1(butt)\30MHz#2_24to30mm_C1.dat');

%% 算參數

for r_iter = 1:r_num
    for d_iter = 1:d_num
            file = fileset((r_iter-1)*d_num*pos_num_per_day+(d_iter-1)*pos_num_per_day+1:(r_iter-1)*d_num*pos_num_per_day+d_iter*pos_num_per_day, :);
%             roi_selection;
%             roi_position(:, d_iter, r_iter) = roi_info;
            parameter_calculation;
            us_QUS(:, r_iter, d_iter) = QUS;
    end
end
%% 計算皮膚厚度、高度與3D建模參數(surface)

clear temp;

for r_iter = 1:r_num
    for d_iter = 1:d_num
        file = fileset((r_iter-1)*d_num*pos_num_per_day+(d_iter-1)*pos_num_per_day+1:(r_iter-1)*d_num*pos_num_per_day+d_iter*pos_num_per_day, :);
        
        %輸出該次實驗掃描之影像
        us_image = [];
        for f=1:size(file, 1)
            temp  = get_us_data(strtrim(file(f, :)), struct('is_save', 0));
            us_image(:, :, (f-1)*img_num_per_section+1:f*img_num_per_section) = temp;
        end
        us_image = permute(us_image, [3 1 2]);
        clear temp
        
%         reconstruct_3d_image;
%         skin_parameter(d_iter, r_iter) = struct('avg_skin_height', avg_skin_height, 'avg_skin_thickness', avg_skin_thickness, 'surface', surface);
    end
%     save(['D:\daming\skinflap\skinflap in wang lab\rat#1\skin_parameter_from_3dimage_' num2str(r_iter) '.mat'], 'skin_parameter');
end
%
%% 使用皮膚厚度計算皮膚厚度隨時間變化的等高線圖

for i = 1:6 %rat
    for j = 1:5 %術後時間
        for k = 0:4 %不同position
            temp = skin_parameter(j, i).avg_skin_thickness(k*30+1:k*30+30);
            [outlier_value, outlier_index] = rmoutliers(temp);
            temp(outlier_index) = mean(outlier_value);
            T(i, j, k*30+1:k*30+30) = datasmooth(temp, 5);
        end
    end
end
clear temp
T1 = squeeze(mean(T, 1));
for k=0:4
    T2(:, :, k+1) = mean(T(:, :, k*30+1:k*30+30), 3);
end
T3 = squeeze(nanmean(T2, 1));
T4 = squeeze(nanvar(T2, 0, 1));
% T1 = T1./T1(1, :);
T4 = T4./T3;
T3 = T3./T3(1, :);

x = [30 22.5 15 7.5 0];
y = [0 1 3 5 7];
[xi, yi] = meshgrid(30:-0.1:0, 0:0.1:7);
out_m_mean = interp2(x, y, T3, xi, yi, 'linear');
out_m_var = interp2(x, y, T4, xi, yi, 'linear');

figure
imagesc(yi(:, 1), xi(1, :), out_m_mean'*100);
colormap jet(256);
cb = colorbar;
set(gca, 'FontSize', 16, 'FontName', 'Times', 'FontWeight', 'bold', 'XTick', [0 1 2 3 4 5 6 7], 'CLim', [60 100])
xlabel('Time (day)', 'FontSize', 18, 'FontName', 'Times', 'FontWeight', 'bold')
ylabel('Distance (mm), Y', 'FontSize', 18, 'FontName', 'Times', 'FontWeight', 'bold');
set(cb, 'Ticks', [60 70 80 90 99], 'TickLabels', {'60%', '70%', '80%', '90%', '100%'}, 'FontSize', 12);
hold on
[cntr, h] = contour(yi(:, 1), xi(1, :), out_m_mean'*100, [65 70 75 80 85 90 95], 'showText', 'on', 'LineColor', 'white');
% [cntr, h] = contour(yi(:, 1), xi(1, :), out_m', [1 2 3 4 5], 'showText', 'on', 'LineColor', 'white');
clabel(cntr, h, 'FontName', 'Times', 'FontWeight', 'bold', 'FontSize', 12);

figure
imagesc(yi(:, 1), xi(1, :), out_m_var'*100);
colormap jet(256);
cb = colorbar;
set(gca, 'FontSize', 16, 'FontName', 'Times', 'FontWeight', 'bold', 'XTick', [0 1 2 3 4 5 6 7], 'YTick', [0 5 10 15 20 25 30], 'CLim', [0 5])
xlabel('Time (day)', 'FontSize', 18, 'FontName', 'Times', 'FontWeight', 'bold')
ylabel('Distance (mm), Y', 'FontSize', 18, 'FontName', 'Times', 'FontWeight', 'bold');
set(cb, 'Ticks', [0 1 2 3 4 5], 'TickLabels', {'0%', '1%', '2%', '3%', '4%', '5%'}, 'FontSize', 12);
hold on
[cntr, h] = contour(yi(:, 1), xi(1, :), out_m_var'*100, [1 2 3 4 5], 'showText', 'on', 'LineColor', 'white');
clabel(cntr, h, 'FontName', 'Times', 'FontWeight', 'bold', 'FontSize', 12);
%% 畫3D, 2D nakagami image
load('L:\黃大銘實驗資料\skinflap\WMC_nakagami_image_5.mat');

temp = permute(mean(skin_parameter_5, 4), [1 3 2]);
temp(temp<=0.1) = nan;
xAxis = (1:size(temp, 2))*x_interval;
yAxis = (1:size(temp, 1))*y_interval;
zAxis = ((1:size(temp, 3))+delay)*z_interval;
figure('position', [100 100 900 300], 'color', 'white')
set(gca, 'Position', [0.1 0.2 0.8 0.8])
h = slice(xAxis, yAxis, zAxis, (temp-0.1)/0.6*256, xAxis, [], []);
set(h, 'EdgeColor','none', 'FaceColor','interp')
alpha(.05)
view(50, 10)
axis image;
grid off
set(gca, 'YLim', [0 32], 'YTick', [0 5 10 15 20 25 30], 'YTickLabel', {'30', '25', '20', '15', '10', '5', '0'})
set(gca, 'ZLim', [7.7 11.5], 'ZTick', [8 9 10 11], 'ZDir', 'reverse') 
set(gca, 'FontName', 'Times', 'FontWeight', 'bold', 'CLim', [0 255], 'FontSize', 14)
xlabel('Distance (mm), X', 'Position', [-3 9 15], 'FontSize', 16, 'FontName', 'Times', 'FontWeight', 'bold')
ylabel('Distance (mm), Y', 'Position', [0 22 15], 'FontSize', 16, 'FontName', 'Times', 'FontWeight', 'bold')
zlabel('Depth (mm), Z', 'FontSize', 16, 'FontName', 'Times', 'FontWeight', 'bold')
colormap jet(256)
cb = colorbar;
set(cb, 'Position', [0.93 0.2 0.02 0.7], 'Ticks', [0 42.5 85 127.5 170 212.5 255], 'TickLabels', {'0.1', '0.2', '0.3', '0.4', '0.5', '0.6', '0.7'})

% load 'K:\黃大銘實驗資料\skinflap\WMC_nakagami_image_3.mat';
% temp = permute(mean(skin_parameter_3, 4), [2 3 1]);
% temp(temp<=0) = nan;
% figure('Color', 'white')
% for i=1:150
%     image(xAxis, zAxis, temp(:, :, i)/0.7*256)
%     set(gca, 'XLim', [1 7], 'XTick', 1:1:7)
%     set(gca, 'YLim', [7.5 11.5], 'YTick', 7.5:1:11.5)
%     set(gca, 'FontSize', 16, 'FontName', 'Times', 'FontWeight', 'bold')
%     xlabel('Distance (mm)', 'FontSize', 20, 'FontName', 'Times', 'FontWeight', 'bold')
%     ylabel('Depth (mm)', 'FontSize', 20, 'FontName', 'Times', 'FontWeight', 'bold');
%     colormap jet(256)
% %     cb = colorbar;
% %     set(cb, 'FontSize', 10, 'Ticks', [1 37 74 110 147 183 220 256], 'TickLabels', {'0', '0.1', '0.2', '0.3', '0.4', '0.5', '0.6', '0.7'})
%     savePath = ['K:\黃大銘實驗資料\skinflap\journal\G6#2_nakagamiimage\day3\' num2str(i) '.jpg'];
%     [X,Map] = rgb2ind(frame2im(getframe(1)), 256);
%     imwrite(X, Map, savePath, 'JPEG');
% end
%% 將參數平均

evaluate_range = [11 20];
r_num = 3;
d_num = 5;
position_num = 5;
clear avg_qus
for r_iter = 1:r_num
    for d_iter = 1:d_num
        for s_iter = position_num-1:-1:0
            temp_qus = us_QUS(:, r_iter, d_iter);
            
            temp_param = [temp_qus(s_iter*30+evaluate_range(1):s_iter*30+evaluate_range(2)).IB];
            avg_qus.ib(5-s_iter, d_iter, r_iter) = mean(rmoutliers(temp_param, 'ThresholdFactor', 2));
            
            temp_SNR = [temp_qus(s_iter*30+evaluate_range(1):s_iter*30+evaluate_range(2)).SNR];
            temp_param = [temp_qus(s_iter*30+evaluate_range(1):s_iter*30+evaluate_range(2)).nakagami];
            avg_qus.nakagami(5-s_iter, d_iter, r_iter) = mean(rmoutliers(temp_param(temp_SNR>20), 'ThresholdFactor', 2));
            
%             temp_param = [temp_qus(s_iter*30+1:s_iter*30+evaluate_range(2)).WMCNakagami];
%             temp_output(d_iter, 14+5-s_iter) = {mean(rmoutliers(temp_param, 'ThresholdFactor', 2))};

            temp_param = [temp_qus(s_iter*30+1:s_iter*30+evaluate_range(2)).SNR];
            avg_qus.snr(5-s_iter, d_iter, r_iter) = mean(rmoutliers(temp_param, 'ThresholdFactor', 2));
            
            temp_param = [temp_qus(s_iter*30+evaluate_range(1):s_iter*30+evaluate_range(2)).hist_mean];
            avg_qus.hist_mean(5-s_iter, d_iter, r_iter) = mean(rmoutliers(temp_param, 'ThresholdFactor', 2));
            
            temp_param = [temp_qus(s_iter*30+evaluate_range(1):s_iter*30+evaluate_range(2)).hist_var];
            avg_qus.hist_var(5-s_iter, d_iter, r_iter) = mean(rmoutliers(temp_param, 'ThresholdFactor', 2));
            
            temp_param = [temp_qus(s_iter*30+evaluate_range(1):s_iter*30+evaluate_range(2)).hist_skew];
            avg_qus.hist_skew(5-s_iter, d_iter, r_iter) = mean(rmoutliers(temp_param, 'ThresholdFactor', 2));
            
            temp_param = [temp_qus(s_iter*30+evaluate_range(1):s_iter*30+evaluate_range(2)).hist_kurt];
            avg_qus.hist_kurt(5-s_iter, d_iter, r_iter) = mean(rmoutliers(temp_param, 'ThresholdFactor', 2));
        end
    end
end
%% 呈現各種切面的超音波影像 #2

imgs = zeros(3000, 801, 150, 5);
for f = 1:25
    file = strtrim(fileset(f, :));
    section = mod(f-1, 5);
    day = floor((f-1)/5)+1;
    rat = floor((f-1)/25)+1;
    [imgs(:, :, section*30+1:section*30+30, day), xAxis, yAxis] = showCscanImage(file, struct('is_save', 0));
end

xy_image = zeros(3000, 801, 5);
for i=1:5
    for j=1:150
        if skin_parameter(i).avg_skin_height(j)>0
            xy_image(1:end-skin_parameter(i).avg_skin_height(j), j, i) = imgs(skin_parameter(i).avg_skin_height(j)+1:end, 101, j, i);
        else
            xy_image(1-skin_parameter(i).avg_skin_height(j):end, j, i) = imgs(1:end+skin_parameter(i).avg_skin_height(j), 100, j, i);
        end
    end
end
zAxis = (150:-1:1)*0.2;
figure('color', 'white', 'position', [100 100 800 180])
image(zAxis, yAxis, xy_image(:, :, 5))
colormap gray(256)
axis image
% set(gca, 'FontSize', 12, 'FontName', 'Times', 'FontWeight', 'bold', 'YTick',  round(yAxis(1)):1:yAxis(end), 'Position', [0.1 0.19 0.85 0.85], 'XDir', 'reverse')
xlabel('Distance (mm)', 'FontSize', 14, 'FontName', 'Times', 'FontWeight', 'bold')
ylabel('Depth (mm)', 'FontSize', 14, 'FontName', 'Times', 'FontWeight', 'bold');
axis([1 30 7.5 11.5])

temp = ones(size(rsz_image))*150;
hold on
xAxis = (1:size(rsz_image, 2))*x_interval/resize_ratio;
yAxis = (1:size(rsz_image, 1))*y_interval;
zAxis = ((1:size(rsz_image, 3))+(delay+img_start_pts)*resize_ratio)*z_interval/resize_ratio;
h = slice(xAxis, yAxis, zAxis, temp, 5, [], []);
colormap gray(256)
set(h, 'EdgeColor','none', 'FaceColor','interp','FaceAlpha',.7)
h = line([0 0 9 9 0], [3 3 3 3 3], [7.8 11 11 7.8 7.8]);
h.Color = 'black'; h.LineStyle = '--'; h.LineWidth=2;
h = line([0 0 9 9 0], [15 15 15 15 15], [7.8 11 11 7.8 7.8]);
h.Color = 'black'; h.LineStyle = '--'; h.LineWidth=2;
h = line([0 0 9 9 0], [27 27 27 27 27], [7.8 11 11 7.8 7.8]);
h.Color = 'black'; h.LineStyle = '--'; h.LineWidth=2;

hold off
%% output 3D B-mode image、縱切面B-mode、橫切面B-mode(大致上等於#1+#2，並且此段code仍需搭配#1和#2的資料處理code)
% i=3; figure('position', [100 100 700 250], 'color', 'white') hiso = patch('Vertices',skin_parameter(i).surface.vertices,...% 
% 'Faces',skin_parameter(i).surface.faces,...% 'FaceColor',[1,.75,.65],... 'EdgeColor','none'); 
% set(gca, 'ZDir', 'reverse', 'Position', [0.09 0.15 0.9 0.9]) xlabel('Distance 
% (mm), X', 'Position', [-3 9 15], 'FontSize', 14, 'FontName', 'Times', 'FontWeight', 
% 'bold') ylabel('Distance (mm), Y', 'Position', [0 22 15], 'FontSize', 14, 'FontName', 
% 'Times', 'FontWeight', 'bold') zlabel('Depth (mm), Z', 'FontSize', 14, 'FontName', 
% 'Times', 'FontWeight', 'bold') view(50, 10) axis image; set(gca, 'YLim', [0 
% 32], 'YTick', [0 5 10 15 20 25 30], 'YTickLabel', {'30','25','20','15','10','5','0'}, 
% 'ZLim', [7.7 11.5], 'ZTick', [8 9 10 11], 'FontSize', 12, 'FontName', 'Times', 
% 'FontWeight', 'bold') lightangle(45,-45); set(gcf,'Renderer','zbuffer'); lighting 
% phong set(hiso, 'SpecularColorReflectance',0,'SpecularExponent',50) temp = ones(size(rsz_image))*150; 
% hold on xAxis_downsample = (1:size(rsz_image, 2))*x_interval/resize_ratio; yAxis_downsample 
% = (1:size(rsz_image, 1))*y_interval; zAxis_downsample = ((1:size(rsz_image, 
% 3))+(delay+img_start_pts)*resize_ratio)*z_interval/resize_ratio; h = slice(xAxis_downsample, 
% yAxis_downsample, zAxis_downsample, temp, 5, [3 15 27], []); colormap gray(256) 
% set(h, 'EdgeColor','none', 'FaceColor','interp','FaceAlpha',.5)
% 
% figure('position', [100 100 190 190], 'color', 'white') image(xAxis, yAxis, 
% imgs(:, :, 15, i))% axis image colormap gray(256) set(gca, 'FontSize', 12, 'FontName', 
% 'Times', 'FontWeight', 'bold', 'XTick', round(xAxis(1)):1:round(xAxis(end)), 
% 'YTick', round(yAxis(1)):1:yAxis(end), 'Position', [0.25 0.2 0.7 0.7]) title('Distal') 
% xlabel('Distance (mm), X', 'FontSize', 14, 'FontName', 'Times', 'FontWeight', 
% 'bold') ylabel('Depth (mm), Z', 'FontSize', 14, 'FontName', 'Times', 'FontWeight', 
% 'bold'); axis([1 7 7.5 11.5])
% 
% figure('position', [100 100 190 190], 'color', 'white') image(xAxis, yAxis, 
% imgs(:, :, 75, i))% axis image colormap gray(256) set(gca, 'FontSize', 12, 'FontName', 
% 'Times', 'FontWeight', 'bold', 'XTick', round(xAxis(1)):1:round(xAxis(end)), 
% 'YTick', round(yAxis(1)):1:yAxis(end), 'Position', [0.25 0.2 0.7 0.7]) title('Middle') 
% xlabel('Distance (mm), X', 'FontSize', 14, 'FontName', 'Times', 'FontWeight', 
% 'bold') ylabel('Depth (mm), Z', 'FontSize', 14, 'FontName', 'Times', 'FontWeight', 
% 'bold'); axis([1 7 7.5 11.5])
% 
% figure('position', [100 100 190 190], 'color', 'white') image(xAxis, yAxis, 
% imgs(:, :, 135, i))% axis image colormap gray(256) set(gca, 'FontSize', 12, 
% 'FontName', 'Times', 'FontWeight', 'bold', 'XTick', round(xAxis(1)):1:round(xAxis(end)), 
% 'YTick', round(yAxis(1)):1:yAxis(end), 'Position', [0.25 0.2 0.7 0.7]) title('Proximal') 
% xlabel('Distance (mm), X', 'FontSize', 14, 'FontName', 'Times', 'FontWeight', 
% 'bold') ylabel('Depth (mm), Z', 'FontSize', 14, 'FontName', 'Times', 'FontWeight', 
% 'bold'); axis([1 7 7.5 11.5])
% 
% figure('position', [100 100 700 160], 'color', 'white') image(zAxis, yAxis, 
% xy_image(:, :,i))% colormap gray(256) axis image set(gca, 'FontSize', 12, 'FontName', 
% 'Times', 'FontWeight', 'bold', 'YTick', round(yAxis(1)):1:yAxis(end), 'XDir', 
% 'reverse', 'Position', [0.07 0.17 0.9 0.9]) xlabel('Distance (mm), Y', 'FontSize', 
% 14, 'FontName', 'Times', 'FontWeight', 'bold') ylabel('Depth (mm), Z', 'FontSize', 
% 14, 'FontName', 'Times', 'FontWeight', 'bold'); axis([1 30 7.5 11.5])

avg_thickness = zeros(5, d_num, r_num);
for i = 1:r_num %rat
    for j = 1:d_num %術後時間
        for k = 0:4 %不同position
            temp = skin_parameter(j, i).avg_skin_thickness(k*30+1:k*30+30);
            avg_thickness(5-k, j, i) = mean(rmoutliers(temp));
        end
    end
end
%% section

x_axis = (0:800)*10/1000;
y_axis = (1500+(1:3000))/2/250e6*1540*1000;
for i = 1:r_num %rat
    figure
    for j = 1:d_num %術後時間
        for k = [1 3 5] %不同position
            subplot(3, 5, 5*(k-1)/2+j);
            image(x_axis, y_axis, squeeze(nimgs((i-1)*25+(j-1)*5+(6-k), 3, :, :))/0.8*255);
            colormap(jet(256))
            axis image
            axis([1 7 7.7 11.5]);
            set(gca, 'FontSize', 18, 'FontWeight', 'bold', 'FontName', 'Times', 'YTick', [7 8 9 10 11], 'XTick', [1 2 3 4 5 6 7])
        end
    end
end
%% 計算某隻老鼠某一天的所有data

dynamic_range = 42;
for r_iter = 1:r_num
    for d_iter = 1:1
        file = fileset((r_iter-1)*d_num*pos_num_per_day+(d_iter-1)*pos_num_per_day+1:(r_iter-1)*d_num*pos_num_per_day+d_iter*pos_num_per_day, :);
        fileName = strtrim(file(1, :));
        firstParam = headFile_NI(fileName);
        us_rf = firstParam.rf;
        for f = 1:size(file, 1)
            fileName = strtrim(file(f, :));
            batchSize = length(dir([fileName(1:end-5) '*.dat']));
            if f==1
                for b = 2:batchSize
                    param = headFile_NI([fileName(1:end-5) num2str(b) '.dat']);
                    us_rf(:, :, end+1:end+size(param.rf, 3)) = param.rf;
                end
            else
                for b = 1:batchSize
                    param = headFile_NI([fileName(1:end-5) num2str(b) '.dat']);
                    us_rf(:, :, end+1:end+size(param.rf, 3)) = param.rf;
                end
            end
        end
    end
end
us_env = abs(hilbert(us_rf));
us_image = 20*log10(us_env./max(us_env, [], [1 2]))+42;
us_image = us_image/42*255;
us_image(:, :, 2:2:end) = fliplr(us_image(:, :, 2:2:end));
clear us_env us_rf;
%% 用於計算兩種方法測量nakagami m的error 1.擷取整張包封訊號中ROI部分直接用來計算m (global method) 2.擷取rf訊號中ROI部分，取包封後計算m (local method)

%另外取包封的方法也分為hilbert和quadrature
clc
tic
clear QUS
QUS(400, 3) = struct();
for r_iter = 1:r_num
    for d_iter = 1:d_num
        file = fileset((r_iter-1)*d_num*pos_num_per_day+(d_iter-1)*pos_num_per_day+1:(r_iter-1)*d_num*pos_num_per_day+d_iter*pos_num_per_day, :);
        fileName = strtrim(file(1, :));
        firstParam = headFile_NI(fileName);
        us_rf = firstParam.rf;
        for f = 1:size(file, 1)
            fileName = strtrim(file(f, :));
            batchSize = length(dir([fileName(1:end-5) '*.dat']));
            if f==1
                for b = 2:batchSize
                    param = headFile_NI([fileName(1:end-5) num2str(b) '.dat']);
                    us_rf(:, :, end+1:end+size(param.rf, 3)) = param.rf;
                end
            else
                for b = 1:batchSize
                    param = headFile_NI([fileName(1:end-5) num2str(b) '.dat']);
                    us_rf(:, :, end+1:end+size(param.rf, 3)) = param.rf;
                end
            end
        end
        hilbert_env = abs(hilbert(us_rf));
%         us_image = 20*log10(hilbert_env./max(hilbert_env, [], [1 2]))+dynamic_range;
%         us_image = us_image/dynamic_range*255;
        demod_env = zeros(size(us_rf));
        for i=1:size(us_rf, 3)
%         for i=10:size(us_rf, 3)
            [I, Q] = demod(us_rf(:, :, i), 30*1e6, 1e9,'qam');
            demod_env(:, :, i) = abs(complex(I, Q));
            
            temp_img = zeros(size(us_rf(:, :, i)));
            x_axis = (1:param.Aline)*param.XInterval/1000;
            y_axis = ((1:param.DataLength)+param.Delay)/2/(param.SamplingRate*1e6)*1540*1000;
            image(x_axis, y_axis, temp_img)
%             ROI_obj = drawrectangle('Position', roi_position(i, d_iter, r_iter).roi_pos, 'RotationAngle', roi_position(i, d_iter, r_iter).roi_degree, 'Color', 'y', 'InteractionsAllowed', 'translate', 'Rotatable', true);
            ROI_obj = drawrectangle('Position', roi_position(i, d_iter+1, r_iter).roi_pos, 'RotationAngle', roi_position(i, d_iter+1, r_iter).roi_degree, 'Color', 'y', 'InteractionsAllowed', 'translate', 'Rotatable', true);
            temp_BW = createMask(ROI_obj);
            temp_BW_index = find(temp_BW);
            temp_hilbert_env = hilbert_env(:, :, i).*temp_BW;
            temp_demod_env = demod_env(:, :, i).*temp_BW;
            temp_rf = us_rf(:, :, i);
            
            hilbert_env_roi = zeros(size(temp_BW));
            demod_env_roi = zeros(size(temp_BW));
            
            for c_iter = 1:size(temp_rf, 2)
                temp = temp_BW(:, c_iter);
                if sum(temp)
                    hilbert_env_roi(temp, c_iter) = abs(hilbert(temp_rf(temp, c_iter)));
                    [I, Q] = demod(temp_rf(temp, c_iter), 30*1e6, 1e9,'qam');
                    demod_env_roi(temp, c_iter) = abs(complex(I, Q));
                end
            end
            
            QUS(i, d_iter).hilbert_error = mean(abs((temp_hilbert_env-hilbert_env_roi)./temp_hilbert_env/sum(temp_BW, 'all')), 'all', 'omitnan');
            QUS(i, d_iter).demod_error = mean(abs((temp_demod_env-demod_env_roi)./temp_demod_env/sum(temp_BW, 'all')), 'all', 'omitnan');
            
            temp_hilbert_env = temp_hilbert_env/max(temp_hilbert_env, [], 'all')*255;
            temp_demod_env = temp_demod_env/max(temp_demod_env, [], 'all')*255;
            hilbert_env_roi = hilbert_env_roi/max(hilbert_env_roi, [], 'all')*255;
            demod_env_roi = demod_env_roi/max(demod_env_roi, [], 'all')*255;
            
            nakagami_param = mle(temp_hilbert_env(temp_BW_index), 'distribution', 'nakagami');
            QUS(i, d_iter).nakagami_hilbert_all = nakagami_param(1);
            nakagami_param = mle(temp_demod_env(temp_BW_index), 'distribution', 'nakagami');
            QUS(i, d_iter).nakagami_demod_all = nakagami_param(1);
            nakagami_param = mle(hilbert_env_roi(temp_BW_index), 'distribution', 'nakagami');
            QUS(i, d_iter).nakagami_hilbert_roi = nakagami_param(1);
            nakagami_param = mle(demod_env_roi(temp_BW_index), 'distribution', 'nakagami');
            QUS(i, d_iter).nakagami_demod_roi = nakagami_param(1);
            
%             
        end
    end
end
clear ROI_obj temp_img temp_BW temp_BW_index temp_rf roi_rf
toc

for d_iter=1:d_num
    for i=1:400
        hilbert_signal_error(i, d_iter) = QUS(i, d_iter).hilbert_error;
        demod_signal_error(i, d_iter) = QUS(i, d_iter).demod_error;
        hilbert_nakagami_error(i, d_iter) = (QUS(i, d_iter).nakagami_hilbert_all - QUS(i, d_iter).nakagami_hilbert_roi)/QUS(i, d_iter).nakagami_hilbert_roi;
        demod_nakagami_error(i, d_iter) = (QUS(i, d_iter).nakagami_demod_all - QUS(i, d_iter).nakagami_demod_roi)/QUS(i, d_iter).nakagami_demod_roi;
    end
end
mean(hilbert_signal_error, 'all')*100
mean(demod_signal_error, 'all')*100
mean(abs(hilbert_nakagami_error), 'all')*100
mean(abs(demod_nakagami_error), 'all')*100
%% 計算wmc nakagami和一般roi nakagami的error

n_i = [14 15 16; 
    64 65 66;
    114 115 116;
    164 165 166;
    214 215 216;
    264 265 266;
    314 315 316;
    364 365 366];
n_i = n_i';
for i=1:8
    for j=1:3
        temp = nimgs(:, :, j, i);
        x_axis = (1:param.Aline)*param.XInterval/1000;
        y_axis = ((1:param.DataLength)+param.Delay)/2/(param.SamplingRate*1e6)*1540*1000;
        image(x_axis, y_axis, zeros(size(temp)))
%             ROI_obj = drawrectangle('Position', roi_position(i, d_iter, r_iter).roi_pos, 'RotationAngle', roi_position(i, d_iter, r_iter).roi_degree, 'Color', 'y', 'InteractionsAllowed', 'translate', 'Rotatable', true);
        ROI_obj = drawrectangle('Position', roi_position(n_i(j, i), 2, 1).roi_pos, 'RotationAngle', roi_position(n_i(j, i), 2, 1).roi_degree, 'Color', 'y', 'InteractionsAllowed', 'translate', 'Rotatable', true);
        temp_BW = createMask(ROI_obj);
%         wmcn_global_error(j, i) = (QUS(n_i(j, i), 1).nakagami_demod_roi-mean(temp(temp_BW), 'all'))/QUS(n_i(j, i), 1).nakagami_demod_roi;
        wmcn_local_error(j, i) = (QUS(n_i(j, i), 1).nakagami_demod_roi-mean(temp(temp_BW), 'all'))/QUS(n_i(j, i), 1).nakagami_demod_roi;
    end
end
%% deep learning for segmentation

close all
validation_ratio = 0.2;
imageDir = 'D:\daming\skinflap\skinflap in wang lab\rat#1\day3\traindata_128';
labelDir = 'D:\daming\skinflap\skinflap in wang lab\rat#1\day3\labeldata_128';
imds = imageDatastore(imageDir);
pxds = imageDatastore(labelDir);

% imds = imageDatastore('D:\exosome_segmentation\data_v3\augmentationdata', ...
%     'IncludeSubfolders',true,'LabelSource','foldernames');

classNames = ["flap","background"];
labelIDs   = [255 0];
train_ds = imds;
% val_ds = imageDatastore('D:\exosome_segmentation\data_v3\testdata', ...
%     'IncludeSubfolders',true,'LabelSource','foldernames');
% [train_ds, val_ds] = splitEachLabel(imds, 0.8);
[train_index, val_index, ~] = dividerand(size(imds.Files, 1), 1-validation_ratio, validation_ratio, 0);
train_imds = imageDatastore(imds.Files(train_index));
val_imds = imageDatastore(imds.Files(val_index));
train_pxds = pixelLabelDatastore(pxds.Files(train_index), classNames,labelIDs);
val_pxds = pixelLabelDatastore(pxds.Files(val_index), classNames,labelIDs);

imageSize = [256 256];
numClasses = 2;
lgraph = unetLayers(imageSize, numClasses, 'EncoderDepth',4);
% lgraph = vgg16('Weights','imagenet');
% lgraph = layerGraph(lgraph.Layers);

% lgraph = removeLayers(lgraph,'input');
% new_layer = imageInputLayer([256 256 3], 'Name', 'input');
% lgraph = addLayers(lgraph, [new_layer]);
% lgraph = connectLayers(lgraph, 'input', 'conv1_1');

% lgraph = removeLayers(lgraph,'conv1_1');
% new_layer = convolution2dLayer([3 3], 64, 'NumChannels', 'auto', 'Name', 'conv1_1');
% lgraph = addLayers(lgraph, [new_layer]);
% lgraph = connectLayers(lgraph, 'input', 'conv1_1');
% lgraph = connectLayers(lgraph, 'conv1_1', 'relu1_1');

% lgraph = removeLayers(lgraph,'fc6');
% new_layer = fullyConnectedLayer(4096,'Name','fc6');
% lgraph = addLayers(lgraph, [new_layer]);
% lgraph = connectLayers(lgraph, 'pool5', 'fc6');
% lgraph = connectLayers(lgraph, 'fc6', 'relu6');
% 
% lgraph = removeLayers(lgraph,'fc8');
% new_layer = fullyConnectedLayer(2,'Name','fc8');
% lgraph = addLayers(lgraph, [new_layer]);
% lgraph = connectLayers(lgraph, 'drop7', 'fc8');
% lgraph = connectLayers(lgraph, 'fc8', 'prob');
% 
% lgraph = removeLayers(lgraph,'output');
% new_layer = classificationLayer('Name', 'output', 'Classes', {'abnormal', 'normal'});
% lgraph = addLayers(lgraph, [new_layer]);
% lgraph = connectLayers(lgraph, 'prob', 'output');


% layers = [ ...
%     imageInputLayer([128 128 1], 'Name', 'input_image')
%     convolution2dLayer(3,64, 'Name', 'conv1', 'Padding', 'same')
%     reluLayer('Name', 'relu1')
%     dropoutLayer('Name', 'dropout1')
%     convolution2dLayer(3,64, 'Name', 'conv2_1', 'Padding', 'same')
%     reluLayer('Name', 'relu2_1')
%     convolution2dLayer(3,64, 'Name', 'conv2_2', 'Padding', 'same')
%     reluLayer('Name', 'relu2_2')
%     dropoutLayer('Name', 'dropout2')
%     convolution2dLayer(3,64, 'Name', 'conv3', 'Padding', 'same')
%     reluLayer('Name', 'relu3')
%     convolution2dLayer(3,2, 'Name', 'convfinal', 'Padding', 'same')
%     softmaxLayer('Name', 'Softmax-Layer')
%     pixelClassificationLayer('Name', 'Segmentation-Layer')];
% lgraph = layerGraph(layers);
lgraph = removeLayers(lgraph,'Segmentation-Layer');
% tb = countEachLabel(train_ds);
new_seg_layer = dicePixelClassificationLayer('Classes', {'flap', 'background'},'Name','Segmentation-Layer')
% new_seg_layer = pixelClassificationLayer('Classes', {'flap', 'background'},'ClassWeights',[tb.ImagePixelCount(1)/tb.PixelCount(1) tb.ImagePixelCount(1)/tb.PixelCount(2)],'Name','Segmentation-Layer'); 
lgraph = addLayers(lgraph, [new_seg_layer]);
lgraph = connectLayers(lgraph, 'Softmax-Layer', 'Segmentation-Layer');

train_ds = pixelLabelImageDatastore(train_imds,train_pxds);
val_ds = pixelLabelImageDatastore(val_imds,val_pxds);
options = trainingOptions('adam', ...
    'InitialLearnRate',1e-4, ...
    'MaxEpochs',100, ...
    'VerboseFrequency',5,...
    'MiniBatchSize', 32,...
    'ValidationData', val_ds,...
    'plot', 'training-progress');


[net_2, train_result] = trainNetwork(train_ds,lgraph_2,options);

% pxdsTruth = val_pxds;
% pxdsResults = semanticseg(val_imds,net);
% metrics = evaluateSemanticSegmentation(pxdsResults,pxdsTruth);
%% transfer learning pre-processing
% function [dataout] = random_affine_transform(datain, info) if ~isfield(info, 
% 'times') info.times = 1; end rng for i=1:info.times end

transfer_layer = lgraph;
for i=1:56
    transfer_layer = replaceLayer(transfer_layer, transfer_layer.Layers(i).Name, net.Layers(i));
end
imageAugmenter = imageDataAugmenter( ...
    'RandXReflection',true, ...
    'RandXTranslation',[-6 6], ...
    'RandYTranslation',[-6 6]);
train_ds = pixelLabelImageDatastore(train_imds,train_pxds,'DataAugmentation', imageAugmenter);
val_ds = pixelLabelImageDatastore(train_imds,train_pxds,'DataAugmentation', imageAugmenter);
options = trainingOptions('sgdm', ...
    'InitialLearnRate',1e-5, ...
    'MaxEpochs',50, ...
    'VerboseFrequency',10,...
    'MiniBatchSize', 32,...
    'Shuffle', 'every-epoch',...
    'ValidationData', val_ds,...
    'plot', 'training-progress');
net = trainNetwork(train_ds,transfer_layer,options);
% delete(findall(0));

%% *用於計算之前在clean區掃的skinflap厚度(用NN圈ROI)*

fileset = char('D:\daming\skinflap\skinflap in wang lab\rat#4\day3\30MHz_9kto16k_0to5mm_C1.dat',...
    'D:\daming\skinflap\skinflap in wang lab\rat#4\day3\30MHz_9kto16k_5to10mm_C1.dat',...
    'D:\daming\skinflap\skinflap in wang lab\rat#4\day3\30MHz_9kto16k_10to15mm_C1.dat',...
    'D:\daming\skinflap\skinflap in wang lab\rat#4\day3\30MHz_9kto16k_15to20mm_C1.dat',...
    'D:\daming\skinflap\skinflap in wang lab\rat#4\day3\30MHz_9kto16k_20to25mm_C1.dat',...
    'D:\daming\skinflap\skinflap in wang lab\rat#4\day3\30MHz_9kto16k_25to30mm_C1.dat',...
    'D:\daming\skinflap\skinflap in wang lab\rat#4\day3\30MHz_9kto16k_30to35mm_C1.dat',...
    'D:\daming\skinflap\skinflap in wang lab\rat#4\day3\30MHz_9kto16k_35to40mm_C1.dat');
file_num_per_dir = 8;           %
image_num_per_file = 50;        %同檔名所有.dat檔的影像數目(一個C或B scan的掃的檔案數)
image_z_range = [740:5935];     %影像截頭去尾後保留的部分
resized_img_size = [256 256];   %輸出train image的大小
skin_thickness = zeros(image_num_per_file, size(fileset, 1));
gcp;
tic
parfor fn=1:size(fileset, 1)
    [us_image, ~, ~, x_axis, y_axis,] = get_us_data(fileset(fn, :), struct('is_save', 1, 'dynamic_range',42));
    
    img_index = 1;
    [dir_path, ~, ~]= fileparts(fileset(fn, :));
    if exist([dir_path '\traindata_256_withoutaugment'], 'dir')==0
        mkdir([dir_path '\traindata_256_withoutaugment']);
    end
    if exist([dir_path '\segment_by_AI_256_withoutaugment'], 'dir')==0
        mkdir([dir_path '\segment_by_AI_256_withoutaugment']);
    end
    for i = 1:image_num_per_file
        temp_img = imresize(uint8(us_image(image_z_range, :, i)), resized_img_size, 'method', 'bilinear');
        train_img_index = (mod(fn-1, file_num_per_dir))*image_num_per_file+i;
        BW = semanticseg(temp_img, dice_loss_net);
        B = labeloverlay(temp_img, BW);
        
        %儲存overlay的影像
        imshow(B)
        imwrite(B, gray(256), [dir_path '\segment_by_AI_256_withoutaugment\' num2str(train_img_index) '.png'], 'PNG');
        imwrite(temp_img, [dir_path '\traindata_256_withoutaugment\' num2str(train_img_index) '.png'], 'PNG');
        
%         %計算skinflap厚度
%         BW = imresize((BW=='flap'), [size(us_image, 1) size(us_image, 2)], 'method', 'bilinear');
%         [outlier_value, ~] = rmoutliers(sum(BW));
%         skin_thickness(i, fn) = mean(outlier_value);
%         close(gcf);
    end
end
toc
% delete(gcp('nocreate'))

% for i=1:5   %section
%     for j=1:5   %day
%         for k=1:6   %rat
%             temp = skin_thickness(:, i, j, k);
%             avg_thickness(i, j, k) = mean(rmoutliers(temp));
%         end
%     end
% end

%% *augment image and output*

% augmenter = imageDataAugmenter( ...
%     'RandXReflection', true,...
%     'RandRotation',[-30 30],...
%     'RandYScale',[0.8 2],...
%     'RandYTranslation', [-20 20]);
imageDir = 'D:\exosome_segmentation\data_v3\traindata\normal';
target_dir = 'D:\exosome_segmentation\data_v3\augmentationdata\normal';
% labelDir = 'D:\exosome_segmentation\data_v2\normal';
aug_times = 5;
imds = imageDatastore(imageDir);
file_num = size(imds.Files, 1);
% pxds = imageDatastore(labelDir);

for t = 1:aug_times
    parfor i=1:file_num
        I = readimage(imds, i);
%         C = readimage(pxds, i);
        [~, filename, ~] = fileparts(imds.Files{i});
        img_index = (t-1)*file_num+i;
        tform = randomAffine2d("Rotation",[-15 15],...
            'XReflection', true,...
            'YTranslation', [-20 20]);
        rout = affineOutputView(size(I),tform);
        augment_I = imwarp(I,tform,'OutputView',rout);
        augment_I = imresize(augment_I, [224 224], 'method', 'bilinear');
%         augment_C = imwarp(C,tform,'OutputView',rout);
        imwrite(augment_I, [target_dir '\' num2str(img_index) '.png'], 'PNG');
%         imwrite(augment_C, [labelDir '\' num2str(img_index) '.png'], 'PNG');
    end
end

% imshow(labeloverlay(augment_I,augment_C))
% reset(imds);
% reset(pxds);

%% 輸出指定某層的feature maps
% close all
layer = 'relu3_3';
imageDir = 'D:\exosome_segmentation\data_v3\augmentationdata_with_bright\normal';
imds = imageDatastore(imageDir);
[~, name, ~] = fileparts(imds.Files);
name = cellfun(@(x) {str2num(x)}, name);
[~, order] = sort(cell2mat(name));
imds.Files = imds.Files(order);
% selected_image = [1:size(imds.Files, 1)];
selected_image = [183 230];
r=8; c=8;
imds.Files = imds.Files(selected_image);
feature_map = activations(net, imds, layer);
feature_num = size(feature_map, 3);
size(feature_map)

rng;
map_num = randperm(feature_num, r*c);

for k=1:size(imds.Files, 1)
    figure('position', [100 50 900 900])
    p = panel();
    p.margin = [1, 1, 1, 1];
    r=8;
    c=8;
    p.pack(r, c);
    for i=1:r
        for j=1:c
            p(i, j).select();
            imshow(feature_map(:, :, map_num((i-1)*c+j), k))
%             imshow(feature_map(:, :, ((i-1)*c+j)*feature_num/r/c, k))
        end
    end
end

% figure('position', [100 50 2400 900])
% p = panel();
% p.margin = [8, 8, 8, 8];
% p.pack(r, c);
%     for i=1:r
%         for j=1:c
%             p(i, j).select();
%             plot(squeeze(feature_map(1, 1, :, (i-1)*c+j)))
%         end
%     end

%% 利用現有的net分類定檔案路徑下的圖片
imageDir = 'D:\exosome_segmentation\data_v4\augmentation\abnormal';
imds = imageDatastore(imageDir);
[~, name, ~] = fileparts(imds.Files);
name = cellfun(@(x) {str2num(x)}, name);
name = cell2mat(name);
[~, order] = sort(name);
imds.Files = imds.Files(order);
selected_image = [1:size(imds.Files, 1)];
imds.Files = imds.Files(selected_image);

YPred = classify(net_100,imds);

%%
file_set = char('');

us_image = get_us_data(param.file_path, 'is_save', 0, 'dynamic_range', 45);
us_image = uint8(us_image);
%     file_dir = fileparts(param.file_path);
file_num = size(us_image, 3);

if isnan(target_dir)
    param.target_dir = fileparts(target_dir);
    output_image_index = 1;
else
    output_image_index = size(dir(param.target_dir),1);
end

if(exist(param.target_dir))
    rmdir(param.target_dir);
end
mkdir(param.target_dir);

for i=1:file_num
    I = us_image(:, :, i);
    if ~isnan(param.crop_region)
        I = I(param.crop_region(1):param.crop_region(2), :);
    end
    output_image(:, :, i) = imresize(I, param.target_size, 'method', 'bilinear');
    imwrite(output_image(:, :, i), [param.target_dir '\' num2str(output_image_index) '.png'], 'PNG');
    output_image_index = output_image_index+1;
end

%% 重畫圖3D B-mode image
% 讀取I:\黃大銘實驗資料\skinflap\skin_roi_mask_by_acm_2.mat (G6-no1)
% front_mask = skin_parameter(x).front_mask;      x:想要重劃的天數,以第0天為例x=1
% 用 "%% 參數設定" & "%% 計算皮膚厚度、高度與3D建模參數(surface)" 的code讀取對應該x的us_image
% 執行 reconstruct_3d_image.m的 "標頭" "%% 影像smoothing" "%% 計算厚度" 
% 執行 reconstruct_3d_image.m的 "%% 根據表皮位置(avg_skin_thickness)校正影像" "%% Create an isosurface"
% 執行 "%% 呈現各種切面的超音波影像 #2" 下面那段code

%% 利用python predict得到的mask.png跟B-mode影像作fusion image
img_set = char('D:\daming\skinflap\skinflap in wang lab\rat#11_PBNO_withNIR\day7\30MHz_9kto16k_0to5mm_C1.dat',...
    'D:\daming\skinflap\skinflap in wang lab\rat#11_PBNO_withNIR\day7\30MHz_9kto16k_5to10mm_C1.dat',...
    'D:\daming\skinflap\skinflap in wang lab\rat#11_PBNO_withNIR\day7\30MHz_9kto16k_10to15mm_C1.dat',...
    'D:\daming\skinflap\skinflap in wang lab\rat#11_PBNO_withNIR\day7\30MHz_9kto16k_15to20mm_C1.dat',...
    'D:\daming\skinflap\skinflap in wang lab\rat#11_PBNO_withNIR\day7\30MHz_9kto16k_20to25mm_C1.dat',...
    'D:\daming\skinflap\skinflap in wang lab\rat#11_PBNO_withNIR\day7\30MHz_9kto16k_25to30mm_C1.dat',...
    'D:\daming\skinflap\skinflap in wang lab\rat#11_PBNO_withNIR\day7\30MHz_9kto16k_30to35mm_C1.dat',...
    'D:\daming\skinflap\skinflap in wang lab\rat#11_PBNO_withNIR\day7\30MHz_9kto16k_35to40mm_C1.dat');
% imds1 = imageDatastore('D:\daming\skinflap\skinflap in wang lab\rat#13_PBNO_withoutNIR\day7\us_image');
mask_dir = 'C:\Users\sgogo\Desktop\data\mask\11\day7';
for i=1:size(img_set, 1)
    us_image = get_us_data(img_set(i, :), 0, 'dynamic_range', 45);
    for f = 1:size(us_image, 3)
        frame_index = (i-1)*50+f;
        mask_image = imread([mask_dir '\' num2str(frame_index) '.png']);
        mask_image = imresize(mask_image(:, :, 1), [7000 200], 'method', 'bilinear');
        mask_image(mask_image<128) = 0;
        mask_image(mask_image>=128) = 255;
        mask_image = int16(mask_image);
        temp = mask_image(1:end-2, 1:end-2)+mask_image(1:end-2, 2:end-1)+mask_image(1:end-2, 3:end)+...
                mask_image(2:end-1, 1:end-2)+mask_image(2:end-1, 2:end-1)+mask_image(2:end-1, 3:end)+...
                mask_image(3:end, 1:end-2)+mask_image(3:end, 2:end-1)+mask_image(3:end, 3:end);
        temp(temp<255*9) = 0;
        temp(temp>=255*9) = 255;
        temp = [zeros(1, 200); [zeros(6998, 1) temp zeros(6998, 1); zeros(1, 200)]];
        border = mask_image-temp;
        BW = imdilate(border, strel('rectangle',[50, 2]));
        
        figure('color', 'white')
        img = us_image(:, :, f);
        img(BW==255) = 256;
        cm = gray(256);
        cm(257, :) = [1, 0, 0];
        image(x_axis, y_axis, img+1)
        colormap(cm)
        
        set(gca, 'FontSize', 16, 'FontName', 'Times', 'FontWeight', 'bold', 'XTick', round(x_axis(1)):1:x_axis(end), 'YTick', round(y_axis(1)):1:y_axis(end))
        xlabel('Distance (mm)', 'FontSize', 20, 'FontName', 'Times', 'FontWeight', 'bold')
        ylabel('Depth (mm)', 'FontSize', 20, 'FontName', 'Times', 'FontWeight', 'bold');
        axis equal tight
        [X,Map] = rgb2ind(frame2im(getframe(gcf)), 256);
        savePath = [mask_dir '\overlay\' num2str(frame_index) '.png'];
        imwrite(X, Map, savePath, 'PNG');
        close all
    end
end