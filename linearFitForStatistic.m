function [slope_50hz] = linearFitForStatistic(filePath, fileName, fileRange, dist)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
    xlsFile = [filePath '/' fileName '.xlsx'];
    fileSize = fileRange(2)-fileRange(1)+1;
    [number, ~, ~] = xlsread(xlsFile, 'phaseAngle');
    xaxis = (0:0.5:9.5)';
    slopeNum = size(dist, 2);
    slope = zeros(slopeNum, fileSize*6-1);
    slope_50hz = zeros(1, fileSize);
    for i=1:fileSize
        for j=1:4
            yaxis = number(22:41, (i-1)*6+j+1);
            for k=1:slopeNum
                slope(k, (i-1)*6+1) = (dist(k)-1)/2;
                slope(k, (i-1)*6+j+1) = xaxis(1:dist(k))\yaxis(1:dist(k));
            end
        end
        slope_50hz(i) = slope(1, (i-1)*6+2);
    end
    %   為了方便NCC統計用code
    
    
%     xlsData(6:6+fileSize-1) = num2cell(slope_50hz);
%     [tempStart tempEnd] = regexp(fileName,'\(\d+\)');
%     myXlswrite([filePath '/variousRangedDepth.xlsx' ], xlsData, 'velocity', ['B' fileName(tempStart+1:tempEnd-1)]);

    myXlswrite(xlsFile, slope, 'phaseAngle', 'A44');
%     velocity_50hz = num2cell(velocity_50hz);
end

