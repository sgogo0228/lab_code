%% wavelet transform
clc
close all

% load svm.mat 
% for r=5:5
    timeAxis = (0:(modelParams.pushNum*modelParams.unitAline-1))/detectSamplingRate;
    % 去除push造成的雜訊
    for i=pushNum-1:-1:0
        shift = modelParams.unitAline*i;
        sgn(:, :, :, (1+modelParams.unitAline-modelParams.damping)+shift:modelParams.unitAline+shift) = [];
        sgn(:, :, :, 1+shift:(modelParams.frontNoise+modelParams.backNoise)+shift) = [];
        timeAxis(1+(modelParams.unitAline-modelParams.backNoise-modelParams.damping)+shift:modelParams.unitAline+shift) = [];
        timeAxis(1+shift:(modelParams.frontNoise)+shift) = [];
    end
    filteredDemodSignal = sgn;
    timeAxis = timeAxis(1:modifiedUnitAline*3);

    % 去除低頻雜訊(動物呼吸、環境干擾等)
    [paramB, paramA] = butter(5, bandFilter/(detectSamplingRate/2));
    tempSgn = permute(sgn, [4 3 2 1]);
    tempSgn = filtfilt(paramB, paramA, tempSgn);
    sgn = permute(tempSgn, [4 3 2 1]);

    % 去除前後較不穩定的訊號
    sgn(:, :, :, 1:(unusedCycle-1)*modifiedUnitAline) = [];
    sgn(:, :, :, end-modifiedUnitAline+1:end) = [];

    % 中間較穩定的訊號做平均作為kalman filter輸出
    for f=1:fnum
        for i=1:distNum
            for r=1:analysisDistRange(2)-analysisDistRange(1)+1
                tempSgn = mean(reshape(sgn(f, i, r, :), modifiedUnitAline, modelParams.usedCycle), 2)';
                for j=0:modelParams.usedCycle-1
                    sgn(f, i, r, (1+j*modifiedUnitAline):((j+1)*modifiedUnitAline)) = tempSgn;
                end
            end
        end
    end
    sgn(:, :, :, modifiedUnitAline*modelParams.usedCycle+1:end) = [];
    s = sgn;

%% feature extration
clear c l
waveletCompNum =5;
for f=1:fnum
    for dist=1:distNum
        for depth = 1:analysisDistRange(2)-analysisDistRange(1)+1
            [c(f, dist, depth, :), l(f, dist, depth, :)] = wavedec(squeeze(filteredDemodSignal(f, dist, depth, :)), waveletCompNum, 'db10');
        end
    end
end

waveStart = zeros(waveletCompNum+2, 1);
waveStart(1) = 1;
for i=2:waveletCompNum+2
    waveStart(i) = l(1, 1, 1, i-1)+waveStart(i-1);
end
waveMean = zeros(fnum, distNum, analysisDistRange(2)-analysisDistRange(1)+1, waveletCompNum+1);
waveSD = zeros(fnum, distNum, analysisDistRange(2)-analysisDistRange(1)+1, waveletCompNum+1);
waveEngery = zeros(fnum, distNum, analysisDistRange(2)-analysisDistRange(1)+1, waveletCompNum+1);
for i=1:waveletCompNum+1
    waveMean(:, :, :, i) = mean(c(:, :, :, waveStart(i):waveStart(i+1)-1), 4);
    waveSD(:, :, :, i) = std(c(:, :, :, waveStart(i):waveStart(i+1)-1), 0, 4);
%     waveEngery(:, :, :, i) = mean(c(:, :, :, waveStart(i):waveStart(i+1)-1).^2, 4);
end

paramNum = 720;
param = zeros(fnum, paramNum);
for n = 1:fnum
    i = 1;
    for dist = 1:10
        for depth = 1:6
            for wlcomp = 1:6
                param(n, i) = waveSD(n, dist, depth, wlcomp);
                param(n, i+1) = waveMean(n, dist, depth, wlcomp);
                i = i+2;
%                 i = i+1;
            end
        end
    end
end
T = array2table(param);

groupNum = 6;
groupFileNum = 36;
for g = 1:groupNum
    for n = 1:groupFileNum
        if n<=3
            T2.param721(n+(g-1)*groupFileNum, :) = 'ctrl';
        elseif n>=4 && n<=12
            T2.param721(n+(g-1)*groupFileNum, :) = 'dstr';
        elseif n>=13 && n<=27
            T2.param721(n+(g-1)*groupFileNum, :) = 'repr';
        else
            T2.param721(n+(g-1)*groupFileNum, :) = 'rmdl';
        end
    end
end