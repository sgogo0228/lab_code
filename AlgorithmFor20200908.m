clear
clc
% close all
%%
% 能夠一次分析好幾組shear wave
% file = char('K:\黃大銘資料夾20180420前\school\shearWaveData\20160604_shearWave\gelatin3_pos1_dist1_1_SW0');
file = char('L:\G8\20220113_ratskin_d7\shearwave\avtech_30MHz_dist0_depth9to17k_amp600_cycle5100_1_SW0');
params = headFile_NI([strtrim(file(1, :)) '.dat']);
push_num = 5;                           %在固定的蒐集時間內(params.aline)，push打了幾次，跟Push的PRF有關
position_num = 10;                      %蒐集多少個position的signal
push_noise = 15;                          %push干擾detect的點數
baseFrequency = 50;
% hormonicNum = 5;                      %kalman filter故估計頻率的上限(如果push的PRF是50Hz，那就會最高抓到50*此變數)
% slopeFitDotNum = [6 8];               %最後計算斜率時參考的點數
unitAline = params.Aline/push_num;      %push打一次所需要的時間
depth = [3001 7000];                    %可設定想分析的深度範圍
detect_sampling_rate=10000;   %千萬不要打錯!! 是指detect探頭的PRF
modelParams = struct('unitAline', unitAline, 'push_num', push_num, 'usedCycle', 3, 'frontNoise', push_noise-3, 'backNoise', 3, 'damping', 0);
analyzeInterval = 250;                  % 經驗上分析肌肉的SW 100或200(以上也可以500)都可以，太少變異大

overall_depth = depth(2)-depth(1)+1;
analysis_region = [1 overall_depth/analyzeInterval];   %可能不會想整個深度都分析，可自設區間
fnum = size(file, 1);
bandFilter = [baseFrequency*0.7 baseFrequency*6];
modified_unit_aline = (modelParams.unitAline-modelParams.frontNoise-modelParams.backNoise-modelParams.damping);
unusedCycle=modelParams.push_num-modelParams.usedCycle;

for i=1:overall_depth/analyzeInterval
    range(i, 1) = (i-1)*analyzeInterval+1;
    range(i, 2) = i*analyzeInterval;
end
% xcorrDepth = 3;

for f = 1:fnum
    filename = [strtrim(file(f, :)) '.dat'];
    if ~exist(filename, 'file')
        f
        disp(filename);
        return;
    end
end
tic
% gcp;
avg_sgn = zeros(modified_unit_aline*push_num, analysis_region(2)-analysis_region(1)+1, position_num, fnum);
for f=1:fnum
    temp_sgn = zeros(modified_unit_aline*push_num, overall_depth, position_num);
    for j=0:position_num-1
%         waitbar(j/position_num, h, ['(' num2str(j) '/' num2str(position_num) ')']);
        filename = regexprep(strtrim(file(f, :)), '_SW0', ['_SW' num2str(j) '.dat']);
        [sgnParam, temp_sgn(:, :, j+1)]=shearWaveByDemod20200908(filename, depth, unitAline, push_num, push_noise);
    end
    [paramB, paramA] = butter(3, 300/(detect_sampling_rate/2));    %第一個參數須視情況調整
    temp_sgn = filtfilt(paramB, paramA, temp_sgn);
    for r = 1:analysis_region(2)-analysis_region(1)+1
        avg_sgn(:, r, :, f) = squeeze(mean(temp_sgn(:, range(r, 1):range(r, 2), :), 2));
    end
end
% delete(gcp('nocreate'));
temp = avg_sgn;
avg_sgn = temp;
toc

% 將第2到第4個push收到的SW，第1跟第5個因為較不穩定不列入分析
avg_sgn = reshape(avg_sgn, modified_unit_aline, push_num, analysis_region(2)-analysis_region(1)+1, position_num, fnum);
avg_sgn = squeeze(mean(avg_sgn(:, 2:4, :, :), 2));
a = xyx;

%% 輸出SW image
figure('color', 'white')
graphInterval = 2;
colorScale = [0 1];

% 輸出2D的SW圖
for f=1:fnum
    imgSgn(:, :, :, f) = avg_sgn(:, :, :, f)-min(avg_sgn(:, :, :, f), [], 'all');
    [filepath, filename, ~] = fileparts(file(f, :));
    savePath = [filepath '\gif_' strtrim(filename) '.gif'];
    i=1;
%     imagesc(squeeze((sgn(f, :, :, i)))');
    param = headFile_NI([strtrim(file(f, :)) '.dat']);
    xAxis = (0:position_num-1)*0.5;
    yAxis = (((1:param.DataLength)+param.Delay)/1000000000/2*1540*1000);
    imagesc(xAxis, yAxis, squeeze((imgSgn(i, :, :, f))));
    caxis(colorScale)
    title(num2str(i))
    set(gca, 'FontSize', 24, 'FontName', 'Times', 'FontWeight', 'bold')
    xlabel('Distance (mm)', 'FontSize', 28, 'FontWeight', 'bold')
    ylabel('Depth (mm)', 'FontSize', 28, 'FontWeight', 'bold')
%     axis image
    [X,Map] = rgb2ind(frame2im(getframe(1)), 256);
    if exist(savePath, 'file')==2
        delete(savePath);
    end
    imwrite(X, Map, savePath, 'GIF', 'WriteMode', 'overwrite', 'DelayTime', 0, 'LoopCount', Inf);
    i = graphInterval+1;
    while i<=modified_unit_aline                % 任何一個 loop 
%         imagesc(squeeze((sgn(f, :, :, i)))');
        imagesc(xAxis, yAxis, squeeze((imgSgn(i, :, :, f))));
        caxis(colorScale)
        colorbar
        title([num2str(i*0.1) 'ms'])
        set(gca, 'FontSize', 24, 'FontName', 'Times', 'FontWeight', 'bold')
        xlabel('Distance (mm)', 'FontSize', 28, 'FontWeight', 'bold')
        ylabel('Depth (mm)', 'FontSize', 28, 'FontWeight', 'bold')
%         axis image
        [X,Map] = rgb2ind(frame2im(getframe(1)), 256);
        %在 loop內 的writemode 須改為 append ,這時候frame才會不斷的增加進去
        imwrite(X, Map, savePath,'GIF','WriteMode','append','DelayTime', 0);
        i = i+graphInterval;
    end
end

%%   CC
% for r=1:analysisDistRange(2)-analysisDistRange(1)+1
%     maxCCindex = zeros(fnum, distNum);
%     phaseAngle = zeros(fnum, 20, hormonicNum);
%     maxCCindex(:, 1) = size(sgn, 4);
%     for f=1:fnum
%         xcr = zeros(distNum, 2*size(sgn, 4)-1);
%         for d=2:distNum
%             xcr(d, :) = xcorr(squeeze(sgn(f, 1, r, :)), squeeze(sgn(f, d, r, :)));
%             [~, I] = findpeaks(squeeze(xcr(d, :)));
%             [~, tempIdx] = min(abs(I-maxCCindex(f, d-1)));
%             maxCCindex(f, d) = I(tempIdx);
%         end
%     end
%     maxCCindex = maxCCindex-size(sgn, 4);
%     phaseAngle(:, 1:distNum, 1) = maxCCindex*2*pi/(detectSamplingRate/baseFrequency); %2*pi/(detectSamplingRate/baseFrequency)用於計算一個detect
%     phaseAngle = permute(phaseAngle, [2 3 1]);
% 
%     phase_50hz(r, :, :) = linearFitForStatistic20181210(analyzeFilePath, ['0_analyze_20170403_sample (' num2str(r) ')'], [1 fnum], phaseAngle, slopeFitDotNum);
% end
% 
% tempPhase = [];
% for i=1:length(slopeFitDotNum)
%     tempPhase = [tempPhase; squeeze(phase_50hz(:, i, :))];
% end
% phase_50hz = tempPhase;
% myXlswrite([analyzeFilePath '/variousRangedDepth.xlsx' ], baseFrequency*360*0.001./phase_50hz, 'velocity', 'B1');

a = b;