%% deep learning for segmentation

close all

for i=1:3
validation_ratio = 0.2;
imageDir = 'C:\Users\Buslab\Desktop\Experiment data\DaMing\exosome_classification\augmentation';
% labelDir = 'D:\daming\skinflap\skinflap in wang lab\rat#1\day3\labeldata_128';
% imds = imageDatastore(imageDir);
% pxds = imageDatastore(labelDir);

imds = imageDatastore(imageDir, 'IncludeSubfolders', true, 'LabelSource', 'foldernames');

classNames = ["flap","background"];
labelIDs   = [255 0];
train_ds = imds;
% val_ds = imageDatastore('D:\exosome_segmentation\data_v5\test', ...
%     'IncludeSubfolders',true,'LabelSource','foldernames');
[train_ds, val_ds] = splitEachLabel(imds, 0.8);
% [train_index, val_index, ~] = dividerand(size(imds.Files, 1), 0.8, 0.2, 0);
% train_imds = imageDatastore(imds.Files(train_index));
% val_imds = imageDatastore(imds.Files(val_index));
% train_pxds = pixelLabelDatastore(pxds.Files(train_index), classNames,labelIDs);
% val_pxds = pixelLabelDatastore(pxds.Files(val_index), classNames,labelIDs);

imageSize = [128 128 1];
numClasses = 2;
% lgraph = unetLayers(imageSize, numClasses, 'EncoderDepth',2);
% lgraph = alexnet('Weights','imagenet');
lgraph = vgg19('Weights', 'imagenet');
% lgraph = googlenet('Weights','imagenet');
% lgraph = inceptionv3('Weights','imagenet');
% lgraph = resnet50('Weights','imagenet');
lgraph = layerGraph(lgraph);
% lgraph = lgraph_1;

temp = imageInputLayer(imageSize, 'Name', 'input');
lgraph = replaceLayer(lgraph, 'input', temp);
temp = convolution2dLayer([3 3], 64, 'Name', 'conv1_1', 'NumChannels', 'auto', 'Stride', [1 1], 'padding', [1 1], 'WeightL2Factor', 0);
lgraph = replaceLayer(lgraph, 'conv1_1', temp);
temp = fullyConnectedLayer(4096,'Name','fc6');
lgraph = replaceLayer(lgraph, 'fc6', temp);
temp = fullyConnectedLayer(2,'Name','fc8');
lgraph = replaceLayer(lgraph, 'fc8', temp);
temp = classificationLayer('Name', 'output', 'Classes', {'abnormal', 'normal'});
lgraph = replaceLayer(lgraph, 'output', temp);

% layers = [ ...
%     imageInputLayer([128 128 1], 'Name', 'input_image')
%     convolution2dLayer(3,64, 'Name', 'conv1', 'Padding', 'same')
%     reluLayer('Name', 'relu1')
%     dropoutLayer('Name', 'dropout1')
%     convolution2dLayer(3,64, 'Name', 'conv2_1', 'Padding', 'same')
%     reluLayer('Name', 'relu2_1')
%     convolution2dLayer(3,64, 'Name', 'conv2_2', 'Padding', 'same')
%     reluLayer('Name', 'relu2_2')
%     dropoutLayer('Name', 'dropout2')
%     convolution2dLayer(3,64, 'Name', 'conv3', 'Padding', 'same')
%     reluLayer('Name', 'relu3')
%     convolution2dLayer(3,2, 'Name', 'convfinal', 'Padding', 'same')
%     softmaxLayer('Name', 'Softmax-Layer')
%     pixelClassificationLayer('Name', 'Segmentation-Layer')];
% lgraph = layerGraph(layers);
% lgraph = removeLayers(lgraph,'output');
% tb = countEachLabel(train_ds);
% new_seg_layer = dicePixelClassificationLayer('Classes', {'flap', 'background'},'Name','Segmentation-Layer')
% new_seg_layer = pixelClassificationLayer('Classes', {'flap', 'background'},'ClassWeights',[tb.ImagePixelCount(1)/tb.PixelCount(1) tb.ImagePixelCount(1)/tb.PixelCount(2)],'Name','Segmentation-Layer'); 
% lgraph = addLayers(lgraph, [new_seg_layer]);
% lgraph = connectLayers(lgraph, 'prob', 'output');
% plot(lgraph);

% train_ds = pixelLabelImageDatastore(train_imds,train_pxds);
% val_ds = pixelLabelImageDatastore(val_imds,val_pxds);
% ds = pixelLabelImageDatastore(imds,pxds,'DataAugmentation', imageAugmenter);
options = trainingOptions('sgdm', ...
    'InitialLearnRate',1e-4, ...
    'MaxEpochs',100, ...
    'VerboseFrequency',5,...
    'MiniBatchSize', 16,...
    'ValidationData', val_ds,...
    'plot', 'training-progress');


[net, train_result] = trainNetwork(train_ds, lgraph, options);
save(['C:\Users\Buslab\Desktop\Experiment data\DaMing\exosome_classification\vgg_' num2str(i) '.mat'], 'net', 'train_result');
end
% pxdsTruth = val_pxds;
% pxdsResults = semanticseg(val_imds,net);
% metrics = evaluateSemanticSegmentation(pxdsResults,pxdsTruth);
%% transfer learning pre-processing
% function [dataout] = random_affine_transform(datain, info) if ~isfield(info, 
% 'times') info.times = 1; end rng for i=1:info.times end

transfer_layer = lgraph;
for i=1:56
    transfer_layer = replaceLayer(transfer_layer, transfer_layer.Layers(i).Name, net.Layers(i));
end
imageAugmenter = imageDataAugmenter( ...
    'RandXReflection',true, ...
    'RandXTranslation',[-6 6], ...
    'RandYTranslation',[-6 6]);
train_ds = pixelLabelImageDatastore(train_imds,train_pxds,'DataAugmentation', imageAugmenter);
val_ds = pixelLabelImageDatastore(train_imds,train_pxds,'DataAugmentation', imageAugmenter);
options = trainingOptions('sgdm', ...
    'InitialLearnRate',1e-5, ...
    'MaxEpochs',50, ...
    'VerboseFrequency',10,...
    'MiniBatchSize', 32,...
    'Shuffle', 'every-epoch',...
    'ValidationData', val_ds,...
    'plot', 'training-progress');
net = trainNetwork(train_ds,transfer_layer,options);
% delete(findall(0));

%% *用於計算之前在clean區掃的skinflap厚度(用NN圈ROI)*

fileset = char('D:\daming\skinflap\skinflap in wang lab\rat#4\day3\30MHz_9kto16k_0to5mm_C1.dat',...
    'D:\daming\skinflap\skinflap in wang lab\rat#4\day3\30MHz_9kto16k_5to10mm_C1.dat',...
    'D:\daming\skinflap\skinflap in wang lab\rat#4\day3\30MHz_9kto16k_10to15mm_C1.dat',...
    'D:\daming\skinflap\skinflap in wang lab\rat#4\day3\30MHz_9kto16k_15to20mm_C1.dat',...
    'D:\daming\skinflap\skinflap in wang lab\rat#4\day3\30MHz_9kto16k_20to25mm_C1.dat',...
    'D:\daming\skinflap\skinflap in wang lab\rat#4\day3\30MHz_9kto16k_25to30mm_C1.dat',...
    'D:\daming\skinflap\skinflap in wang lab\rat#4\day3\30MHz_9kto16k_30to35mm_C1.dat',...
    'D:\daming\skinflap\skinflap in wang lab\rat#4\day3\30MHz_9kto16k_35to40mm_C1.dat');
file_num_per_dir = 8;           %
image_num_per_file = 50;        %同檔名所有.dat檔的影像數目(一個C或B scan的掃的檔案數)
image_z_range = [740:5935];     %影像截頭去尾後保留的部分
resized_img_size = [256 256];   %輸出train image的大小
skin_thickness = zeros(image_num_per_file, size(fileset, 1));
gcp;
tic
parfor fn=1:size(fileset, 1)
    [us_image, ~, ~, x_axis, y_axis,] = get_us_data(fileset(fn, :), struct('is_save', 1, 'dynamic_range',42));
    
    img_index = 1;
    [dir_path, ~, ~]= fileparts(fileset(fn, :));
    if exist([dir_path '\traindata_256_withoutaugment'], 'dir')==0
        mkdir([dir_path '\traindata_256_withoutaugment']);
    end
    if exist([dir_path '\segment_by_AI_256_withoutaugment'], 'dir')==0
        mkdir([dir_path '\segment_by_AI_256_withoutaugment']);
    end
    for i = 1:image_num_per_file
        temp_img = imresize(uint8(us_image(image_z_range, :, i)), resized_img_size, 'method', 'bilinear');
        train_img_index = (mod(fn-1, file_num_per_dir))*image_num_per_file+i;
        BW = semanticseg(temp_img, dice_loss_net);
        B = labeloverlay(temp_img, BW);
        
        %儲存overlay的影像
        imshow(B)
        imwrite(B, gray(256), [dir_path '\segment_by_AI_256_withoutaugment\' num2str(train_img_index) '.png'], 'PNG');
        imwrite(temp_img, [dir_path '\traindata_256_withoutaugment\' num2str(train_img_index) '.png'], 'PNG');
        
%         %計算skinflap厚度
%         BW = imresize((BW=='flap'), [size(us_image, 1) size(us_image, 2)], 'method', 'bilinear');
%         [outlier_value, ~] = rmoutliers(sum(BW));
%         skin_thickness(i, fn) = mean(outlier_value);
%         close(gcf);
    end
end
toc
% delete(gcp('nocreate'))

% for i=1:5   %section
%     for j=1:5   %day
%         for k=1:6   %rat
%             temp = skin_thickness(:, i, j, k);
%             avg_thickness(i, j, k) = mean(rmoutliers(temp));
%         end
%     end
% end

%% *augment image and output*

% augmenter = imageDataAugmenter( ...
%     'RandXReflection', true,...
%     'RandRotation',[-30 30],...
%     'RandYTranslation', [-20 20]);
imageDir = 'D:\daming\exosome_DrWu_journal\resize\normal';
target_dir = 'D:\daming\exosome_DrWu_journal\augmentation\normal';
% labelDir = 'D:\exosome_segmentation\data_v2\normal';
aug_times = 3;
imds = imageDatastore(imageDir);
file_num = size(imds.Files, 1);
% pxds = imageDatastore(labelDir);

for t = 1:aug_times
    parfor i=1:file_num
        I = readimage(imds, i);
%         C = readimage(pxds, i);
        [~, filename, ~] = fileparts(imds.Files{i});
        img_index = (t)*file_num+i;
        tform = randomAffine2d("Rotation", [-15 15],...
            "XReflection",true,...
            "YReflection",true,...
            'XTranslation', [-20 20],...
            'YTranslation', [-20 20]);
        rout = affineOutputView(size(I),tform);
        augment_I = imwarp(I,tform,'OutputView',rout);
%         augment_C = imwarp(C,tform,'OutputView',rout);
        imwrite(augment_I, [target_dir '\' num2str(img_index) '.png'], 'PNG');
%         imwrite(augment_C, [labelDir '\' num2str(img_index) '.png'], 'PNG');
    end
end

% imshow(labeloverlay(augment_I,augment_C))
% reset(imds);
% reset(pxds);

%% 輸出指定某層的feature maps
% close all
% layer = 'block1_conv1';
% layer = 'block14_sepconv2_point-wise';
% layer = 'block14_sepconv2_bn';

layer = 'output';
image_dir = 'D:\daming\exosome_DrWu_journal\augmentation\normal';
imds = imageDatastore(image_dir);

% % 如果檔名是純數字可以用這段code排序
% [~, name, ~] = fileparts(imds.Files);
% name = cellfun(@(x) {str2num(x)}, name);
% [~, order] = sort(cell2mat(name));
% imds.Files = imds.Files(order);
% selected_image = [1:size(imds.Files, 1)];
% selected_image = [1];
% r=8; c=8;
% imds.Files = imds.Files(selected_image);
feature_map = activations(net, imds, layer);
feature_num = size(feature_map, 3);
size(feature_map)

% rng;
% map_num = randperm(feature_num, r*c);
map_numb = (1:64);

for k=1:size(imds.Files, 1)
    figure('position', [100 50 900 900])
    p = panel();
    p.margin = [1, 1, 1, 1];
    r=8;
    c=8;
    p.pack(r, c);
    for i=1:r
        for j=1:c
            p(i, j).select();
            imshow(feature_map(:, :, map_num((i-1)*c+j), k))
%             imshow(feature_map(:, :, ((i-1)*c+j)*feature_num/r/c, k))
        end
    end
end

% figure('position', [100 50 2400 900])
% p = panel();
% p.margin = [8, 8, 8, 8];
% p.pack(r, c);
%     for i=1:r
%         for j=1:c
%             p(i, j).select();
%             plot(squeeze(feature_map(1, 1, :, (i-1)*c+j)))
%         end
%     end

%% 利用現有的net分類定檔案路徑下的圖片
image_dir = 'K:\temp\frequently_mispredict_vgg19';
imds = imageDatastore(image_dir);
[~, name, ~] = fileparts(imds.Files);
name = cellfun(@(x) {str2num(x)}, name);
name = cell2mat(name);
[~, order] = sort(name);
imds.Files = imds.Files(order);
selected_image = [1:size(imds.Files, 1)];
imds.Files = imds.Files(selected_image);

YPred = classify(net_100,imds);

%% resize
image_dir = 'D:\daming\exosome_DrWu_journal\NewImages\normal';
target_dir = 'D:\daming\exosome_DrWu_journal\resize\normal';
imds = imageDatastore(image_dir);
image_size = [128 128];
if ~exist(target_dir, 'dir')
    mkdir(target_dir);
end

for i=1:size(imds.Files, 1)
    img = readimage(imds, i);
    img = imresize(img(:, :, 1), image_size, 'method', 'bilinear');
    imwrite(img, [target_dir '\' num2str(i) '.png'], 'PNG');
end

%% 輸出cam
% close all
% normal [5 11];
% abnormal [6 7 9];
netName = 'xception';
im_dir = 'D:\exosome_segmentation\data_v5\test\normal';
input_size = [224 224];
feature_map_layer = 'block14_sepconv2_act';
weight_layer = 'predictions';
output_layer = 'ClassificationLayer_predictions';
max_map_num = 10;
imds = imageDatastore(im_dir);
for i=1:1
    img_idx = i;
    classes =  net.Layers(find(strcmp({net.Layers.Name}, 'ClassificationLayer_predictions'))).Classes;

    % while ishandle(h)
    im = readimage(imds, img_idx);
    imResized = imresize(im,[input_size(1), NaN]);
    imageActivations = activations(net,imResized,feature_map_layer);
    weight = net.Layers(strcmp({net.Layers.Name}, weight_layer)).Weights(1, :);
    bias = net.Layers(strcmp({net.Layers.Name}, weight_layer)).Bias(1);
    scores = squeeze(mean(imageActivations,[1 2])).*weight';

    %     if netName ~= "squeezenet"
    %         fcWeights = net.Layers(find(strcmp({net.Layers.Name}, 'predictions'))).Weights;
    %         fcBias = net.Layers(find(strcmp({net.Layers.Name}, 'predictions'))).Bias;
    %         scores =  fcWeights*scores + fcBias;
    %         [~,classIds] = maxk(scores,3);
    %         
    %         weightVector = shiftdim(fcWeights(classIds(1),:),-1);
    %         classActivationMap = sum(imageActivations.*weightVector,3);
    %     else
            [~,classIds] = maxk(weight,max_map_num);
            classActivationMap = imageActivations(:,:,classIds);
    %     end
    %     scores = exp(scores)/sum(exp(scores));     
    %     maxScores = scores(classIds);
    %     labels = classes(classIds);
    %     subplot(1,2,1)
    %     imshow(im)

    %     subplot(1,2,2)
    CAMshow(im, classActivationMap, weight(classIds), bias);
    [X,Map] = rgb2ind(frame2im(getframe(gcf)), 256);
    if ~exist([im_dir '\cam' num2str(max_map_num) '\'])
        mkdir([im_dir '\cam' num2str(max_map_num) '\']);
    end
%     imwrite(X, Map, [im_dir '\cam' num2str(max_map_num) '\' num2str(i) '.jpg'], 'JPEG');
end
% end

%% 各種參數
TA = [(1:7000)'/70400*200 train_result.TrainingAccuracy'];
TL = [(1:7000)'/7000*200 train_result.TrainingLoss'];
VA = [[1 (50:50:7000)]'/7000*200 train_result.ValidationAccuracy(~isnan(train_result.ValidationAccuracy))'];
VL= [[1 (50:50:7000)]'/7000*200 train_result.ValidationLoss(~isnan(train_result.ValidationAccuracy))'];
imds1 = imageDatastore('D:\exosome_segmentation\data_v5\test\normal');
imds2 = imageDatastore('D:\exosome_segmentation\data_v5\test\abnormal');
sens_and_spec = [sum(classify(net, imds1)=='normal')/12 sum(classify(net, imds2)=='abnormal')/10];

%%

orig_img = imread('D:\daming\exosome_DrWu_journal\worst_abnormal_542_orig.jpg');
X = imread('D:\daming\exosome_DrWu_journal\worst_abnormal_542.png');
label = classify(net, X);
scoreMap = gradCAM(net,X,label);
scoreMap = imresize(scoreMap, [1024 1024], 'Method', 'bilinear');

figure('color', 'white')
imshow(orig_img)
hold on
imagesc(scoreMap,'AlphaData',0.25)
set(gca,'visible','off')
colormap jet