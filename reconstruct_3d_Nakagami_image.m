% clear
% clc
% close all
x_interval = 0.05;
y_interval = 0.2;
z_interval = 1/250e6*1540/2*1000;
delay = 1500;   %pts

img_start_z = 7.7;  %mm
img_end_z = 11.5;
resize_ratio = 0.5;
img_start_pts = round(img_start_z/1000/1540*250e6*2)-delay;  %pts
img_end_pts = round(img_end_z/1000/1540*250e6*2)-delay;
rf_for_thickness = [101 108];
noise_range = [2801 1 200 50];
resolutionAxial = 0.0591; %mm
resolutionLateral = 0.1003;
windowFold=[2 3 4]*3;
AxialROIFold = windowFold;
LateralROIFold = windowFold;

file = char('K:\黃大銘實驗資料\skinflap\G6\20190704_skinflap_post\no1(butt)\30MHz#2_0to6mm_C1.dat',...
    'K:\黃大銘實驗資料\skinflap\G6\20190704_skinflap_post\no1(butt)\30MHz#2_6to12mm_C1.dat',...
    'K:\黃大銘實驗資料\skinflap\G6\20190704_skinflap_post\no1(butt)\30MHz#2_12to18mm_C1.dat',...
    'K:\黃大銘實驗資料\skinflap\G6\20190704_skinflap_post\no1(butt)\30MHz#2_18to24mm_C1.dat',...
    'K:\黃大銘實驗資料\skinflap\G6\20190704_skinflap_post\no1(butt)\30MHz#2_24to30mm_C1.dat');

us_image = [];
for f=1:size(file, 1)
    [temp, xAxis, yAxis]  = showCscanImage(strtrim(file(f, :)), struct('is_save', 0));
    us_image(:, :, (f-1)*30+1:(f-1)*30+30) = temp;
end
us_image = permute(us_image, [3 1 2]);
clear temp

%% 影像smoothing
H = fspecial('average',21);
smoothing_image = zeros(size(us_image));
for f=1:size(us_image, 1)
    smoothing_image(f, :, :) = imfilter(squeeze(us_image(f, :, :)), H, 'replicate');
end
clear H

skin_bound = ones(2, rf_for_thickness(2)-rf_for_thickness(1)+1, size(smoothing_image, 1));
front_mask = zeros(size(smoothing_image));
tic
gcp;
parfor f=1:size(smoothing_image, 1)
    temp = squeeze(smoothing_image(f, :, :));
    temp(temp<20) = 0;
    
    %% 膨脹與侵蝕
%     se = strel('rectangle', [5 5]);
%     temp = imerode(temp, se);
%     subplot(1, 3, 2); image(xAxis, yAxis, temp); axis image;
%     temp = imdilate(temp, se);
%     subplot(1, 3, 3); image(xAxis, yAxis, temp); axis image;
%     pause(0.5)
%     close all;

    %% active contour model
    mask = zeros(size(temp));
    mask(img_start_pts:img_end_pts, 1:end) = 1;
    BW = activecontour(temp,mask, 1000);
    front_mask(f, :, :) = BW;
%     figure; image(BW.*squeeze(smoothing_image(f, :, :))); colormap gray(256);
%     pause(0.4)
%     close all;
end
toc
% clear temp mask BW;

%% 計算厚度
for f=1:size(smoothing_image, 1)
    signal = squeeze(front_mask(f, :, rf_for_thickness(1):rf_for_thickness(2)));
    for i=1:size(signal, 2)
        is_find_up_bound = 0;
        for j=1:size(signal, 1)-49
            if sum(signal(j:j+49, i))>35 && is_find_up_bound == 0
                is_find_up_bound = 1;
                skin_bound(1, i, f) = j;
            end
            if sum(signal(j:j+49, i))<35 && is_find_up_bound == 1
                is_find_up_bound = 0;
                skin_bound(2, i, f) = j;
                break;
            end
        end
    end
end
avg_skin_height = round(mean(squeeze(skin_bound(1, :, :)), 1));
avg_skin_height = avg_skin_height-avg_skin_height(1);
avg_skin_thickness = z_interval*round(mean(squeeze(skin_bound(2, :, :)-skin_bound(1, :, :)), 1));

%% 根據表皮位置(avg_skin_thickness)校正rf訊號
us_rf = zeros(size(us_image));
for f=1:size(file, 1)
    param = headFile_NI(strtrim(file(f, :)));
    us_rf((f-1)*30+1:(f-1)*30+30, :, :) = permute(param.rf, [3 1 2]);
end
shift_us_rf = zeros(size(us_image));
for f=1:size(us_rf, 1)
    if avg_skin_height(f) > 0
        shift_us_rf(f, 1:end-avg_skin_height(f), :) = squeeze(us_rf(f, avg_skin_height(f)+1:end, :));
    else
        shift_us_rf(f, abs(avg_skin_height(f))+1:end, :) = squeeze(us_rf(f, 1:end-abs(avg_skin_height(f)), :));
    end
end

% env_rf=abs(hilbert(shift_us_rf));
% log_rf=20*log10(env_rf/max(env_rf, [], 'all'));
xAxis = (0:param.Aline-1)*param.XInterval/1000;
yAxis = ((1:param.DataLength)+param.Delay)/2/(param.SamplingRate*1e6)*1540*1000;
nakagami_img = zeros([size(us_image) length(windowFold)])-1;
clear front_mask signal smoothing_image

%     noiseLevel = mean(mean(log_rf(noiseRange(1):noiseRange(1)+noiseRange(3), noiseRange(2):noiseRange(2)+noiseRange(4))));
%     nakagamiImg = zeros(param.DataLength, param.Aline, length(windowFold))-1;
tic
    parfor wf = 1:length(windowFold)
        windowZ = round(2*resolutionAxial/1000/1540*param.SamplingRate*1e6*AxialROIFold(wf));
        windowY = 5;
        windowX =  round(resolutionLateral/(param.XInterval*1e-3)*LateralROIFold(wf));
        off_set_z = round(windowZ/2);
        off_set_y = 2;
        off_set_x = round(windowX/2);

% % computations take place here
        tempImg = zeros(size(us_image))-1;
        for yy = 15:15
%         for yy = 1+off_set_y:size(us_image, 1)-off_set_y
            noise_roi = us_rf((yy-off_set_y):(yy+off_set_y), end-2*off_set_z+1:end, end-2*off_set_x+1:end);
            for jj = img_start_pts:img_end_pts
                for ii = 1+off_set_x:param.Aline-off_set_x
                    roi = shift_us_rf((yy-off_set_y):(yy+off_set_y), (jj-off_set_z):(jj+off_set_z)-1,(ii-off_set_x):(ii+off_set_x)-1);
    %                     noise_roi = rf(noiseRange(1):noiseRange(1)+windowZ-1, noiseRange(2):noiseRange(2)+windowX-1);
                        roi_SNR = 10*log10(sum(roi.^2, 'all')/sum(noise_roi.^2, 'all'));
%                     roi_SNR = 10*log10(sum(sum(roi.^2))/noiseSquareSum(wf));
    %                     noiseNum = length(find(log_rf((j-off_set_z):(j+off_set_z),(i-off_set_x):(i+off_set_x)) < 0.8*noiseLevel));
                    if(roi_SNR < 20)
    %                     if(noiseNum > 0.2*numel(roi))
                        mu = 0;
                    else
                        roi_reshape=reshape(roi, 1, [])';
                        env_roi = abs(hilbert(roi_reshape));
                        env_roi = env_roi/max(max(env_roi))*255;
                        mu = (mean((env_roi).^2).^2)/mean((env_roi.^2-mean(env_roi.^2)).^2);
                    end
                    tempImg(yy, jj, ii) = mu;
                end
            end
        end
        nakagami_img(:, :, :,wf) = tempImg;
    end
toc
delete(gcp('nocreate'));
