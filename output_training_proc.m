
output = true;
val_interval = 5;
data_len = length(train_loss);

if output
    figure('position', [200 200 800 500], 'color', 'white')
    hold on
    plot(train_loss, 'k-o', 'LineWidth', 3, 'markerfacecolor', 'k')
    plot(train_mse, 'k-^', 'LineWidth', 3, 'markerfacecolor', 'k')
    plot(train_xcorr, 'k-square', 'LineWidth', 3, 'markerfacecolor', 'k')
    plot([1 val_interval:val_interval:data_len], val_loss([1 val_interval:val_interval:data_len]), 'r-o', 'LineWidth', 3, 'markerfacecolor', 'r')
    plot([1 val_interval:val_interval:data_len], val_mse([1 val_interval:val_interval:data_len]), 'r-^', 'LineWidth', 3, 'markerfacecolor', 'r')
    plot([1 val_interval:val_interval:data_len], val_xcorr([1 val_interval:val_interval:data_len]), 'r-square', 'LineWidth', 3, 'markerfacecolor', 'r')
    xlabel('Epoch')
    ylabel('Loss')
    xlim([1 data_len])
    
    % ylim([0 1.2])
    set(gca, 'fontsize', 24, 'fontname', 'times', 'fontweight', 'bold')
    set(gca, 'box', 'on', 'linewidth', 2, 'tickdir', 'out')
    legend('training loss', 'training mse', 'training xcorr', 'val loss', 'val mse', 'val xcorr', 'fontsize', 14, 'fontname', 'times', 'numcolumns', 2)
    legend('boxoff')
    % axis([-inf, inf, 0, 1.5])
end
loss_all = min(val_loss(val_loss~=0));
loss_mse = min(val_mse(val_mse~=0));
loss_xcorr = min(val_xcorr(val_xcorr~=0));
time1 = sum(training_time)/3600;
time2 = mean(val_time(val_time~=0))/275;
