%%  2D或3D內插

function [post] = my_interp_23D(pre,interp_factor)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
if length(interp_factor)==2
    [temp.X,temp.Y] = meshgrid(1:size(pre, 2), ...
        1:size(pre, 1));
    [temp.Xq,temp.Yq] = meshgrid(linspace(1, size(pre, 2), size(pre, 2)*interp_factor(2)), ...
        linspace(1, size(pre, 1), size(pre, 1)*interp_factor(1)));
    post = interp2(temp.X, temp.Y, pre, temp.Xq, temp.Yq, "cubic");
else
    [temp.X,temp.Y,temp.Z] = meshgrid(1:size(pre, 2), ...
        1:size(pre, 1), ...
        1:size(pre, 3));
    [temp.Xq,temp.Yq,temp.Zq] = meshgrid(1:size(pre, 2), linspace(1, size(pre, 1), size(pre, 1)*2), 1:size(pre, 3));
    [temp.Xq,temp.Yq,temp.Zq] = meshgrid(linspace(1, size(pre, 2), size(pre, 2)*interp_factor(2)), ...
        linspace(1, size(pre, 1), size(pre, 1)*interp_factor(1)), ...
        linspace(1, size(pre, 3), size(pre, 3)*interp_factor(3)));
    post = interp3(temp.X, temp.Y, temp.Z, pre, temp.Xq, temp.Yq, temp.Zq, "cubic");
end