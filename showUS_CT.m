close all
clc
filePath = 'F:\20180731_legImg_d7\left';
fileName = 'cross_deeper';
dynamicRange = 42;
file = dir([filePath '\' fileName '_C*']);
fileNum = length(file);
firstParam = headFile_NI_forCscan([filePath '\' fileName '_C1.dat']);
CImage = zeros(firstParam.DataLength, firstParam.Aline, firstParam.MoveTimes);
figure('color', 'white')

imgNum = 0;
for f=1:fileNum
    param = headFile_NI_forCscan([filePath '\' fileName '_C' num2str(f) '.dat']);
    for i=1:size(param.rf, 3)
        imgNum = imgNum + 1;
%         if mod(i, 2)==1
        if mod(i, 2)==0
            CImage(:, :, imgNum) = fliplr(param.rf(:, :, i));
        else
            CImage(:, :, imgNum) = param.rf(:, :, i);
        end
    end
end
for i=1:param.MoveTimes
    log_rf=abs(hilbert(CImage(:, :, i)));
    log_rf=20*log10(log_rf/min(min(log_rf)));
    log_rf=log_rf-max(max(log_rf))+dynamicRange;
    img=255*log_rf/dynamicRange;
    xAxis = (1:firstParam.Aline)*firstParam.XInterval/1000;
%             yAxis = (1:param.DataLength);
    yAxis = ((1:firstParam.DataLength)+firstParam.Delay)/2/(firstParam.SamplingRate*1e6)*1500*1000;
    image(xAxis, yAxis, img)
    xlabel('Distance (mm)', 'FontSize', 14, 'FontName', 'Arial')
    ylabel('Depth (mm)', 'FontSize', 14, 'FontName', 'Arial');
    colormap(gray(255))
    axis image
%             axis equal tight
    [X,Map] = rgb2ind(frame2im(getframe(1)), 256);
    savePath = [filePath '\0_' fileName num2str(i), '.jpg'];
    if exist(savePath, 'file')==2
        delete(savePath);
    end

    imwrite(X, Map, savePath, 'JPEG');
end
for i=1:2:firstParam.Aline
    log_rf=abs(hilbert(squeeze(CImage(:, i, :))));
    log_rf=20*log10(log_rf/min(min(log_rf)));
    log_rf = fliplr(log_rf);
    log_rf=log_rf-max(max(log_rf))+dynamicRange;
    img=255*log_rf/dynamicRange;
    xAxis = (1:firstParam.MoveTimes)*firstParam.YInterval/1000;
%             yAxis = (1:param.DataLength);
    yAxis = ((1:firstParam.DataLength)+firstParam.Delay)/2/(firstParam.SamplingRate*1e6)*1500*1000;
    image(xAxis, yAxis, img)
    xlabel('Distance (mm)', 'FontSize', 14, 'FontName', 'Arial')
    ylabel('Depth (mm)', 'FontSize', 14, 'FontName', 'Arial');
    colormap(gray(255))
    axis image
%             axis equal tight
    [X,Map] = rgb2ind(frame2im(getframe(1)), 256);
    savePath = [filePath '\0_lateral' num2str(i), '.jpg'];
    if exist(savePath, 'file')==2
        delete(savePath);
    end

    imwrite(X, Map, savePath, 'JPEG');
end