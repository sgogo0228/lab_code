%% prepare the input dataset
nClass = length(unique(T{:, end}));
trainedData = T{:, 1:end-1}';
classes = zeros(nClass, size(T, 1));
for i=1:size(T, 1)
    if strcmp(T{i, end}, 'normal')
        classes(1, i) = 1;
    elseif strcmp(T{i, end}, 'inflammation')
        classes(2, i) = 1;
    elseif strcmp(T{i, end}, 'week1')
        classes(3, i) = 1;
    elseif strcmp(T{i, end}, 'week2')
        classes(4, i) = 1;
    else
        classes(5, i) = 1;
    end
end

% net = feedforwardnet([10 10]);
% net = train(net,trainedData,classes);
% view(net)
% y = net(trainedData);
% perf = perform(net,y,classes)
% plotconfusion(classes,y)
%% NN and validation
indices = crossvalind('Kfold',T{:, end},10);
trainAcc = zeros(10, 1);
testAcc = zeros(10, 1);
tic
for k = 1:10
    net = feedforwardnet([60 60 60], 'traincgf');
%     net = feedforwardnet([10 10], 'traingda');
%     net = feedforwardnet([10 10], 'trainscg');
    net.trainParam.epochs = 1000;
    net.divideFcn = 'dividetrain';
    net.trainParam.goal = 1e-6;
%     net.trainParam.time = 3600;
    net.trainParam.min_grad = 1e-6;
    
    testing = (indices == k);
    testX = trainedData(:, find(testing));
    testY = classes(:, find(testing));
    curX = trainedData(:, find(~testing));
    curY = classes(:, find(~testing));
    net = train(net,curX,curY,'useGPU','yes');
    
    predicted = net(curX);
    [~, curY] = max(curY);
    [~, predicted] = max(predicted);
    cm = confusionmat(curY, predicted);
    for c=1:nClass
        trainAcc(k) = trainAcc(k) + cm(c, c)/sum(sum(cm));
    end
    
    predicted = net(testX);
    [~, testY] = max(testY);
    [~, predicted] = max(predicted);
    cm = confusionmat(testY, predicted);
    for c=1:nClass
        testAcc(k) = testAcc(k) + cm(c, c)/sum(sum(cm));
    end
end
disp(['training accuracy: ' num2str(mean(trainAcc))])
disp(['testing accuracy: ' num2str(mean(testAcc))])
toc