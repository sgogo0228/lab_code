function[fileList] = findMatchedFile(fileDir, prefix)   %fileDir:搜尋的目標資料夾、prefix:用於匹配標準表示字串
    fileList = dir(fileDir);    %儲存所有目標資料夾下的檔案
    i = 1;
    while i<=length(fileList)
        if(isempty(regexp(fileList(i).name, prefix)))   %如果檔名中找不到符合的字串就會直接從陣列中把該黨刪掉
            fileList(i) = [];
        else
            i=i+1;
        end
    end
end