unitAline = 199;
pushNum = 5;
distNum = 10;
samplingRate = 10000;
CImage = '30MHz_dist0to5_depth5to14';
path =['J:\temp\right\'];
file = char('L:\黃大銘實驗資料\contusion\45cm\20201218_shearwave_#11\day4\right\30MHz_dist0to5_depth7to16k_amp600_cycle5100_1_SW0');
pushNoise=10;
hormonicNum = 5;
baseFrequency=50;
modelParams = struct('unitAline', unitAline, 'pushNum', pushNum, 'usedCycle', 3, 'frontNoise', pushNoise-3, 'backNoise', 3, 'damping', 0);
depthInterval = 250;

rf = headFile_NI([file(1, :) '.dat']);
depth = [1 rf.DataLength];
imgTop = rf.Delay;
fnum = size(file, 1);
bandFilter = [baseFrequency*0.7 baseFrequency*6];
overallDepth = depth(2)-depth(1)+1;
xAxis = (0:distNum-1)*0.5;
yAxis = ((0:depth(2)/depthInterval-1)*depthInterval);
yAxis = (((1:depth(2)/depthInterval)*depthInterval)-depthInterval/2+imgTop)/2/1000000000*1500*1000;
modifiedUnitAline = (modelParams.unitAline-modelParams.frontNoise-modelParams.backNoise-modelParams.damping);
unusedCycle=modelParams.pushNum-modelParams.usedCycle;


demodSignal = zeros(fnum, distNum, overallDepth, unitAline*pushNum);
h = waitbar(0, ['Processing...f=1(0/' num2str(distNum) ')']);
for f=1:fnum
    for j=0:distNum-1
        waitbar(j/distNum, h, ['Processing...f=' num2str(f) '(' num2str(j) '/' num2str(distNum) ')']);
        filename = [strtrim(file(f, 1:end-1)) num2str(j) '.dat'];
        [sgnParam demodSignal(f, j+1, :, :)]=shearWaveByDemod20170327(filename, depth, unitAline, pushNum, pushNoise);
    end
end
close(h)

shearWaveImage = zeros(fnum,  distNum, overallDepth/depthInterval, unitAline*pushNum);
for f=1:fnum
    for j=0:distNum-1
        for d = 1:depthInterval:overallDepth-depthInterval+1
            shearWaveImage(f, j+1, (d+depthInterval-1)/depthInterval, :) = mean(squeeze(demodSignal(f, j+1, d:d+depthInterval-1, :)), 1);
        end
    end
end
sgn = shearWaveImage;
timeAxis = (0:(modelParams.pushNum*modelParams.unitAline-1))/samplingRate*1000;

% % 去除push造成的雜訊
for i=pushNum-1:-1:0
    shift = modelParams.unitAline*i;
    sgn(:, :, :, 1+shift:(modelParams.frontNoise+modelParams.backNoise)+shift) = [];
    timeAxis(1+shift:(modelParams.frontNoise+modelParams.backNoise)+shift) = [];
end

% % 去除低頻雜訊(動物呼吸、環境干擾等)
[paramB, paramA] = butter(5, bandFilter/(samplingRate/2));
temp = permute(sgn, [4 3 2 1]);
temp = filtfilt(paramB, paramA, temp);
sgn = permute(temp, [4 3 2 1]);

% % 去除前後較不穩定的訊號
sgn(:, :, :, 1:(unusedCycle-1)*modifiedUnitAline) = [];
sgn(:, :, :, end-modifiedUnitAline+1:end) = [];

% % 將中間較穩定之訊號座平均作為最終的分析訊號
for f=1:fnum
    for i=1:distNum
        for d=1:overallDepth/depthInterval
            tempSgn = mean(reshape(sgn(f, i, d, :), (modelParams.unitAline-modelParams.frontNoise-modelParams.backNoise), modelParams.usedCycle), 2)';
            tempSgn((end-modelParams.damping+1):end) = [];
            for j=0:modelParams.usedCycle-1
                sgn(f, i, d, (1+j*modifiedUnitAline):((j+1)*modifiedUnitAline)) = tempSgn;
            end
            sgn(f, i, d, :) = squeeze(sgn(f, i, d, :))-min(sgn(f, i, d, :));
            sgn(f, i, d, :) = squeeze(sgn(f, i, d, :))/max(sgn(f, i, d, :));
        end
    end
end
sgn(:, :, :, modifiedUnitAline*modelParams.usedCycle+1:end) = [];

clear temp

%%
load('H:\school\shearWaveData\colorMapGroup.mat')
figure('color', 'white')
graphInterval = 2;
colorScale = [0 1];

% % 輸出2D的SW圖
for f=1:fnum
    sgn(f, :, :, :) = sgn(f, :, :, :)-min(min(min(min(sgn(f, :, :, :)))));
    innerFolderIndex = strfind(file(f, :), '\');
    if isempty(innerFolderIndex)==0
        savePath = [file(f, 1:innerFolderIndex(end))  '0_' file(f, innerFolderIndex(end)+1:end) '_test.gif'];
    else
        savePath = ['0_' file(f, :) '_test.gif'];
    end
    i=1;
    imagesc(xAxis, yAxis, squeeze((sgn(f, :, :, i)))');
    caxis(colorScale)
    colorbar
    title(['Time lapse:' num2str(i*0.1) ' ms'])
    set(gca, 'FontSize', 24, 'FontName', 'Times', 'FontWeight', 'bold')
    xlabel('Distance (Y, mm)', 'FontSize', 28, 'FontWeight', 'bold')
    ylabel('Depth (Z, mm)', 'FontSize', 28, 'FontWeight', 'bold')
    [X,Map] = rgb2ind(frame2im(getframe(1)), 256);
    if exist(savePath, 'file')==2
        delete(savePath);
    end
    imwrite(X, Map, savePath, 'GIF', 'WriteMode', 'overwrite', 'DelayTime', 0, 'LoopCount', Inf);
%     imwrite(X, Map, [path 'photo\1.jpg'], 'jpeg');
    i = graphInterval+1;
    while i<=modifiedUnitAline                % 任何一個 loop 
        imagesc(xAxis, yAxis, squeeze((sgn(f, :, :, i)))');
        caxis(colorScale)
        colorbar
        title(['Time lapse:' num2str(i*0.1) ' ms'])
        set(gca, 'FontSize', 24, 'FontName', 'Times', 'FontWeight', 'bold')
        xlabel('Distance (Y, mm)', 'FontSize', 28, 'FontWeight', 'bold')
        ylabel('Depth (Z, mm)', 'FontSize', 28, 'FontWeight', 'bold')
        [X,Map] = rgb2ind(frame2im(getframe(1)), 256);
        %在 loop內 的writemode 須改為 append ,這時候frame才會不斷的增加進去
        imwrite(X, Map, savePath,'GIF','WriteMode','append','DelayTime', 0);
%         imwrite(X, Map, [path 'photo\' num2str(i) '.jpg']);
        i = i+graphInterval;
    end
end

% % 輸出3D的SW圖
% for f=1:fnum
%     innerFolderIndex = strfind(file(f, :), '\');
%     if isempty(innerFolderIndex)==0
%         savePath3D = [path file(f, 1:innerFolderIndex)  '0_' file(f, innerFolderIndex+1:end) '_3D.gif'];
%     else
%         savePath3D = [path '0_' file(f, :) '_3D.gif'];
%     end
%     close all
%     figure('position', [900 200 670 500])
%     surf(xAxis, yAxis, squeeze((sgn(f, :, :, i)))');
%     zlim(colorScale)
%     caxis(colorScale)
%     view(-45, -60);
%     title(num2str(i))
%     xlabel('Distance (mm)', 'FontSize', 14)
%     ylabel('Depth (mm)', 'FontSize', 14)
%     pause(0.5)
% %         print('-djpeg', [photoPath fname num2str(i) '.jpg']);
%     [X,Map] = rgb2ind(frame2im(getframe(gcf)), 256);
%     if exist(savePath3D, 'file')==2
%         delete(savePath3D);
%     end
%     imwrite(X, Map, savePath3D, 'GIF', 'WriteMode', 'overwrite', 'DelayTime', 0, 'LoopCount', Inf);
%     
%     i = graphInterval+1;
%     while i<=modifiedUnitAline                % 任何一個 loop
%         close
%         figure('position', [900 200 670 500])
%         surf(xAxis, yAxis, squeeze((sgn(f, :, :, i)))');
%         zlim(colorScale)
%         caxis(colorScale)
%         view(-45, -60);
%         title(num2str(i))
%         xlabel('Distance (mm)', 'FontSize', 14)
%         ylabel('Depth (mm)', 'FontSize', 14)
%         pause(0.5)
%     %         print('-djpeg', [photoPath fname num2str(i) '.jpg']);
%         [X,Map] = rgb2ind(frame2im(getframe(gcf)), 256);
%         imwrite(X, Map, savePath3D, 'GIF', 'WriteMode', 'append','DelayTime', 0);
% 
%         i = i+graphInterval;
%     end
% end