function [atanv] = myatan(sinVal, cosVal)
%UNTITLED2 SummarsinVal of this function goes here
%   Detailed ecosValplanation goes here
    atanv=nan;
    if cosVal>0
        atanv=atan(sinVal/cosVal);
    end
    if sinVal>=0 && cosVal<0
        atanv=pi+atan(sinVal/cosVal);
    end
    if sinVal<0 && cosVal<0
        atanv=-pi+atan(sinVal/cosVal);
    end
    if sinVal>0 && cosVal==0
        atanv=pi/2;
    end
    if sinVal<0 && cosVal==0
        atanv=-pi/2;
    end
end

