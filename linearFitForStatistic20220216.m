% dist代表用於計算斜率的點數，如果是[8 20]則代表用前8個點與前20個點計算斜率
function [xaxis, slope] = linearFitForStatistic20220216(phaseAngle, pos, depth)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
    xaxis = linspace(0, 4.5, size(phaseAngle, 1));
    slope = zeros(length(pos), length(depth));
    window_size = 15;
    for p = 1:length(pos)
        for d = 1:length(depth)
            if p < window_size
%                 temp = polyfit(phaseAngle(pos(p)-10:pos(p), depth(d), 1), xaxis(pos(p)-10:pos(p)), 1);
                temp = polyfit(phaseAngle(1:pos(p), depth(d), 1), xaxis(1:pos(p)), 1);
            else
%                 temp = polyfit(phaseAngle(pos(p)-window_size+1:pos(p), depth(d), 1), xaxis(pos(p)-window_size+1:pos(p)), 1);
                temp = polyfit(phaseAngle(1:pos(p), depth(d), 1), xaxis(1:pos(p)), 1);
            end
            slope(p, d) = temp(1);
        end
    end

    xaxis = xaxis(pos);
end

