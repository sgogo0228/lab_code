function [] = my_augmentation(imds, aug_times, paramin)
    if nargin < 3
        return;
    end
    %operation_type_num: 統計有被選擇作為augmentation操作方法的數量
    %operation_type: 用數字來記錄那些操作方法被選擇了, operation_type(2) = 3代表第2種操作方法為y_translation
    %可選的操作方法: rotate, translation等, 預設rotate=1, translation=2 3
    operation_type_num = 0;
    if isfield(paramin,'rotate')
        rotate = paramin.rotate;
        operation_type_num = operation_type_num+1;
        operation_type(operation_type_num) = 1;
    else
        rotate = [0 0]; 
    end
    
    if isfield(paramin,'x_translation')
        x_translation = paramin.x_translation;
        operation_type_num = operation_type_num+1;
        operation_type(operation_type_num) = 2;
    else
        x_translation = [0 0]; 
    end
    
    if isfield(paramin,'y_translation')
        y_translation = paramin.y_translation;
        operation_type_num = operation_type_num+1;
        operation_type(operation_type_num) = 3;
    else
        y_translation = [0 0]; 
    end
    
    if isfield(paramin,'brightness')
        brightness = paramin.brightness;
        operation_type_num = operation_type_num+1;
        operation_type(operation_type_num) = 4;
    else
        brightness = [0 0]; 
    end
    
    %在這裡就先決定好要用哪種操作方法被augmentation出來
    %operation(3, 1)=2代表第3張影像在第1次被augmentation時是用第2種操作方法
    rng;
    operation = randi(operation_type_num, size(imds.Files, 1), aug_times);
    
    [file_dir, ~, ~] = fileparts(imds.Files{1});
    target_dir = [file_dir '\augmentation'];
    if ~exist(target_dir, 'dir')
        mkdir(target_dir)
    end
    for times = 1:aug_times
        for i = 1:size(imds.Files, 1)
            img = readimage(imds, i);
%             這邊會改變影像的維度，原本應該是認為沒辦法坐RGB影像才轉成灰階
%             if numel(size(img))==3
%                 img = rgb2gray(img);
%             end
            image_size = size(img);
            if operation_type(operation(i, times)) == 1
                tform = randomAffine2d('Rotation', rotate);
                rout = affineOutputView(image_size,tform);
                aug_img  = imwarp(img,tform,'OutputView',rout);
                if tform.T(2, 1)>0
                    first_non_zero_index = find(aug_img(:, 1), 1);
                else
                    first_non_zero_index = find(aug_img(1, :), 1);
                end
                aug_img = aug_img(first_non_zero_index:end-first_non_zero_index-1, first_non_zero_index:end-first_non_zero_index-1);
                aug_img = imresize(aug_img, image_size, 'method', 'bilinear');
            elseif operation_type(operation(i, times)) == 2
                tform = randomAffine2d('XTranslation', x_translation);
                rout = affineOutputView(image_size,tform);
                aug_img  = imwarp(img,tform,'OutputView',rout);
                shift_num = ceil(abs(tform.T(3, 1)));
                if tform.T(3, 1)>0
                    aug_img(:, 1:shift_num) = aug_img(:, shift_num+1:shift_num+shift_num);
                else
                    aug_img(:, end-shift_num+1:end) = aug_img(:, end-shift_num-shift_num+1:end-shift_num);
                end
            elseif operation_type(operation(i, times)) == 3
                tform = randomAffine2d('YTranslation', y_translation);
                rout = affineOutputView(image_size,tform);
                aug_img  = imwarp(img,tform,'OutputView',rout);
                shift_num = ceil(abs(tform.T(3, 2)));
                if tform.T(3, 2)>0
                    aug_img(1:shift_num, :) = aug_img(shift_num+1:shift_num+shift_num, :);
                else
                    aug_img(end-shift_num+1:end, :) = aug_img(end-shift_num-shift_num+1:end-shift_num, :);
                end
            elseif operation_type(operation(i, times)) == 4
                rng;
                bright = rand(1)*(brightness(2)-brightness(1))+brightness(1);
                aug_img = img*bright;
            end
            imwrite(aug_img, [target_dir '\' num2str((times)*size(imds.Files, 1)+i) '.png'], 'PNG');
        end
    end
end