function [] = dataRename(folder, before, after)
    % Get all PDF files in the current folder
    files = dir([folder '/*' before '*.dat']);
    % Loop through each
    for id = 1:length(files)
        oldName = files(id).name;
        demIndex = strfind(oldName, before);
        newName = [oldName(1:demIndex-1) after oldName(demIndex+size(before, 2):end)];
        movefile([folder '/' oldName], [folder '/' newName]);
    end
end

