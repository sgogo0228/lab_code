% clear
% clc
% close all
%%
% 能夠一次分析好幾組shear wave

is_output = false;
% fileSet = {'L:\黃大銘實驗資料\contusion\20180129_contusion_d3\rat#2_contusion_left\30MHz_dist0to10_depth9to14k_amp600_cycle5100_1_SW0'};
fileSet = readcell('K:\黃大銘實驗資料\contusion\path_right.txt', 'Delimiter', ',');

distPostfix_skin = {'0', '0p5', '1', '1p5', '2', '2p5', '3', '3p5', '4', '4p5'};
distPostfix_muscle = mat2cell(0:99, 1, 100);

fileSet = cellfun(@(x) (x), fileSet(:, 1), 'UniformOutput', false);
params = headFile_NI([strtrim(fileSet{1}) '.dat']);
push_num = 5;                           %在固定的蒐集時間內(params.aline)，push打了幾次，跟Push的PRF有關
position_num = 10;                      %蒐集多少個position的signal
interp_dist_fold = 1;                  %內差的倍數
interp_time_fold = 1;
interp_depth_fold = 1;
interp_dist_pts = (position_num) * interp_dist_fold;
push_noise=15;                          %push干擾detect的點數
baseFrequency=50;
hormonicNum = 5;                      %kalman filter故估計頻率的上限(如果push的PRF是50Hz，那就會最高抓到50*此變數)
% slopeFitDotNum = [6 8];               %最後計算斜率時參考的點數
% depth = [1 params.DataLength];
depth = [1 7000];
detect_sampling_rate=10000;   %千萬不要打錯!! 是指detect探頭的PRF
analyzeInterval = 100;                  % 經驗上分析肌肉的SW 100或200(以上也可以500)都可以，太少變異大
W0 = 30*1e6;
unitAline = params.Aline/push_num;      %push打一次所需要的時間
modelParams = struct('unitAline', unitAline, 'push_num', push_num, 'usedCycle', 3, 'front_noise', push_noise-3, 'backNoise', 3, 'damping', 0);


overall_depth = depth(2)-depth(1)+1;
fnum = size(fileSet, 1);
bandFilter = [baseFrequency*0.7 baseFrequency*6];
modifiedUnitAline = (modelParams.unitAline-modelParams.front_noise-modelParams.backNoise-modelParams.damping);
unusedCycle=modelParams.push_num-modelParams.usedCycle;
sw_data_num = 1;

for f = 1:fnum
    filename = [strtrim(fileSet{f}) '.dat'];
    if ~exist(filename, 'file')
        f
        disp(filename);
        return;
    end

end

% gcp
D = size(fnum, 1);
% sgn = zeros(modifiedUnitAline, overall_depth/analyzeInterval*interp_depth_fold, interp_dist_pts, fnum);
h = waitbar(1/position_num, ['f = 1 (1/' num2str(position_num) ')']);

addpath('npy_matlab');

for f = 1:fnum
    params = headFile_NI([strtrim(fileSet{f}) '.dat']);
    if params.Delay > 8000
        depth = [1 5000];
    else
        depth = [1 7000];
    end
    overall_depth = depth(2)-depth(1)+1;

    temp_sgn = zeros(modifiedUnitAline*push_num*interp_time_fold, depth(2)-depth(1)+1, position_num);
    waitbar(1/position_num, h, ['f = ' num2str(f) '(1/' num2str(position_num) ')']);
    parfor j=1:position_num
        %如果要下parfor這行要註解掉
        % waitbar(j/position_num, h, ['f = ' num2str(f) '(' num2str(j) '/' num2str(position_num) ')']);

%         %  --for skin scanning protocol
%         filename = [regexprep(strtrim(fileSet{f}), 'dist0', ['dist' num2str(distPostfix_skin{1}(j))]) '.dat'];
 
        %   --for muscle scanning protocol
        filename = [regexprep(strtrim(fileSet{f}), 'SW0', ['SW' num2str(distPostfix_muscle{1}(j))]) '.dat'];
        [sgnParam, temp_sgn(:, :, j)]= shear_wave_demod_20230717(filename, depth, unitAline, push_num, push_noise, W0, interp_time_fold);
%         [sgnParam, temp_sgn(:, :, j)]= shear_wave_NCC_20230703(filename, depth, unitAline, push_num, push_noise, 50, interp_time_fold);
        
    end
    [paramB, paramA] = butter(5, bandFilter(2)/(interp_time_fold*detect_sampling_rate/2));    %第一個參數須視情況調整
    temp_sgn = filtfilt(paramB, paramA, temp_sgn);

    %   這段是用來輸出learning用的資料.npy檔(depth範圍只有1000)
    if is_output
        save_dir = 'D:\DaMing\sw_data\all_data';
        [~, us_rf, ~, ~, ~, I, Q] = get_us_data([strtrim(fileSet{f}) '.dat'], 'is_save', 0);
        idx = [unitAline*0+1:unitAline*0+15, ...
               unitAline*1+1:unitAline*1+15, ...
               unitAline*2+1:unitAline*2+15, ...
               unitAline*3+1:unitAline*3+15, ...
               unitAline*4+1:unitAline*4+15];
        us_rf(:, idx, :) = [];
        I(:, idx, :) = [];
        Q(:, idx, :) = [];
       
        for i=1:position_num
            writeNPY(single(us_rf(depth(1):depth(2), :, i)), [save_dir '\data\' num2str(sw_data_num) '.npy']);
            writeNPY(single(I(depth(1):depth(2), :, i)), [save_dir '\I\' num2str(sw_data_num) '.npy']);
            writeNPY(single(Q(depth(1):depth(2), :, i)), [save_dir '\Q\' num2str(sw_data_num) '.npy']);
            % writeNPY(temp_sgn(:, :, i)', [save_dir '\label\' num2str(sw_data_num) '.npy']);
            % writeNPY(temp_sgn(:, :, i)', [save_dir '\label_NCC\' num2str(sw_data_num) '.npy']);
            sw_data_num = sw_data_num + 1;
        end
    end
    
    temp_sgn = reshape(temp_sgn, modifiedUnitAline, push_num, analyzeInterval, overall_depth/analyzeInterval, position_num);
    temp_sgn = squeeze(mean(temp_sgn(:, 2:4, :, :, :), [2, 3]));

    sgn{f} = my_interp_23D(temp_sgn, [interp_time_fold interp_depth_fold interp_dist_fold]);

%     sgn = sgn - min(sgn, [], [1]);
%     sgn = sgn ./ max(sgn, [], [1]);
    
% %     --為計算以voltage為單位的SW振福，用到的參數beta、W0、D都跟2007這篇一樣
%     temp.beta = max(sgn(:, :, :, f));
%     D(f) = temp.beta*1540/2/W0;
end
close(h);
delete(gcp('nocreate'));
clear temp
backup = sgn;
sgn = backup;

%%
% clear x y
% interp_time_fold = 3;
% CC_window = [round(size(sgn, 1)*interp_time_fold*0.8):round(size(sgn, 1)*interp_time_fold*1.2)];
% anal_pos = [1:size(sgn, 3)];
% anal_depth = [1:size(sgn, 2)];
% for f=1:fnum
%     clear C
%     [temp.X,temp.Y,temp.Z] = meshgrid(1:size(sgn, 2), 1:size(sgn, 1), 1:size(sgn, 3));
%     [temp.Xq,temp.Yq,temp.Zq] = meshgrid(1:size(sgn, 2), linspace(1, size(sgn, 1), size(sgn, 1)*interp_time_fold), 1:size(sgn, 3));
%     temp.interp_sgn = interp3(temp.X,temp.Y,temp.Z, sgn(:, :, :, f), temp.Xq,temp.Yq,temp.Zq, "cubic");
%     phaseAngle = zeros(interp_dist_pts, size(sgn, 2), hormonicNum);
%     for r=1:size(sgn, 2)
%         temp.xcr = xcorr(squeeze(temp.interp_sgn(:, r, :)));
%         temp.xcr = temp.xcr(:, 1:interp_dist_pts);
%         [~, temp.I(:, r)] = max(temp.xcr(CC_window, :), [], 1);
%     end
%     temp.maxCCindex = temp.I - size(sgn, 1)*interp_time_fold + CC_window(1) - 1;
%     phaseAngle(:, :, 1) = temp.maxCCindex / interp_time_fold * 0.1;     %一個點代表0.1 ms
% 
%     [x, y(:, :, f)] = linearFitForStatistic20220216(phaseAngle, anal_pos, anal_depth);
%     
% %     [path, ~, ~] = fileparts(strtrim(fileSet{f}));
% %     if exist([path(1:end-10) '\avtech_30MHz_amp1p5_freq2_pos6_B0.dat'])
% % 
% %         [imgs, ~, ~, xAxis, yAxis] = get_us_data([path(1:end-10) '\avtech_30MHz_amp1p5_freq2_pos6_B0'], 'dynamic_range', 45);
% % %         image(xAxis, yAxis, imgs(:, :, 1))
% %         hold on
% %         xAxis = linspace(5, 9.5, size(sgn, 1)*interp_time_fold);
% %         yAxis = ((depth(1):depth(2))+params.Delay)/params.SamplingRate/1e6/2*1540*1000;
% %         h = image(xAxis, yAxis+0.2, 257 + abs(y(:, :, f))'/4 * 256);
% %         set(h, 'AlphaData', 0.4);
% %         colormap([gray(256); turbo(256)])
% %         colorbar
% %         axis image
% %         axis([4 11 7 12])
% %         [X,Map] = rgb2ind(frame2im(getframe(gcf)), 256);
% %         savePath = ['L:\黃大銘實驗資料\skin\temp\' num2str(f) '_withoutWindow.png'];
% %         imwrite(X, Map, savePath, 'PNG');
% %     end
% %     xAxis = linspace(5, 9.5, size(sgn, 1)*interp_time_fold);
% %     yAxis = ((depth(1):depth(2))+params.Delay)/params.SamplingRate/1e6/2*1540*1000;
% 
%     C{1, 1} = 'position';
%     for d=1:length(anal_depth)
%         d_mm = round((((d - 1) * analyzeInterval / interp_depth_fold) + depth(1) + params.Delay - 1) / 2 / params.SamplingRate / 1e6 * 1000 * 1540, 2);
%         C{1, d+1} = ['depth_' num2str(d_mm)];
%     end
%     C = [C; num2cell([x' abs(y(:, :, f))])];
% 
%     [path, name, ~] = fileparts(strtrim(fileSet{f}));
%     if exist([path '_' name '_velocity.xlsx'])
%         delete([path '_' name '_velocity.xlsx']);
%     end
% %     if exist(['L:\黃大銘實驗資料\skin\temp\' num2str(f) '_withoutWindow.xlsx'])
% %         delete(['L:\黃大銘實驗資料\skin\temp\' num2str(f) '_withoutWindow.xlsx']);
% %     end
%     writecell(C, [path '_' name '_velocity.xlsx']);
% %     writecell(C, ['L:\黃大銘實驗資料\skin\temp\' num2str(f) '_withoutWindow.xlsx']);
%     close all
% end

