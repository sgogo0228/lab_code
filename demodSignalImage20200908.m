% clear
clc
% close all
%%
% 能夠一次分析好幾組shear wave
fileSet = char('L:\G9\20220203_ratskin_d28\shearwave\avtech_30MHz_dist0_depth9to17k_amp600_cycle0_1_SW0',...
            'L:\G9\20220203_ratskin_d28\shearwave\avtech_30MHz_dist0_depth9to17k_amp600_cycle3400_1_SW0',...
            'L:\G9\20220203_ratskin_d28\shearwave\avtech_30MHz_dist0_depth9to17k_amp600_cycle3400_2_SW0');

params = headFile_NI([strtrim(fileSet(1, :)) '.dat']);
push_num = 5;                           %在固定的蒐集時間內(params.aline)，push打了幾次，跟Push的PRF有關
position_num = 10;                      %蒐集多少個position的signal
interp_dist_pts = 100;                        %內差的倍數
push_noise=15;                          %push干擾detect的點數
baseFrequency=50;
% hormonicNum = 5;                      %kalman filter故估計頻率的上限(如果push的PRF是50Hz，那就會最高抓到50*此變數)
% slopeFitDotNum = [6 8];               %最後計算斜率時參考的點數
unitAline = params.Aline/push_num;      %push打一次所需要的時間
depth = [2501 5500];
detect_sampling_rate=10000;   %千萬不要打錯!! 是指detect探頭的PRF
modelParams = struct('unitAline', unitAline, 'push_num', push_num, 'usedCycle', 3, 'front_noise', push_noise-3, 'backNoise', 3, 'damping', 0);
analyzeInterval = 10;                  % 經驗上分析肌肉的SW 100或200(以上也可以500)都可以，太少變異大

overall_depth = depth(2)-depth(1)+1;
analysis_region = [1 overall_depth/analyzeInterval];   %可能不會想整個深度都分析，可自設區間
fnum = size(fileSet, 1);
bandFilter = [baseFrequency*0.7 baseFrequency*6];
modifiedUnitAline = (modelParams.unitAline-modelParams.front_noise-modelParams.backNoise-modelParams.damping);
unusedCycle=modelParams.push_num-modelParams.usedCycle;

distPostfix = {'0', '0p5', '1', '1p5', '2', '2p5', '3', '3p5', '4', '4p5'};

for i=1:depth(2)/analyzeInterval
    range(i, 1) = (i-1)*analyzeInterval+1;
    range(i, 2) = i*analyzeInterval;
end

for f = 1:fnum
    filename = [strtrim(fileSet(f, :)) '.dat'];
    if ~exist(filename, 'file')
        f
        disp(filename);
        return;
    end
end


% gcp
sgn = zeros(modifiedUnitAline*push_num, analysis_region(2)-analysis_region(1)+1, interp_dist_pts, fnum);
h = waitbar(1/position_num, ['f = 1 (1/' num2str(position_num) ')']);
for f=1:fnum
    temp.sgn = zeros(modifiedUnitAline*push_num, overall_depth, position_num);
    waitbar(1/position_num, h, ['f = ' num2str(f) '(1/' num2str(position_num) ')']);
    for j=1:position_num
        waitbar(j/position_num, h, ['f = ' num2str(f) '(' num2str(j) '/' num2str(position_num) ')']);
        filename = [regexprep(strtrim(fileSet(f, :)), 'dist0', ['dist' num2str(distPostfix{j})]) '.dat'];
        [sgnParam, temp.sgn(:, :, j)]= shearWaveByDemod20220211(filename, depth, unitAline, push_num, push_noise);
    end
    [paramB, paramA] = butter(5, bandFilter/(detect_sampling_rate/2));    %第一個參數須視情況調整
    temp.sgn = filtfilt(paramB, paramA, temp.sgn);
    
%     [X,Y,Z] = meshgrid(1:size(temp.sgn, 2), 1:size(temp.sgn, 1), 1:size(temp.sgn, 3));
%     [Xq,Yq,Zq] = meshgrid(1:size(temp.sgn, 2), 1:size(temp.sgn, 1), linspace(1, size(temp.sgn, 3), position_num*interp_time));
%     temp.sgn = interp3(X, Y, Z, temp.sgn, Xq, Yq, Zq);

    for r = 1:analysis_region(2)-analysis_region(1)+1
        temp.a(:, r, :) = mean(temp.sgn(:, range(r, 1):range(r, 2), :), 2);
%         sgn(:, r, :, f) = squeeze(mean(temp.sgn(:, range(r, 1):range(r, 2), :), 2));
    end
    [temp.X,temp.Y,temp.Z] = meshgrid(1:size(temp.a, 2), 1:size(temp.a, 1), 1:size(temp.a, 3));
    [temp.Xq,temp.Yq,temp.Zq] = meshgrid(1:size(temp.a, 2), 1:size(temp.a, 1), linspace(1, size(temp.a, 3), interp_dist_pts));
    sgn(:, :, :, f) = interp3(temp.X, temp.Y, temp.Z, temp.a, temp.Xq, temp.Yq, temp.Zq, "cubic");
end
close(h);
delete(gcp('nocreate'));
clear temp
backup = sgn;
sgn = backup;

% 將第2到第4個push收到的SW，第1跟第5個因為較不穩定不列入分析
sgn = reshape(sgn, modifiedUnitAline, push_num, analysis_region(2)-analysis_region(1)+1, interp_dist_pts, fnum);
sgn = squeeze(mean(sgn(:, 2:4, :, :, :), 2));

%% 輸出SW image
% sgn(時間, 深度, detect距離, 檔案) 
clear imgSgn
figure('color', 'white')
graphInterval = 2;
colorScale = [0 0.2];

% 輸出2D的SW圖
for f=13:15
    i=1;
    imgSgn(:, :, :, f) = (sgn(:, :, :, f)-min(sgn(:, :, :, f), [], [1]));
    [filepath, filename, ~] = fileparts(fileSet(f, :));
    savePath = [filepath '\gif_' strtrim(filename) '.gif'];
    param = headFile_NI([strtrim(fileSet(f, :)) '.dat']);
    xAxis = (0:position_num-1)*0.5;
    yAxis = (((depth(1):depth(2))+param.Delay)/1000000000/2*1540*1000);
    if exist(savePath, 'file')==2
        delete(savePath);
    end

    while i<=modifiedUnitAline                % 任何一個 loop 
        imagesc(xAxis, yAxis, squeeze((imgSgn(i, :, :, f))));
%         axis
%         colormap jet(256)
        caxis(colorScale)
        title([num2str(i*0.1) 'ms'])
        set(gca, 'FontSize', 24, 'FontName', 'Times', 'FontWeight', 'bold')
        xlabel('Distance (mm)', 'FontSize', 28, 'FontWeight', 'bold')
        ylabel('Depth (mm)', 'FontSize', 28, 'FontWeight', 'bold')
        axis image
        [X,Map] = rgb2ind(frame2im(getframe(1)), 256);
        %在 loop內 的writemode 須改為 append ,這時候frame才會不斷的增加進去
        if i==1
            imwrite(X, Map, savePath, 'GIF', 'WriteMode', 'overwrite', 'DelayTime', 0, 'LoopCount', Inf);
        else
            imwrite(X, Map, savePath,'GIF','WriteMode','append','DelayTime', 0);
        end
        i = i+graphInterval;
    end
end

%% 輸出3D的SW圖
colorScale = [0 0.3];
temp.b = permute(movmedian(permute(imgSgn, [2 1 3 4]), 11), [2 1 3 4]);

figure('position', [900 200 670 500], 'color', 'white')

for f=1:fnum
    i=1;
    [filepath, filename, ~] = fileparts(fileSet(f, :));
    savePath = [filepath '\3Dgif_' strtrim(filename) '.gif'];
    if exist(savePath, 'file')==2
        delete(savePath);
    end
    while i<=modifiedUnitAline                % 任何一個 loop
        surf(squeeze((temp.b(i, :, :, f)))', 'EdgeColor', 'black', 'LineStyle', ':', 'FaceColor', 'interp');
        zlim(colorScale)
        caxis(colorScale)
        view(-45, -60);
        title(num2str(i))
        xlabel('Distance (mm)', 'FontSize', 14)
        ylabel('Depth (mm)', 'FontSize', 14)
        pause(0.5)

        [X,Map] = rgb2ind(frame2im(getframe(gcf)), 256);
        if i==1
            imwrite(X, Map, savePath, 'GIF', 'WriteMode', 'overwrite', 'DelayTime', 0, 'LoopCount', Inf);
        else
            imwrite(X, Map, savePath, 'GIF', 'WriteMode', 'append','DelayTime', 0);
        end
        i = i+graphInterval;
    end
end

%%

for r=1:analysisDistRange(2)-analysisDistRange(1)+1
    maxCCindex = zeros(fnum, distNum);
    phaseAngle = zeros(fnum, 20, hormonicNum);
    maxCCindex(:, 1) = size(sgn, 4);
    for f=1:fnum
        xcr = zeros(distNum, 2*size(sgn, 4)-1);
        for d=2:distNum
            xcr(d, :) = xcorr(squeeze(sgn(f, 1, r, :)), squeeze(sgn(f, d, r, :)));
            [~, I] = findpeaks(squeeze(xcr(d, :)));
            [~, tempIdx] = min(abs(I-maxCCindex(f, d-1)));
            maxCCindex(f, d) = I(tempIdx);
        end
    end
    maxCCindex = maxCCindex-size(sgn, 4);
    phaseAngle(:, 1:distNum, 1) = maxCCindex*2*pi/(detectSamplingRate/baseFrequency); %2*pi/(detectSamplingRate/baseFrequency)用於計算一個detect
    phaseAngle = permute(phaseAngle, [2 3 1]);

    phase_50hz(r, :, :) = linearFitForStatistic20181210(analyzeFilePath, ['0_analyze_20170403_sample (' num2str(r) ')'], [1 fnum], phaseAngle, slopeFitDotNum);
end

tempPhase = [];
for i=1:length(slopeFitDotNum)
    tempPhase = [tempPhase; squeeze(phase_50hz(:, i, :))];
end
phase_50hz = tempPhase;
myXlswrite([analyzeFilePath '/variousRangedDepth.xlsx' ], baseFrequency*360*0.001./phase_50hz, 'velocity', 'B1');