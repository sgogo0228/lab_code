function [params, demodSignal] = shearWaveByDemod20200908(filename, depth, unitAline, pushNum, push_noise)
    % nfft = 4096;
    % Ū���ɮ�
    params = headFile_NI(filename);
    data = params.rf;
    data = data*params.Vpp/255;

    % demodulation
    [I, Q] = demod(data,30*1e6,1e9,'qam');
    temp = -atan(Q./I);
    shift_I = zeros(size(I));
    shift_Q = zeros(size(Q));
    for d=depth(1):depth(2)
        shift_phase = mean(rmoutliers(temp(d, :), 'ThresholdFactor',2));
        shift_I(d, :) = I(d, :)*cos(shift_phase)-Q(d, :)*sin(shift_phase);
        shift_Q(d, :) = Q(d, :)*cos(shift_phase)+I(d, :)*sin(shift_phase);
    end
    temp = permute(-atan(shift_Q(depth(1):depth(2), :)./shift_I(depth(1):depth(2), :)), [2 1]);
    temp = reshape(temp, unitAline, [], depth(2)-depth(1)+1);
    temp(1:push_noise, :, :) = [];
    demodSignal = reshape(temp, size(temp, 1)*size(temp, 2), depth(2)-depth(1)+1);
end

