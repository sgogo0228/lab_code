function [X, kalmanInput, filteredSignal, xMinus, G, gainTemp] = kalman20160808(signal, detectSamplingRate, hormonicNum, baseFrequency, modelParams)
%     close all

    %build a modified model
    pushNum = modelParams.pushNum;
    unitAline = modelParams.unitAline;
    frontNoise = modelParams.frontNoise;
    backNoise = modelParams.backNoise;
    damping = modelParams.damping;
    modifiedModelUnitAline = unitAline-frontNoise-backNoise-damping;
    t0 = (0:unitAline-1)/detectSamplingRate;
    H0 = zeros(unitAline, 2*hormonicNum);
    for i=1:hormonicNum
        H0(:, 2*i-1) = sin(2*pi*baseFrequency*i*t0)';
        H0(:, 2*i) = cos(2*pi*baseFrequency*i*t0)';
    end
    H0(unitAline-backNoise-damping+1:unitAline, :) = [];
    H0(1:frontNoise, :) = [];
    H = zeros(modifiedModelUnitAline*pushNum, 2*hormonicNum);
    for i=0:pushNum-1
        H(1+modifiedModelUnitAline*i:modifiedModelUnitAline+modifiedModelUnitAline*i, :) = H0;
    end
    
    kalmanInput = signal;
    % 贺把计﹍て
    % 场だ把酚Kalman Filter Motion Detection for Vibro-acoustography using Pulse Echo Ultrasound 
    xMinus = zeros(2*hormonicNum, modifiedModelUnitAline*pushNum);
    gainTemp = zeros(modifiedModelUnitAline*pushNum);
    G = zeros(2*hormonicNum, modifiedModelUnitAline*pushNum);
    X = zeros(2*hormonicNum, modifiedModelUnitAline*pushNum);
%     X(1, 1) = 1;
%     X(3, 1) = 1;
%     X(5, 1) = 1;
%     X(7, 1) = 1;
%     X(9, 1) = 1;
    P = 2*hormonicNum*eye(2*hormonicNum);
    F = eye(2*hormonicNum);
    Q = 0.01*eye(2*hormonicNum);                 %%modelvariance
    R = 100;                                     %%代秖variance
    
% kalmanfilter
    for i=2:modifiedModelUnitAline*pushNum
        xMinus(:, i) = F*X(:, i-1);
        pMinus = F*P*F'+Q;
        G(:, i) = pMinus*H(i, :)'/(H(i, :)*pMinus*H(i, :)'+R);
        gainTemp(i) = H(i, :)*xMinus(:, i);
        X(:, i) = xMinus(:, i)+G(:, i)*(kalmanInput(i)-gainTemp(i));
        P = (eye(2*hormonicNum)-G(:, i)*H(i, :))*pMinus;
        
%         filteredSignal = zeros(1, time);
%         for j=1:2:(2*signalNum-1)
%             filteredSignal = filteredSignal+H(:, j)'.*X(j, :)+H(:, j+1)'.*X(j+1, :);
%         end
%         plot(i, filteredSignal(i), 'ro')
%         title(['time: ' num2str(i) ', kalmanInput: ' num2str(kalmanInput(i))  ', temp: ' num2str(temp)])
%         pause(5)
    end

    filteredSignal = zeros(1, modifiedModelUnitAline*pushNum);
    for i=1:2:(2*hormonicNum-1)
        filteredSignal = filteredSignal+H(:, i)'.*X(i, :)+H(:, i+1)'.*X(i+1, :);
    end
end