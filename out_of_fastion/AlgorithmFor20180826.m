clear
clc
% close all
%%
% 能夠一次分析好幾組shear wave
unitAline = 199;
pushNum = 5;
distNum = 10;
depth = [1 9000];
% CImage = '30MHz_dist0to5_depth5to14';
analyzeFilePath = ['H:\school\shearWaveData\contusion\20180409_contusion_d0\rat#head_contusion_right'];
% file = char('K:\黃大銘資料夾20180420前\school\shearWaveData\20151119_shearWave\gelatin3_dist0');
file = char('J:\temp\right\30MHz_dist0to5_depth7to16k_amp600_cycle5100_1');
pushNoise=10;
detectSamplingRate=10000;   %千萬不要打錯!! 是指detect探頭的PRF
hormonicNum = 5;
baseFrequency=50;
modelParams = struct('unitAline', unitAline, 'pushNum', pushNum, 'usedCycle', 3, 'frontNoise', pushNoise-3, 'backNoise', 3, 'damping', 0);
slopeFitDotNum = [6 8];
analysisDistRange = [1 36];

fnum = size(file, 1);
bandFilter = [baseFrequency*0.7 baseFrequency*6];
overallDepth = depth(2)-depth(1)+1;
modifiedUnitAline = (modelParams.unitAline-modelParams.frontNoise-modelParams.backNoise-modelParams.damping);
unusedCycle=modelParams.pushNum-modelParams.usedCycle;

analyzeInterval = 250;  %% 經驗上分析肌肉的SW 100或200(以上也可以500)都可以，太少變異大
for i=1:overallDepth/analyzeInterval
    range(i, 1) = (i-1)*analyzeInterval+1;
    range(i, 2) = i*analyzeInterval;
end
% xcorrDepth = 3;
sgn = zeros(fnum, distNum, analysisDistRange(2)-analysisDistRange(1)+1, unitAline*pushNum);

for f = 1:fnum
    filename = [strtrim(file(f, :)) '_SW0.dat'];
    if ~exist(filename, 'file')
        f
        disp(filename);
        return;
    end
end

% gcp;
    for f=1:fnum
        demodSignal = zeros(distNum, overallDepth, unitAline*pushNum);
        parfor j=0:distNum-1
            filename = [strtrim(file(f, :)) '_SW' num2str(j) '.dat'];
            [sgnParam demodSignal(j+1, :, :)]=shearWaveByDemod20170327(filename, depth, unitAline, pushNum, pushNoise);
        end
        for r = 1:analysisDistRange(2)-analysisDistRange(1)+1
            sgn(f, :, r, :) = squeeze(mean(demodSignal(:, range(r, 1):range(r, 2), :), 2));
        end
    end
delete(gcp('nocreate'));
temp = sgn;

% Macro_SWanalysis20181210;
% showCscanImage(path, CImage, 42);
a = b;