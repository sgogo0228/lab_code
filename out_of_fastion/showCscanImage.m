%{
paramin
    'is_save' 是否要執行figure並儲存在硬碟中
    'dynamic_range' DR
    'output_range' 指定顯示的x軸與z軸範圍
%}
function [us_image, xAxis, yAxis] = showCscanImage(fullPath, paramin)
    close all
    if nargin < 2
        paramin = struct();
    end
    if isfield(paramin,'is_save'), is_save = paramin.is_save; else, is_save = 1; end
    if isfield(paramin,'dynamic_range'), dynamic_range = paramin.dynamic_range; else, dynamic_range = 42; end
    [dirPath, fileName, ~] = fileparts(strtrim(fullPath));
    batchSize = length(dir([dirPath '\' fileName(1:end-1) '*.dat']));
    firstParam = headFile_NI([dirPath '\' fileName(1:end-1) '1.dat']);
    rf = firstParam.rf;
    for b = 2:batchSize
        param = headFile_NI([dirPath '\' fileName(1:end-1) num2str(b) '.dat']);
        rf(:, :, size(rf, 3)+1:size(rf, 3)+size(param.rf, 3)) = param.rf;
    end
    for i=2:2:size(rf, 3)
        rf(:, :, i) = fliplr(rf(:, :, i));
    end
    xAxis = (1:firstParam.Aline)*firstParam.XInterval/1000;
    yAxis = ((1:firstParam.DataLength)+firstParam.Delay)/2/(firstParam.SamplingRate*1e6)*1540*1000;
    if isfield(paramin,'output_range'), output_range = paramin.output_range; else, output_range = [xAxis(1) xAxis(end) yAxis(1) yAxis(end)]; end
    env_rf=abs(hilbert(rf));

    % 用demodulator實現demodulation
%     [I, Q] = demod(rf, 30*1e6, 1e9,'qam');
%     env_rf = abs(hilbert(I, Q));
    log_rf=20*log10(env_rf./max(env_rf, [], [1 2]));
    us_image=(log_rf+dynamic_range)/dynamic_range*255;
    
    if is_save
        figure('color', 'white'); 
        for i=1:size(us_image, 3)
            image(xAxis, yAxis, us_image(:, :, i))
            set(gca, 'FontSize', 16, 'FontName', 'Times', 'FontWeight', 'bold', 'XTick', round(xAxis(1)):1:xAxis(end), 'YTick', round(yAxis(1)):1:yAxis(end))
            xlabel('Distance (mm)', 'FontSize', 20, 'FontName', 'Times', 'FontWeight', 'bold')
            ylabel('Depth (mm)', 'FontSize', 20, 'FontName', 'Times', 'FontWeight', 'bold');
            colormap(gray(255))
            axis equal tight
            axis(output_range)
            [X,Map] = rgb2ind(frame2im(getframe(1)), 256);
            savePath = [dirPath '\cscan_image\' fileName '_' num2str(i) '.jpg'];
            if exist([dirPath '\cscan_image'], 'dir')==0
                mkdir([dirPath '\cscan_image']);
            end
            if exist(savePath, 'file')==2
                delete(savePath);
            end

            imwrite(X, Map, savePath, 'JPEG');
        end
    end
end

