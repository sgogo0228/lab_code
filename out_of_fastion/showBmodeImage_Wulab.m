function [img] = showBmodeImage_Wulab(filePath, dynamicRange, outputName)
    [dirPath, fileName, ~] = fileparts(filePath);
    if nargin < 3
        outputName = fileName;
    end
    
    param = headFile_NI_Wulab(filePath);
    rf = param.rf;
    rf=hilbert(rf);
    rf=abs(rf);
    log_rf=20*log10(rf/min(min(rf)));
    log_rf=log_rf-max(max(log_rf))+dynamicRange;
    img=255*log_rf/dynamicRange;
    xAxis = (1:param.Aline)*param.XInterval/1000;
    yAxis = ((1:param.DataLength)+param.Delay)/2/(param.SamplingRate*1e6)*1500*1000;
    figure('color', 'white')
    image(xAxis, yAxis, img)
    colormap(gray(255))
    axis equal tight
    xlabel('Distance (mm)', 'FontSize', 14, 'FontName', 'Arial')
    ylabel('Depth (mm)', 'FontSize', 14, 'FontName', 'Arial');
%     axis equal tight
    [X,Map] = rgb2ind(frame2im(getframe(1)), 256);
    savePath = [dirPath '\' outputName '.jpg'];
    if exist(savePath, 'file')==2
        delete(savePath);
    end

    imwrite(X, Map, savePath, 'JPEG');
end

