clear
clc
close all
%%
% 能夠一次分析好幾組shear wave
unitAline = 199;
pushNum = 5;
distNum = 10;
depth = [1 5000];
CImage = '30MHz_dist0to5_depth5to14';
path = ['L:\黃大銘資料夾20181028\school\shearWaveData\20180421_contusion_d11\rat#head_contusion_right\'];
file = char('30MHz_dist0to5_depth9to14k_amp600_cycle5100_1',...
    '30MHz_dist0to5_depth9to14k_amp600_cycle5100_2',...
    '30MHz_dist0to5_depth9to14k_amp600_cycle5100_3');
pushNoise=20;
detectSamplingRate=10000;
hormonicNum = 5;
baseFrequency=50;
modelParams = struct('unitAline', unitAline, 'pushNum', pushNum, 'usedCycle', 3, 'frontNoise', pushNoise-3, 'backNoise', 3, 'damping', 0);

fnum = size(file, 1);
bandFilter = [baseFrequency*0.7 baseFrequency*6];
OverallDepth = depth(2)-depth(1)+1;
modifiedUnitAline = (modelParams.unitAline-modelParams.frontNoise-modelParams.backNoise-modelParams.damping);
unusedCycle=modelParams.pushNum-modelParams.usedCycle;

demodSignal = zeros(fnum, distNum, OverallDepth, unitAline*pushNum);
pool = parpool(4);
% matlabpool local 4
for f=1:fnum
    parfor j=0:distNum-1
        filename = [path  strtrim(file(f, :)) '_SW' num2str(j) '.dat'];
        demodSignal(f, j+1, :, :)=shearWaveByDemod20170327(filename, depth, unitAline, pushNum, pushNoise);
    end
end
% matlabpool close
delete(pool);

% Macro_SWanalysis20180425;
% showCscanImage(path, CImage, 42);
a = b;