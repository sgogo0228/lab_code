function [img] = showBmodeImage(filePath, dynamicRange, outputName)
    [dirPath, fileName, ~] = fileparts(filePath);
    if nargin < 3
        outputName = fileName;
    end
    
    param = headFile_NI(filePath);
    rf = param.rf;
    rf=hilbert(rf);
    rf=abs(rf);
    log_rf=20*log10(rf/min(min(rf)));
    log_rf=log_rf-max(max(log_rf))+dynamicRange;
    img=255*log_rf/dynamicRange;
    xAxis = (0:994)*0.1;
    yAxis = ((1:param.DataLength)+param.Delay)/2/(param.SamplingRate*1e6)*1500*1000;
    figure('color', 'white')
    image(xAxis, yAxis(1:3000), img(1:3000, :))
    colormap(gray(255))
%     axis equal tight
    xlabel('Time (ms)', 'FontSize', 16, 'FontName', 'Times', 'FontWeight', 'bold')
    ylabel('Depth (mm)', 'FontSize', 16, 'FontName', 'Times', 'FontWeight', 'bold');
%     axis equal tight
    [X,Map] = rgb2ind(frame2im(getframe(1)), 256);
    savePath = [dirPath '\' outputName '.jpg'];
    if exist(savePath, 'file')==2
        delete(savePath);
    end

    imwrite(X, Map, savePath, 'JPEG');
end

