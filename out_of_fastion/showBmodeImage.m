function [xAxis, yAxis, img] = showBmodeImage(filePath, outputRange, outputName, dynamicRange)
    [dirPath, fileName, ~] = fileparts(filePath);
    if nargin < 3
        outputName = fileName;
        dynamicRange = 42;
    end
    
    directionNum = regexp(regexp(filePath, '_B\d+.dat', 'match'), '\d+', 'match');
    param = headFile_NI(filePath);
%     if mod(str2double(directionNum{1}), 2)==0
        rf = param.rf;
%     else
%         rf = fliplr(param.rf);
%     end
    rf=hilbert(rf);
    rf=abs(rf);
    log_rf=20*log10(rf/min(min(rf)));
    log_rf=log_rf-max(max(log_rf))+dynamicRange;
    img=255*log_rf/dynamicRange;
    xAxis = (0:param.Aline)*param.XInterval/1000;
    yAxis = ((1:param.DataLength)+param.Delay)/2/(param.SamplingRate*1e6)*1540*1000;
    if nargin < 2
        outputRange = [xAxis(1) xAxis(end) yAxis(1) yAxis(end)];
    end
    figure('color', 'white')
%     image(img)
    image(xAxis, yAxis, img)
    colormap(gray(255))
    colorbar
    axis image
    axis([outputRange(1) outputRange(2) outputRange(3) outputRange(4)])
    set(gca, 'FontSize', 16, 'FontName', 'Times', 'FontWeight', 'bold', 'XTick', round(xAxis(1)):1:xAxis(end), 'YTick', round(yAxis(1)):1:yAxis(end))
    xlabel('Distance (mm)', 'FontSize', 20, 'FontName', 'Times', 'FontWeight', 'bold', 'fontName', 'Times')
    ylabel('Depth (mm)', 'FontSize', 20, 'FontName', 'Times', 'FontWeight', 'bold', 'fontName', 'Times');
    [X,Map] = rgb2ind(frame2im(getframe(gcf)), 256);
    savePath = [dirPath '\Bmode_' outputName '.jpg'];
    if exist(savePath, 'file')==2
        delete(savePath);
    end

    imwrite(X, Map, savePath, 'JPEG');
end

