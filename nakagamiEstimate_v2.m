% figure
% close all
% clear;
% clc

% 如果是Cscan只需要輸入第一筆(_C1.dat)的檔名就好
% % 如果檔名顯示有衰減過，需要在讀檔時補償回去
stainlessBlockData = 'K:\黃大銘實驗資料\skinflap\20190624_XDCR_30MHz\30MHz#2_atten10dB_B1.dat';
fileSet = char('L:\黃大銘實驗資料\skinflap\G5(female rats)\20190624_skinflap_post\no1\30MHz#2_0to6mm_C1.dat',...
    'L:\黃大銘實驗資料\skinflap\G5(female rats)\20190624_skinflap_post\no1\30MHz#2_6to12mm_C1.dat',...
    'L:\黃大銘實驗資料\skinflap\G5(female rats)\20190624_skinflap_post\no1\30MHz#2_12to18mm_C1.dat',...
    'L:\黃大銘實驗資料\skinflap\G5(female rats)\20190624_skinflap_post\no1\30MHz#2_18to24mm_C1.dat',...
    'L:\黃大銘實驗資料\skinflap\G5(female rats)\20190624_skinflap_post\no1\30MHz#2_24to30mm_C1.dat',...
    'L:\黃大銘實驗資料\skinflap\G5(female rats)\20190625_skinflap_d1\no1_butt\30MHz#2_0to6mm_C1.dat',...
    'L:\黃大銘實驗資料\skinflap\G5(female rats)\20190625_skinflap_d1\no1_butt\30MHz#2_6to12mm_C1.dat',...
    'L:\黃大銘實驗資料\skinflap\G5(female rats)\20190625_skinflap_d1\no1_butt\30MHz#2_12to18mm_C1.dat',...
    'L:\黃大銘實驗資料\skinflap\G5(female rats)\20190625_skinflap_d1\no1_butt\30MHz#2_18to24mm_C1.dat',...
    'L:\黃大銘實驗資料\skinflap\G5(female rats)\20190625_skinflap_d1\no1_butt\30MHz#2_24to30mm_C1.dat',...
    'L:\黃大銘實驗資料\skinflap\G5(female rats)\20190627_skinflap_d3\no1_butt\30MHz#2_0to6mm_C1.dat',...
    'L:\黃大銘實驗資料\skinflap\G5(female rats)\20190627_skinflap_d3\no1_butt\30MHz#2_6to12mm_C1.dat',...
    'L:\黃大銘實驗資料\skinflap\G5(female rats)\20190627_skinflap_d3\no1_butt\30MHz#2_12to18mm_C1.dat',...
    'L:\黃大銘實驗資料\skinflap\G5(female rats)\20190627_skinflap_d3\no1_butt\30MHz#2_18to24mm_C1.dat',...
    'L:\黃大銘實驗資料\skinflap\G5(female rats)\20190627_skinflap_d3\no1_butt\30MHz#2_24to30mm_C1.dat',...
    'L:\黃大銘實驗資料\skinflap\G5(female rats)\20190629_skinflap_d5\no1_butt\30MHz#2_0to6mm_C1.dat',...
    'L:\黃大銘實驗資料\skinflap\G5(female rats)\20190629_skinflap_d5\no1_butt\30MHz#2_6to12mm_C1.dat',...
    'L:\黃大銘實驗資料\skinflap\G5(female rats)\20190629_skinflap_d5\no1_butt\30MHz#2_12to18mm_C1.dat',...
    'L:\黃大銘實驗資料\skinflap\G5(female rats)\20190629_skinflap_d5\no1_butt\30MHz#2_18to24mm_C1.dat',...
    'L:\黃大銘實驗資料\skinflap\G5(female rats)\20190629_skinflap_d5\no1_butt\30MHz#2_24to30mm_C1.dat',...
    'L:\黃大銘實驗資料\skinflap\G5(female rats)\20190701_skinflap_d7\no1_butt\30MHz#2_0to6mm_C1.dat',...
    'L:\黃大銘實驗資料\skinflap\G5(female rats)\20190701_skinflap_d7\no1_butt\30MHz#2_6to12mm_C1.dat',...
    'L:\黃大銘實驗資料\skinflap\G5(female rats)\20190701_skinflap_d7\no1_butt\30MHz#2_12to18mm_C1.dat',...
    'L:\黃大銘實驗資料\skinflap\G5(female rats)\20190701_skinflap_d7\no1_butt\30MHz#2_18to24mm_C1.dat',...
    'L:\黃大銘實驗資料\skinflap\G5(female rats)\20190701_skinflap_d7\no1_butt\30MHz#2_24to30mm_C1.dat',...
    'L:\黃大銘實驗資料\skinflap\G5(female rats)\20190625_skinflap_d1\no2\30MHz#2_0to6mm_C1.dat',...
    'L:\黃大銘實驗資料\skinflap\G5(female rats)\20190625_skinflap_d1\no2\30MHz#2_6to12mm_C1.dat',...
    'L:\黃大銘實驗資料\skinflap\G5(female rats)\20190625_skinflap_d1\no2\30MHz#2_12to18mm_C1.dat',...
    'L:\黃大銘實驗資料\skinflap\G5(female rats)\20190625_skinflap_d1\no2\30MHz#2_18to24mm_C1.dat',...
    'L:\黃大銘實驗資料\skinflap\G5(female rats)\20190625_skinflap_d1\no2\30MHz#2_24to30mm_C1.dat',...
    'L:\黃大銘實驗資料\skinflap\G5(female rats)\20190627_skinflap_d3\no2\30MHz#2_0to6mm_C1.dat',...
    'L:\黃大銘實驗資料\skinflap\G5(female rats)\20190627_skinflap_d3\no2\30MHz#2_6to12mm_C1.dat',...
    'L:\黃大銘實驗資料\skinflap\G5(female rats)\20190627_skinflap_d3\no2\30MHz#2_12to18mm_C1.dat',...
    'L:\黃大銘實驗資料\skinflap\G5(female rats)\20190627_skinflap_d3\no2\30MHz#2_18to24mm_C1.dat',...
    'L:\黃大銘實驗資料\skinflap\G5(female rats)\20190627_skinflap_d3\no2\30MHz#2_24to30mm_C1.dat',...
    'L:\黃大銘實驗資料\skinflap\G5(female rats)\20190629_skinflap_d5\no2\30MHz#2_0to6mm_C1.dat',...
    'L:\黃大銘實驗資料\skinflap\G5(female rats)\20190629_skinflap_d5\no2\30MHz#2_6to12mm_C1.dat',...
    'L:\黃大銘實驗資料\skinflap\G5(female rats)\20190629_skinflap_d5\no2\30MHz#2_12to18mm_C1.dat',...
    'L:\黃大銘實驗資料\skinflap\G5(female rats)\20190629_skinflap_d5\no2\30MHz#2_18to24mm_C1.dat',...
    'L:\黃大銘實驗資料\skinflap\G5(female rats)\20190629_skinflap_d5\no2\30MHz#2_24to30mm_C1.dat',...
    'L:\黃大銘實驗資料\skinflap\G5(female rats)\20190701_skinflap_d7\no2\30MHz#2_0to6mm_C1.dat',...
    'L:\黃大銘實驗資料\skinflap\G5(female rats)\20190701_skinflap_d7\no2\30MHz#2_6to12mm_C1.dat',...
    'L:\黃大銘實驗資料\skinflap\G5(female rats)\20190701_skinflap_d7\no2\30MHz#2_12to18mm_C1.dat',...
    'L:\黃大銘實驗資料\skinflap\G5(female rats)\20190701_skinflap_d7\no2\30MHz#2_18to24mm_C1.dat',...
    'L:\黃大銘實驗資料\skinflap\G5(female rats)\20190701_skinflap_d7\no2\30MHz#2_24to30mm_C1.dat');
for f = 1:size(fileSet)
    if ~exist(fileSet(f, :), 'file')
        f
        disp(fileSet(f, :));
        return;
    end
end
dynamicRange = 42;
resolutionAxial = 0.0591; %mm
resolutionLateral = 0.1003;
AxialROIFold = 10;
LateralROIFold = 40;
nfft = 4096;

% 計算雜訊
estimateNoiseLevel;
noise4IB = noiseSquareSum(1);

windowFold=[2;3;4;5]*3;
AxialROIFold = windowFold;
LateralROIFold = windowFold;
resolutionAxial = 0.0591; %mm
resolutionLateral = 0.0591;
estimateNoiseLevel;
noise4WMC = noiseSquareSum;

% % 計算鋼筷頻譜
param = headFile_NI(stainlessBlockData);
% % 根據是否有加上衰減器對rf做補償
stainlessBlockSgn = mean(param.rf/10^-(0.5), 2);
stainlessBlockFFT = abs(fft(stainlessBlockSgn, nfft)).^2;
gcp;

for f=39:size(fileSet)
    fullFilename = strtrim(fileSet(f, :));
    BmodeDirection = regexp(fullFilename, '_(B|C)\d+.dat', 'match');
    if (BmodeDirection{1}(2) == 'B')
        param = headFile_NI(fullFilename);
        BmodeDirection = regexp(regexp(fullFilename, '_B\d+.dat', 'match'), '\d+', 'match'); %找出該影像是否需要左右相反(B後面是奇數)
        if mod(str2num(BmodeDirection{1}{1}), 2)==1
            rf = fliplr(param.rf);
        else
            rf = param.rf;
        end
    else
        param = headFile_NI(fullFilename);
        rf = param.rf;
        batchSize = length(dir([fullFilename(1:end-5) '*dat']));
        for b = 2:batchSize
            param = headFile_NI([fullFilename(1:end-5) num2str(b) '.dat']);
            rf(:, :, size(rf, 3)+1:size(rf, 3)+size(param.rf, 3)) = param.rf;
        end
        for i=2:2:size(rf, 3)
            rf(:, :, i) = fliplr(rf(:, :, i));
        end
    end
    
    for i = 13:17
        resolutionAxial = 0.0591; %mm
        resolutionLateral = 0.1003;
        AxialROIFold = 10;
        LateralROIFold = 40;
        env_rf=abs(hilbert(rf(:, :, i)));
        log_rf=20*log10(env_rf/max(max(env_rf)));
        img=255*(log_rf+dynamicRange)/dynamicRange;
        xAxis = (1:param.Aline)*param.XInterval/1000;
        yAxis = ((1:param.DataLength)+param.Delay)/2/(param.SamplingRate*1e6)*1540*1000;
        figure('color', 'white', 'position', [500 200 1000 500])
        image(xAxis, yAxis, img)
        colormap(gray(255))
        axis equal tight
    %     axis([0 8 7.5 10.5])
        title(i)
        xlabel('Distance (mm)', 'fontSize', 28, 'fontWeight', 'bold', 'fontName', 'Times')
        ylabel('Depth (mm)', 'fontSize', 28, 'fontWeight', 'bold', 'fontName', 'Times')
        set(gca, 'FontSize', 16, 'FontName', 'Times', 'FontWeight', 'bold', 'XTick', round(xAxis(1)):2:xAxis(end), 'YTick', round(yAxis(1)):1:yAxis(end))

        % for f=1:frameNum %c-scan
        % for f=1:1 %c-scan
        %     cal_times = cal_times+1;
        % 	if mod(f,2)==0       
        %         rf1 = fliplr(reshape(rf(datalength*aline*(f-1)+1:datalength*aline*f), datalength, aline)); % fliplr：左右翻轉
        %     else
        %         rf1 = reshape(rf(datalength*aline*(f-1)+1:datalength*aline*f), datalength, aline);
        %     end
        % 	%% Bandpass Filter
        % 	fs = sampling_rate*10^(3); % sample rate (kHz)
        % 	order = 2;   % order of filter
        % 	lowcut = 25*10^(3);   % low cut frequency (kHz)
        %  	highcut = 40*10^(3);   % high cut frequency (kHz)
        %  	[b,a] = butter(order, [lowcut, highcut]/(fs/2), 'bandpass'); %-6db bandwidth
        %  	rf_filter = filter(b, a, rf1);       
            %% Select ROI
        origY = (1+param.Delay)/(param.SamplingRate*10^6)*1540*1000/2;
        if (exist('pos', 'var'))
            h = imrect(gca, pos);
        else
            h = imrect(gca,[0 origY resolutionLateral*LateralROIFold resolutionAxial*AxialROIFold]);
        end
    %     [g,api]=imRectRot('pos', [1 9 1 1 10], 'showLims', 0, 'cross', 2);
        wait(h);


        %     k = waitforbuttonpress;
        % 	api = iptgetapi(h); 
        % 	api.addNewPositionCallback(@(p) title(mat2str(p,3)));  %得到最新座標值
        % 	fcn = makeConstrainToRectFcn('imrect',get(gca,'XLim'),get(gca,'YLim'));  %不能將框挪至影像以外區域
        %  	api.setPositionConstraintFcn(fcn);  %顯示
        % 
        % 	k = waitforbuttonpress;
        pos = getPosition(h); %record position
        close(gcf);

        pos_x1=pos(1);
        pos_x2=pos(1)+pos(3);
        pos_y1=pos(2);
        pos_y2=pos(2)+pos(4);

        roi_x1 = 1+round(pos_x1/(param.XInterval*10^(-3)));
        roi_x2 = 1+round(pos_x2/(param.XInterval*10^(-3)));
        roi_y1 = 1+round(2*pos_y1*param.SamplingRate/(1540*1000*10^(-6)))-param.Delay;
        roi_y2 = 1+round(2*pos_y2*param.SamplingRate/(1540*1000*10^(-6)))-param.Delay;

        xlen = roi_x2-roi_x1+1;
        ylen = roi_y2-roi_y1+1;
        
        roi_image = reshape(img(roi_y1:roi_y2, roi_x1:roi_x2), [], 1);
        roi_image(roi_image<0) = 0;
        roi_image(roi_image>255) = 255;
        hist_feature(1, i, f) = mean(roi_image);
        hist_feature(2, i, f) = var(roi_image);
        hist_feature(3, i, f) = (sum((roi_image-hist_feature(1, i, f)).^3)./length(roi_image)) ./ (hist_feature(2, i, f).^1.5);
        hist_feature(4, i, f) = (sum((roi_image-hist_feature(1, i, f)).^4)./length(roi_image)) ./ (hist_feature(2, i, f).^2);
%         figure; hist = histogram(roi_image,'BinWidth' , 10);

        roi = rf(roi_y1:roi_y2, roi_x1:roi_x2, i);
        fft_roi = abs(fft(roi, nfft));
        fft_roi_avg = mean(fft_roi, 2);
        log_roi = 20*log10(fft_roi_avg/max(fft_roi_avg));
        [value, index] = max(log_roi(1:end/2)); % Find center freq.
        x1 = index; x2 = x1;
        while log_roi(x1) > -6 && x1>1
           x1 = x1-1;
        end
        while log_roi(x2) > -6 && x2<length(log_roi)
           x2 = x2+1;
        end
        s1 = sum(fft_roi_avg(x1+1:x2));
        s2 = sum(stainlessBlockFFT(x1+1:x2));
        IB(i, f) = 20*log10((s1/s2)/(x2-x1));  %鋼塊 bandwidth：12  取得點數要相同
        
        
        NakagamiSNR(i, f) = 10*log10(sum(sum(roi.^2))/noise4IB);
        roi = rf(roi_y1:roi_y2, roi_x1:roi_x2, i);
        env_roi = abs(hilbert(roi));
%         figure; hist = histogram(env_roi,'BinWidth' , 9.9);
        env_roi = env_roi/max(max(env_roi))*255;
        roi_reshape=reshape(env_roi, xlen*ylen,1);
        nakagamiParam = mle(roi_reshape(:,1), 'distribution', 'nakagami');
        NakagamiMLE(i, f) = nakagamiParam(1);
        
        windowFold=[2;3;4;5]*3;
        AxialROIFold = windowFold;
        LateralROIFold = windowFold;
        resolutionAxial = 0.0591; %mm
        resolutionLateral = 0.0591;
        parfor wf = 1:length(windowFold)
            windowZ = round(2*resolutionAxial/1000/1540*param.SamplingRate*1e6*AxialROIFold(wf));
            windowX =  round(resolutionLateral/(param.XInterval*1e-3)*LateralROIFold(wf));
            off_set_z = round(windowZ/2);
            off_set_x = round(windowX/2);

% % computations take place here
            tempImg = zeros(param.DataLength, param.Aline)-1;
            for jj = roi_y1:roi_y2
                for ii = roi_x1:roi_x2
                    roi = rf((jj-off_set_z):(jj+off_set_z)-1,(ii-off_set_x):(ii+off_set_x)-1);
                    roi_SNR = 10*log10(sum(sum(roi.^2))/noise4WMC(wf));
%                     noiseNum = length(find(log_rf((j-off_set_z):(j+off_set_z),(i-off_set_x):(i+off_set_x)) < 0.8*noiseLevel));
                    if(roi_SNR < 20)
%                     if(noiseNum > 0.2*numel(roi))
                        mu = 0;
                    else
                        roi_reshape=reshape(roi, 1, [])';
                        env_roi = abs(hilbert(roi_reshape));
                        env_roi = env_roi/max(max(env_roi))*255;
                        mu = (mean((env_roi).^2).^2)/mean((env_roi.^2-mean(env_roi.^2)).^2);
                    end
                    tempImg(jj, ii) = mu;
                end
            end
            nakagamiImg(:, :,wf) = tempImg;
        end
        WMCNakagami(i, f) = mean(mean(mean(nakagamiImg(roi_y1:roi_y2, roi_x1:roi_x2, :))));
    end
end
a = c