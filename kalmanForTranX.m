function [output, kalmanInput, filteredSignal] = kalmanForTranX(demodSignal, time, HPcutoff)
%     close all
    filteredSignal = [];
    kalmanInput = squeeze(demodSignal);
    
    % 各種參數初始化
    % 此部分參照Kalman Filter Motion Detection for Vibro-acoustography using Pulse Echo Ultrasound 
    detectSamplingRate = 20000;
    t = (0:time-1)/detectSamplingRate;
    signalNum = 10;
    output = zeros(2*signalNum, time);
%     figure
%     plot(kalmanInput)
    for curFrequency=1:size(HPcutoff, 2)
        [b, a] = butter(5, HPcutoff(curFrequency)/(detectSamplingRate/2), 'high');
        kalmanInput = filtfilt(b, a, kalmanInput);
%         figure
%         nfft = 16384;
%         fftS = 20*log10(abs(fft(kalmanInput, nfft)));
%         f = (0:nfft-1)*20000/nfft;
%         plot(f, fftS)
        FrequencyNum = signalNum-curFrequency+1;
        X = zeros(2*FrequencyNum, time);
    %     X(1, 1) = 1;
    %     X(3, 1) = 1;
    %     X(5, 1) = 1;
    %     X(7, 1) = 1;
        P = 2*FrequencyNum*eye(2*FrequencyNum);
        F = eye(2*FrequencyNum);
        H = zeros(time, 2*FrequencyNum);
        for i=1:FrequencyNum
            H(:, 2*i-1) = sin(2*pi*50*(i+curFrequency-1)*t)';
            H(:, 2*i) = cos(2*pi*50*(i+curFrequency-1)*t)';
        end
        Q = 0.0001*eye(2*FrequencyNum);
        R = 10;

        % kalmanfilter
        for i=2:time
            xMinus = F*X(:, i-1);
            pMinus = F*P*F'+Q;
            G = pMinus*H(i, :)'/(H(i, :)*pMinus*H(i, :)'+R);
            X(:, i) = xMinus+G*(kalmanInput(i)-H(i, :)*xMinus);
            P = (eye(2*FrequencyNum)-G*H(i, :))*pMinus;
        end
        output((2*curFrequency-1):end, :) = X;
    end
%     filteredSignal = zeros(1, time);
%     for i=1:2:(2*signalNum-1)
%         filteredSignal = filteredSignal+H(:, i)'.*X(i, :)+H(:, i+1)'.*X(i+1, :);
%     end
    
end