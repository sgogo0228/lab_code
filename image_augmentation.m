image_dir = 'D:\daming\skinflap\skinflap in wang lab\rat#1\day3\resize_image_256';
label_dir = 'D:\daming\skinflap\skinflap in wang lab\rat#1\day3\resize_label_256';
image_target_dir = 'D:\daming\skinflap\skinflap in wang lab\rat#1\day3\aug_image_256';
label_target_dir = 'D:\daming\skinflap\skinflap in wang lab\rat#1\day3\aug_label_256';
target_size = [256 256];
aug_times = 1;
tform = randomAffine2d("Rotation",[-15 15],...
            'XReflection', true,...
            'YTranslation', [-20 20]);
        
% augmenter = imageDataAugmenter( ...
%     'RandXReflection', true,...
%     'RandRotation',[-30 30],...
%     'RandYScale',[0.8 2],...
%     'RandYTranslation', [-20 20]);
% labelDir = 'D:\exosome_segmentation\data_v2\normal';

image_imds = imageDatastore(image_dir);
label_imds = imageDatastore(label_dir);

if(~exist(image_target_dir))
    mkdir(image_target_dir);
end
if(~exist(label_target_dir))
    mkdir(label_target_dir);
end

for i=1:file_num
        I = readimage(image_imds, i);
        label_I = readimage(label_imds, i);
        imwrite(I, [image_target_dir '\' num2str(i) '.png'], 'PNG');
        imwrite(label_I, [label_target_dir '\' num2str(i) '.png'], 'PNG');
    end

for t = 1:aug_times
    for i=1:file_num
        img_index = t*file_num+i;
        I = readimage(image_imds, i);
        label_I = readimage(label_imds, i);
        rout = affineOutputView(size(I),tform);
        augment_I = imwarp(I,tform,'OutputView',rout);
        augment_label_I = imwarp(label_I,tform,'OutputView',rout);
        imwrite(augment_I, [image_target_dir '\' num2str(img_index) '.png'], 'PNG');
        imwrite(augment_label_I, [label_target_dir '\' num2str(img_index) '.png'], 'PNG');
    end
end