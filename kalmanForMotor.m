function [output, kalmanInput, filteredSignal] = kalmanForMotor(demodSignal, time, frequency)
%     close all
    kalmanInput = demodSignal;
    
    % 各種參數初始化
    % 此部分參照Kalman Filter Motion Detection for Vibro-acoustography using Pulse Echo Ultrasound 
    detectSamplingRate = 20000;
    t = (0:time-1)/detectSamplingRate;
    signalNum = 10;
    X = zeros(2*signalNum, time);
%     X(1, 1) = 1;
%     X(3, 1) = 1;
%     X(5, 1) = 1;
%     X(7, 1) = 1;
    P = 20*eye(2*signalNum);
    F = eye(2*signalNum);
    H = zeros(time, 2*signalNum);
    for i=1:signalNum
        H(:, 2*i-1) = sin(2*pi*frequency*i*t)';
        H(:, 2*i) = cos(2*pi*frequency*i*t)';
    end
    Q = 0.00001*eye(2*signalNum);
    R = 10;

    % kalmanfilter
    for i=2:time
        xMinus = F*X(:, i-1);
        pMinus = F*P*F'+Q;
        G = pMinus*H(i, :)'/(H(i, :)*pMinus*H(i, :)'+R);
        X(:, i) = xMinus+G*(kalmanInput(i)-H(i, :)*xMinus);
        P = (eye(2*signalNum)-G*H(i, :))*pMinus;
    end
    filteredSignal = zeros(1, time);
    for i=1:2:(2*signalNum-1)
        filteredSignal = filteredSignal+H(:, i)'.*X(i, :)+H(:, i+1)'.*X(i+1, :);
    end
    output = X;
end