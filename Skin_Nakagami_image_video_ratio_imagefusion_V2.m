clc
clear
close all
delete(gcp('nocreate'))
% pool=parpool;

%Power_Sptrum_Size
Power_Sptrum_Size =2048;

lawidth_img=19;
LW_time = 0.6;

soundspeed=1540e3; %mm 24C

B = 0;
UCA = 1;
C = 1;
LW_time = 1;
roi_size = 8;

Gain_comp=10^2;
Ax = -6;

% filepath='F:\HC_LIN\animal\Leg\20161121_rat_N2_6month\';
% filename=['v321_uca' int2str(UCA) '_1C' int2str(C) '_30dB_7MHz_600mV_B'];

% filepath='F:\HC_LIN\animal\20170102_flow_phantom\';
% filename=['v321_uca' int2str(UCA) '_1C' int2str(C) '_30dB_7MHz_600mV_B'];

%讀入資料
waiting_time = waitbar(0,'Please wait...');
if 1
% for k = C
% filepath='F:\HC_LIN\animal\Contusion\G2\D24\L\';
% filename=['v321_uca' int2str(UCA) '_1C' int2str(k) '_40dB_7MHz_600mV_B'];
filepath='F:\skin\G2\20181107_ratskin#3_d0\';
filename=['avtech_70MHz_amp1p5_freq4_pos3_B'];
    if 1
%     for i = B
        z =1;
        zz =1;
        ii=i-1;
        filename_1=[filepath filename  int2str(B) '.dat'];
        file=fopen(filename_1,'r');
        rf=fread(file,'int8');
        fclose(file);
        %============head file=============
        if rf(3)==0
            pulser='5900PR';
        else
            pulser='AVB2-TB-C';
        end
        scan_speed=rf(4)*16129+rf(5)*127+rf(6);
        aline=rf(7)*16129+rf(8)*127+rf(9);
        datalength=rf(10)*16129+rf(11)*127+rf(12);
        sampling_rate=rf(13)*16129+rf(14)*127+rf(15);% MHz
        delay=rf(16)*16129+rf(17)*127+rf(18);
        vpp=rf(19)*16129+rf(20)*127+rf(21);% mV
        x_interval=rf(22)*16129+rf(23)*127+rf(24);% μm
        y_interval=rf(25)*16129+rf(26)*127+rf(27);
        move_times=rf(28)*16129+rf(29)*127+rf(30);% μm
        Doppler=rf(31)*16129+rf(32)*127+rf(33);
        %=================================================
        rf1 = (reshape(rf(34:end),datalength,aline)/255*vpp); %fliplr
        max_vpp = vpp/2+0.5;

        filename_2=[filepath filename  int2str(B) '.dat'];
        file=fopen(filename_2,'r');
        rf=fread(file,'int8');
        fclose(file);
        %============head file=============
        if rf(3)==0
            pulser='5900PR';
        else
            pulser='AVB2-TB-C';
        end
        scan_speed=rf(4)*16129+rf(5)*127+rf(6);
        aline=rf(7)*16129+rf(8)*127+rf(9);
        datalength=rf(10)*16129+rf(11)*127+rf(12);
        sampling_rate=rf(13)*16129+rf(14)*127+rf(15);% MHz
        delay=rf(16)*16129+rf(17)*127+rf(18);
        vpp=rf(19)*16129+rf(20)*127+rf(21);% mV
        x_interval=rf(22)*16129+rf(23)*127+rf(24);% μm
        y_interval=rf(25)*16129+rf(26)*127+rf(27);
        move_times=rf(28)*16129+rf(29)*127+rf(30);% μm
        Doppler=rf(31)*16129+rf(32)*127+rf(33);
        %=================================================
        rf2 = (reshape(rf(34:end),datalength,aline)/255*vpp); %fliplr

        rf_high = rf1(:,1:aline-1);
        rf_low = fliplr(rf2(:,1:aline-1)); %fliplr Low

        %%
        lawidth_img_y=ceil((lawidth_img./sampling_rate*soundspeed*10^-3/x_interval));
        % lawidth_y = LW_time*floor(lawidth./sampling_rate*soundspeed*10^-3/x_interval);

        %%
        position= [63, 1160, 90, 585]; %20170327 L_small
        %%
        P1 = (position(2)+delay)/sampling_rate*soundspeed/2*10^-6;
        P2 = (position(1))*x_interval/1000;
        P3 = position(4)/sampling_rate*soundspeed/2*10^-6;
        P4 = position(3)*x_interval/1000;
        %%
            rf_Nakami_high_window=zeros(datalength,aline,roi_size-2);
            rf_Nakami_low_window=zeros(datalength,aline,roi_size-2);

        for w = 1:roi_size-2
            %% 改變roi_size
            roi = w+2;            
            lawidth_img_window = lawidth_img*roi;
            lawidth_img_y_window = lawidth_img_y*roi;
            % computations take place here
            waitbar(w/roi_size-2)     
            Temp_rf_Nakami_high_window=zeros(datalength,aline);
            Temp_rf_Nakami_low_window=zeros(datalength,aline);
            for jj = 1:datalength-lawidth_img_window
                for iii = 1:aline-lawidth_img_y_window
                    tic
                    rf_high1=rf_high(jj:floor(lawidth_img_window+jj),iii:floor(lawidth_img_y_window+iii-1));
                    rf_low1=rf_low(jj:floor(lawidth_img_window+jj),iii:floor(lawidth_img_y_window+iii-1));
%                     toc
                    rf_rsp_h=reshape(rf_high1,1,[])';
                    rf_rsp_l=reshape(rf_low1,1,[])';
%                     tic
                    rf_env_h=abs(hilbert(rf_rsp_h));
                    rf_env_l=abs(hilbert(rf_rsp_l));
%                     toc
                    rf_env_h(abs(rf_env_h)<=22)=0;
                    rf_env_l(abs(rf_env_l)<=22)=0;     
                    rf_env_h_norm=rf_env_h/max(max(rf_env_h))*255;
                    rf_env_l_norm=rf_env_l/max(max(rf_env_l))*255;
%                     tic
                    mu_high = (nanmean((rf_env_h_norm).^2).^2)/nanmean((rf_env_h_norm.^2-nanmean(rf_env_h_norm.^2)).^2);
                    mu_low = (nanmean((rf_env_l_norm).^2).^2)/nanmean((rf_env_l_norm.^2-nanmean(rf_env_l_norm.^2)).^2);
                    Temp_rf_Nakami_high_window(floor((1+jj+lawidth_img_window+jj)/2),floor((1+iii+lawidth_img_y_window+iii)/2))=mu_high;
                    Temp_rf_Nakami_low_window(floor((1+jj+lawidth_img_window+jj)/2),floor((1+iii+lawidth_img_y_window+iii)/2))=mu_low;  
                    toc
                end
            end
        rf_Nakami_high_window(:,:,w)=Temp_rf_Nakami_high_window;
        rf_Nakami_low_window(:,:,w)=Temp_rf_Nakami_low_window;
        end
        

        %%
        position= [53, 1160, 90, 585]; %20170327 L_small
        P1 = (position(2)+delay)/sampling_rate*soundspeed/2*10^-6;
        P2 = (position(1))*x_interval/1000;
        P3 = position(4)/sampling_rate*soundspeed/2*10^-6;
        P4 = position(3)*x_interval/1000;
        %%
        % rf_Nakami_high3(:,:,roi_size-2) =(rf_Nakami_high_window(:,:,1:roi_size-2));
        % rf_Nakami_low3(:,:,roi_size-2) =(rf_Nakami_low_window(:,:,1:roi_size-2));

        rf_Nakami_high=nanmean(rf_Nakami_high_window,3);
        rf_Nakami_low=nanmean(rf_Nakami_low_window,3);

        rf_Nakami_high(isnan(rf_Nakami_high))=0;
        rf_Nakami_high(isinf(rf_Nakami_high))=0;

        rf_Nakami_low(isnan(rf_Nakami_low))=0;
        rf_Nakami_low(isinf(rf_Nakami_low))=0;

        Env_l=abs(hilbert(rf_low));
        Env_h=abs(hilbert(rf_high));

        Env_low=Env_l;
        Env_high=Env_h;

        Log_Env_h=20*log10(Env_h/min(min(Env_h)));
        Temp_h=Log_Env_h-max(max(Log_Env_h)) + 42;
        lmage_high=255*Temp_h/42;

        Log_Env_l=20*log10(Env_l/min(min(Env_h)));
        Temp_l=Log_Env_l-max(max(Log_Env_l)) + 42;
        % Log_Env_l=20*log10(Env_l/min(min(Env_l)));
        % Temp_l=Log_Env_l-max(max(Log_Env_l)) + 42;
        lmage_low=255*Temp_l/42;

        XX = (2:aline)*x_interval/1000; %mm
        YY = (1+delay:datalength+delay)./sampling_rate*soundspeed/2*10^-6; %mm

        lawidth_img_window = lawidth_img*(roi_size-2); 
        lawidth_img_y_window = lawidth_img_y*(roi_size-2);
        XX1 = XX(1:floor((1+2*(aline-lawidth_img_y_window)+lawidth_img_y*3)/2));
        YY1 = YY(1:floor((1+2*(datalength-lawidth_img*3)+lawidth_img*3)/2));

        %% B-mode image_low
        close all
        figure,image(XX, YY, lmage_low);
        % image(XX1(lawidth_img_y:yy), YY1(lawidth_img:xx), lmage_low(lawidth_img:xx,lawidth_img_y:yy));
        % image(XX1,YY1,  lmage_low(1:floor((1+2*(datalength-lawidth_img*3)+lawidth_img*3)/2)...
        %     ,1:floor((1+2*(aline-lawidth_img_y*3)+lawidth_img_y*3)/2)));
        caxis([0,255])
        colormap(gray(255))
        colorbar('EastOutside','YTick',[0 51 102 153 204 255],'YTickLabel',...
        {'0','50','100','150','200','255'});
        set(gca,'FontSize',20.,'FontName','Times New Roman')
        ylabel('Axial distance (mm)','FontSize',20,'FontName','Times New Roman')
        xlabel('Lateral distance (mm)','FontSize',20,'FontName','Times New Roman')
        % h1 = imrect(gca, [P2 P1 P4 P3]); %324
        % setColor(h1,'yellow');													% change impoly lines colour
        % lines = findall(h1,'type','line');									% find line objects
        % set(lines,'linewidth',4);
        % % rectangle('Position',box,'EdgeColor','black','LineWidth',2);
        % addNewPositionCallback(h1,@(p) title(mat2str(p,3)));
        % fcn = makeConstrainToRectFcn('imrect',get(gca,'XLim'),get(gca,'YLim'));
        % setPositionConstraintFcn(h1,fcn);
        axis equal tight
        % axis([2 17 40 57])

        %% save img
        saveas(gcf,['uca1_B_1C' int2str(C) '_B' int2str(2*ii+1+(k-1)*200) '.jpg']);
        %% B-mode image_high
        close all
        % image(XX1(lawidth_img_y:yy), YY1(lawidth_img:xx), lmage_high(lawidth_img:xx,lawidth_img_y:yy));
        % image(XX, YY, lmage_high);
        image(XX1,YY1,lmage_high(1:floor((1+2*(datalength-lawidth_img*3)+lawidth_img*3)/2)...
            ,1:floor((1+2*(aline-lawidth_img_y*3)+lawidth_img_y*3)/2)));
        caxis([0,255])
        colormap(gray(255))
        colorbar('EastOutside','YTick',[0 51 102 153 204 255],'YTickLabel',...
        {'0','50','100','150','200','255'});
        set(gca,'FontSize',20.,'FontName','Times New Roman')
        ylabel('Axial distance (mm)','FontSize',20,'FontName','Times New Roman')
        xlabel('Lateral distance (mm)','FontSize',20,'FontName','Times New Roman')
        % h1 = imrect(gca, [P2 P1 P4 P3]); %324
        % setColor(h1,'yellow');													% change impoly lines colour
        % lines = findall(h1,'type','line');									% find line objects
        % set(lines,'linewidth',4);
        % % rectangle('Position',box,'EdgeColor','black','LineWidth',2);
        % addNewPositionCallback(h1,@(p) title(mat2str(p,3)));
        % fcn = makeConstrainToRectFcn('imrect',get(gca,'XLim'),get(gca,'YLim'));
        % setPositionConstraintFcn(h1,fcn);
        axis equal tight
        % axis([2 17 40 57])

        %% save img
        saveas(gcf,['uca1_B_1C' int2str(C) '_B' int2str(2*ii+(k-1)*200) '.jpg']);
        %% N_image_low
        close all
        N_max_high = max(max(rf_Nakami_high));
        N_min_high = mean(mean(rf_Nakami_high));
        N_max_low = max(max(rf_Nakami_low));
        N_min_low = mean(mean(rf_Nakami_low));

        color_gain_low = rf_Nakami_low*250;
        color_gain_high = rf_Nakami_high*250;

        image(XX, YY, color_gain_low);
        % image(XX1, YY1, color_gain_low);
        % image(XX1, YY1, rf_Nakami_low*color_gain_low);
        ylabel('Axial distance (mm)','FontSize',20,'FontName','Times New Roman')
        xlabel('Lateral distance (mm)','FontSize',20,'FontName','Times New Roman')
        colormap(jet(100));
        colorbar('EastOutside','YTick',[0 20 40 60 80 100],'YTickLabel',...
        {'0','0.1','0.2','0.3','0.4','0.5'});
        set(gca,'FontSize',20.,'FontName','Times New Roman')
        axis equal tight
        % axis([2 17 40 57])
        %% save img
        saveas(gcf,['uca1_N_1C' int2str(C) '_B' int2str(2*ii+1+(k-1)*200) '.jpg']);
        %% N_image_high
        close all
        image(XX1, YY1, color_gain_high);
        ylabel('Axial distance (mm)','FontSize',20,'FontName','Times New Roman')
        xlabel('Lateral distance (mm)','FontSize',20,'FontName','Times New Roman')
        colormap(jet(100));
        colorbar('EastOutside','YTick',[0 20 40 60 80 100],'YTickLabel',...
        {'0','0.1','0.2','0.3','0.4','0.5'});
        set(gca,'FontSize',20.,'FontName','Times New Roman')
        axis equal tight
        % axis([2 17 40 57])
        %% save img
        saveas(gcf,['uca1_N_1C' int2str(C) '_B' int2str(2*ii+(k-1)*200) '.jpg']);
        %%

        %% Nakagami ratio
        close all
        A = (rf_Nakami_high./rf_Nakami_low);
        A1 = (A)-min(min((A)))-1;
        A1(isnan(A))=0;
        A1(isinf(A))=0;
        % max(max(A-min(min((A)))))
        color_gain_A1 = 400*(abs(A1)); % leg1121:25

        % max(max(color_gain_A1))
        % mean(mean(color_gain_A1))
        % max(max(color_gain_A1))
        % min(min(color_gain_A1))

        image(XX1, YY1, color_gain_A1);
        % figure,image(color_gain_A1);
        ylabel('Axial distance (mm)','FontSize',20,'FontName','Times New Roman')
        xlabel('Lateral distance (mm)','FontSize',20,'FontName','Times New Roman')
        colormap(hot(255)); % leg1121: 65
        colorbar('EastOutside','YTick',[0 51 102 153 204 255],'YTickLabel',...
        {'-0.01','0','0.01','0.02','0.03','0.04'});
        set(gca,'FontSize',20.,'FontName','Times New Roman')
        axis equal tight
        axis([2 17 40 57])

        %%
        saveas(gcf,['uca1_N_ratio_1C' int2str(C) '_B' int2str(2*ii+1+(k-1)*200) '.jpg']);
        clear A A1

        %% N-mode fusion image
        close all
        color_gain_A2 = (color_gain_A1+260); %B_ratio_hot
        colormap([gray(255);hot(255)])
        h(1) = image(XX1,YY1,lmage_high(1:floor((1+2*(datalength-lawidth_img*3)+lawidth_img*3)/2)...
            ,1:floor((1+2*(aline-lawidth_img_y*3)+lawidth_img_y*3)/2)));
        hold on
        h(2) = image(XX1,YY1,color_gain_A2);
        alpha(h(2),0.3);
        hold off
        ylabel('Axial distance (mm)','FontSize',20,'FontName','Times New Roman')
        xlabel('Lateral distance (mm)','FontSize',20,'FontName','Times New Roman')
        colorbar('EastOutside','YTick',[1 40 80 120 160 200 240 295 335 375 415 455 495 535 575],'YTickLabel',...
        {'0','40','80','120','160','200','240','-0.01','0','0.01','0.02','0.03','0.04'});
        set(gca,'FontSize',20.,'FontName','Times New Roman')
        axis equal tight
        axis([2 17 40 57])
        %%
        saveas(gcf,['uca1_N_fusion_ratio_1C' int2str(C) '_B' int2str(2*ii+1+(k-1)*200) '.jpg']);
        clear h color_gain_A2

    end
end
close(waiting_time) 

% 63*soundspeed/2*10^-6

% RF(:,1) = (1+delay:datalength+delay)./sampling_rate;
% RF(:,2)=rf_low(:,65);
% RF(:,3)=rf_high(:,65);
% 
% figure,plot(RF(:,1),RF(:,2))
% hold on
% plot(RF(:,1),RF(:,3),'r')

% time_P1 = (position(2)+delay)./sampling_rate
% time_P2 = (position(2)+lawidth+delay)./sampling_rate


